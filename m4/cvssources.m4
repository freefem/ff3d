AC_DEFUN([AC_CHECK_CVSSOURCES],[
  AH_TEMPLATE(HAVE_GUI_LIBS,
   [Defined to 1 if GUI libraries are present on the system])

dnl  if test x"$HAS_CVS_RELEASE_TOOLS" != x"true"
dnl  then
dnl	AC_MSG_WARN([Cannot check if these are cvs sources. Install:$CVS_TOOLS_MISSING]);
dnl  else
        for i in $srcdir/CVS/Entries $srcdir/*/CVS/Entries $srcdir/*/*/CVS/Entries $srcdir/*/*/*/CVS/Entries $srcdir/*/*/*/*/CVS/Entries  $srcdir/*/*/*/*/*/CVS/Entries
        do
           if test -f "$i"
           then
             j=`echo $i | $SED 's,'${srcdir}\/',,'`
             CVS_STAMP_DEPENDENCIES="$CVS_STAMP_DEPENDENCIES $j"
           fi
        done
dnl  fi

 AC_SUBST(CVS_STAMP_DEPENDENCIES)
 AM_CONDITIONAL(GENERATE_CVS_STAMP,test x"$CVS_STAMP_DEPENDENCIES" != x)
])
