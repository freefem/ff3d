AC_DEFUN([AC_CHECK_PETSC],
[
  AH_TEMPLATE(HAVE_PETSC,
   [Defined to 1 if PETSc is present on the system])

  AC_ARG_ENABLE(
    petsc,
  AC_HELP_STRING([--enable-petsc],[Builts ff3d with PETSc support]),
  [ ac_petsc=$enableval ],
  [ ac_petsc="auto"])

  REQUIRED_PETSC_MAJOR=2
  REQUIRED_PETSC_MINOR=0

  REQUIRED_PETSC=`echo "$REQUIRED_PETSC_MAJOR.$REQUIRED_PETSC_MINOR"`

  petscinclude=""
  petsclibpath=""
dnl look for petsc in standard pathes
  for i in /usr /usr/local
  do
    if test -e "$i/include/petsc/petscversion.h"
    then
      PETSC_VERSION_MAJOR=`grep PETSC_VERSION_MAJOR $i/include/petsc/petscversion.h | grep \#define`
      PETSC_VERSION_MAJOR=`expr "$PETSC_VERSION_MAJOR" : '.*MAJOR.* \(.*\)'`
      PETSC_VERSION_MINOR=`grep PETSC_VERSION_MINOR $i/include/petsc/petscversion.h | grep \#define`
      PETSC_VERSION_MINOR=`expr "$PETSC_VERSION_MINOR" : '.*MINOR.* \(.*\)'`
      petscinclude=$i/include/petsc

      PETSC_USES_MPI=`grep '^\#include \"mpi.h\"' $i/include/petsc/petsc.h`

      if test "x`ls $i/lib/libpetsc.* 2> /dev/null`" != "x"
      then
        petsclibpath="$i/lib"
      fi

      if test $PETSC_VERSION_MAJOR -lt $REQUIRED_PETSC_MAJOR
      then
        FOUND_PETSC=false
      else
        if test $PETSC_VERSION_MINOR -lt $REQUIRED_PETSC_MINOR
        then
          FOUND_PETSC=false
        else 
          FOUND_PETSC=true
        fi
      fi
    fi
  done

  if test "x$petsclibpath" != "x"
  then
    if test "x$FOUND_PETSC" != "xtrue"
    then
       AC_MSG_RESULT(no (header files not found))
    else
       if test "x$PETSC_USES_MPI" != "x"
       then
	 AC_CHECK_MPI
       fi
       PETSC_CXXFLAGS="-I$petscinclude"
       PETSC_LDADD="-L$petsclibpath -lpetscksp -lpetscdm -lpetscmat -lpetscvec -lpetsc"
       use_petsc="true"
    fi
  else
    AC_MSG_RESULT(no)
  fi

  AC_MSG_CHECKING([for PETSC library (>= $REQUIRED_PETSC)])

  if test "x$use_petsc" == xtrue
  then
     case  $ac_petsc in
     ("yes")
         AC_MSG_RESULT(yes)
         AC_DEFINE(HAVE_PETSC)
       ;;	
     ("no"|"auto")
         use_petsc="false"
	 AC_MSG_RESULT(NO *** possible but desactivated ***)
       ;;
      *)
	 AC_MSG_ERROR(acinclude error! Should not reach that case.)
       ;;
     esac
  else
     case $ac_petsc in
      "yes")
	 AC_MSG_RESULT(no)
	 AC_MSG_ERROR(PETSc was not found on your system)
	  ;;
	("auto"|"no")
	    AC_MSG_RESULT(no)
	  ;;
	  *)
	    AC_MSG_ERROR(acinclude error! Should not reach that case.)
          ;;
	esac
  fi

  AM_CONDITIONAL(USES_PETSC,test "x$use_petsc" == xtrue)

  AC_SUBST(PETSC_CXXFLAGS)
  AC_SUBST(PETSC_LDADD)
])
