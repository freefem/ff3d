//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef CONFORM_TRANSFORMATION_HPP
#define CONFORM_TRANSFORMATION_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <Vertex.hpp>
#include <Cell.hpp>

#include <Hexahedron.hpp>
#include <CartesianHexahedron.hpp>

#include <Tetrahedron.hpp>

#include <Triangle.hpp>
#include <Quadrangle.hpp>

#include <QuadratureFormula.hpp>

#include <ErrorHandler.hpp>

class Domain;
class ScalarFunctionBase;

class ConformTransformationP1Triangle
{
public:
  friend class ConformTransformationP1TriangleJacobian;

private:
  const Triangle& __T;

  const TinyVector<3, real_t> __a;
  const TinyVector<3, real_t> __b_a; // (b-a) vector
  const TinyVector<3, real_t> __c_a; // (c-a) vector

public:
  //! computes the value at X (X is inside the reference element).
  inline void value(const TinyVector<3, real_t>& X,
		    TinyVector<3, real_t>& result) const
  {
    value(X[0],X[1],X[2],result);
  }

  //! computes the value at (x,y,z) ((x,y,z) is inside the reference element).
  inline void value(const real_t& x, const real_t& y, const real_t& z,
		    TinyVector<3, real_t>& result) const
  {
    result  = __a;
    result += __b_a*x;
    result += __c_a*y;
  }



  inline void dx(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dx(X[0], X[1], X[2], __result);
  }

  inline void dx(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = __b_a;
  }

  inline void dy(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dy(X[0], X[1], X[2], __result);
  }

  inline void dy(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = __c_a;
  }

  inline void dz(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dz(X[0], X[1], X[2], __result);
  }

  inline void dz(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = 0;
  }






  //! Computes Xhat, the point which transformed is X
  inline bool invertT(const TinyVector<3, real_t>& X,
		      TinyVector<3, real_t>& Xhat) const
  {
    return invertT(X[0],X[1],X[2], Xhat);
  }

  //! 
  /** 
   * Computes @a Xhat, the point which transformed is (@a x ,@a y,@a z)
   * 
   * @param x @f$ x @f$
   * @param y @f$ y @f$
   * @param z @f$ z @f$
   * @param Xhat @f$ \hat{\mathbf{x}} @f$
   * 
   * @return true
   * 
   * @warning This only works for planar triangles
   */
  inline bool invertT(const real_t& x, const real_t& y, const real_t& z,
		      TinyVector<3, real_t>& Xhat) const
  {
    TinyMatrix<2,2,real_t> A;
    for (size_t i=0; i<2; ++i) {
      A(i,0) = __b_a[i];
      A(i,1) = __c_a[i];
    }

    TinyVector<2,real_t> b;
    b[0] = x-__a[0];
    b[1] = y-__a[1];

    TinyVector<2,real_t> u = b/A;
    Xhat[0] = u[0];
    Xhat[1] = u[1];
    Xhat[2] = 0;

    TinyVector<3,real_t> result  = __a;
    result += __b_a*u[0];
    result += __c_a*u[1];

    return true;
  }

  /*!  Computes the integrale of the function f on an element using
    the reference element
  */
  real_t integrate(const ScalarFunctionBase& f) const;

  ConformTransformationP1Triangle(const Triangle& T)
    : __T(T),
      __a(T(0)),
      __b_a(T(1)-T(0)),
      __c_a(T(2)-T(0))
  {
    ;
  }
};


class ConformTransformationP1TriangleJacobian
{
public:
  typedef ConformTransformationP1Triangle AssociatedTransformation;

private:
  const ConformTransformationP1Triangle& __T;

  TinyMatrix<3,3> __invJacobian;

  real_t __det;

  void dx(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[0]*x;
  }

  void dy(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[1]*y;
  }

  void dz(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[2]*z;
  }
  
  
public:
  
  /*! return the invert of the transposed of the jacobian of the
    transformation.
  */
  const TinyMatrix<3,3,real_t>& invJacobian() const
  {
    return __invJacobian;
  }

  //! Returns the value of the invert of the jacobian.
  const real_t& invJacobian(const size_t i, const size_t j) const
  {
    return __invJacobian(i,j);
  }

  //! returns the jacobian of the transformation
  const real_t& jacobianDet() const
  {
    return __det;
  }

  ConformTransformationP1TriangleJacobian(const ConformTransformationP1Triangle& T)
    : __T(T),
      __invJacobian(0),
      __det(0)
  {
//     throw ErrorHandler(__FILE__,__LINE__,
// 		       "not implemented",
// 		       ErrorHandler::unexpected);

    //     for (size_t i=0; i<3; ++i)
    //       __invJacobian(i,i) = 1./(__T.__h[i]);

    __det = 2*__T.__T.volume();
  }

};

class ConformTransformationQ1Quadrangle
{
public:
  friend class ConformTransformationQ1QuadrangleJacobian;

private:
  const Quadrangle& __Q;

  const TinyVector<3, real_t> __a;
  const TinyVector<3, real_t> __b_a;	// (b-a) vector
  const TinyVector<3, real_t> __d_a;	// (d-a) vector
  const TinyVector<3, real_t> __ca_b_d; // (c+a-b-d) vector

public:
  //! computes the value at X (X is inside the reference element).
  inline void value(const TinyVector<3, real_t>& X,
		    TinyVector<3, real_t>& result) const
  {
    value(X[0],X[1],X[2],result);
  }

  //! computes the value at (x,y,z) ((x,y,z) is inside the reference element).
  inline void value(const real_t& x, const real_t& y, const real_t& z,
		    TinyVector<3, real_t>& result) const
  {
    result  = __a;
    result += __b_a*x;
    result += __d_a*y;
    result += __ca_b_d*x*y;
  }

  //! Computes Xhat, the point which transformed is X
  inline bool invertT(const TinyVector<3, real_t>& X,
		      TinyVector<3, real_t>& Xhat) const
  {
    return invertT(X[0],X[1],X[2], Xhat);
  }

  //! Computes Xhat, the point which transformed is (x,y,z)
  inline bool invertT(const real_t& x, const real_t& y, const real_t& z,
		      TinyVector<3, real_t>& Xhat) const
  {
    // Use least square to solve the linear system
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);

    return false;
  }

  /*!  Computes the integrale of the function f on an element using
    the reference element
  */
  real_t integrate(const ScalarFunctionBase& f) const;

  ConformTransformationQ1Quadrangle(const Quadrangle& Q)
    : __Q(Q),
      __a(Q(0)),
      __b_a(Q(1)-Q(0)),
      __d_a(Q(3)-Q(0)),
      __ca_b_d(Q(2)-__b_a-Q(3))
  {
    ;
  }
};

class ConformTransformationQ1QuadrangleJacobian
{
public:
  typedef ConformTransformationQ1Quadrangle AssociatedTransformation;

private:
  const ConformTransformationQ1Quadrangle& __T;

  TinyMatrix<3,3> __invJacobian;

  real_t __det;

  void dx(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[0]*x;
  }

  void dy(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[1]*y;
  }

  void dz(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    //     __result = __T.__h[2]*z;
  }


public:

  /*! return the invert of the transposed of the jacobian of the
    transformation.
  */
  const TinyMatrix<3,3,real_t>& invJacobian() const
  {
    return __invJacobian;
  }

  //! Returns the value of the invert of the jacobian.
  const real_t& invJacobian(const size_t i, const size_t j) const
  {
    return __invJacobian(i,j);
  }

  //! returns the jacobian of the transformation
  const real_t& jacobianDet() const
  {
    return __det;
  }

  ConformTransformationQ1QuadrangleJacobian(const ConformTransformationQ1Quadrangle& T)
    : __T(T),
      __invJacobian(0),
      __det(0)
  {
    __det = __T.__Q.volume();
  }
};

class ConformTransformationQ1Hexahedron
{
public:
  friend class ConformTransformationQ1HexahedronJacobian;
  typedef ConformTransformationQ1Quadrangle
  BoundaryConformTransformation;

private:
  const Hexahedron& __H;

  //  TinyVector<8, TinyVector<3, real_t> > __vertices;

  const TinyVector<3, real_t> __a;
  const TinyVector<3, real_t> __b;
  const TinyVector<3, real_t> __c;
  const TinyVector<3, real_t> __d;
  const TinyVector<3, real_t> __e;
  const TinyVector<3, real_t> __f;
  const TinyVector<3, real_t> __g;
  const TinyVector<3, real_t> __h;

  bool __inside(const real_t& x, const real_t& y, const real_t& z,
		TinyVector<6, bool>& faces) const;

public:

  inline void dx(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dx(X[0], X[1], X[2], __result);
  }

  inline void dx(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = __b;
    __result += y*__e;
    __result += z*__f;
    __result += (y*z)*__h;
  }

  inline void dy(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dy(X[0], X[1], X[2], __result);
  }

  inline void dy(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = __c;
    __result += x*__e;
    __result += z*__g;
    __result += (x*z)*__h;
  }

  inline void dz(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dz(X[0], X[1], X[2], __result);
  }

  inline void dz(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result = __d;
    __result += x*__f;
    __result += y*__g;
    __result += (x*y)*__h;
  }

  inline void value(const TinyVector<3, real_t>& X,
		    TinyVector<3, real_t>& __result) const
  {
    value(X[0],X[1],X[2],__result);
  }

  inline void value(const real_t& x, const real_t& y, const real_t& z,
		    TinyVector<3, real_t>& __result) const
  {
    __result  = __a;
    __result += x*__b;
    __result += y*__c;
    __result += z*__d;
    __result += (x*y)*__e;
    __result += (x*z)*__f;
    __result += (y*z)*__g;
    __result += (x*y*z)*__h;
  }

  /*! Computes Xhat, the point which transformed is (x,y,z)
    returns false if no Xhat is found...
  */
  bool invertT(const real_t& x, const real_t& y, const real_t& z,
	       TinyVector<3, real_t>& Xhat) const;

  //! Computes Xhat, the point which transformed is X
  inline bool invertT(const TinyVector<3, real_t>& X,
		      TinyVector<3, real_t>& Xhat) const
  {
    return invertT(X[0],X[1],X[2], Xhat);
  }

  /*!
   * Computes the integrale of the function f on an element using the
   * reference element
   */
  real_t integrate(const ScalarFunctionBase& f) const;

  ConformTransformationQ1Hexahedron(const Hexahedron& H)
    : __H(H),
      __a(H(0)),
      __b(H(1) - H(0)),
      __c(H(3) - H(0)),
      __d(H(4) - H(0)),
      __e(H(2) + H(0) - H(1) - H(3)),
      __f(H(5) + H(0) - H(1) - H(4)),
      __g(H(7) + H(0) - H(3) - H(4)),
      __h(H(1) - H(0) - H(2) + H(3) + H(4) - H(5) + H(6) - H(7))
  {
    ;
  }
};

class ConformTransformationQ1HexahedronJacobian
{
private:
  const ConformTransformationQ1Hexahedron& __T;

  TinyMatrix<3,3> __jacobian;
  TinyMatrix<3,3> __invJacobian;

  real_t __det;

public:

  //! return the jacobian of the transformation.
  const TinyMatrix<3,3,real_t>& jacobian() const
  {
    return __jacobian;
  }

  /*! return the invert of the transposed of the jacobian of the
    transformation.
  */
  const TinyMatrix<3,3,real_t>& invJacobian() const
  {
    return __invJacobian;
  }

  //! returns the jacobian of the transformation
  const real_t& jacobianDet() const
  {
    return __det;
  }

  //! Returns the value of the invert of the jacobian.
  const real_t& invJacobian(const size_t i, const size_t j) const
  {
    return __invJacobian(i,j);
  }

  ConformTransformationQ1HexahedronJacobian(const ConformTransformationQ1Hexahedron& T)
    : __T(T)
  {
    const TinyVector<QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints,
                     TinyVector<3, real_t> >& IntegrationVertices
      = QuadratureFormulaQ1Hexahedron::instance().vertices();

    __jacobian = 0;

    TinyVector<QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints, TinyVector<3, real_t> > dxPhi;
    TinyVector<QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints, TinyVector<3, real_t> > dyPhi;
    TinyVector<QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints, TinyVector<3, real_t> > dzPhi;

    for (size_t i=0; i<QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints; ++i) {
      __T.dx(IntegrationVertices[i][0],
	     IntegrationVertices[i][1],
	     IntegrationVertices[i][2],dxPhi[i]);
      __T.dy(IntegrationVertices[i][0],
	     IntegrationVertices[i][1],
	     IntegrationVertices[i][2],dyPhi[i]);
      __T.dz(IntegrationVertices[i][0],
	     IntegrationVertices[i][1],
	     IntegrationVertices[i][2],dzPhi[i]);
      for (size_t j=0; j<3; ++j) {
        __jacobian(0,j) += dxPhi[i][j];
	__jacobian(1,j) += dyPhi[i][j];
	__jacobian(2,j) += dzPhi[i][j];
      }
    }

    __jacobian *= 1./QuadratureFormulaQ1Hexahedron::numberOfQuadraturePoints;

    TinyVector<3, TinyVector<3, real_t> > I;
    for (size_t i=0; i<3; ++i) {
      for (size_t j=0; j<3; ++j)
	I[i][j] = (i==j)?1:0;
    }

    TinyVector<3,  TinyVector<3, real_t> > J;
    __det = gaussPivot(__jacobian,I,J);

    for (size_t i=0; i<3; ++i) {
      for (size_t j=0; j<3; ++j) {
	//! We compute the transposed of the inverse.
	__invJacobian(j,i) = J[i][j];
      }
    }
  }
};


class ConformTransformationQ1CartesianHexahedron
{
public:
  friend class ConformTransformationQ1CartesianHexahedronJacobian;

  typedef ConformTransformationQ1Quadrangle BoundaryConformTransformation;

private:
  const CartesianHexahedron& __CH;

  const TinyVector<3, real_t> __a;
  const TinyVector<3, real_t> __h;

public:
  inline void dx(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dx(X[0], X[1], X[2], __result);
  }

  inline void dx(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result[0] = __h[0];
    __result[1] = 0;
    __result[2] = 0;
  }

  inline void dy(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dy(X[0], X[1], X[2], __result);
  }

  inline void dy(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result[0] = 0;
    __result[1] = __h[1];
    __result[2] = 0;
  }

  inline void dz(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dz(X[0], X[1], X[2], __result);
  }

  inline void dz(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    __result[0] = 0;
    __result[1] = 0;
    __result[2] = __h[2];
  }

  //! computes the value at X (X is inside the reference element).
  inline void value(const TinyVector<3, real_t>& X,
		    TinyVector<3, real_t>& result) const
  {
    value(X[0],X[1],X[2],result);
  }

  //! computes the value at (x,y,z) ((x,y,z) is inside the reference element).
  inline void value(const real_t& x, const real_t& y, const real_t& z,
		    TinyVector<3, real_t>& result) const
  {
    result  = __a;
    result[0] += __h[0]*x;
    result[1] += __h[1]*y;
    result[2] += __h[2]*z;
  }

  //! Computes Xhat, the point which transformed is X
  inline bool invertT(const TinyVector<3, real_t>& X,
		      TinyVector<3, real_t>& Xhat) const
  {
    return invertT(X[0],X[1],X[2], Xhat);
  }

  //! Computes Xhat, the point which transformed is (x,y,z)
  inline bool invertT(const real_t& x, const real_t& y, const real_t& z,
		      TinyVector<3, real_t>& Xhat) const
  {
    Xhat[0] = (x-__a[0])/__h[0];
    Xhat[1] = (y-__a[1])/__h[1];
    Xhat[2] = (z-__a[2])/__h[2];
    return true;
  }

  /*!  Computes the integrale of the function f on an element using
    the reference element
  */
  real_t integrate(const ScalarFunctionBase& f) const;

  /*!
   * Computes the integrale of the characteristic function of a domain
   * on an element using the reference element
   */
  real_t integrateCharacteristic(const Domain& d) const;

  ConformTransformationQ1CartesianHexahedron(const CartesianHexahedron& H)
    : __CH(H),
      __a(H(0)),
      __h(H(6)-H(0))
  {
    ;
  }
};


class ConformTransformationQ1CartesianHexahedronJacobian
{
public:
  typedef ConformTransformationQ1CartesianHexahedron AssociatedTransformation;

private:
  ConformTransformationQ1CartesianHexahedron& __T;

  TinyMatrix<3,3> __jacobian;

  TinyMatrix<3,3> __invJacobian;

  real_t __det;

  void dx(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    __result = __T.__h[0]*x;
  }

  void dy(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    __result = __T.__h[1]*y;
  }

  void dz(const real_t& x, const real_t& y, const real_t& z,
	  TinyVector<3, real_t>& __result) const
  {
    __result = __T.__h[2]*z;
  }
public:

  inline real_t
  jacobian(const size_t i, const size_t j) const
  {
    return __jacobian(i,j);
  }

  /*! return the invert of the transposed of the jacobian of the
    transformation.
  */
  const TinyMatrix<3,3,real_t>& invJacobian() const
  {
    return __invJacobian;
  }

  //! Returns the value of the invert of the jacobian.
  const real_t& invJacobian(const size_t i, const size_t j) const
  {
    return __invJacobian(i,j);
  }

  //! returns the jacobian of the transformation
  const real_t& jacobianDet() const
  {
    return __det;
  }

  ConformTransformationQ1CartesianHexahedronJacobian(ConformTransformationQ1CartesianHexahedron& T)
    : __T(T),
      __jacobian(0),
      __invJacobian(0),
      __det(0)
  {
    for (size_t i=0; i<3; ++i) {
      __invJacobian(i,i) = 1./(__T.__h[i]);
      __jacobian(i,i) = (__T.__h[i]);
    }

    __det = __T.__h[0] * __T.__h[1] * __T.__h[2];

  }
};

class ConformTransformationP1Tetrahedron
{
public:
  friend class ConformTransformationP1TetrahedronJacobian;
  typedef ConformTransformationP1Tetrahedron BoundaryConformTransformation;

private:

  const TinyVector<3, real_t> __b;
  TinyMatrix<3,3, real_t> __A;
  TinyMatrix<3,3, real_t> __A_1;

public:
  inline void dx(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dx(X[0], X[1], X[2], __result);
  }

  inline void dx(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    for (size_t i=0; i<3; ++i) __result[i] = __A(i,0);
  }

  inline void dy(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dy(X[0], X[1], X[2], __result);
  }

  inline void dy(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    for (size_t i=0; i<3; ++i) __result[i] = __A(i,1);
  }

  inline void dz(const TinyVector<3, real_t>& X,
		 TinyVector<3, real_t>& __result) const
  {
    dz(X[0], X[1], X[2], __result);
  }

  inline void dz(const real_t& x, const real_t& y, const real_t& z,
		 TinyVector<3, real_t>& __result) const
  {
    for (size_t i=0; i<3; ++i) __result[i] = __A(i,2);
  }

  //! computes the value at X (X is inside the reference element).
  inline void value(const TinyVector<3, real_t>& X,
		    TinyVector<3, real_t>& result) const
  {
    result = __A*X;
    result += __b;
  }

  //! computes the value at (x,y,z) ((x,y,z) is inside the reference element).
  inline void value(const real_t& x, const real_t& y, const real_t& z,
		    TinyVector<3, real_t>& result) const
  {
    TinyVector<3,real_t> X(x,y,z);
    value(X, result);
  }

  //! Computes Xhat, the point which transformed is X
  inline bool invertT(const TinyVector<3, real_t>& X,
		      TinyVector<3, real_t>& Xhat) const
  {
    Xhat = __A_1*(X-__b);
    return true;
  }

  //! Computes Xhat, the point which transformed is (x,y,z)
  inline bool invertT(const real_t& x, const real_t& y, const real_t& z,
		      TinyVector<3, real_t>& Xhat) const
  {
    TinyVector<3,real_t> X(x,y,z);
    return invertT(X, Xhat);
  }

  /*!  Computes the integrale of the function f on an element using
    the reference element
  */
  real_t integrate(const ScalarFunctionBase& f) const;

  ConformTransformationP1Tetrahedron(const Tetrahedron& T)
    : __b(T(0))
  {
    for (size_t j=0; j<3; ++j) {
      for (size_t i=0; i<3; ++i) {
	__A(i,j) = T(j+1)[i]-T(0)[i];
      }
    }

    __A_1 = __A.invert();
  }
};

class ConformTransformationP1TetrahedronJacobian
{
public:
  typedef ConformTransformationP1Tetrahedron AssociatedTransformation;

private:
  TinyMatrix<3,3>& __invJacobian;

  real_t __det;
public:

  /*! return the invert of the transposed of the jacobian of the
    transformation.
  */
  const TinyMatrix<3,3,real_t>& invJacobian() const
  {
    return __invJacobian;
  }

  //! Returns the value of the invert of the jacobian.
  const real_t& invJacobian(const size_t i, const size_t j) const
  {
    return __invJacobian(i,j);
  }

  //! returns the jacobian of the transformation
  const real_t& jacobianDet() const
  {
    return __det;
  }

  ConformTransformationP1TetrahedronJacobian(ConformTransformationP1Tetrahedron& T)
    : __invJacobian(T.__A_1),
      __det(0)
  {
    __det = det(T.__A);
  }

};

#endif // CONFORM_TRANSFORMATION_HPP
