//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SPECTRAL_LEGENDRE_DISCRETIZATION_CONFORM_HPP
#define SPECTRAL_LEGENDRE_DISCRETIZATION_CONFORM_HPP

#include <Problem.hpp>

#include <DegreeOfFreedomSet.hpp>
#include <SpectralMesh.hpp>

#include <DoubleHashedMatrix.hpp>
#include <UnAssembledMatrix.hpp>

#include <Discretization.hpp>
#include <ScalarDiscretizationTypeLegendre.hpp>

#include <Mesh.hpp>

#include <SpectralLegendreDiscretizer.hpp>

#include <ErrorHandler.hpp>
#include <Timer.hpp>

class SpectralLegendreDiscretizationConform
  : public Discretization
{
private:
  SpectralLegendreDiscretizer __spectralLegendreDiscretizer;

public:  
  /** 
   * Assembles the matrix associated to the PDE operators of the PDE
   * problem.
   * 
   */
  void assembleMatrix()
  {
    switch ((this->__A).type()) {
    case BaseMatrix::doubleHashedMatrix: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Spectral Method cannot be used with assembled matrices",
			 ErrorHandler::unexpected);
    }
    case BaseMatrix::unAssembled: {
      UnAssembledMatrix& A = dynamic_cast<UnAssembledMatrix&>(this->__A);
      A.setDiscretization(this);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected matrix type",
			 ErrorHandler::unexpected);
    }
    }
  }
  
  /** 
   *  Applies directly the operator discretization to the vector X.
   * 
   * @param u input vector
   * @param v \f$ v=Au \f$
   */
  void timesX(const BaseVector& u, BaseVector& v) const
  {
    dynamic_cast<Vector<real_t>&>(v)=0;
    __spectralLegendreDiscretizer.timesX(u,v);
  }
  
  /** 
   *  Applies directly the operator discretization to the vector X.
   * 
   * @param u input vector
   * @param v \f$ v=A^T u \f$
   */
  void transposedTimesX(const BaseVector& u, BaseVector& v) const
  {
    dynamic_cast<Vector<real_t>&>(v) = 0;
    __spectralLegendreDiscretizer.transposedTimesX(u,v);
  }

  /** 
   * Computes diagonal of the operator
   * 
   * @param z diagonal of the operator
   */
  void getDiagonal(BaseVector& z) const
  {
    dynamic_cast<Vector<real_t>&>(z)=0;
    __spectralLegendreDiscretizer.getDiagonal(z);
  }

  /** 
   * Computes diagonal of the operator
   * 
   * @param A multi-diagonal matrix
   */
  void getMultiDiagonal(BaseMatrix& A) const
  {
    A.reset();
    __spectralLegendreDiscretizer.getMultiDiagonal(A);
  }

  /** 
   * Second member assembling
   * 
   */
  void assembleSecondMember()
  {
    Timer t;
    t.start();
    
    ffout(2) << "- assembling second member\n";
    
    //! The elementary vector
    Vector<real_t>& b = (static_cast<Vector<real_t>&>(this->__b));
    b = 0;

    __spectralLegendreDiscretizer.assembleSecondMember();

    ffout(2) << "- assembling second member: done";
    t.stop();
    ffout(3) << " [cost: " << t << ']';
    ffout(2) << '\n';
  }
public:
  
  /** 
   * Constructor of the discretization
   * 
   * @param p the problem
   * @param m the mesh used for discretization
   * @param a matrix storing discretization
   * @param bb vector that stores second member discretization
   * @param dof degrees of freedom set
   * @param discretizationType type of discretization
   */
  SpectralLegendreDiscretizationConform(const Problem& p,
					const SpectralMesh& m,
					BaseMatrix& a,
					BaseVector& bb,
					const DegreeOfFreedomSet& dof,
					const DiscretizationType& discretizationType);  
  /** 
   * Destructor
   * 
   */
  ~SpectralLegendreDiscretizationConform();
};

    
#endif // SPECTRAL_LEGENDRE_DISCRETIZATION_CONFORM_HPP
