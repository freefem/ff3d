//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_REFERENCE_HPP
#define BOUNDARY_REFERENCE_HPP

#include <Boundary.hpp>
#include <set>

/**
 * @file   BoundaryReferences.hpp
 * @author Stephane Del Pino
 * @date   Fri Dec 26 18:22:16 2003
 * 
 * @brief  Set of references associated to a boundary
 * 
 */
class BoundaryReferences
  : public Boundary
{
public:
  typedef std::set<size_t> ReferencesSet; /**< The set of references type */

private:
  ReferencesSet __references;	/**< The set of references associated with this boundary */

  void put(std::ostream& os) const
  {
    for (ReferencesSet::const_iterator i = __references.begin();
	 i != __references.end(); ++i) {
      os << *i << ',';
    }
    os << "\b ";
  }

public:
  const ReferencesSet& references() const
  {
    return __references;
  }

  void add(const size_t& i)
  {
    __references.insert(i);
  }

  BoundaryReferences(const BoundaryReferences& B)
    : Boundary(B),
      __references(B.__references)
  {
    ;
  }

  BoundaryReferences()
    : Boundary(Boundary::references)
  {
    ;
  }

  ~BoundaryReferences()
  {
    ;
  }
};


#endif // BOUNDARY_REFERENCE_HPP
