//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_CONDITION_SURFACE_MESH_ASSOCIATION_HPP
#define BOUNDARY_CONDITION_SURFACE_MESH_ASSOCIATION_HPP

#include <VariationalLinearBorderOperator.hpp>
#include <VariationalBilinearBorderOperator.hpp>

#include <VariationalBorderOperatorFV.hpp>
#include <VariationalBorderOperatorAlphaUV.hpp>
#include <Dirichlet.hpp>
#include <Neumann.hpp>
#include <Fourrier.hpp>

#include <BoundaryMeshAssociation.hpp>

#include <map>
#include <vector>

/**
 * @file   BoundaryConditionSurfaceMeshAssociation.hpp
 * @author St�phane Del Pino
 * @date   Wed Jul 24 09:30:22 2002
 * 
 * @brief  This class is used to map boundary conditions to there meshes
 * 
 * This class is used to map boundary conditions to their meshes, and
 * more over, it classifies them by sorting natural and non-natural
 * boundary conditions when they come from PDE-like problem definition.
 * 
 * @todo Should try to put back reference counters [may be not
 * possible according to the design].
 */

class BoundaryConditionSurfaceMeshAssociation
{
public:
  typedef
  std::
  multimap<const Dirichlet*,
	   ConstReferenceCounting<SurfaceMesh> >
  DirichletMeshAssociation;

  typedef
  std::
  multimap<ConstReferenceCounting<VariationalLinearBorderOperator>,
	   ConstReferenceCounting<SurfaceMesh> >
  LinearBorderOperatorMeshAssociation;

  typedef
  std::
  multimap<ConstReferenceCounting<VariationalBilinearBorderOperator>,
	   ConstReferenceCounting<SurfaceMesh> >
  BilinearBorderOperatorMeshAssociation;

private:
  std::vector<DirichletMeshAssociation>
  __dirichletMeshAssociation;  /**< Association of BoundaryCondition
				  and Mesh (uses a vector since it
				  appears for each equation)*/

  LinearBorderOperatorMeshAssociation
  __lboMeshAssociation; /**< Association of Linear Border Operator and
			   Mesh */
  
  BilinearBorderOperatorMeshAssociation
  __bboMeshAssociation; /**< Association of Bilinear Border Operator
			   and Mesh */

public:

  /** 
   * Adds a boundary condition coming from the PDE description and
   * sort it according to its type.
   * 
   * @param i the number of the equation it comes from
   * @param bc the boundary condition itself
   * @param S the mesh
   */
  void addPDEBoundaryConditionAndMesh(const size_t& i,
				      const BoundaryCondition* bc,
				      const SurfaceMesh* S) {

    switch (bc->condition()->type()) {
    case PDECondition::dirichlet: {
      const Dirichlet& D = dynamic_cast<const Dirichlet&>(*bc->condition());
      __dirichletMeshAssociation[i].insert(std::pair<const Dirichlet*,
					   const SurfaceMesh*>(&D,S));
      break;
    }
    case PDECondition::neumann: {
      const Neumann& N = dynamic_cast<const Neumann&>(*bc->condition());

      ConstReferenceCounting<VariationalLinearBorderOperator>
	LV = new VariationalBorderOperatorFV(i, N.g(), bc->boundary());

      __lboMeshAssociation.insert(std::pair<ConstReferenceCounting<VariationalLinearBorderOperator>,
 				  const SurfaceMesh*>(LV,S));
      break;
    }
    case PDECondition::fourrier: {
      const Fourrier& F = dynamic_cast<const Fourrier&>(*bc->condition());

      ConstReferenceCounting<VariationalLinearBorderOperator>
	LV = new VariationalBorderOperatorFV(i, F.g(), bc->boundary());

      __lboMeshAssociation.insert(std::pair<ConstReferenceCounting<VariationalLinearBorderOperator>,
 				  const SurfaceMesh*>(LV,S));

      ConstReferenceCounting<VariationalBilinearBorderOperator>
	BLV = new VariationalBorderOperatorAlphaUV(i,F.unknownNumber(),
						   F.alpha(), bc->boundary());

      __bboMeshAssociation.insert(std::pair<ConstReferenceCounting<VariationalBilinearBorderOperator>,
				  const SurfaceMesh*>(BLV, S));

      break;
    }
    }
  }
  
  /** 
   * Returns the boundary condition mesh association related to test
   * function number i
   * 
   * @param i 
   */
  DirichletMeshAssociation& bc(const size_t& i)
  {
    ASSERT(i<__dirichletMeshAssociation.size());
    return __dirichletMeshAssociation[i];
  }

  /** 
   * Returns the boundary condition mesh association related to test
   * function number i
   * 
   * @param i 
   */
  const DirichletMeshAssociation& bc(const size_t& i) const
  {
    ASSERT(i<__dirichletMeshAssociation.size());
    return __dirichletMeshAssociation[i];
  }

  /** 
   * Sets the number of test functions.
   * 
   * @param i 
   */
  void bcSetSize(const size_t& i)
  {
    __dirichletMeshAssociation.resize(i);
  }

  /** 
   * Adds a bilinear border operator and its mesh to the list.
   * 
   * @param v the variational border operator
   * @param s the surface mesh
   */
  void bilinearBorderOperatorAdd(ConstReferenceCounting<VariationalBilinearBorderOperator> v,
				 const SurfaceMesh* s)
  {
    __bboMeshAssociation.insert(std::pair<ConstReferenceCounting<VariationalBilinearBorderOperator>,
				const SurfaceMesh*>(v,s));
  }

  /** 
   * Adds a linear border operator and its mesh to the list.
   * 
   * @param v the variational border operator
   * @param s the surface mesh
   */
  void linearBorderOperatorAdd(ConstReferenceCounting<VariationalLinearBorderOperator> v,
			       const SurfaceMesh* s)
  {
    __lboMeshAssociation.insert(std::pair<ConstReferenceCounting<VariationalLinearBorderOperator>,
				          const SurfaceMesh*>(v,s));
  }

  /** 
   * Gets the begin iterator for the list of bi linear operators
   * 
   * @return __bboMeshAssociation.begin()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::BilinearBorderOperatorMeshAssociation
  ::iterator beginOfBilinear()
  {
    return __bboMeshAssociation.begin();
  }

  /** 
   * Gets the begin iterator for the list of bi linear operators
   * 
   * @return __bboMeshAssociation.begin()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::BilinearBorderOperatorMeshAssociation
  ::const_iterator beginOfBilinear() const
  {
    return __bboMeshAssociation.begin();
  }

  /** 
   * Gets the end iterator for the list of bi linear operators
   * 
   * @return __bboMeshAssociation.end()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::BilinearBorderOperatorMeshAssociation
  ::const_iterator endOfBilinear() const
  {
    return __bboMeshAssociation.end();
  }

  /** 
   * Gets the begin iterator for the list of linear operators
   * 
   * @return __lboMeshAssociation.begin()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::LinearBorderOperatorMeshAssociation
  ::iterator beginOfLinear()
  {
    return __lboMeshAssociation.begin();
  }

  /** 
   * Gets the begin iterator for the list of linear operators
   * 
   * @return __lboMeshAssociation.begin()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::LinearBorderOperatorMeshAssociation
  ::const_iterator beginOfLinear() const
  {
    return __lboMeshAssociation.begin();
  }

  /** 
   * Gets the end iterator for the list of linear operators
   * 
   * @return __lboMeshAssociation.end()
   */
  BoundaryConditionSurfaceMeshAssociation
  ::LinearBorderOperatorMeshAssociation
  ::const_iterator endOfLinear() const
  {
    return __lboMeshAssociation.end();
  }

  /** 
   * Constructor
   * 
   */
  BoundaryConditionSurfaceMeshAssociation(const Problem& P,
					  const BoundaryMeshAssociation& bma)
    : __dirichletMeshAssociation(P.numberOfUnknown())
  {
    for (size_t i=0; i<P.numberOfUnknown(); ++i) {
      const BoundaryConditionSet& bcSet
	= P.boundaryConditionSet(i);

      for(size_t j=0; j<bcSet.nbBoundaryCondition(); ++j) {
	const BoundaryCondition& bc = bcSet[j];
	const Boundary& b = *bc.boundary();
	this->addPDEBoundaryConditionAndMesh(i,&bc,bma[b]);
      }
    }

    // Now sets the natural boundary conditions coming from a variational problem
    if(P.type() == Problem::variational) {
      const VariationalProblem& VP = dynamic_cast<const VariationalProblem&>(P);
      for (VariationalProblem::bilinearBorderOperatorConst_iterator i = VP.beginBilinearBorderOperator();
	   i != VP.endBilinearBorderOperator(); ++i) {
	const Boundary& b = dynamic_cast<const Boundary&>(*(*i)->boundary());
	this->bilinearBorderOperatorAdd(*i,bma[b]);
      }
      for (VariationalProblem::linearBorderOperatorConst_iterator i = VP.beginLinearBorderOperator();
	   i != VP.endLinearBorderOperator(); ++i) {
	const Boundary& b = dynamic_cast<const Boundary&>(*(*i)->boundary());
	this->linearBorderOperatorAdd(*i,bma[b]);
      }
    }
  }

  /** 
   * Copy constructor
   * 
   * @param B a BoundaryConditionSurfaceMeshAssociation
   */
  BoundaryConditionSurfaceMeshAssociation(const BoundaryConditionSurfaceMeshAssociation& B)
    : __dirichletMeshAssociation(B.__dirichletMeshAssociation),
      __lboMeshAssociation(B.__lboMeshAssociation),
      __bboMeshAssociation(B.__bboMeshAssociation)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~BoundaryConditionSurfaceMeshAssociation()
  {
    ;
  }
};

#endif // BOUNDARY_CONDITION_SURFACE_MESH_ASSOCIATION_HPP

