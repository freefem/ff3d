//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_HPP
#define MESH_HPP

#include <Vector.hpp>

#include <ReferenceCounting.hpp>

#include <VerticesSet.hpp>
#include <VerticesCorrespondance.hpp>

#include <EdgesSet.hpp>

#include <Cell.hpp>
#include <Edge.hpp>

#include <string>

template <typename MeshType>
class MeshExtractor;

class Mesh
{
public:
  typedef enum {
    cartesianHexahedraMesh,
    hexahedraMesh,
    octreeMesh,
    tetrahedraMesh,
    trianglesMesh,
    spectralMesh,
    surfaceMeshTriangles,
    surfaceMeshQuadrangles,
  } Type;

  //! Family
  typedef enum {
    volume,
    surface,
    plane
  } Family;

private:
  //! Mesh extractors are friends
  template <typename MeshType>
  friend class MeshExtractor;

  size_t static __getNextMeshId()
  {
    static size_t nextId = 0;
    return nextId++;
  }

  const Type __type;		/**< type of the mesh */
  const Family __family;	/**< family of the mesh */
  const size_t __meshId;	/**< identifier of the mesh */
protected:

  template <typename MeshType,
	    typename CellType = typename MeshType::CellType>
  class T_iterator
  {
  protected:
    MeshType* __mesh;
    CellType* __iterator;
    size_t __number;

  public:
    typedef enum {
      Begin,
      End
    } Position;

    const size_t& number() const
    {
      return __number;
    }

    inline CellType& operator*()
    {
      return *__iterator;
    }

    inline CellType* pointer() const
    {
      return __iterator;
    }

    inline bool end() const
    {
      return (__number >= __mesh->numberOfCells());
    }

    inline bool operator<(const T_iterator<MeshType, CellType>& i) const
    {
      return (__number < i.__number);
    }

    virtual T_iterator<MeshType, CellType>&
    operator=(const T_iterator<MeshType, CellType>& i)
    {
      __mesh = i.__mesh;
      __iterator = i.__iterator;
      __number = i.__number;

      return *this;
    }

    virtual T_iterator<MeshType, CellType>&
    operator=(CellType* iterator)
    {
      __iterator = iterator;
      __number = static_cast<size_t>(__iterator-&(__mesh->cell(0)));

      return *this;
    }

    inline T_iterator<MeshType, CellType> operator++(int)
    {
      T_iterator<MeshType, CellType> i = *this;
      ++(*this);
      return i;
    }

    inline T_iterator<MeshType, CellType> operator++()
    {
      for(;;) {
	++__number;
	if (this->end()) {
	  break;
	}
	++__iterator;
	if (not(__iterator->isFictitious())) {
	  break;
	}
      }
      return *this;
    }

    T_iterator(MeshType& m,
	       T_iterator::Position position = T_iterator::Begin)
      : __mesh(&m),
	__iterator(0),
	__number(0)
    {
      if ((m.numberOfCells() == 0)
	  or (position == T_iterator<MeshType, CellType>::End)) {
	__number = __mesh->numberOfCells();
      } else {
	__iterator = & m.cell(0);
	if (__iterator->isFictitious()) {
	  ++(*this);
	}
      }
    }

    T_iterator(MeshType& m,
	       const size_t& cellNumber)
      : __mesh(&m),
	__iterator(0),
	__number(cellNumber)
    {
      if (cellNumber < m.numberOfCells()) {
	__iterator = & m.cell(cellNumber);
      }
    }

    T_iterator(const T_iterator<MeshType, CellType>& i)
      : __mesh(i.__mesh),
	__iterator(i.__iterator),
	__number(i.__number)
    {
      ;
    }

    virtual ~T_iterator()
    {
      ;
    }
  };

  ReferenceCounting<VerticesSet>
  __verticesSet;		/**< Container for vertices */

  ReferenceCounting<VerticesCorrespondance>
  __verticesCorrespondance;	/**< Container for vertices
				   correspondances */

  ReferenceCounting<EdgesSet>
  __edgesSet;			/**< Container for edges */

  /** 
   * Copy constructor
   * 
   * @note copies of meshes are almost forbidden
   * 
   * @param mesh given mesh
   */
  Mesh(const Mesh& mesh)
    : __type(mesh.__type),
      __family(mesh.__family),
      __meshId(mesh.__meshId),
      __verticesSet(mesh.__verticesSet)
  {
    ;
  }

public:

  virtual bool hasBorderMesh() const = 0;

  virtual ConstReferenceCounting<Mesh> borderBaseMesh() const = 0;


  /** 
   * Checks if a mesh has a periodic topology
   * 
   * @return true if the mesh is periodic
   */
  virtual bool isPeriodic() const
  {
    return __verticesSet->numberOfVertices() != __verticesCorrespondance->numberOfCorrespondances();
  }

  /** 
   * Returns the typename of the mesh
   * 
   * @return type name of the mesh
   */
  virtual std::string typeName() const = 0;

  /** 
   * Checks if @f$ (x,y,z) @f$ is inside the mesh
   * 
   * @param x @f$ x @f$
   * @param y @f$ y @f$
   * @param z @f$ z @f$
   * 
   * @return @b true if @f$ (x,y,z) @f$ is inside the mesh.
   */
  virtual bool inside(const real_t& x,
		      const real_t& y,
		      const real_t& z) const =0;

  /** 
   * Checks if a point is inside the mesh
   * 
   * @param p the point to localize
   * 
   * @return @b true if the point @a p is inside the mesh.
   */
  virtual bool inside(const TinyVector<3, real_t>& p) const = 0;

  /** 
   * Read-only access to the type of the mesh
   * 
   * @return __type
   */
  const Mesh::Type& type() const
  {
    return __type;
  }

  /** 
   * Read-only access to the family of the mesh
   * 
   * @return __family
   */
  const Mesh::Family& family() const
  {
    return __family;
  }

  /** 
   * Read-only access to the id of the mesh
   * 
   * @return __meshId
   */
  const size_t& id() const
  {
    return __meshId;
  }

  /** 
   * Returns the number of the vertex v in the list
   * 
   * @param v a vertex
   * 
   * @return the vertex \a v number
   * 
   * @note polymorphism is not used for clarity reason
   */
  inline size_t vertexNumber(const Vertex& v) const
  {
    return __verticesSet->number(v);
  }

  /** 
   * tests if the EdgesSet has been built
   * 
   * @return true if __edgesSet is not NULL
   */
  bool hasEdges() const
  {
    return __edgesSet != 0;
  }

  /** 
   * builds the faces of a mesh
   * 
   */
  virtual void buildFaces() = 0;

  /** 
   * tests if the FacesSet has been built
   * 
   */
  virtual bool hasFaces() const = 0;

  /** 
   * builds the edges of a mesh
   * 
   */
  virtual void buildEdges() = 0;

  /** 
   *  Returns the number of the Edge e in the list
   * 
   * @param e an Edge
   * 
   * @return the edge \a E number
   * 
   * @note polymorphism is not used for clarity reason
   */
  inline size_t edgeNumber(const Edge& e) const
  {
    ASSERT(__edgesSet != 0);
    return __edgesSet->number(e);
  }

  /** 
   * Read-only access to the number of vertices
   * 
   * @return number of vertices
   */
  inline const size_t& numberOfVertices() const
  {
    return __verticesSet->numberOfVertices();
  }

  /** 
   * Changes the size of the vertices container.
   * 
   * @param size vertices set new size
   * @bug this function should not be allowed!
   */
  inline void setNumberOfVertices(const size_t& size)
  {
    if (__verticesSet == 0) {
      __verticesSet = new VerticesSet(size);
      __verticesCorrespondance = new VerticesCorrespondance(size);
    } else {
      __verticesSet->setNumberOfVertices(size);
      __verticesCorrespondance = new VerticesCorrespondance(size);
    }
  }

  /** 
   * Read-only access to the number of cells
   * 
   * @return number of cells
   */
  virtual const size_t& numberOfCells() const = 0;

  /** 
   * Read-only access to the number of edges
   * 
   * @return number of edges
   */
  inline size_t numberOfEdges() const
  {
    ASSERT(__edgesSet != 0);
    return __edgesSet->numberOfEdges();
  }

  /** 
   * Access to the vertices set of the mesh
   * 
   * @return __verticesSet
   */
  inline ReferenceCounting<VerticesSet> verticesSet()
  {
    return __verticesSet;
  }

  /** 
   * Read only access to the vertices set of the mesh
   * 
   * @return __verticesSet
   */
  inline ConstReferenceCounting<VerticesSet> verticesSet() const
  {
    return __verticesSet;
  }

  /** 
   * Access to the vertices correspondance table
   * 
   * @return __verticesCorrespondance
   */
  inline ReferenceCounting<VerticesCorrespondance> verticesCorrespondance()
  {
    return __verticesCorrespondance;
  }

  /** 
   * Read only access to the vertices correspondance table
   * 
   * @return __verticesCorrespondance
   */
  inline ConstReferenceCounting<VerticesCorrespondance> verticesCorrespondance() const
  {
    return __verticesCorrespondance;
  }

  /** 
   * Read only access to the ith vertex of the mesh
   * 
   * @param i the number of the vertex to access
   * 
   * @return the ith vertex
   */
  inline const Vertex& vertex(const size_t& i) const
  {
    return (*__verticesSet)[i];
  }

  /** 
   * Access to the ith vertex of the mesh
   * 
   * @param i the number of the vertex to access
   * 
   * @return the ith vertex
   */
  inline Vertex& vertex(const size_t& i)
  {
    return (*__verticesSet)[i];
  }

  /** 
   * Read only access to the ith vertex "real" number (ie: when
   * dealing with periodic boundaries)
   * 
   * @param i the number of the vertex 
   * 
   * @return the vertex "real" number
   */
  inline const size_t& correspondance(const size_t& i) const
  {
    return (*__verticesCorrespondance)[i];
  }

  /** 
   * read-only access to the ith edge 
   * 
   * @param i the number of the edge
   * 
   * @return ith edge
   */
  inline const Edge&
  edge(const size_t& i) const
  {
    return (*__edgesSet)[i];
  }

  /** 
   * access to the ith edge 
   * 
   * @param i the number of the edge
   * 
   * @return ith edge
   */
  inline Edge& edge(const size_t& i)
  {
    return (*__edgesSet)[i];
  }

  /** 
   * Constructor
   * 
   * @param type the type of the mesh
   * @param family the family of the mesh
   * @param numberOfVertices the number of vertices
   */
  Mesh(const Mesh::Type& type,
       const Mesh::Family& family,
       const size_t& numberOfVertices)
    : __type(type),
      __family(family),
      __meshId(Mesh::__getNextMeshId()),
      __verticesSet(new VerticesSet(numberOfVertices)),
      __verticesCorrespondance(new VerticesCorrespondance(numberOfVertices)),
      __edgesSet(0)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param type the type of the mesh
   * @param family the family of the mesh
   * @param vertices set of vertices
   * @param correspondance correspondance table for periodic meshes
   */
  Mesh(const Mesh::Type& type,
       const Mesh::Family& family,
       ReferenceCounting<VerticesSet> vertices,
       ReferenceCounting<VerticesCorrespondance> correspondance)
    : __type(type),
      __family(family),
      __meshId(Mesh::__getNextMeshId()),
      __verticesSet(vertices),
      __verticesCorrespondance(correspondance),
      __edgesSet(0)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~Mesh()
  {
    ;
  }
};

#endif // MESH_HPP

