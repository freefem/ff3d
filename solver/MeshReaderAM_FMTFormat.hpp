//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_READER_AM_FMT_FORMAT_HPP
#define MESH_READER_AM_FMT_FORMAT_HPP

#include <MeshReader.hpp>

#include <cstdio>

/**
 * @file   MeshReaderAM_FMTFormat.hpp
 * @author Stephane Del Pino
 * @date   Thu Sep 14 23:27:53 2006
 * 
 * @brief  Reads mesh from file in the 'am_fmt' format
 */
class MeshReaderAM_FMTFormat
  : public MeshReader
{
private:
  struct VertexError {};

  /** 
   * Check that vertex number is between 0 and the number of vertices
   * 
   * @param n vertex number to examine
   */
  void __checkVertex(const size_t& n);

  /** 
   * Reads the next vertex number in the file
   * 
   * @return the vertex number
   */
  size_t __getVertexNumber();

  /** 
   * Reads the set of vertices
   * 
   */
  void __readVertices();

  /** 
   * Reads the vertices references
   * 
   */
  void __readVerticesReferences();

  /** 
   * Reads the set of triangles
   * 
   */
  void __readTriangles();

  /** 
   * Reads the triangles references
   * 
   */
  void __readTrianglesReferences();

  /** 
   * Copy constructor is forbidden
   * 
   * @param M a given MeshReaderFMTFormat
   */
  MeshReaderAM_FMTFormat(const MeshReaderAM_FMTFormat& M);

public:
  /** 
   * Constructor
   * 
   * @param s the filename
   */
  MeshReaderAM_FMTFormat(const std::string & s);

  /** 
   * Destructor
   * 
   */
  ~MeshReaderAM_FMTFormat()
  {
    ;
  }
};

#endif // MESH_READER_AM_FMT_FORMAT_HPP
