//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 Stephane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#include <DegreeOfFreedomSetManager.hpp>

#include <ScalarDiscretizationTypeBase.hpp>
#include <ScalarDiscretizationTypeLagrange.hpp>
#include <ScalarDiscretizationTypeLegendre.hpp>

#include <ScalarDegreeOfFreedomPositionsSet.hpp>
#include <StreamCenter.hpp>

#include <Structured3DMesh.hpp>
#include <MeshOfTetrahedra.hpp>

#include <Mesh.hpp>


class DegreeOfFreedomSetManager::Internal
{
private:
  typedef std::string DiscretizationLabel;

  typedef
  std::map<std::pair<const Mesh*, DiscretizationLabel>,
	   std::pair<size_t, ScalarDegreeOfFreedomPositionsSet*> >
  MeshesDOFPositionsCorrespondance;

  MeshesDOFPositionsCorrespondance __meshesDOFPositions;

public:
  ConstReferenceCounting <ScalarDegreeOfFreedomPositionsSet>
  getDOFPositionsSet(const Mesh& mesh,
		     const ScalarDiscretizationTypeBase& discretizationType)
  {
    DiscretizationLabel label = ScalarDiscretizationTypeBase::name(discretizationType);

    switch (discretizationType.type()) {
    case ScalarDiscretizationTypeBase::spectralLegendre: {
      const ScalarDiscretizationTypeLegendre& spectralDiscretizationType
	= dynamic_cast<const ScalarDiscretizationTypeLegendre&>(discretizationType);
      label += "-"+stringify(spectralDiscretizationType.degrees());
      break;
    }
    default: {
    }
    }

    MeshesDOFPositionsCorrespondance::iterator
      i = __meshesDOFPositions.find(std::make_pair(&mesh,label));

    if (i != __meshesDOFPositions.end()) {
      (*i).second.first++;
      return (*i).second.second;
    } else {
      ScalarDegreeOfFreedomPositionsSet* dof
	= new ScalarDegreeOfFreedomPositionsSet(discretizationType,mesh);

      __meshesDOFPositions[std::make_pair(&mesh, label)]
	= std::make_pair(1,dof);
      return dof;
    }
  }

  void unsubscribe(ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet> dofSet)
  {
    for (MeshesDOFPositionsCorrespondance::iterator i
	   = __meshesDOFPositions.begin();
	 i != __meshesDOFPositions.end(); ++i) {
      if ((*i).second.second == dofSet) {
	(*i).second.first--;
	if ((*i).second.first == 0) {
	  __meshesDOFPositions.erase(i);
	}
	return;
      }
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "DOF Set not found!",
		       ErrorHandler::unexpected);

  }

  Internal()
  {
    ;
  }

  ~Internal()
  {
    if (__meshesDOFPositions.size() > 0) {
      fferr(1) << __FILE__ << ':' << __LINE__
	       << ":warning: degrees of freedom set manager is not empty!\n";
    }
  }
};


ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet>
DegreeOfFreedomSetManager::
getDOFPositionsSet(const Mesh& mesh,
		   const ScalarDiscretizationTypeBase& discretizationType)
{
  return __internal->getDOFPositionsSet(mesh, discretizationType);
}

void DegreeOfFreedomSetManager::
unsubscribe(ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet> dofSet)
{
  __internal->unsubscribe(dofSet);
}


DegreeOfFreedomSetManager::
DegreeOfFreedomSetManager()
{
  __internal = new Internal();
}

DegreeOfFreedomSetManager::
~DegreeOfFreedomSetManager()
{
  delete __internal;
}
