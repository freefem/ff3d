//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARYCONDITION_HPP
#define BOUNDARYCONDITION_HPP

#include <Boundary.hpp>
#include <PDECondition.hpp>

/**
 * @file   BoundaryCondition.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 14:51:31 2006
 * 
 * @brief This class is used to describe sets of boundary conditions
 * applied to a scalar unknown.
 * 
 */
class BoundaryCondition
{
private:
  //! a PDE like condition.
  ConstReferenceCounting<PDECondition> __pdeCondition;

  //! The associated boundary.
  ConstReferenceCounting<Boundary> __boundary;

public:
  /** 
   * read only access to the boundary.
   * 
   * @return __boundary
   */
  ConstReferenceCounting<Boundary>
  boundary() const
  {
    return __boundary;
  }

  /** 
   * read only access to the condition.
   * 
   * @return __pdeCondition
   */
  ConstReferenceCounting<PDECondition>
  condition() const
  {
    return __pdeCondition;
  }

  /** 
   * writes the boundary condition.
   * 
   * @param os given output stream
   * @param bc given boundary condition
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os,
	       const BoundaryCondition& bc)
  {
    os << "Boundary Condition\n";
    os << *bc.__pdeCondition;
    os << *bc.__boundary;
    return os;
  }

  /** 
   * Constructor
   * 
   * @param pdeCondition a PDE condition
   * @param aBoundary a boundary
   */
  BoundaryCondition(ConstReferenceCounting<PDECondition> pdeCondition,
		    ConstReferenceCounting<Boundary> aBoundary)
    : __pdeCondition(pdeCondition),
      __boundary(aBoundary)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param b a boundary condition
   */
  BoundaryCondition(const BoundaryCondition& b)
    : __pdeCondition(b.__pdeCondition),
      __boundary(b.__boundary)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~BoundaryCondition()
  {
    ;
  }
};

#endif // BOUNDARYCONDITION_HPP

