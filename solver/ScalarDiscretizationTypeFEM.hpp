//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_DISCRETIZATION_TYPE_FEM_HPP
#define SCALAR_DISCRETIZATION_TYPE_FEM_HPP

#include <ScalarDiscretizationTypeBase.hpp>

/**
 * @file   ScalarDiscretizationTypeFEM.hpp
 * @author St�phane Del Pino
 * @date   Tue Jan  1 22:13:19 2008
 * 
 * @brief This class describes types of discretization for scalar
 * finite element quantities 
 * 
 * 
 */
class ScalarDiscretizationTypeFEM
  : public ScalarDiscretizationTypeBase
{
private:
  void __instanciable() const {}

public:
  ScalarDiscretizationTypeFEM(const ScalarDiscretizationTypeBase::Type& type)
    : ScalarDiscretizationTypeBase(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d originale fem discretization type
   */
  ScalarDiscretizationTypeFEM(const ScalarDiscretizationTypeFEM& d)
    : ScalarDiscretizationTypeBase(d)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarDiscretizationTypeFEM()
  {
    ;
  }
};

#endif // SCALAR_DISCRETIZATION_TYPE_FEM_HPP
