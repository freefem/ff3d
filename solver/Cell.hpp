//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef CELL_HPP
#define CELL_HPP

#include <Vertex.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>
 
/**
 * @file   Cell.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 15 19:49:43 2003
 * 
 * @brief  This class describes 3D Mesh cells.
 * 
 * 
 */
class Cell {
public:
  enum Type {
    hexahedron,
    cartesianHexahedron,
    hexahedronByEdge,
    tetrahedron,
    triangle3d,
    quadrangle3d
  };

protected:
  //! Reference of the Cell
  size_t __reference;

  //! Volume or surface of the cell
  real_t __volume;

  //! Pointers on vertices of the cell
  Vertex** __vertices;

  //! indicates if the cell is to be taken into accounts for computations
  mutable bool __isFictitious;

public:
  /** 
   *  Find the vertex @a P in the cell
   * 
   * 
   * @return bool
   */
  inline bool find(const Vertex * P) {
    for(size_t i=0 ; i<numberOfVertices() ; ++i) {
      if(__vertices[i]==P) {
	return true;
      }
    }
    return false;
  }

  /** 
   * Read-only access to the volume of the cell
   * 
   * @return the volume
   */
  inline const real_t& volume() const
  {
    return __volume;
  }

  /** 
   * Replaces a vertex by another
   * 
   * @param v0 the vertex to replace
   * @param v1 the new vertex
   */
  inline void replace(Vertex* v0, Vertex* v1)
  {
    for (size_t i = 0; i<numberOfVertices(); ++i) {
      if (__vertices[i] == v0) {
	__vertices[i] = v1;
	return;
      }
    }

    const std::string errorMsg
      = "vertex "+stringify(v0)+" was not found while replacing "+stringify(v1);

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg,
		       ErrorHandler::unexpected);
  }

  /** 
   * Read-only access to the fictitious state of the cell
   * 
   * @return true if the cell is fictitious
   */
  const bool& isFictitious() const
  {
    return __isFictitious;
  }

  /** 
   * Sets fictitious state of the cell
   * 
   * @note method is const since this does not change the cell.
   * Fictitious state is a mutable member!
   */
  void setFictitious(const bool& b) const
  {
    __isFictitious = b;
  }

  /** 
   * Read-only ccess to the number of vertices
   * 
   * @return the number of vertices
   */
  virtual size_t numberOfVertices() const = 0;

  /** 
   * Access to the type of the cell
   * 
   * @return the cell type
   */
  virtual Cell::Type type() const = 0;

  /** 
   * Read-only access to the reference of the Cell.
   * 
   * @return a const reference to the reference
   */
  inline const size_t& reference() const
  {
    return __reference;
  }

  /** 
   *  Access to the reference of the Cell.
   * 
   * @return a reference to the reference
   */
  inline size_t& reference()
  {
    return __reference;
  }

  /** 
   * Access to the ith Vertex of the Cell.
   * 
   * @param i the local number of the vertex
   * 
   * @return a reference to a the vertex
   */
  inline Vertex& operator () (const size_t& i)
  {
    ASSERT (i<numberOfVertices());
    return *__vertices[i];
  }

  /** 
   * Read-only access to the ith Vertex of the Cell.
   * 
   * @param i the local number of the vertex
   * 
   * @return a const reference to a the vertex
   */
  inline const Vertex& operator () (const size_t& i) const
  {
    ASSERT (i<numberOfVertices());
    return *(__vertices[i]);
  }

  /** 
   * Sets the cell to a given one
   * 
   * @param C the given cell
   * 
   * @return the modified cell
   */
  inline const Cell& operator=(const Cell& C)
  {
    __reference = C.__reference;
    __volume = C.__volume;

    for (size_t i=0; i<C.numberOfVertices(); ++i)
      __vertices[i] = C.__vertices[i];

    return *this;
  }

  /** 
   * Constructs a cell with a given number of vertices
   * 
   * @param numberOfVertices the number of vertices
   */
  Cell(const size_t& numberOfVertices)
    : __reference(0),
      __volume(0),
      __isFictitious(false)
  {
    ASSERT(numberOfVertices > 0);
    __vertices = new Vertex*[numberOfVertices];
  }

  /** 
   * Constructs a cell with a given number of vertices and a given
   * reference
   * 
   * @param numberOfVertices the number of vertices
   * @param reference the given reference
   */
  Cell(const size_t& numberOfVertices, const size_t& reference)
    : __reference(reference),
      __volume(0),
      __isFictitious(false)
  {
    ASSERT(numberOfVertices > 0);
    __vertices = new Vertex*[numberOfVertices];
  }

  /** 
   * Copies a given cell
   * 
   * @param C the given cell
   */
  Cell(const Cell& C)
    : __reference(C.__reference),
      __volume(C.__volume),
      __isFictitious(C.__isFictitious)
  {
    __vertices = new Vertex*[C.numberOfVertices()];
    for (size_t i=0; i<C.numberOfVertices(); ++i)
      __vertices[i] = C.__vertices[i];
  }

  /** 
   * The destructor
   * 
   */
  virtual ~Cell()
  {
    delete [] __vertices;
  }
};

#endif // CELL_HPP

