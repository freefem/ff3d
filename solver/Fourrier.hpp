//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// this class allows to define Fourrier Boundary Conditions

#ifndef FOURRIER_HPP
#define FOURRIER_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <PDECondition.hpp>

/**
 * @file   Fourrier.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 18:51:29 2006
 * 
 * @brief This class sets Fourrier or Robin boundary conditions:
 * @f$\alpha u + A\nabla u \cdot n = g @f$ on @f$\gamma@f$.
 * 
 * @f$ A\nabla u \cdot n @f$ is the @b conormal derivative arising
 * from the operator @f$ \nabla \cdot A\nabla @f$ when applying the
 * Green formula on the @em variationnal problem associated to the \em
 * PDE problem.
 */
class Fourrier
  : public PDECondition
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __Alpha;			/**< The @f$\alpha@f$ coeficient */

  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< The function to impose as a
				   Fourrier Boundary Condition */

public:
  /** 
   * Read only access to the mass coefficient
   * 
   * @return @f$\alpha@f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  alpha() const
  {
    return __Alpha;
  }

  /** 
   * Rad-only access to the second member
   * 
   * 
   * @return @f$ g @f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  g() const
  {
    return __g;
  }

  /** 
   * Gets the type name of the condition
   * 
   * @return "Fourrier"
   */
  std::string typeName() const
  {
    return "Fourrier";
  }

  /** 
   * Constructs the condition
   * 
   * @param ualpha @f$\alpha@f$
   * @param ug @f$ g @f$
   * @param unknownNumber the unknown number
   */
  Fourrier(ConstReferenceCounting<ScalarFunctionBase> ualpha,
	   ConstReferenceCounting<ScalarFunctionBase> ug,
	   const size_t& unknownNumber)
    : PDECondition(PDECondition::fourrier,
		   unknownNumber),
      __Alpha(ualpha),
      __g(ug)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param F given Fourrier condition
   */
  Fourrier(const Fourrier& F)
    : PDECondition(F),
      __Alpha(F.__Alpha),
      __g(F.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Fourrier()
  {
    ;
  }
};

#endif // FOURRIER_HPP
