//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef OCTREE_MESH_HPP
#define OCTREE_MESH_HPP

#include <Mesh.hpp>

#include <CartesianHexahedron.hpp>
#include <SurfaceMeshOfQuadrangles.hpp>

#include <VerticesSet.hpp>
#include <VerticesCorrespondance.hpp>

#include <Connectivity.hpp>

/**
 * @file   OctreeMesh.hpp
 * @author Stephane Del Pino
 * @date   Sat Aug 18 17:35:49 2007
 * 
 * @brief Describes a non conform mesh that tries to fit the geometry
 * 
 */
class OctreeMesh
  : public Mesh
{
public:
  typedef CartesianHexahedron CellType;
  typedef Quadrangle FaceType;

  enum {
    family = Mesh::volume
  };

  typedef SurfaceMeshOfQuadrangles BorderMeshType;

private:
  ReferenceCounting<Vector<CartesianHexahedron> >
  __cells;			/**< set of cells */

  Connectivity<OctreeMesh> __connectivity; /**< Connectivity */

  /** 
   * Copy constructor is forbidden
   * 
   */
  OctreeMesh(const OctreeMesh&);

public:
  typedef Mesh::T_iterator<OctreeMesh, CartesianHexahedron> iterator;
  typedef Mesh::T_iterator<const OctreeMesh, const CartesianHexahedron> const_iterator;

  /** 
   * Checks if a mesh has a periodic topology
   * 
   * @return true if the mesh is periodic
   */
  bool isPeriodic() const
  {
    return false;
  }

  /** 
   * Checks if the mesh has a surface mesh
   * 
   * @return true if a surface mesh has been built
   */
  bool hasBorderMesh() const
  {
    return false;
  }

  ConstReferenceCounting<Mesh> borderBaseMesh() const
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented yet",
		       ErrorHandler::unexpected);

    return 0;
  }

  /** 
   * Read only access to the surface mesh
   * 
   * @return the surface mesh
   */
  ConstReferenceCounting<BorderMeshType> borderMesh() const
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
    return 0;
  }

  /** 
   * Access to the @a i th cell of the mesh
   * 
   * @param i cell number
   * 
   * @return @a i th cell
   */
  CartesianHexahedron& cell(const size_t& i)
  {
    return (*__cells)[i];
  }

  /** 
   * Read-only access to the @a i th cell of the mesh
   * 
   * @param i cell number
   * 
   * @return @a i th cell
   */
  const CartesianHexahedron& cell(const size_t& i) const
  {
    return (*__cells)[i];
  }

  /** 
   * Name of the mesh type
   * 
   * @return type name of the mesh
   */
  std::string typeName() const
  {
    return "non conforming octree-mesh";
  }

  /** 
   * Checks if the point @f$ (x,y,z) @f$
   * 
   * @param x @f$ x @f$
   * @param y @f$ y @f$
   * @param z @f$ z @f$
   * 
   * @return true if the point is inside the mesh
   */
  bool inside(const real_t& x, const real_t& y, const real_t& z) const
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot check if a vertex is inside an octree mesh",
		       ErrorHandler::unexpected);
    return false;
  }

  /** 
   * Checks if the point @f$ P @f$
   * 
   * @param p @f$ P @f$
   * 
   * @return true if the point is inside the mesh
   */
  bool inside(const TinyVector<3u, real_t>& p) const
  {
    return this->inside(p[0],p[1],p[2]);
  }

  /** 
   * Creates faces of the mesh
   * 
   */
  void buildFaces()
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }

  /** 
   * Creates edges of the mesh
   * 
   */
  void buildEdges()
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }

  /** 
   * Checks if the mesh has faces
   * 
   * @return true if the mesh has faces
   */
  bool hasFaces() const
  {
    return false;
  }

  /** 
   * Read-only access to the number of cells
   * 
   * @return the number of cells of the mesh
   */
  const size_t& numberOfCells() const;

  size_t cellNumber(const CartesianHexahedron& h) const
  {
    return __cells->number(h);
  }

  const FaceType& face(const size_t& i) const
  {
    static FaceType badFace;
    return badFace;
  }

  size_t faceNumber(const FaceType& f) const
  {
    return std::numeric_limits<size_t>::max();
  }

  //! Read-only access to the number of cells.
  const size_t& numberOfFaces() const
  {
    static size_t badValue;
    return badValue;
  }

  /** 
   * Read only access to the mesh connectivity
   * 
   * @return the connectivity
   */
  const Connectivity<OctreeMesh>& connectivity() const
  {
    return __connectivity;
  }

  /** 
   * Access to the mesh connectivity
   * 
   * @return the connectivity
   */
  Connectivity<OctreeMesh>& connectivity()
  {
    return __connectivity;
  }

  /** 
   * Constructor
   * 
   * @param vertices the set of vertices
   * @param correspondance the vertices corresponcance vector
   * @param hexahedra the list of cells
   */
  OctreeMesh(ReferenceCounting<VerticesSet> vertices,
	     ReferenceCounting<VerticesCorrespondance> correspondance,
	     ReferenceCounting<Vector<CartesianHexahedron> > hexahedra)
    :  Mesh(Mesh::octreeMesh,
	    Mesh::volume,
	    vertices,
	    correspondance),
       __cells(hexahedra),
       __connectivity(*this)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~OctreeMesh()
  {
    ;
  }
};

#endif // OCTREE_MESH_HPP
