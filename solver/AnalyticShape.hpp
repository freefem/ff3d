//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef ANALYTIC_FUNCTION_HPP
#define ANALYTIC_FUNCTION_HPP

#include <Shape.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   AnalyticShape.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 01:19:16 2006
 * 
 * @brief  Shape defined by @f$ \{x \in \mathbf{R}^3 / f(x)>0 \} @f$
 * 
 */
class AnalyticShape
  : public Shape
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __function;			/**< given function */

protected:
  /** 
   * Writes the AnalyticShape to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "one(" << *__function << ")";
    return os;
  }

  /** 
   * Checks if a point is in the AnalyticShape
   * 
   * @param x given point
   * 
   * @return true if @f$ f(x)>0 @f$
   */
  bool __inShape(const TinyVector<3, real_t>& x) const
  {
    const ScalarFunctionBase& f = *__function;
    return (f(x)>0);
  }

  /** 
   * Gets a copy of the AnalyticShape
   * 
   * @return deep copy of the AnalyticShape
   */
  ReferenceCounting<Shape> __getCopy() const;

public:

  /** 
   * Constructor
   * 
   * @param function function defining the shape
   */
  AnalyticShape(ConstReferenceCounting<ScalarFunctionBase> function);

  /** 
   * Copy constructor
   * 
   * @param analyticShape given analyticShape
   */
  AnalyticShape(const AnalyticShape& analyticShape);

  /** 
   * Destructor
   * 
   */
  ~AnalyticShape();
};

#endif // ANALYTIC_FUNCTION_HPP
