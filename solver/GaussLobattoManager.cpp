//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008- St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <GaussLobattoManager.hpp>

void
GaussLobattoManager::
__buildGaussLobatto(const size_t& degree) const
{
  const size_t oldDegrees = __gaussLobatto.size();
  for (size_t i=oldDegrees; i<=degree; ++i) {
    __gaussLobatto.push_back(new GaussLobatto(i));
  }
}

const GaussLobatto&
GaussLobattoManager::
get(const size_t& degree) const
{
  if (degree+1>__gaussLobatto.size()) {
    this->__buildGaussLobatto(degree);
  }

  return *__gaussLobatto[degree];
}

ConstReferenceCounting<GaussLobatto>
GaussLobattoManager::
getReference(const size_t& degree) const
{
  if (degree+1>__gaussLobatto.size()) {
    this->__buildGaussLobatto(degree);
  }
  return __gaussLobatto[degree];
}
