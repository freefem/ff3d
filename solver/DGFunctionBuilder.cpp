//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <DGFunctionBase.hpp>

#include <DGFunctionBuilder.hpp>
#include <FiniteElementTraits.hpp>

#include <Structured3DMesh.hpp>
#include <SpectralMesh.hpp>
#include <MeshOfTetrahedra.hpp>
#include <MeshOfHexahedra.hpp>

#include <MeshOfTriangles.hpp>

#include <DGFunction.hpp>

template <typename MeshType>
void
DGFunctionBuilder::
__build(const ScalarDiscretizationTypeDG& d,
	const MeshType* mesh)
{
  typedef typename MeshType::CellType CellType;

  switch(d.type()) {
  case ScalarDiscretizationTypeBase::DGFEM0: {
    typedef
      FiniteElementTraits<CellType,ScalarDiscretizationTypeBase::lagrangianFEM0>
      FETraits;

    __builtFunction
      = new DGFunction<MeshType, FETraits>(mesh);
    break;
  }
  case ScalarDiscretizationTypeBase::DGFEM1: {
    typedef
      FiniteElementTraits<CellType,ScalarDiscretizationTypeBase::lagrangianFEM1>
      FETraits;

    __builtFunction
      = new DGFunction<MeshType, FETraits>(mesh);
    break;
  }
  case ScalarDiscretizationTypeBase::DGFEM2: {
    typedef
      FiniteElementTraits<CellType,ScalarDiscretizationTypeBase::lagrangianFEM2>
      FETraits;

    __builtFunction
      = new DGFunction<MeshType, FETraits>(mesh);
    break;

  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown discretization type",
		       ErrorHandler::unexpected);
  }
  }
}


void
DGFunctionBuilder::
build(const ScalarDiscretizationTypeDG& d,
      const Mesh* mesh)
{
  switch (mesh->type()) {
  case Mesh::cartesianHexahedraMesh: {
    const Structured3DMesh* m
      = dynamic_cast<const Structured3DMesh*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::hexahedraMesh: {
    const MeshOfHexahedra* m
      = dynamic_cast<const MeshOfHexahedra*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::spectralMesh: {
    const SpectralMesh* m
      = dynamic_cast<const SpectralMesh*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::tetrahedraMesh: {
    const MeshOfTetrahedra* m
      = dynamic_cast<const MeshOfTetrahedra*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::trianglesMesh: {
    const MeshOfTriangles* m
      = dynamic_cast<const MeshOfTriangles*>(mesh);
    this->__build(d, m);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not supported mesh type",
		       ErrorHandler::unexpected);
  }
  }
}

void
DGFunctionBuilder::
build(const ScalarDiscretizationTypeDG& d,
      const Mesh* mesh,
      const ScalarFunctionBase& u)
{
  this->build(d,mesh);
  DGFunctionBase& dgFunctionView = dynamic_cast<DGFunctionBase&>(*__builtFunction);
  dgFunctionView = u;
}


void
DGFunctionBuilder::
build(const ScalarDiscretizationTypeDG& d,
      const Mesh* mesh,
      const Vector<real_t>& values,
      const real_t& outsideValue)
{
  this->build(d,mesh);
  DGFunctionBase& dgFunctionView = dynamic_cast<DGFunctionBase&>(*__builtFunction);
  dgFunctionView = values;
  dgFunctionView.setOutsideValue(outsideValue);
}

ReferenceCounting<DGFunctionBase>
DGFunctionBuilder::
getBuiltDGFunction()
{
  return __builtFunction;
}

ConstReferenceCounting<ScalarFunctionBase>
DGFunctionBuilder::
getBuiltScalarFunction() const
{
  return static_cast<const DGFunctionBase*>(__builtFunction);
}

DGFunctionBuilder::
DGFunctionBuilder()
{
  ;
}

DGFunctionBuilder::
~DGFunctionBuilder()
{
  ;
}
