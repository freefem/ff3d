//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef CONNECTIVITY_HPP
#define CONNECTIVITY_HPP

/**
 * @file   Connectivity.hpp
 * @author Stephane Del Pino
 * @date   Thu Jan 16 18:32:19 2003
 * 
 * @brief  Stores all kind of mesh connectivities
 * 
 * 
 */

#include <ReferenceCounting.hpp>
#include <ErrorHandler.hpp>

#include <Vector.hpp>
#include <Edge.hpp>
#include <TinyVector.hpp>
#include <Stringify.hpp>

#include <set>

template <typename MeshType>
class ConnectivityBuilder;

template<typename MeshType>
class Connectivity
{
public:
  typedef enum {
    CellToCells = (1<< 0),
    CellToFaces = (1<< 1),
    CellToEdges = (1<< 2),
    CellToVertices = (1<< 3),
    FaceToCells = (1<< 4),
    FaceToEdges = (1<< 5),
    FaceToVertices = (1<< 6),
    EdgeToCells = (1<< 7),
    EdgeToFaces = (1<< 8),
    EdgeToVertices = (1<< 9),
    VertexToCell = (1<<10),
    VertexToFaces = (1<<11),
    VertexToEdges = (1<<12),
    VertexToVertices = (1<<13),	/**< connected by edges */
    VertexToVerticesGeneralized = (1<<15) /**< connected by elements */
  } Type;

  friend class ConnectivityBuilder<MeshType>;

private:
  const MeshType& __mesh;

public:
  typedef typename MeshType::CellType CellType;
  typedef typename MeshType::CellType::FaceType FaceType;

  typedef TinyVector<CellType::NumberOfFaces,
		     const CellType*> CellToCellsType;

  typedef TinyVector<CellType::NumberOfFaces,
		     const FaceType*> CellToFacesType;

  typedef TinyVector<CellType::NumberOfEdges,
		     const Edge*> CellToEdgesType;

  // This associates to faces the cells that carry it and the local
  // number of the face in the cell
  typedef TinyVector<2, std::pair<const CellType*,
                                  size_t> > FaceToCellsType;

  typedef std::set<const CellType*> EdgeToCellsType;

  typedef std::set<const CellType*> VertexToCellsType;
  typedef std::set<const Vertex*> VertexToVerticesType;

private:
  ReferenceCounting<Vector<CellToCellsType> >
  __cellToCells;


  ReferenceCounting<Vector<CellToFacesType> > __cellToFaces;

  ReferenceCounting<Vector<CellToEdgesType> >
  __cellToEdges;

  class Undefined {};
  ReferenceCounting<Undefined> __cellToVertices;

  ReferenceCounting<Vector<FaceToCellsType> > __faceToCells;

  ReferenceCounting<Undefined> __faceToEdges;
  ReferenceCounting<Undefined> __faceToVertices;

  ReferenceCounting<EdgeToCellsType> __edgeToCells;

  ReferenceCounting<Undefined> __edgeToFaces;
  ReferenceCounting<Undefined> __edgeToVertices;

  ReferenceCounting<Vector<VertexToCellsType> >
  __vertexToCells;

  ReferenceCounting<Undefined> __vertexToFaces;
  ReferenceCounting<Undefined> __vertexToEdges;
  ReferenceCounting<Undefined> __vertexToVertices;

  ReferenceCounting<Vector<VertexToVerticesType> >
  __vertexToVerticesGeneralized;

  /** 
   * Copy constructor is forbidden
   * 
   * @param c connectivity
   */
  Connectivity(const Connectivity& c);

public:

  void setCellToCells(ReferenceCounting<Vector<CellToCellsType> >& c)
  {
    __cellToCells = c;
  }

  void setCellToEdges(ReferenceCounting<Vector<CellToEdgesType> >& c)
  {
    __cellToEdges = c;
  }

  void setCellToFaces(ReferenceCounting<Vector<CellToFacesType> >& c)
  {
    __cellToFaces = c;
  }

  void setFaceToCells(ReferenceCounting<Vector<FaceToCellsType> >& c)
  {
    __faceToCells = c;
  }

  void setEdgeToCells(ReferenceCounting<Vector<EdgeToCellsType> >& c)
  {
    __edgeToCells = c;
  }

  void setVertexToCells(ReferenceCounting<Vector<VertexToCellsType> >& c)
  {
    __vertexToCells = c;
  }

  void setVertexToVerticesGeneralized(ReferenceCounting<Vector<VertexToVerticesType> >& c)
  {
    __vertexToVerticesGeneralized = c;
  }

  bool hasCellToCells() const
  {
    return __cellToCells != 0;
  }

  bool hasCellToFaces() const
  {
    return __cellToFaces != 0;
  }

  bool hasCellToEdges() const
  {
    return __cellToEdges != 0;
  }

  bool hasCellToVertices() const
  {
    return __cellToVertices != 0;
  }

  bool hasFaceToCells() const
  {
    return __faceToCells != 0;
  }

  bool hasFaceToEdges() const
  {
    return __faceToEdges != 0;
  }

  bool hasFaceToVertices() const
  {
    return __faceToVertices != 0;
  }

  bool hasEdgeToCells() const
  {
    return __edgeToCells != 0;
  }

  bool hasEdgeToFaces() const
  {
    return __edgeToFaces != 0;
  }

  bool hasEdgeToVertices() const
  {
    return __edgeToVertices != 0;
  }

  bool hasVertexToCells() const
  {
    return __vertexToCells != 0;
  }

  bool hasVertexToFaces() const
  {
    return __vertexToFaces != 0;
  }

  bool hasVertexToEdges() const
  {
    return __vertexToEdges != 0;
  }

  bool hasVertexToVerices() const
  {
    return __vertexToVertices != 0;
  }

  bool hasVertexToVericesGeneralized() const
  {
    return __vertexToVerticesGeneralized != 0;
  }

  const CellToCellsType& cells(const CellType& c) const
  {
    return (*__cellToCells)[__mesh.cellNumber(c)];
  }

  CellToCellsType& cells(const CellType& c)
  {
    return (*__cellToCells)[__mesh.cellNumber(c)];
  }

  const VertexToCellsType& cells(const Vertex& v) const
  {
    return (*__vertexToCells)[__mesh.vertexNumber(v)];
  }

  VertexToCellsType& cells(const Vertex& v)
  {
    return (*__vertexToCells)[__mesh.vertexNumber(v)];
  }

  const FaceToCellsType& cells(const FaceType& f) const
  {
    return (*__faceToCells)[__mesh.faceNumber(f)];
  }

  FaceToCellsType& cells(const FaceType& f)
  {
    return (*__faceToCells)[__mesh.faceNumber(f)];
  }

  const CellToFacesType& faces(const CellType& c) const
  {
    return (*__cellToFaces)[__mesh.cellNumber(c)];
  }

  CellToFacesType& faces(const CellType& c)
  {
    return (*__cellToFaces)[__mesh.cellNumber(c)];
  }

  const CellToEdgesType& edges(const CellType& c) const
  {
    return (*__cellToEdges)[__mesh.cellNumber(c)];
  }

  CellToEdgesType& edges(const CellType& c)
  {
    return (*__cellToEdges)[__mesh.cellNumber(c)];
  }

  const VertexToVerticesType& verticesGeneralized(const Vertex& v) const
  {
    return (*__vertexToVerticesGeneralized)[__mesh.cellNumber(v)];
  }

  VertexToVerticesType& verticesGeneralized(const Vertex& v)
  {
    return (*__vertexToVerticesGeneralized)[__mesh.cellNumber(v)];
  }

  Connectivity(const MeshType& mesh)
    : __mesh(mesh),
      __cellToCells(0),
      __cellToFaces(0),
      __cellToEdges(0),
      __cellToVertices(0),
      __faceToCells(0),
      __faceToEdges(0),
      __faceToVertices(0),
      __edgeToCells(0),
      __edgeToFaces(0),
      __edgeToVertices(0),
      __vertexToCells(0),
      __vertexToFaces(0),
      __vertexToEdges(0),
      __vertexToVertices(0),
      __vertexToVerticesGeneralized(0)
  {
    ;
  }

  ~Connectivity()
  {
    ;
  }
};

#endif // CONNECTIVITY_HPP
