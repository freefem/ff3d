//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DEGREE_OF_FREEDOM_SET_HPP
#define DEGREE_OF_FREEDOM_SET_HPP

#include <Vector.hpp>

#include <ReferenceCounting.hpp>
#include <ErrorHandler.hpp>

#include <DegreeOfFreedomPositionsSet.hpp>

#include <cstddef>
/*!
  \class DegreeOfFreedomSet

  This class defines degrees of freedom sets in the case where all the
  unknown share the same sets of vertices.

  \author St�phane Del Pino
*/

class DegreeOfFreedomSet
{
public:
  typedef Vector<int> Correspondance;
private:
  ConstReferenceCounting<Correspondance> __correspondance;
  ConstReferenceCounting<DegreeOfFreedomPositionsSet> __dofPositionsSet;

  const size_t __numberOfVariables;

  Vector<size_t> __offset;

public:
  const ScalarDegreeOfFreedomPositionsSet& positionsSet(const size_t& i) const
  {
    return (*__dofPositionsSet)[i];
  }

//   const size_t& numberOfUsedDOFPositions() const
//   {
//     return __numberOfUsedDOFPositions;
//   }

  const size_t& numberOfDOFPositions(const size_t& i) const
  {
    return (*__dofPositionsSet)[i].number();
  }

  const size_t& numberOfVariables() const
  {
    return __numberOfVariables;
  }

  size_t size() const
  {
    return __offset[__numberOfVariables];
  }

  size_t operator()(const size_t& variableNumber,
		    const size_t& scalarDOFNumber) const
  {
    const size_t dofNumber = __offset[variableNumber]+scalarDOFNumber;

    ASSERT((*__correspondance)[dofNumber] != -1);

    return ((*__correspondance)[dofNumber]);
  }

  bool isDOFVertex(const size_t& variableNumber,
		   const size_t& scalarDOFNumber) const
  {
    const size_t dofNumber = __offset[variableNumber]+scalarDOFNumber;
    return ((*__correspondance)[dofNumber] != -1);
  }

  DegreeOfFreedomSet(ConstReferenceCounting<DegreeOfFreedomPositionsSet> dofPositions,
		     ConstReferenceCounting<Correspondance> correspondance)
    : __correspondance(correspondance),
      __dofPositionsSet(dofPositions),
      __numberOfVariables(dofPositions->number()),
      __offset(__numberOfVariables+1)
  {
    __offset[0] = 0;
    const DegreeOfFreedomPositionsSet& dofPositionsSet = *dofPositions;
    for(size_t i=0; i<dofPositionsSet.number(); ++i) {
      __offset[i+1] = __offset[i]+(*dofPositions)[i].number();
    }
  }

  ~DegreeOfFreedomSet()
  {
    ;
  }
};

#endif // DEGREE_OF_FREEDOM_SET_HPP

