//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef EDGES_BUILDER_HPP
#define EDGES_BUILDER_HPP

#include <map>
#include <set>
#include <TinyVector.hpp>

/**
 * @file   EdgesBuilder.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct 16 16:34:36 2005
 * 
 * @brief  Builds edges of given meshes.
 * @todo   Should use elements sets and not meshes to build edges sets
 */

template <typename MeshType>
class EdgesBuilder
{
private:
  typedef typename MeshType::CellType CellType;
  typedef TinyVector<Edge::NumberOfVertices, size_t> EdgeVertices;

  std::map<EdgeVertices, EdgeVertices> __edgesIdSet;

  ReferenceCounting<EdgesSet> __edgesSet;
public:

  ReferenceCounting<EdgesSet>
  edgesSet()
  {
    return __edgesSet;
  }

  EdgesBuilder(MeshType& mesh)
  {
    ffout(3) << "- EdgesBuilder: building...\n";
    for (typename MeshType::const_iterator i(mesh); not(i.end()); ++i) {
      const CellType& cell = *i;
      for (size_t j=0; j<CellType::NumberOfEdges; ++j) {
	std::set<size_t> vertices;
	EdgeVertices edge;
	for (size_t k=0; k<Edge::NumberOfVertices ;++k) {
	  edge[k] = mesh.vertexNumber(cell(CellType::edges[j][k]));
	  vertices.insert(edge[k]);
	}
	ASSERT(vertices.size() == Edge::NumberOfVertices);
	EdgeVertices edgeId;
	size_t k=0;
	for (std::set<size_t>::const_iterator iv = vertices.begin();
	     iv != vertices.end(); ++iv,++k) {
	  edgeId[k] = *iv;
	}

	if (__edgesIdSet.find(edgeId) == __edgesIdSet.end()) {
	  __edgesIdSet.insert(__edgesIdSet.end(),
			      std::make_pair(edgeId, edge));
	}
      }
    }

    __edgesSet = new EdgesSet(__edgesIdSet.size());
    EdgesSet& edgesSet = *__edgesSet;
    size_t i=0;
    for (typename std::map<EdgeVertices, EdgeVertices>::const_iterator
	   f = __edgesIdSet.begin(); f != __edgesIdSet.end(); ++f, ++i) {
      TinyVector<Edge::NumberOfVertices, Vertex*> newEdge;

      for (size_t k=0; k<Edge::NumberOfVertices; ++k) {
	const size_t vertexNumber = f->second[k];
	newEdge[k] = &mesh.vertex(vertexNumber);
      }

      edgesSet[i] = Edge(*newEdge[0], *newEdge[1]);
    }
    ffout(3) << "- EdgesBuilder: done\n";
  }

  ~EdgesBuilder()
  {
    ;
  }
};

#endif // EDGES_BUILDER_HPP
