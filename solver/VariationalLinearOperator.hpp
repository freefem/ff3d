//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_LINEAR_OPERATOR_HPP
#define VARIATIONAL_LINEAR_OPERATOR_HPP

#include <VariationalOperator.hpp>

/**
 * @file   VariationalLinearOperator.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  2 22:04:06 2002
 * 
 * @brief  describes linear operators
 * 
 * Linear operators, ie: right hand side of the equation
 */
class VariationalLinearOperator
  : public VariationalOperator
{
public:
  enum Type {
    FV,
    FdxGV,
    FdxV,
    FgradGgradV
  };

private:
  const VariationalLinearOperator::Type
  __type;			/**< linear oerator type */

public:
  /** 
   * Returns the type of the operator
   * 
   * @return __type
   */
  const VariationalLinearOperator::Type& type() const
  {
    return __type;
  }

  /** 
   * "Multiplies" the linear operator by some function
   * 
   * @param u the given function
   * 
   * @return the new VariationalLinearOperator
   */
  virtual ReferenceCounting<VariationalLinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& u) const = 0;

  /** 
   * Constructor
   * 
   * @param t the linear operator type
   * @param testFunctionNumber the test function number
   * @param testFunctionProperty the test function operator property
   */
  VariationalLinearOperator(const VariationalLinearOperator::Type& t,
			    const size_t& testFunctionNumber,
			    const VariationalOperator::Property& testFunctionProperty)
    : VariationalOperator(testFunctionNumber, testFunctionProperty),
      __type(t)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V original VariationalLinearOperator
   */
  VariationalLinearOperator(const VariationalLinearOperator& V)
    : VariationalOperator(V),
      __type(V.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalLinearOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_LINEAR_OPERATOR_HPP
