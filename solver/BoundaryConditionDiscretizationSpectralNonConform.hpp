//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_NON_CONFORM_HPP
#define BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_NON_CONFORM_HPP

#include <Problem.hpp>
#include <DegreeOfFreedomSet.hpp>

#include <BoundaryConditionDiscretization.hpp>
#include <BoundaryConditionSurfaceMeshAssociation.hpp>

#include <ConformTransformation.hpp>
#include <LegendreBasis.hpp>

#include <DiscretizationType.hpp>
#include <ScalarDiscretizationTypeSpectral.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <OctreeMesh.hpp>

class BoundaryConditionDiscretizationSpectralNonConform
  : public BoundaryConditionDiscretization
{
private:
  const DegreeOfFreedomSet& __degreeOfFreedomSet;
  const OctreeMesh& __mesh;
  
  ReferenceCounting<BoundaryConditionSurfaceMeshAssociation>
  __bcMeshAssociation;		/**< The boundary mesh association */

  const DiscretizationType& __discretizationType;
  
  Vector<TinyVector<3,size_t> >  __dofShape;
  
public:
  /** 
   * Associates meshes to boundary conditions
   * 
   */
  void associatesMeshesToBoundaryConditions()
  {
    BoundaryMeshAssociation bma(__problem, __mesh);
    __bcMeshAssociation = new BoundaryConditionSurfaceMeshAssociation(__problem,bma);
  }
  
  void getDiagonal(BaseVector& X) const;
  
  void setMatrix(ReferenceCounting<BaseMatrix> A,
		 ReferenceCounting<BaseVector> b) const;
  
  void setSecondMember(ReferenceCounting<BaseMatrix> A,
		       ReferenceCounting<BaseVector> b) const;

  
  void timesX(const BaseVector& X, BaseVector& Z) const;
  
  void transposedTimesX(const BaseVector& X, BaseVector& Z) const;
  
  
  BoundaryConditionDiscretizationSpectralNonConform(const Problem& problem,
						    const OctreeMesh& mesh,
						    const DegreeOfFreedomSet& dof,
						    const DiscretizationType& discretizationType)
    : BoundaryConditionDiscretization(problem,dof),
      __degreeOfFreedomSet(dof),
      __mesh(mesh),
      __discretizationType(discretizationType),
      __dofShape(this->problem().numberOfUnknown())
  {
    for (size_t i=0; i < this->problem().numberOfUnknown(); i++) {
      const ScalarDiscretizationTypeSpectral& discretization
	= dynamic_cast<const ScalarDiscretizationTypeSpectral&>(discretizationType[i]);
      
      __dofShape[i] = discretization.degrees()+TinyVector<3,size_t>(1,1,1);
    }
  }
  
  ~BoundaryConditionDiscretizationSpectralNonConform()
  {
    ;
  }
};

#endif // BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_NON_CONFORM_HPP
