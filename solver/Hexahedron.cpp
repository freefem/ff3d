//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Hexahedron.hpp>
#include <ConformTransformation.hpp>

Hexahedron::Hexahedron(Vertex& x0,
		       Vertex& x1,
		       Vertex& x2,
		       Vertex& x3,
		       Vertex& x4,
		       Vertex& x5,
		       Vertex& x6,
		       Vertex& x7,
		       const size_t& ref)
  : Cell(Hexahedron::NumberOfVertices, ref)
{
  __vertices[0] = &x0;
  __vertices[1] = &x1;
  __vertices[2] = &x2;
  __vertices[3] = &x3;
  __vertices[4] = &x4;
  __vertices[5] = &x5;
  __vertices[6] = &x6;
  __vertices[7] = &x7;

  ConformTransformationQ1Hexahedron T(*this);
  ConformTransformationQ1HexahedronJacobian J(T);

  __volume = J.jacobianDet();
}


const size_t
Hexahedron::
faces[Hexahedron::NumberOfFaces][Hexahedron::FaceType::NumberOfVertices]
= {{0,1,2,3},{0,4,5,1},{1,5,6,2},{2,6,7,3},{0,3,7,4},{4,7,6,5}};

const size_t
Hexahedron::
edges[Hexahedron::NumberOfEdges][Edge::NumberOfVertices]
= {{0,1},{1,2},{2,3},{3,0},{0,4},{1,5},{2,6},{3,7},{4,5},{5,6},{6,7},{7,4}};
