//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005-2008 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_DISCRETIZATION_TYPE_BASE_HPP
#define SCALAR_DISCRETIZATION_TYPE_BASE_HPP

#include <ErrorHandler.hpp>
#include <string>

/**
 * @file   ScalarDiscretizationTypeBase.hpp
 * @author Stephane Del Pino
 * @date   Mon May 30 23:28:37 2005
 * 
 * @brief This class describes types of discretization for scalar
 * quantities
 * 
 */
class ScalarDiscretizationTypeBase
{
public:
  enum Type {
    undefined       =-1,	/**< not defined */
    functionLike    = 0,	/**< keeps function type if possible */

    // finite element-like discretizations
    lagrangianFEM0  =10,	/**< Lagrangian finite element of degree 0 */
    lagrangianFEM1  =11,	/**< Lagrangian finite element of degree 1 */
    lagrangianFEM2  =12,	/**< Lagrangian finite element of degree 2 */

    // finite element-like discretizations
    DGFEM0          =20,	/**< discontinuous finite element of degree 0 */
    DGFEM1          =21,	/**< discontinuous finite element of degree 1 */
    DGFEM2          =22,	/**< discontinuous finite element of degree 2 */

    // Spectral-like discretizations
    spectralLegendre=50,	/**< Lagrange spectral method */
    spectralLagrange=51		/**< Lagrange spectral method */
  };

private:
  Type __type;		/**< Type of discretization */

  /** 
   * This function prevents to instanciate this class
   * 
   */
  virtual void __instanciable() const = 0; 
public:
  static Type getDefault(const Type& type)
  {
    return (type==undefined)?lagrangianFEM1:type;
  }

  const ScalarDiscretizationTypeBase&
  operator=(const ScalarDiscretizationTypeBase::Type& type)
  {
    __type=type;
    return *this;
  }

  /** 
   * convertes type in its name
   * 
   * @param type type of discretization
   * 
   * @return name of the type 
   */
  static std::string name(const ScalarDiscretizationTypeBase::Type& type)
  {
    switch (type) {
    case lagrangianFEM0:  return "FEM-P0";
    case lagrangianFEM1:  return "FEM-P1";
    case lagrangianFEM2:  return "FEM-P2";
    case DGFEM0:          return "DG-P0";
    case DGFEM1:          return "DG-P1";
    case DGFEM2:          return "DG-P2";
    case spectralLegendre:return "spectral Legendre";
    case spectralLagrange:return "spectral Lagrange";
    case undefined:       return "undefined";
    case functionLike:    return "function-like";
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown Discretization type",
		       ErrorHandler::unexpected);
  }

  /** 
   * convertes type in its name
   * 
   * @param discretization type of discretization
   * 
   * @return name of the type 
   */
  static std::string name(const ScalarDiscretizationTypeBase& discretization)
  {
    return ScalarDiscretizationTypeBase::name(discretization.type());
  }

  /** 
   * Read-only access to the type
   * 
   * @return __type
   */
  const Type& type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param type type of discretization
   */
  explicit ScalarDiscretizationTypeBase(const ScalarDiscretizationTypeBase::Type& type)
    : __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d originale discretization type
   */
  explicit ScalarDiscretizationTypeBase(const ScalarDiscretizationTypeBase& d)
    : __type(d.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~ScalarDiscretizationTypeBase()
  {
    ;
  }
};

#endif // SCALAR_DISCRETIZATION_TYPE_BASE_HPP
