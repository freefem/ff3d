//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ScalarFunctionObjectCharacteristic.hpp>
#include <Scene.hpp>
#include <Object.hpp>

real_t
ScalarFunctionObjectCharacteristic::
operator()(const TinyVector<3, real_t>& X) const
{
  for (std::list<ConstReferenceCounting<Object> >::const_iterator i = __objects.begin();
       i != __objects.end(); ++i) {
    if ((*i)->inside(X)) {
      return 1;
    }
  }
  return 0;
}

ScalarFunctionObjectCharacteristic::
ScalarFunctionObjectCharacteristic(ConstReferenceCounting<Scene> scene,
				   const TinyVector<3, real_t>& ref)
  : ScalarFunctionBase(ScalarFunctionBase::objectCharacteristic),
    __reference(ref)
{
  for (size_t i =0; i<scene->nbObjects() ; ++i) {
    if (scene->object(i)->reference() == ref) {
      __objects.push_back(scene->object(i));
    }
  }

  if (__objects.size() == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: no object in the Scene has reference "
		       +stringify(ref),
		       ErrorHandler::normal);
  }
}

ScalarFunctionObjectCharacteristic::
ScalarFunctionObjectCharacteristic(const ScalarFunctionObjectCharacteristic& f)
  : ScalarFunctionBase(f),
    __reference(f.__reference),
    __objects(f.__objects)
{
  ;
}

ScalarFunctionObjectCharacteristic::
~ScalarFunctionObjectCharacteristic()
{
  ;
}
