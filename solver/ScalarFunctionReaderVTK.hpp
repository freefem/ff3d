//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_READER_VTK_HPP
#define SCALAR_FUNCTION_READER_VTK_HPP

#include <ScalarFunctionReaderBase.hpp>

/**
 * @file   ScalarFunctionReaderVTK.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 11:32:14 2006
 * 
 * @brief  VTK file format reader
 */
class ScalarFunctionReaderVTK
  : public ScalarFunctionReaderBase
{
public:
  /** 
   * Read the function in a file
   * 
   * @return the read function
   */
  ConstReferenceCounting<ScalarFunctionBase>
  getFunction() const;

  /** 
   * Constructor
   * 
   * @param filename file name
   * @param mesh given mesh
   * @param functionName function name
   * @param componentNumber component number
   */
  ScalarFunctionReaderVTK(const std::string& filename,
			  ConstReferenceCounting<Mesh> mesh,
			  const std::string& functionName,
			  const int& componentNumber);

  /** 
   * Copy cnstructor
   * 
   * @param f given function
   */
  ScalarFunctionReaderVTK(const ScalarFunctionReaderVTK& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionReaderVTK();
};

#endif // SCALAR_FUNCTION_READER_VTK_HPP
