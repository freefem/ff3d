//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <cmath>
#include <LegendreBasis.hpp>
#include <Interval.hpp>
#include <GaussLobatto.hpp>

void GaussLobatto::
__bisection(const Interval& interval,
	    real_t& root)
{
  LegendreBasis Ln(__degree);
  const real_t epsilon = 1e-10;

  real_t a = interval.a();
  real_t b = interval.b();

  do {
    root= 0.5*(a+b);
    
    if (Ln.getDerivativeValue(root,__degree)== 0) return;
    if (Ln.getDerivativeValue(root,__degree)*Ln.getDerivativeValue(a,__degree) <0) {
      b=root;
    } else {
      a=root;
    }
  } while (std:: abs(b-a) > epsilon );
}

void GaussLobatto::
__newton(const Interval& interval,
	 real_t& root)
{
  LegendreBasis Ln(__degree);

  const real_t epsilon = 1e-15;
  real_t a = interval.a();
  real_t b = interval.b();
  root= 0.5*(a+b);
  real_t xn= root;
  real_t delta = 0;
  do {
    xn = root;
    if (Ln.getDerivativeValue(xn,__degree)== 0) return;
    delta = (Ln.getDerivativeValue(xn,__degree) / Ln.getSecondDerivativeValue(xn,__degree));
    root -= delta;
  } while (std:: abs(delta) > epsilon );
}

GaussLobatto::
GaussLobatto(const size_t& degree)
  : __degree(degree),
    __vertices(degree+1),
    __weights(degree+1)
{
  if (degree==0) {
    __vertices[0] = 0.;     
    __weights[0] = 2;
  } else {
    __vertices[0] = -1; 
    __vertices[degree] = 1;
  
    if (__degree!=1) {
      if (__degree==2) {
	Interval intervalRef(-1.,1.);
      
      // Use Bisection method
      //__bisection(intervalRef,__vertices[1]);
      
      // Use Newton method
	__newton(intervalRef,__vertices[1]);
    
      } else {
	Interval intervalRef(-1.,1.);
	GaussLobatto gaussN_1(__degree-1);   

	for (size_t i=1; i<__degree; ++i) {
	  Interval interval( gaussN_1(i-1),gaussN_1(i));	

	  // Use Bisection method
	  //__bisection(interval,__vertices[i]);
        
	  // Use Newton method
	  __newton(interval,__vertices[i]);
	}
      }
    }

    const real_t w0= 2./(__degree*(__degree+1));
    __weights[0]= w0;
    __weights[__degree]=w0;

    LegendreBasis Ln(__degree);
    for (size_t i=1;i<__degree; ++i) {
      __weights[i]= w0 / (Ln.getValue(__vertices[i],__degree) * Ln.getValue(__vertices[i],__degree));
    }
  }
}
