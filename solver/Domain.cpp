//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003 Pascal Hav�
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#include <Domain.hpp>

#include <Scene.hpp>

#include <Union.hpp>
#include <Intersection.hpp>
#include <Difference.hpp>
#include <Not.hpp>

Domain::
Domain(ConstReferenceCounting<Scene> scene)
  : __isR3(true),
    __objects(0),
    __scene(scene)
{
  ;
}

Domain::
Domain(const Domain& D)
  : __isR3(D.__isR3),
    __objects(D.__objects),
    __scene(D.__scene)
{
  ;
}

Domain::
~Domain()
{
  ;
}

ConstReferenceCounting<Scene> Domain::
scene() const
{
  return __scene;
}

void Domain::__buildReferenceAssociation(const Object& o)
{
  if (o.hasReference()) {
    const TinyVector<3, real_t>& ref = o.reference();
    if (__povToReference.find(ref) == __povToReference.end()) {
      const size_t n = __povToReference.size() + 1;
      __povToReference[ref] = n;
      ffout(2) << "\t\t" << ref << " -> " << n << '\n';
    }
  }

  const Shape& shape = (*o.shape());

  switch(shape.type()) {
  case Shape::union_: {
    const Union& U = static_cast<const Union&>(shape);

    for (Union::const_iterator i = U.begin();
	 i != U.end(); ++i) {
      __buildReferenceAssociation(*(*i));
    }
    break;
  }
  case Shape::difference: {
    const Difference& D = static_cast<const Difference&>(shape);

    for (Difference::const_iterator i = D.begin();
	 i != D.end(); ++i) {
      __buildReferenceAssociation(*(*i));
    }
    break;
  }
  case Shape::intersection: {
    const Intersection& I = static_cast<const Intersection&>(shape);

    for (Intersection::const_iterator i = I.begin();
	 i != I.end(); ++i) {
      __buildReferenceAssociation(*(*i));
    }
    break;
  }
  case Shape::not_: {
    const Object& notObject = *(static_cast<const Not&>(shape).object());
    __buildReferenceAssociation(notObject);
    break;
  }
  default: {
    ;
  }
  }
}
