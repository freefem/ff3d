//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_PERIODIZER_HPP
#define MESH_PERIODIZER_HPP

#include <MeshGenerator.hpp>
#include <list>
#include <utility>

/**
 * @file   MeshPeriodizer.hpp
 * @author Stephane Del Pino
 * @date   Sat Jan  8 17:46:03 2005
 * 
 * @brief  builts a periodic mesh out from a given mesh
 * @note   works only for cartesian mesh by now
 */

class MeshPeriodizer
  : public MeshGenerator
{
public:
  typedef std::list<std::pair<size_t,size_t> > ReferencesMapping;

protected:
  ConstReferenceCounting<Mesh> __input; /**< given mesh */

  const ReferencesMapping __referencesMapping; /**< the periodic description */

  /** 
   * This template function builds the mesh for a given type of input
   * mesh
   * 
   * @param builtLocalizationTools tells to build or not the localization tools
   */
  template <typename MeshType>
  void __build(const bool& builtLocalizationTools);

private:
  /** 
   * Forbiden copy constructor
   * 
   * @param M a given MeshPeriodizer
   * 
   */
  MeshPeriodizer(const MeshPeriodizer& M);

public:
  
  /** 
   * Runs the tetrahedral mesh generation
   * 
   * @param builtLocalizationTools tells to build or not the localization tools
   * 
   */
  virtual void run(const bool& builtLocalizationTools = true);

  /** 
   * Constructor
   * 
   * @param inputMesh the mesh to tetrahedrize
   * @param referencesMapping periodic references mapping
   * 
   */
  MeshPeriodizer(ConstReferenceCounting<Mesh> inputMesh,
		 const ReferencesMapping& referencesMapping)
    : MeshGenerator(),
      __input(inputMesh),
      __referencesMapping(referencesMapping)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~MeshPeriodizer()
  {
    ;
  }
};

#endif // MESH_PERIODIZER_HPP
