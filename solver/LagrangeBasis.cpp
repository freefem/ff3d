//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <LagrangeBasis.hpp>
#include <ErrorHandler.hpp>

#include <GaussLobattoManager.hpp>

LagrangeBasis::
LagrangeBasis(const LagrangeBasis& lagrangeBasis)
  : __degree           (lagrangeBasis.__degree),
    __nodes            (lagrangeBasis.__nodes),
    __denominatorValues(lagrangeBasis.__denominatorValues)
{
  ;
}

LagrangeBasis::
LagrangeBasis(const Vector<real_t>& nodes)
  : __degree           (nodes.size()-1),
    __nodes            (nodes),
    __denominatorValues(nodes.size()) 
{
  __denominatorValues = 1.;
  for(size_t i=0; i< __nodes.size(); ++i) {
    for(size_t j=0; j<i; ++j) {
      if (i!=j) {
	const real_t xi_xj = __nodes[i]-__nodes[j];
	__denominatorValues[i] *=  xi_xj;
	__denominatorValues[j] *= -xi_xj;
      }
    }
  }
  for(size_t i=0; i< __nodes.size(); ++i) {
    __denominatorValues[i] = 1./ __denominatorValues[i]; 
  }
}

LagrangeBasis::
LagrangeBasis(const size_t& degree)
  : __degree           (degree),
    __nodes            (degree+1),
    __denominatorValues(degree+1) 
{
  const GaussLobatto& gauss_lobatto = GaussLobattoManager::instance().get(degree);
  for (size_t i=0; i<__nodes.size(); ++i) {
    __nodes[i] = gauss_lobatto(i);
  }

  __denominatorValues = 1.;
  for(size_t i=0; i< __nodes.size(); ++i) {
    for(size_t j=0; j<i; ++j) {
      if (i!=j) {
	const real_t xi_xj = __nodes[i]-__nodes[j];
	__denominatorValues[i] *=  xi_xj;
	__denominatorValues[j] *= -xi_xj;
      }
    }
  }
  for(size_t i=0; i< __nodes.size(); ++i) {
    __denominatorValues[i] = 1./ __denominatorValues[i]; 
  }
}

LagrangeBasis::
~LagrangeBasis()
{
  ;
}

void LagrangeBasis::
getValues(const real_t& x,
	  Vector<real_t>& values) const
{
  values = __denominatorValues;
  if (__nodes.size()== 0) return ;

  for (size_t i=0; i<values.size(); ++i) {
    const real_t x_xi=x-__nodes[i];
    for (size_t j=0; j<values.size(); ++j) {
      if (j!=i) {
	values[j] *= x_xi;
      }
    }
  }
}

void  LagrangeBasis::
getDerivativeValues(const real_t& x,
		    Vector<real_t>& derivateValues) const
{
  derivateValues.resize(__denominatorValues.size());
  derivateValues=0;

  for (size_t i=0; i<__denominatorValues.size(); ++i) {
    for (size_t k=0; k<__denominatorValues.size(); ++k) {
      if (k==i) continue;
      real_t local_value = __denominatorValues[i];
      for (size_t j=0; j<__nodes.size(); ++j) {
	if ((j!=i) and (k!=j)) {
	  local_value *= x-__nodes[j];
	}
      }
      derivateValues[i] += local_value;
    }
  }
}

real_t LagrangeBasis::
getValue(const real_t& x, 
	 const size_t& i) const
{
  real_t value = __denominatorValues[i];

  for (size_t j=0; j<__nodes.size(); ++j) {
    if (j!=i) {
      const real_t x_xj = x-__nodes[j];
      value *= x_xj;
    }
  }
  
  return value;
}

real_t LagrangeBasis::
getDerivativeValue(const real_t& x, 
		   const size_t& i) const
{
  real_t value=0;
  
  for (size_t k=0; k<__denominatorValues.size(); ++k) {
    if (k==i) continue;
    real_t local_value = __denominatorValues[i];
    for (size_t j=0; j<__nodes.size(); ++j) {
      if ((j!=i) and (k!=j)) {
	local_value *= x-__nodes[j];
      }
    }
    value += local_value;
  }

  return value;
}
