//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001-2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DEGREE_OF_FREEDOM_POSITIONS_SET_HPP
#define DEGREE_OF_FREEDOM_POSITIONS_SET_HPP

#include <ScalarDegreeOfFreedomPositionsSet.hpp>
#include <vector>

/**
 * @file   DegreeOfFreedomPositionsSet.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul 24 01:07:48 2007
 * 
 * @brief Encapsulates degrees of freedom facilities for vectorial
 * problems
 * 
 */
class DegreeOfFreedomPositionsSet
{
private:
  std::vector<ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet> >
  __positionsSet;			/**< position list */

public:
  /** 
   * Adds a position set to the list
   * 
   * @param dofPositionSet the position set to add
   */
  void add(ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet> dofPositionSet )
  {
    __positionsSet.push_back(dofPositionSet);
  }

  /** 
   * Read-only access to the @a i th variable's dof positions set
   * 
   * @param i variable number
   * 
   * @return @a i th variable's dof positions set
   */
  const ScalarDegreeOfFreedomPositionsSet& operator[](const size_t& i) const
  {
    ASSERT(i<__positionsSet.size());
    return *__positionsSet[i];
  }

  /** 
   * Read-only access to the total number of degrees of freedom
   * 
   * @return the total number of degrees of freedom
   */
  size_t number() const
  {
    return __positionsSet.size();
  }

  /** 
   * Constructor
   * 
   */
  DegreeOfFreedomPositionsSet()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~DegreeOfFreedomPositionsSet();
};

#endif // DEGREE_OF_FREEDOM_POSITIONS_SET_HPP
