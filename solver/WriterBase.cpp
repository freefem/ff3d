//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <WriterBase.hpp>

#include <Structured3DMesh.hpp>

#include <ScalarFunctionBase.hpp>
#include <FEMFunctionBase.hpp>
#include <FieldOfScalarFunction.hpp>

#include <Mesh.hpp>


void WriterBase::
add(ConstReferenceCounting<ScalarFunctionBase> function)
{
  __scalarFunctionList.push_back(function);
}

void WriterBase::
add(ConstReferenceCounting<FieldOfScalarFunction> field)
{
  __fieldList.push_back(field);
}


WriterBase::
WriterBase(ConstReferenceCounting<Mesh> mesh,
	   const std::string& filename,
	   const FileDescriptor& fileDescriptor)
  : __mesh(mesh),
    __filename(filename),
    __fileDescriptor(fileDescriptor),
    __CR(fileDescriptor.cr())
{
  ;
}

WriterBase::
~WriterBase()
{
  ;
}
