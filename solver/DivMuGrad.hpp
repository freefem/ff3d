//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// Implement the - div(��grad(�)) operator, in the domain.
// using finit differences on a Structured3DMesh

#ifndef DIV_MU_GRAD_HPP
#define DIV_MU_GRAD_HPP

#include <PDEOperator.hpp>
#include <ScalarFunctionBase.hpp>
#include <ScalarFunctionBuilder.hpp>

/**
 * @file   DivMuGrad.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 16:10:23 2006
 * 
 * @brief This class describes second order partial differencial
 * operators of the form: \f$ \nabla\cdot \mu \nabla \f$
 * 
 * This class describes second order partial differencial operators of
 * the form: \f$ \nabla\cdot \mu \nabla \f$ where
 *
 * \f$
 * \begin{array}{ll}
 * \mu: &\Omega \to R   \\
 *      &x \mapsto \mu(x)
 * \end{array}
 * \f$.
 *
 * \par example: if \f$ \mu = 1 \f$, the operator is the Laplacian.
 * 
 * 
 */
class DivMuGrad
  : public PDEOperator
{
  ConstReferenceCounting<ScalarFunctionBase>
  __mu;				/**< @f$ \mu @f$ */

public:
  /** 
   * Access to the ith coefficient of the operator
   * 
   * @param i the coefficient number
   * 
   * @return @f$ \mu @f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  coefficient(const size_t& i)
  {
    ASSERT (i<1);
    return __mu;
  }

  /** 
   * Gets the name of the operator
   * 
   * @return "DivMuGrad"
   */
  std::string typeName() const
  {
    return "DivMuGrad";
  }

  /** 
   * read-only access to the coefficient
   * 
   * @return @f$ \mu @f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  mu() const
  {
    return __mu;
  }

  /** 
   * "multiplies" the operator by a coefficient @a c
   * 
   * @param c the coefficient
   * 
   * @return @f$ \nabla\cdot c\mu \nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__mu);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);
    return new DivMuGrad(functionBuilder.getBuiltFunction());
  }

  /** 
   * Returns the opposite operator
   * 
   * @return @f$ -\nabla\cdot\mu\nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator-() const
  {
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__mu);
    functionBuilder.setUnaryMinus();    
    return new DivMuGrad(functionBuilder.getBuiltFunction());
  }

  /** 
   * Constructor
   * 
   * @param mu the coefficient
   */
  DivMuGrad(ConstReferenceCounting<ScalarFunctionBase> mu)
    : PDEOperator(PDEOperator::divmugrad,
		  1),
      __mu(mu)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param D given DivMuGrad
   */
  DivMuGrad(const DivMuGrad& D)
    : PDEOperator(D),
      __mu(D.__mu)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~DivMuGrad()
  {
    ;
  }
};

#endif // DIV_MU_GRAD_HPP
