//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Mesh.hpp>
#include <ScalarFunctionReaderBase.hpp>

ScalarFunctionReaderBase::
ScalarFunctionReaderBase(const std::string& filename,
			 ConstReferenceCounting<Mesh> mesh,
			 const std::string& functionName,
			 const size_t& componentNumber)
  : __filename(filename),
    __mesh(mesh),
    __functionName(functionName),
    __componentNumber(componentNumber)
{
  ;
}

ScalarFunctionReaderBase::
ScalarFunctionReaderBase(const ScalarFunctionReaderBase& f)
  : __filename(f.__filename),
    __mesh(f.__mesh),
    __functionName(f.__functionName),
    __componentNumber(f.__componentNumber)
{
  ;
}

ScalarFunctionReaderBase::
~ScalarFunctionReaderBase()
{
  ;
}
