//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef EDGES_SET_HPP
#define EDGES_SET_HPP

#include <Edge.hpp>
#include <Vector.hpp>

/**
 * @file   EdgesSet.hpp
 * @author Stephane Del Pino
 * @date   Fri Jun 11 19:02:20 2004
 * 
 * @brief  Manages edges set
 * 
 * This class main purpose is to provide a unique interface to all
 * edges sets, particularly useful for meshes management.
 */

class EdgesSet
{
private:
    Vector<Edge> __edges;
public:
  /** 
   * Returns the number of the edge \a e in the list
   * 
   * @param e an edge
   * 
   * @return the edge number
   */
  inline size_t number(const Edge& e) const
  {
    return __edges.number(e);
  }

  /**
   * Read-only access to the number of edges.
   * 
   * @return the number of edges of the set
   */
  inline const size_t& numberOfEdges() const
  {
    return __edges.size();
  }

  /** 
   * Change the size of the edge container.
   * 
   * @param size the new size of the edge set
   *
   * @note all data are lost!
   */
  inline void setNumberOfEdges(const size_t& size)
  {
    __edges.resize(size);
  }

  /** 
   * Access to an edge according to its number
   * 
   * @param i the edge number
   * 
   * @return the ith edge
   */
  inline const Edge& operator[](const size_t& i) const
  {
    return __edges[i];/// bounds are checked by the Vector class
  }

  /** 
   * Read-only access to an edge according to its number
   * 
   * @param i the edge number
   * 
   * @return the ith edge
   */
  inline Edge& operator[](const size_t& i)
  {
    return __edges[i];/// bounds are checked by the Vector class
  }

  /** 
   * Constructs an EdgesSet to a given size \a s
   * 
   * @param s the given size
   */
  EdgesSet(const size_t& s)
    : __edges(s)
  {
    ;
  }

  /** 
   * Copies an EdgesSet
   * 
   * @param E the given EdgesSet
   */
  EdgesSet(const EdgesSet& E)
    : __edges(E.__edges)
  {
    ;
  }

  /** 
   * Destructs an EdgesSet
   * 
   */
  ~EdgesSet()
  {
    ;
  }
};

#endif // EDGES_SET_HPP
