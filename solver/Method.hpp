//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef METHOD_HPP
#define METHOD_HPP

#include <Problem.hpp>
#include <Solution.hpp>

#include <ReferenceCounting.hpp>

#include <DiscretizationType.hpp>

/*!
  \class Method

  This is the base class to define methods.

  \author St�phane Del Pino
*/

class Method
{
protected:
  DiscretizationType __discretizationType;

public:
  virtual void Discretize (ConstReferenceCounting<Problem> problem) = 0;
  virtual void Compute (Solution& u) = 0;

  Method(const DiscretizationType& discretizationType)
    : __discretizationType(discretizationType)
  {
    ;
  }

  virtual ~Method()
  {
    ;
  }
};

#endif // METHOD_HPP
