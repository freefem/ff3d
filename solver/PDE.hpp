//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// This class describes a PDE

#ifndef PDE_HPP
#define PDE_HPP

#include <VectorialPDEOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   PDE.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 19:32:23 2006
 * 
 * @brief  Partial differential equation
 * 
 */
class PDE
{
private:
  ConstReferenceCounting<VectorialPDEOperator>
  __vectorPDEOperator;		/**< PDE operators applied  */

  ConstReferenceCounting<ScalarFunctionBase>
  __secondMember;		/**< second member */

  /** 
   * Consructor
   * 
   */
  PDE()
  {
    ;
  }
public:
  /** 
   * "multiplies" a PDE by a coefficient @a c
   * 
   * @param c the coefficient
   * 
   * @return the new PDE
   */
  ReferenceCounting<PDE>
  operator * (const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__secondMember);
    functionBuilder.setBinaryOperation(BinaryOperation::product,
				       c);

    PDE* newPDE = new PDE();
    (*newPDE).__secondMember = functionBuilder.getBuiltFunction();

    (*newPDE).__vectorPDEOperator = (*__vectorPDEOperator) * c;
    return newPDE;
  }

  /** 
   * Read only access to the second member
   * 
   * 
   * @return 
   */
  ConstReferenceCounting<ScalarFunctionBase>
  secondMember() const
  {
    return __secondMember;
  }

  /** 
   * Read-only access to the operators applied to the ith variable of
   * the PDE
   * 
   * @param i the unknown number
   * 
   * @return an operator sum
   */
  ConstReferenceCounting<PDEOperatorSum>
  operator [] (const size_t& i) const
  {
    ASSERT (i<(*__vectorPDEOperator).size());
    return (*__vectorPDEOperator)[i];
  }

  /** 
   * Affects a PDE
   * 
   * @param pde original PDE
   * 
   * @return the copied PDE
   */
  const PDE& operator = (const PDE& pde)
  {
    __vectorPDEOperator=pde.__vectorPDEOperator;
    __secondMember = pde.__secondMember;
    return *this;
  }

  /** 
   * Output of a pde
   * 
   * @param os output stream
   * @param pde the pde to write
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os,
	       const PDE& pde)
  {
    os << *(pde.__vectorPDEOperator) << " = " << "pde.secondMember" << '\n';
    return os;
  }

  /** 
   * Constructor
   * 
   * @param A vectorial PDE operator
   * @param f second member
   */
  PDE(ConstReferenceCounting<VectorialPDEOperator> A,
      ConstReferenceCounting<ScalarFunctionBase> f)
    : __vectorPDEOperator(A),
      __secondMember(f)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param pde 
   */
  PDE(const PDE& pde)
    : __vectorPDEOperator(pde.__vectorPDEOperator),
      __secondMember(pde.__secondMember)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~PDE()
  {
    ;
  }
};

#endif // PDE_HPP
