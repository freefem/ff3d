//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003 Pascal Hav�
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#ifndef WORKING_MESH_HPP
#define WORKING_MESH_HPP

#include <ReferenceCounting.hpp>

#include <Vertex.hpp>
#include <Triangle.hpp>

/**
 * @file   WorkingMesh.hpp
 * @author Stephane Del Pino
 * @date   Sat May 15 19:12:02 2004
 * 
 * @brief  This class is dedicated to the construction of meshes.
 * 
 * @todo Make this class more general to deal not only with surface
 * meshes of triangles
 */

class WorkingMesh
{
private:
  typedef std::set<ReferenceCounting<Vertex> > VerticesList;
  typedef std::set<ReferenceCounting<Triangle> > ElementsList;

  VerticesList __vertices;	/**< list of vertices */
  ElementsList __elements;	/**< list of elements */

  /** 
   * Copy constructor is forbiden
   * 
   * @param wm 
   */
  WorkingMesh(const WorkingMesh& wm);

  /** 
   * This affectation is forbiden
   * 
   * @param wo a given working mesh
   * 
   * @return a const reference to the current object
   */
  const WorkingMesh& operator=(const WorkingMesh& wo);

public:
  /** 
   * inserts a vertex in the list
   * 
   * @param v the vertex to be inserted
   */
  void insert(Vertex* v)
  {
    __vertices.insert(v);
  }

  /** 
   * inserts a triangle in the list
   * 
   * @param t the triangle to add
   */
  void insert(Triangle* t)
  {
    __elements.insert(t);
  }

  /** 
   * Default constructor
   * 
   */
  WorkingMesh()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~WorkingMesh()
  {
    ;
  }
};

#endif // WORKING_MESH_HPP
