//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef CONNECTIVITY_BUILDER_HPP
#define CONNECTIVITY_BUILDER_HPP

#include <set>
#include <limits>
#include <list>
#include <map>

#include <TinyVector.hpp>
#include <Connectivity.hpp>
#include <ReferenceCounting.hpp>

#include <Mesh.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

template <unsigned meshFamily>
struct BorderLinker;

/**
 * @file   ConnectivityBuilder.hpp
 * @author Stephane Del Pino
 * @date   Thu Aug 12 15:16:35 2004
 * 
 * @brief This class is dedicated to the construction of any kind of
 * connectivities
 * 
 * @note It is far from being finished. Some connectivity type may
 * never be implemented.
 *
 * @todo Many optimizations could be performed, but is it necessary?
 */
template <typename MeshType>
class ConnectivityBuilder
{
private:
  MeshType& __mesh;

  bool __buildCellToCells;
  bool __buildCellToFaces;
  bool __buildCellToEdges;
  bool __buildCellToVertices;
  bool __buildFaceToCells;
  bool __buildFaceToEdges;
  bool __buildFaceToVertices;
  bool __buildEdgeToCells;
  bool __buildEdgeToFaces;
  bool __buildEdgeToVertices;
  bool __buildVertexToCell;
  bool __buildVertexToFaces;
  bool __buildVertexToEdges;
  bool __buildVertexToVertices;	/**< Connected by Edges */
  bool __buildVertexToVerticesGeneralized; /**< Connected by elements */

  friend
  struct BorderLinker<MeshType::family>;

  typedef typename MeshType::CellType CellType;
  typedef typename CellType::FaceType FaceType;

  void __getOptions(const size_t& buildType)
  {
    if ((buildType>>15)!=0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown connectivity type",
			 ErrorHandler::unexpected);
    }
    __buildCellToCells
      = (Connectivity<MeshType>::CellToCells & buildType);
    __buildCellToFaces
      = (Connectivity<MeshType>::CellToFaces & buildType);
    __buildCellToEdges
      = (Connectivity<MeshType>::CellToEdges & buildType);
    __buildCellToVertices
      = (Connectivity<MeshType>::CellToVertices & buildType);
    __buildFaceToCells
      = (Connectivity<MeshType>::FaceToCells & buildType);
    __buildFaceToEdges
      = (Connectivity<MeshType>::FaceToEdges & buildType);
    __buildFaceToVertices
      = (Connectivity<MeshType>::FaceToVertices & buildType);
    __buildEdgeToCells
      = (Connectivity<MeshType>::EdgeToCells & buildType);
    __buildEdgeToFaces
      = (Connectivity<MeshType>::EdgeToFaces & buildType);
    __buildEdgeToVertices
      = (Connectivity<MeshType>::EdgeToVertices & buildType);
    __buildVertexToCell
      = (Connectivity<MeshType>::VertexToCell & buildType);
    __buildVertexToFaces
      = (Connectivity<MeshType>::VertexToFaces & buildType);
    __buildVertexToEdges
      = (Connectivity<MeshType>::VertexToEdges & buildType);
    __buildVertexToVertices
      = (Connectivity<MeshType>::VertexToVertices & buildType);
    __buildVertexToVerticesGeneralized
      = (Connectivity<MeshType>::VertexToVerticesGeneralized & buildType);
  }

  /** 
   * Forbidds the copy constructor
   * 
   */
  ConnectivityBuilder(const ConnectivityBuilder<MeshType>&);

  // Vertices numbers have to be stored increasingly (performed at construction by the user).
  typedef TinyVector<FaceType::NumberOfVertices, size_t> FaceVerticesList;
  typedef std::pair<const CellType*, size_t> CellFace;
  typedef std::map<FaceVerticesList,
		   std::list<CellFace> > FaceToCellMapping;

  FaceToCellMapping __faceToCells; // empty means to built

  /** 
   * Builds internal table that associates elements to their faces
   * 
   */
  void __faceToCellsConstruction()
  {
    ffout(4) << " - building face to cells mapping...\n";
    if (not(__faceToCells.empty())) {
      ffout(4) << "already done\n";
      return;
    }

    if (not(__mesh.hasFaces())) {
      __mesh.buildFaces();
    }

    for (typename MeshType::const_iterator i(__mesh); not(i.end()); ++i) {
      for (size_t l=0; l<CellType::NumberOfFaces; ++l) {
	std::set<size_t> verticesSet;
	for (size_t k=0; k<FaceType::NumberOfVertices; ++k) {
	  const size_t vertexNumber
	    = __mesh.vertexNumber((*i)(CellType::faces[l][k]));
	  verticesSet.insert(__mesh.correspondance(vertexNumber));
	}
	FaceVerticesList vertices;
	size_t k=0;
	for (std::set<size_t>::iterator n=verticesSet.begin();
	     n !=  verticesSet.end(); ++n) {
	  vertices[k++]=*n;
	}
	__faceToCells[vertices].push_back(CellFace(i.pointer(),l));
      }
    }
    ffout(4) << "done\n";
    if (__buildFaceToCells) { // storing or not this connectivity
      ffout(3) << "- Storing face to cells connectivity\n";
      if (__mesh.connectivity().hasFaceToCells()) {
	ffout(4) << "already done\n";
	return;
      }

      typedef
	typename Connectivity<MeshType>::FaceToCellsType
	FaceToCellsType;

      ReferenceCounting<Vector<FaceToCellsType> > pFaceToCells
	= new Vector<FaceToCellsType>(__faceToCells.size());
      Vector<FaceToCellsType>& faceToCells = *pFaceToCells;

      size_t faceNumber = 0;
      for (typename FaceToCellMapping::iterator i = __faceToCells.begin();
	   i != __faceToCells.end(); ++i) {
	typename FaceToCellMapping::mapped_type& faceList
	  = (*i).second;

	switch (faceList.size()) {
	case 1: {
	  typename FaceToCellMapping::mapped_type::iterator j
	    = (*i).second.begin();
	  faceToCells[faceNumber][0]=(*j);
	  faceToCells[faceNumber][1]= std::make_pair(static_cast<CellType*>(0),
						     std::numeric_limits<size_t>::max());
	  break;
	}
	case 2: {
	  typename FaceToCellMapping::mapped_type::iterator j1 = (*i).second.begin();
	  typename FaceToCellMapping::mapped_type::iterator j2 = j1;
	  j2++;

	  faceToCells[faceNumber][0] = (*j2);
	  faceToCells[faceNumber][1] = (*j1);
	  break;
	}
	default: {
	  fferr(0) << "\noops: Something strange happened: incorrect mesh:\n";
	  fferr(0) << "        One of the faces of your mesh is related to "
		   << faceList.size() << " cells (vertices: " << (*i).first << ")\n"; ;
#ifndef NDEBUG
	  throw ErrorHandler(__FILE__,__LINE__,
			     "stopping execution since debug mode is activated",
			     ErrorHandler::unexpected);
#endif // NDEBUG
	}
	}
	faceNumber++;
      }
      __mesh.connectivity().setFaceToCells(pFaceToCells);

    }
  }

  /** 
   * Builds the cell to cell connectivity
   * 
   */
  void __cellToCellsConstruction()
  {
    this->__faceToCellsConstruction();
    ffout(3) << "- Storing cell to cells connectivity\n";

    if (__mesh.connectivity().hasCellToCells()) {
      ffout(4) << "already done\n";
      return;
    }

    if (not(__mesh.hasFaces())) {
      __mesh.buildFaces();
    }

    typedef
      typename Connectivity<MeshType>::CellToCellsType
      CellToCellsType;

    ReferenceCounting<Vector<CellToCellsType> > pCellToCells
      = new Vector<CellToCellsType>(__mesh.numberOfCells());
    Vector<CellToCellsType>& cellToCells = *pCellToCells;

    for (typename FaceToCellMapping::iterator i
	   = __faceToCells.begin();
	 i != __faceToCells.end(); ++i) {
      typename FaceToCellMapping::mapped_type& faceList
	= (*i).second;

      switch (faceList.size()) {
      case 1: {
	typename FaceToCellMapping::mapped_type::iterator j
	  = (*i).second.begin();
	// Ensure that the face is 0
	cellToCells(__mesh.cellNumber(*(*j).first))[(*j).second] = 0;
	break;
      }
      case 2: {
	typename FaceToCellMapping::mapped_type::iterator j1 = (*i).second.begin();
	typename FaceToCellMapping::mapped_type::iterator j2 = j1;
	j2++;

	cellToCells(__mesh.cellNumber(*(*j1).first))[(*j1).second] = (*j2).first;
	cellToCells(__mesh.cellNumber(*(*j2).first))[(*j2).second] = (*j1).first;

	break;
      }
      default: {
	fferr(0) << "\noops: Something strange happened: incorrect mesh:\n";
	fferr(0) << "        One of the faces of your mesh is related to "
		 << faceList.size() << " cells (vertices: " << (*i).first << ")\n";
	for(typename FaceToCellMapping::mapped_type::iterator j = faceList.begin();
	    j != faceList.end(); ++j) {
	  const CellType& K = *(*j).first;
	  fferr(0) << "Cell " << (*j).second << ':';
	  for (size_t k=0; k<CellType::NumberOfVertices; ++k) {
	    fferr(0) << ' ' << __mesh.vertexNumber(K(k));
	  }
	  fferr(0) << '\n';
	}
      }
      }
    }
    __mesh.connectivity().setCellToCells(pCellToCells);
  }

  /** 
   * Builds cell to faces connectivity
   * 
   */
  void __cellToFacesConstruction()
  {
    this->__faceToCellsConstruction();
    ffout(3) << "- Storing cell to faces connectivity\n";
    if (__mesh.connectivity().hasCellToFaces()) {
      ffout(3) << "already done\n";
      return;
    }

    if (not(__mesh.hasFaces())) {
      __mesh.buildFaces();
    }

    typedef
      typename Connectivity<MeshType>::CellToFacesType
      CellToFacesType;

    ReferenceCounting<Vector<CellToFacesType> > pCellToFaces
      = new Vector<CellToFacesType>(__mesh.numberOfCells());

    Vector<CellToFacesType>& cellToFaces = *pCellToFaces;
    size_t faceNumber = 0;
    for (typename FaceToCellMapping::iterator i = __faceToCells.begin();
	 i != __faceToCells.end(); ++i) {
      const std::list<CellFace>& cellList = (*i).second;
      for (typename std::list<CellFace>::const_iterator j = cellList.begin();
	   j != (*i).second.end(); ++j) {
	const size_t cellNumber = __mesh.cellNumber(*(*j).first);
	const size_t localFaceNumber = (*j).second;
	cellToFaces[cellNumber][localFaceNumber]
	  = &__mesh.face(faceNumber);
      }
      faceNumber++;
    }

    __mesh.connectivity().setCellToFaces(pCellToFaces);
  }

  /** 
   * Builds cell to edges connectivity
   * 
   */
  void __cellToEdgesConstruction()
  {
    ffout(3) << "- Storing cell to edges connectivity\n";

    if (__mesh.connectivity().hasCellToEdges()) {
      ffout(4) << "already done\n";
      return;
    }

    if (not(__mesh.hasEdges())) {
      __mesh.buildEdges();
    }

    typedef TinyVector<2, size_t> EdgeVerticesList;
    typedef std::pair<const CellType*, size_t> CellEdge;
    typedef std::map<EdgeVerticesList,
                     std::list<CellEdge> > EdgeToCellMapping;

    if (__mesh.numberOfEdges() == 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "edge list not built",
			 ErrorHandler::unexpected);
    }

    EdgeToCellMapping edgeToCells;

    for (typename MeshType::const_iterator icell(__mesh); not(icell.end()); ++icell) {
      for (size_t l=0; l<CellType::NumberOfEdges; ++l) {
	std::set<size_t> verticesSet;
	for (size_t k=0; k<2; ++k) {
	  const size_t vertexNumber
	    = __mesh.vertexNumber((*icell)(CellType::edges[l][k]));
	  verticesSet.insert(__mesh.correspondance(vertexNumber));
	}
	EdgeVerticesList vertices;
	size_t k=0;
	for (std::set<size_t>::iterator n=verticesSet.begin();
	     n !=  verticesSet.end(); ++n) {
	  vertices[k++] = *n;
	}
	edgeToCells[vertices].push_back(CellFace(icell.pointer(),l));
      }
    }

    typedef typename  Connectivity<MeshType>::CellToEdgesType CellToEdgesType;

    ReferenceCounting<Vector<CellToEdgesType> >
      pCellToEdges = new Vector<CellToEdgesType>(__mesh.numberOfCells());
    Vector<CellToEdgesType>& cellToEdges = *pCellToEdges;

    for (size_t i=0; i<__mesh.numberOfEdges(); ++i) {
      const Edge& edge = __mesh.edge(i);
      std::set<size_t> edgeVerticesSet;
      for (size_t k=0; k<2; ++k) {
	const size_t vertexNumber = __mesh.vertexNumber(edge(k));
	edgeVerticesSet.insert(__mesh.correspondance(vertexNumber));
      }

      size_t k=0;
      EdgeVerticesList vertices;

      for (std::set<size_t>::iterator n=edgeVerticesSet.begin();
	   n !=  edgeVerticesSet.end(); ++n) {
	vertices[k++]=*n;
      }
      typename EdgeToCellMapping::iterator ie
	= edgeToCells.find(vertices);
      if (ie == edgeToCells.end()) {
	fferr(0) << "\noops: Something strange happened: incorrect mesh:\n";
	fferr(0) << "        One of the edges of your mesh is related to "
		 << "        no cells (vertices: " << (*ie).first << ")\n";
#ifndef NDEBUG
	throw ErrorHandler(__FILE__,__LINE__,
			   "stopping execution since debug mode is activated",
			   ErrorHandler::unexpected);
#endif // NDEBUG
      }
      for (typename EdgeToCellMapping::mapped_type::iterator
	     j = ie->second.begin();
	   j != ie->second.end(); ++j) {
	const CellType& cell = *j->first;
	const size_t edgeNumber = j->second;
	cellToEdges(__mesh.cellNumber(cell))[edgeNumber] = &edge;
      }
    }
    __mesh.connectivity().setCellToEdges(pCellToEdges);
  }

  /** 
   * builds vertex to cell connectivity
   * 
   */
  void __vertexToCellConstruction()
  {
    ffout(3) << "- Storing Vertex to Cells connectivity\n";

    if (__mesh.connectivity().hasVertexToCells()) {
      ffout(4) << "already done\n";
      return;
    }

    typedef
      typename Connectivity<MeshType>::VertexToCellsType
      VertexToCellsType;

    ReferenceCounting<Vector<VertexToCellsType> > pVertexToCells
      = new Vector<VertexToCellsType>(__mesh.numberOfVertices());
    Vector<VertexToCellsType>& vertexToCells = *pVertexToCells;

    for (typename MeshType::const_iterator i(__mesh); not(i.end()); ++i) {
      const CellType& cell = *i;
      for (size_t n=0; n<CellType::NumberOfVertices; ++n) {
	const size_t vertexNumber = __mesh.vertexNumber(cell(n));
	vertexToCells[__mesh.correspondance(vertexNumber)].insert(&cell);
      }
    }

    __mesh.connectivity().setVertexToCells(pVertexToCells);
  }

  /** 
   * builds generalized vertex to vertices connectivity. Generalized
   * in the sens that they are connected by cells.
   * 
   */
  void __vertexToVerticesGeneralizedConstruction()
  {
    ffout(3) << "- Storing Vertex to Vertices connectivity\n";

    if (__mesh.connectivity().hasVertexToVericesGeneralized()) {
      ffout(4) << "already done\n";
      return;
    }

    typedef
      typename Connectivity<MeshType>::VertexToVerticesType
      VertexToVerticesType;

    ReferenceCounting<Vector<VertexToVerticesType> > pVertexToVertices
      = new Vector<VertexToVerticesType>(__mesh.numberOfVertices());
    Vector<VertexToVerticesType>& vertexToVertices = *pVertexToVertices;

    for (typename MeshType::const_iterator i(__mesh); not(i.end()); ++i) {
      const CellType& cell = *i;
      for (size_t n=0; n<CellType::NumberOfVertices; ++n) {
	for (size_t m=0; m<CellType::NumberOfVertices; ++m) {
	  if (m != n) {
	    vertexToVertices[__mesh.vertexNumber(cell(n))].insert(&cell(m));
	  }
	}
      }
    }

    __mesh.connectivity().setVertexToVerticesGeneralized(pVertexToVertices);
  }

public:

  /** 
   * Builds required connectivities
   * 
   * @param buildType required connectivities
   */
  void generates(const size_t& buildType)
  {
    this->__getOptions(buildType);

    if (__buildCellToCells) {
      this->__cellToCellsConstruction();
    }
    if (__buildCellToFaces) {
      this->__cellToFacesConstruction();
    }
    if (__buildCellToEdges) {
      this->__cellToEdgesConstruction();
    }
    if (__buildCellToVertices) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildFaceToCells) {
      this->__faceToCellsConstruction();
    }
    if (__buildFaceToEdges) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildFaceToVertices) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildEdgeToCells) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildEdgeToFaces) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildEdgeToVertices) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildVertexToCell) {
      this->__vertexToCellConstruction();
    }
    if (__buildVertexToFaces) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildVertexToEdges) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildVertexToVertices) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    if (__buildVertexToVerticesGeneralized) {
      this->__vertexToVerticesGeneralizedConstruction();
    }
  }

  void borderMesh(ReferenceCounting<typename MeshType::BorderMeshType>& border)
  {
    BorderLinker<MeshType::family>(__mesh, *this, border);
  }

  ConnectivityBuilder(const MeshType& m)
    : __mesh(const_cast<MeshType&>(m)), // This allows to built the connectivity "on-demand"
      __buildCellToCells(false),
      __buildCellToFaces(false),
      __buildCellToEdges(false),
      __buildCellToVertices(false),
      __buildFaceToCells(false),
      __buildFaceToEdges(false),
      __buildFaceToVertices(false),
      __buildEdgeToCells(false),
      __buildEdgeToFaces(false),
      __buildEdgeToVertices(false),
      __buildVertexToCell(false),
      __buildVertexToFaces(false),
      __buildVertexToEdges(false)
  {
    ffout(3) << "Starting connectivity builder\n";
  }

  ~ConnectivityBuilder()
  {
    ffout(3) << "Connectivity builder finished\n";
  }
};

template<unsigned meshFamily>
struct BorderLinker
{
  template <typename MeshType>
  BorderLinker(const MeshType& m,
	       const typename ConnectivityBuilder<MeshType>::FaceCorrespondance& faces,
	       ReferenceCounting<typename MeshType::BorderMeshType>)
  {
    ;
  } 
};

template<>
struct BorderLinker<Mesh::volume>
{
  template <typename MeshType>
  BorderLinker(const MeshType& m,
	       ConnectivityBuilder<MeshType>& connectivityBuilder,
	       ReferenceCounting<typename MeshType::BorderMeshType> border)
  {
    if (border != 0) {
      connectivityBuilder.__faceToCellsConstruction();

      ffout(3) << "- Connecting border mesh\n";
      const typename ConnectivityBuilder<MeshType>::FaceToCellMapping&
	faceToCells = connectivityBuilder.__faceToCells;

      for (typename MeshType::BorderMeshType::iterator i(*border);
	   not(i.end()); ++i) {
	std::set<size_t> verticesSet;
	for (size_t k=0; k<ConnectivityBuilder<MeshType>::FaceType::NumberOfVertices; ++k) {
	  const size_t vertexNumber = m.vertexNumber((*i)(k));
	  verticesSet.insert(m.correspondance(vertexNumber));
	}
	size_t k=0; 
	typename ConnectivityBuilder<MeshType>::FaceVerticesList vertices;
	for (std::set<size_t>::iterator n=verticesSet.begin();
	     n !=  verticesSet.end(); ++n) {
	  vertices[k++]=*n;
	}
	typename ConnectivityBuilder<MeshType>::FaceToCellMapping
	  ::const_iterator icell = faceToCells.find(vertices);
	if (icell != faceToCells.end()) {
	  (*i).setMother(icell->second.begin()->first, icell->second.begin()->second);
	} else {
	  const std::string errorMsg
	    = "boundary element number "+stringify(border->cellNumber(*i))
	    +" is not related to any cell of the mesh\n";
	  throw ErrorHandler(__FILE__,__LINE__,
			     errorMsg,
			     ErrorHandler::normal); // error might come from external mesher

	}
      }
    }
  }
};


#endif // CONNECTIVITY_BUILDER_HPP
