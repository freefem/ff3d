//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001-2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SOLVER_INFORMATION_CENTER_HPP
#define SOLVER_INFORMATION_CENTER_HPP

#include <ThreadStaticBase.hpp>
#include <Mesh.hpp>
#include <DiscretizationType.hpp>

#include <Assert.hpp>

#include <stack>

/**
 * @file   SolverInformationCenter.hpp
 * @author St�phane Del Pino
 * @date   Fri Nov  2 21:03:34 2007
 * 
 * @brief  Stores solver informations
 * 
 * Stores solver informations. This can be useful for preconditioners
 * for instance.
 */
class SolverInformationCenter
  : public ThreadStaticBase<SolverInformationCenter>
{
private:
  std::stack<const Mesh*>
  __mesh;			/**<  stack of used meshes*/
  std::stack<const DiscretizationType*>
  __discretizationType;		/**< stack of discretization types */

public:
  /** 
   * Read-only access to the last stored mesh
   * 
   * @return the top of the mesh stack
   */
  const Mesh&
  mesh() const
  {
    ASSERT(__mesh.size() != 0);
    ASSERT(__mesh.top() != 0);
    return *(__mesh.top());
  }

  /** 
   * Read-only access to the last discretization type
   * 
   * @return the top of the discretization type stack
   */
  const DiscretizationType&
  discretizationType() const
  {
    ASSERT(__discretizationType.size() != 0);
    ASSERT(__discretizationType.top() != 0);
    return *__discretizationType.top();
  }

  /** 
   * Pushes a mesh to the stack
   * 
   * @param mesh the mesh to push
   */
  void pushMesh(const Mesh* mesh)
  {
    __mesh.push(mesh);
  }

  /** 
   * Pushes a discretization type to the stack
   * 
   * @param discretizationType the discretization type to push
   */
  void pushDiscretizationType(const DiscretizationType* discretizationType)
  {
    __discretizationType.push(discretizationType);
  }

  /** 
   * popes all stacks
   * 
   */
  void pop()
  {
    __mesh.pop();
    __discretizationType.pop();
  }

  /** 
   * Constructor
   * 
   */
  SolverInformationCenter()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~SolverInformationCenter()
  {
    ;
  }
};

#endif // SOLVER_INFORMATION_CENTER_HPP
