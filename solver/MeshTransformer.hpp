//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_TRANSFORMER_HPP
#define MESH_TRANSFORMER_HPP

#include <MeshGenerator.hpp>

class VerticesSet;
class FieldOfScalarFunction;

/**
 * @file   MeshTransformer.hpp
 * @author Stephane Del Pino
 * @date   Sat Jan  8 17:46:03 2005
 * 
 * @brief Builds a new mesh that is the transformation of a mesh by a
 * field of functions
 */
class MeshTransformer
  : public MeshGenerator
{
protected:
  ConstReferenceCounting<Mesh>
  __input;			/**< given mesh */

  ConstReferenceCounting<FieldOfScalarFunction>
  __field;			/**< field of transformation */

private:
  /** 
   * Forbiden copy constructor
   * 
   * @param M a given MeshTransformer
   */
  MeshTransformer(const MeshTransformer& M);

  /** 
   * Builds the transformation according to the mesh type of a volume
   * mesh
   */
  template <typename MeshType>
  void __transformVolume();

  /** 
   * Builds the transformation according to the mesh type of a surface
   * mesh
   */
  template <typename MeshType>
  void __transformSurface();

  /** 
   * Gets a new cell corresponding to the connectivity of the input
   * mesh
   * 
   * @param V new vertices set
   * @param originalCell cell to copy
   * 
   * @return new cell
   */
  template <typename CellType,
	    typename OriginalCellType>
  CellType __getCell(VerticesSet& V,
		     const OriginalCellType& originalCell);
public:
  /** 
   * Runs the tetrahedral mesh generation
   * 
   */
  void transform();

  /** 
   * Constructor
   * 
   * @param inputMesh the mesh to tetrahedrize
   * @param field field of the transformation
   */
  MeshTransformer(ConstReferenceCounting<Mesh> inputMesh,
		  ConstReferenceCounting<FieldOfScalarFunction> field)
    : MeshGenerator(),
      __input(inputMesh),
      __field(field)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~MeshTransformer()
  {
    ;
  }
};

#endif // MESH_TRANSFORMER_HPP
