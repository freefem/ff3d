//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ScalarFunctionMeshElementsReferences.hpp>

#include <Mesh.hpp>
#include <Structured3DMesh.hpp>
#include <MeshOfHexahedra.hpp>
#include <MeshOfTetrahedra.hpp>


template <typename MeshType> 
real_t ScalarFunctionMeshElementsReferences::
__evaluate(const TinyVector<3, real_t>& X) const
{
  const MeshType& M = static_cast<const MeshType&>(*__mesh);
  typename MeshType::const_iterator icell = M.find(X);

  if (icell.end()) { // outside of the mesh!
    return 0;
  }

  const size_t ref = (*icell).reference();
  FunctionMap::const_iterator i = this->__functionMap.find(ref);
  if (i == this->__functionMap.end()) { // reference was not found in list
    return 0;
  } else {
    return (*i->second)(X);
  }
}

real_t
ScalarFunctionMeshElementsReferences::
operator()(const TinyVector<3, real_t>& X) const
{
  switch (__mesh->type()) {
  case Mesh::cartesianHexahedraMesh: {
    return this->__evaluate<Structured3DMesh>(X);
  } 
  case Mesh::hexahedraMesh: {
    return this->__evaluate<MeshOfHexahedra>(X);
  } 
  case Mesh::tetrahedraMesh: {
    return this->__evaluate<MeshOfTetrahedra>(X);
  } 
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "references are not implemented for this kind of mesh",
		       ErrorHandler::unexpected);
    return 0;
  }
  }
}

ScalarFunctionMeshElementsReferences::
ScalarFunctionMeshElementsReferences(const FunctionMap& functionMap,
				     ConstReferenceCounting<Mesh> mesh)
  : ScalarFunctionBase(ScalarFunctionBase::references),
    __functionMap(functionMap),
    __mesh(mesh)
{
  ;
}

ScalarFunctionMeshElementsReferences::
ScalarFunctionMeshElementsReferences(const ScalarFunctionMeshElementsReferences& f)
  : ScalarFunctionBase(f),
    __functionMap(f.__functionMap),
    __mesh(f.__mesh)
{
  ;
}

ScalarFunctionMeshElementsReferences::
~ScalarFunctionMeshElementsReferences()
{
  ;
}
