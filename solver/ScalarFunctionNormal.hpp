//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_NORMAL_HPP
#define SCALAR_FUNCTION_NORMAL_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionNormal.hpp
 * @author Stephane Del Pino
 * @date   Wed Jun 14 01:00:35 2006
 * 
 * @brief This class is a simple normal component evaluating function
 * 
 * This works using the @see NormalManager class
 * 
 */
class ScalarFunctionNormal
  : public ScalarFunctionBase
{
public:
  enum ComponentType
  {
    undefined,
    x,
    y,
    z
  };

private:
  ComponentType __componentType; /**< The desired component */

  /** 
   * Writes the function to a given stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    switch(__componentType) {
    case ScalarFunctionNormal::undefined: {
      os << "[undefined normal]";
      break;
    }
    case ScalarFunctionNormal::x: {
      os << "nx";
      break;
    }
    case ScalarFunctionNormal::y: {
      os << "ny";
      break;
    }
    case ScalarFunctionNormal::z: {
      os << "nz";
      break;
    }
    }
    return os;
  }

public:
  /** 
   * Evaluates the normal a position @a X
   * 
   * @param X position
   * 
   * @note a boundary @b must be defined to evaluate the normal
   * 
   * @return @f$ n_i(X) @f$
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param normalType the desired component
   */
  ScalarFunctionNormal(const ComponentType& normalType)
    : ScalarFunctionBase(ScalarFunctionBase::normal),
      __componentType(normalType)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionNormal()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_NORMAL_HPP
