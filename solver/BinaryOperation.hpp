//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BINARY_OPERATION_HPP
#define BINARY_OPERATION_HPP

#include <ErrorHandler.hpp>
#include <string>

/**
 * @file   BinaryOperation.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 14:49:51 2006
 * 
 * @brief  Discribes binary operations
 */
class BinaryOperation
{
public:
  enum Type {
    sum,
    difference,
    division,
    modulo,
    power,
    product,

    min,
    max,

    gt,
    lt,
    ge,
    le,
    eq,
    ne,
    or_,
    xor_,
    and_,

    undefined
  };

private:
  const Type __type;		/**< type of the binary operation */

  static BinaryOperation::Type
  __getType(const std::string& typeName)
  {
    if (typeName == "min") return BinaryOperation::min;
    if (typeName == "max") return BinaryOperation::max;
    return BinaryOperation::undefined;
  }

public:
  /** 
   * Writes the binary operation to a stream
   * 
   * @param os given stream
   * @param b binary operation
   * 
   * @return os
   */
  friend std::ostream& operator<<(std::ostream& os,
				  const BinaryOperation& b)
  {
    os << b.name();
    return os;
  }

  /** 
   * Gets the name of the binary operation
   * 
   * @return operator name
   */
  std::string name() const
  {
    switch(__type) {
    case sum: {
      return "+";
    }
    case difference: {
      return "-";
    }
    case product: {
      return "*";
    }
    case division: {
      return "/";
    }
    case modulo: {
      return "%";
    }
    case power: {
      return "^";
    }
    case min: {
      return "min";
    }
    case max: {
      return "max";
    }
    case gt: {
      return ">";
    }
    case lt: {
      return "<";
    }
    case ge: {
      return ">=";
    }
    case le: {
      return "<=";
    }
    case eq: {
      return "==";
    }
    case ne: {
      return "!=";
    }
    case or_: {
      return "or";
    }
    case xor_: {
      return "xor";
    }
    case and_: {
      return "and";
    }
    case undefined: {
      return "undefined";
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown operator type",
			 ErrorHandler::unexpected);
    }
    }
    return "undefined";
  }

  /** 
   * Read-only access to the binary operation type
   * 
   * @return __type
   */
  const BinaryOperation::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param typeName the name of the binary operation
   */
  BinaryOperation(const std::string& typeName)
    : __type(BinaryOperation::__getType(typeName))
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param type the type of the binary operation
   */
  BinaryOperation(const BinaryOperation::Type& type)
    : __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param b a given binary operation
   */
  BinaryOperation(const BinaryOperation& b)
    : __type(b.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~BinaryOperation()
  {
    ;
  }
};

#endif // BINARY_OPERATION_HPP
