//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_OPERATOR_ALPHA_U_V_HPP
#define VARIATIONAL_OPERATOR_ALPHA_U_V_HPP

#include <VariationalBilinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorAlphaUV.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 12:12:23 2006
 * 
 * @brief  describes term @f$ \int \alpha uv @f$
 * 
 */
class VariationalAlphaUVOperator
  : public VariationalBilinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __Alpha;			/**< mass term \f$\alpha\f$ */

public:
  /** 
   * Access to \f$\alpha\f$
   * 
   * @return __Alpha
   */
  ConstReferenceCounting<ScalarFunctionBase> alpha() const
  {
    return __Alpha;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int c\alpha uv @f$
   */
  ReferenceCounting<VariationalBilinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalAlphaUVOperator* newOperator = new VariationalAlphaUVOperator(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__Alpha);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    (*newOperator).__Alpha = functionBuilder.getBuiltFunction();

    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param unknownNumber unknown number
   * @param unknownProperty unknown property
   * @param testFunctionNumber  test function number
   * @param testFunctionProperty  test function property
   * @param alpha the mass coefficient
   */
  VariationalAlphaUVOperator(const size_t& unknownNumber,
			     const VariationalOperator::Property& unknownProperty,
			     const size_t& testFunctionNumber,
			     const VariationalOperator::Property& testFunctionProperty,
			     ConstReferenceCounting<ScalarFunctionBase> alpha)
    : VariationalBilinearOperator(VariationalBilinearOperator::alphaUV,
				  unknownNumber, unknownProperty,
				  testFunctionNumber, testFunctionProperty),
      __Alpha(alpha)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V a given operator
   */
  VariationalAlphaUVOperator(const VariationalAlphaUVOperator& V)
    : VariationalBilinearOperator(V),
      __Alpha(V.__Alpha)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalAlphaUVOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_ALPHA_U_V_HPP
