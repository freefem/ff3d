//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Q1Quadrangle3DFiniteElement.hpp>

TinyVector<3, real_t> Q1Quadrangle3DFiniteElement::__massCenter(0.5, 0.5, 0);

real_t
Q1Quadrangle3DFiniteElement::W(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
  case 0: {
    return (1-x)*(1-y);
  }
  case 1: {
    return x*(1-y);
  }
  case 2: {
    return x*y;
  }
  case 3: {
    return (1-x)*y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q1Quadrangle3DFiniteElement::dxW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& y = X[1];

  switch (i) {
  case 0: {
    return -(1-y);
  }
  case 1: {
    return (1-y);
  }
  case 2: {
    return y;
  }
  case 3: {
    return -y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q1Quadrangle3DFiniteElement::dyW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];

  switch (i) {
  case 0: {
    return -(1-x);
  }
  case 1: {
    return -x;
  }
  case 2: {
    return x;
  }
  case 3: {
    return (1-x);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t 
Q1Quadrangle3DFiniteElement::dzW(const size_t& i, const TinyVector<3>& X) const
{
  switch (i) {
  case 0:
  case 1:
  case 2:
  case 3: {
    return 0;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}
