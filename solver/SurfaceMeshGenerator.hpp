//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SURFACE_MESH_GENERATOR_HPP
#define SURFACE_MESH_GENERATOR_HPP

class Mesh;

class Problem;
class Structured3DMesh;
class SurfaceMesh;

class SurfaceMeshOfTriangles;
class SurfaceMeshOfQuadrangles;

#include <Reference.hpp>
#include <TinyVector.hpp>

#include <Domain.hpp>
#include <Shape.hpp>

#include <set>
/**
 * @file   SurfaceMeshGenerator.hpp
 * @author Stephane Del Pino
 * @date   Mon May 17 19:08:28 2004
 * 
 * @brief  A surface mesh generator
 * 
 * This surface mesh generator builts Surface meshes based on a
 * POV-Ray description using Marching Tetrahedra-like technics.
 */

class SurfaceMeshGenerator
{
private:
  class Internals;
  ReferenceCounting<Internals> __internals;

public:
  void generateSurfacicMesh(const Domain& Omega,
			    const Mesh& M,
			    SurfaceMeshOfTriangles& s_mesh);

  SurfaceMeshGenerator();

  ~SurfaceMeshGenerator();
};
#endif // SURFACE_MESH_GENERATOR_HPP
