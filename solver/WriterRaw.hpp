//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef WRITER_RAW_HPP
#define WRITER_RAW_HPP

#include <WriterBase.hpp>
#include <ostream>

class ScalarFunctionBase;

/**
 * @file   WriterRaw.hpp
 * @author St�phane Del Pino
 * @date   Fri Feb 23 16:26:22 2007
 * 
 * @brief  Writer for raw format
 */
class WriterRaw
  : public WriterBase
{
private:
  /** 
   * Copy constructor is forbidden
   * 
   */
  WriterRaw(const WriterRaw&);

  /** 
   * Save scalar function to a file
   * 
   * @param file given file
   * @param f function to save
   */
  void __saveScalarFunction(std::ostream& file,
			    const ScalarFunctionBase& f) const;
public:
  /** 
   * Save data in the file
   * 
   */
  void proceed() const;

  /** 
   * Constructor
   * 
   * @param mesh given mesh
   * @param filename name of the file to create
   * @param fileDescriptor describes file type
   */
  WriterRaw(ConstReferenceCounting<Mesh> mesh,
	    const std::string& filename,
	    const FileDescriptor& fileDescriptor);

  /** 
   * Destructor
   * 
   */
  ~WriterRaw();
};

#endif // WRITER_RAW_HPP
