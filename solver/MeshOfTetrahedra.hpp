//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_OF_TETRA_HPP
#define MESH_OF_TETRA_HPP

#include <Mesh.hpp>
#include <Tetrahedron.hpp>
#include <Connectivity.hpp>
#include <Octree.hpp>

#include <list>
#include <map>

#include <SurfaceMeshOfTriangles.hpp>
#include <FacesSet.hpp>

/**
 * @file   MeshOfTetrahedra.hpp
 * @author Stephane Del Pino
 * @date   Tue Dec 23 18:39:59 2003
 * 
 * @brief  management of tetrahedra meshes
 * 
 */

class MeshOfTetrahedra
  : public Mesh
{
public:
  typedef Tetrahedron CellType;
  typedef Tetrahedron::FaceType FaceType;

  typedef MeshOfTetrahedra Transformed;

  typedef SurfaceMeshOfTriangles BorderMeshType;

  enum {
    family = Mesh::volume
  };
private:

  ReferenceCounting<Vector<Tetrahedron> > __cells; /**< Cells list */

  ReferenceCounting<Octree<size_t, 3> > __octree; /**< Octree refering to the cells */

  TinyVector<3,real_t> __a;	/**< bounding box lower  corner */
  TinyVector<3,real_t> __b;	/**< bounding box higher corner */

  ReferenceCounting<BorderMeshType> __borderMesh; /**< The various border lists*/
  ReferenceCounting<FacesSet<Triangle> > __facesSet; /**< internal and external faces set*/

  Connectivity<MeshOfTetrahedra> __connectivity; /**< Connectivity */

  /** 
   * Forbids copy contructor
   * 
   * @param m another mesh
   * 
   */

  MeshOfTetrahedra(const MeshOfTetrahedra& m);

public:
  std::string typeName() const
  {
    return "mesh of tetrahedra";
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const real_t& x, const real_t& y, const real_t& z) const
  {
    return not(this->find(x, y, z).end());

  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const TinyVector<3>& p) const
  {
    return this->inside(p[0], p[1], p[2]);
  }

  bool hasBorderMesh() const
  {
    return (__borderMesh != 0);
  }

  ConstReferenceCounting<BorderMeshType> borderMesh() const
  {
    return __borderMesh;
  }

  ConstReferenceCounting<Mesh> borderBaseMesh() const
  {
    return static_cast<const BorderMeshType*>(__borderMesh);
  }

  //! Read-only access to the number of cells.
  inline const size_t& numberOfCells() const
  {
    return (*__cells).size();
  }

  /** 
   * Builds internal edges of the mesh
   * 
   */
  void buildEdges();

  /** 
   * Checks if internal faces set is built
   * 
   * @return true if it is built
   */
  bool hasFaces() const
  {
    return (__facesSet != 0);
  }
  /** 
   * Builds internal faces of the mesh
   * 
   */
  void buildFaces();

  /** 
   * Read-only access to a face
   * 
   * @param i the face number
   * 
   * @return the \a i th face
   */
  const FaceType& face(const size_t& i) const
  {
    ASSERT(__facesSet != 0);
    return (*__facesSet)[i];
  }

  //! Read-only access to the number of faces.
  inline const size_t& numberOfFaces() const
  {
    ASSERT(__facesSet != 0);
    return (*__facesSet).numberOfFaces();
  }

  size_t cellNumber(const Tetrahedron& h) const
  {
    return (*__cells).number(h);
  }

  size_t faceNumber(const FaceType& f) const
  {
    ASSERT(__facesSet != 0);
    return (*__facesSet).number(f);
  }

  typedef Mesh::T_iterator<MeshOfTetrahedra, Tetrahedron> iterator;
  typedef Mesh::T_iterator<const MeshOfTetrahedra, const Tetrahedron> const_iterator;

  void buildLocalizationTools();

  /** 
   * Find the element which contains a point \f$ P \f$
   * 
   * @param P \f$ P\f$
   * 
   * @return an iterator on the element if found else returns the end
   * iterator
   * 
   */
  MeshOfTetrahedra::const_iterator find(const TinyVector<3, real_t>& P) const
  {
    return find(P[0], P[1], P[2]);
  }

  /** 
   * Find the element which contains a point \f$ P \f$
   * 
   * @param x \f$ P_1\f$
   * @param y \f$ P_2\f$ 
   * @param z  \f$ P_3\f$
   * 
   * @return an iterator on the element if found else returns the end
   * iterator
   * 
   * @bug Criteria uses an epsilon that should be relative to the
   * element size
   */
  MeshOfTetrahedra::const_iterator find(const double& x,
					const double& y,
					const double& z) const;

  /** 
   * Access to the cell \a i of the mesh.
   * 
   * @param i the cell number
   * 
   * @return the \a i th tetrahedron
   */
  inline Tetrahedron& cell(const size_t& i)
  {
    return (*__cells)[i];
  }

  /** 
   * Read only access to the cell \a i of the mesh.
   * 
   * @param i the cell number
   * 
   * @return the \a i th tetrahedron
   */
  inline const Tetrahedron& cell(const size_t& i) const
  {
    return (*__cells)[i];
  }

  /** 
   * Read only access to the mesh connectivity
   * 
   * @return the connectivity
   */
  const Connectivity<MeshOfTetrahedra>& connectivity() const
  {
    return __connectivity;
  }

  /** 
   * Access to the mesh connectivity
   * 
   * @return the connectivity
   */
  Connectivity<MeshOfTetrahedra>& connectivity()
  {
    return __connectivity;
  }

  /** 
   * Constructs a Mesh of tetrahedra using a set of vertices, a set of
   * tetrahedra and a given border mesh. Connectivity is computed.
   * 
   * @param vertices given vertices
   * @param correspondances "real" vertices numbers
   * @param tetrahedra given tetrahedra
   * @param triangles a surface mesh
   */
  MeshOfTetrahedra(ReferenceCounting<VerticesSet> vertices,
		   ReferenceCounting<VerticesCorrespondance> correspondances,
		   ReferenceCounting<Vector<Tetrahedron> > tetrahedra,
		   ReferenceCounting<BorderMeshType> triangles = 0);

  ~MeshOfTetrahedra()
  {
    ;
  }
};

#endif // MESH_OF_TETRA_HPP
