//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_COMPOSED_HPP
#define SCALAR_FUNCTION_COMPOSED_HPP

#include <ScalarFunctionBase.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   ScalarFunctionComposed.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:49:09 2006
 * 
 * @brief  composed function @f$ f(g_x,g_y,g_z) @f$
 * 
 */
class ScalarFunctionComposed
  : public ScalarFunctionBase
{
private:
  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << *__function << '('
       << *__functionX << ','
       << *__functionY << ','
       << *__functionZ << ')';
    return os;
  }

  ConstReferenceCounting<ScalarFunctionBase>
  __function;			/**< @f$ f @f$ */
  ConstReferenceCounting<ScalarFunctionBase>
  __functionX;			/**< @f$ g_x @f$ */
  ConstReferenceCounting<ScalarFunctionBase>
  __functionY;			/**< @f$ g_y @f$ */
  ConstReferenceCounting<ScalarFunctionBase>
  __functionZ;			/**< @f$ g_z @f$ */

public:
  /** 
   * Evaluates the composed function at point @f$ X @f$
   * 
   * @param X @f$ X @f$
   * 
   * @return @f$ f(g_x(X),g_y(X),g_z(X)) @f$
   */
  real_t operator()(const TinyVector<3,real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param function @f$ f @f$
   * @param functionX @f$ g_x @f$
   * @param functionY @f$ g_y @f$
   * @param functionZ @f$ g_z @f$
   */
  ScalarFunctionComposed(ConstReferenceCounting<ScalarFunctionBase> function,
			 ConstReferenceCounting<ScalarFunctionBase> functionX,
			 ConstReferenceCounting<ScalarFunctionBase> functionY,
			 ConstReferenceCounting<ScalarFunctionBase> functionZ);

  /** 
   * Copy-constructor
   * 
   * @param f given composed function
   */
  ScalarFunctionComposed(const ScalarFunctionComposed& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionComposed();
};

#endif // SCALAR_FUNCTION_COMPOSED_HPP
