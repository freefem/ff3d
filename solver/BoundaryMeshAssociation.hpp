//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_MESH_ASSOCIATION_HPP
#define BOUNDARY_MESH_ASSOCIATION_HPP

#include <map>
#include <ReferenceCounting.hpp>

#include <Problem.hpp>
#include <Boundary.hpp>

#include <BoundarySurfaceMesh.hpp>
#include <BoundaryReferences.hpp>
#include <BoundaryPOVRay.hpp>

#include <BoundaryConditionSet.hpp>
#include <BoundaryMeshAssociation.hpp>

#include <SurfaceMesh.hpp>
#include <SurfaceMeshOfTriangles.hpp>
#include <SurfaceMeshOfQuadrangles.hpp>

#include <MeshExtractor.hpp>

#include <PDESystem.hpp>
#include <VariationalProblem.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/**
 * @file   BoundaryMeshAssociation.hpp
 * @author Stephane Del Pino
 * @date   Fri Dec 26 18:07:35 2003
 * 
 * @brief  Associates boundaries and meshes
 * 
 * 
 */

class BoundaryMeshAssociation
{
private:
  bool __hasPOVReference;	/**< indicates if pov-ray boundaries are present  */

  typedef std::map<const Boundary*, ConstReferenceCounting<SurfaceMesh> >
  MapBoundaryToMesh;

  MapBoundaryToMesh __boundaryMesh; /**< Correspondance between
				       boundaries and meshes  */

  /** 
   * Stores boundaries and meshes from a given boudary condition set
   * and a given mesh
   * 
   * @param bc the boundary condition set
   * @param M the given mesh
   */
  template <typename MeshType>
  void __storesBoundariesAndMeshes(const BoundaryConditionSet& bc,
				   const MeshType& M)
  {
    for (size_t i=0; i<bc.nbBoundaryCondition(); ++i) {
      if (__boundaryMesh.find(bc[i].boundary())
	  == __boundaryMesh.end()) {
	switch(bc[i].boundary()->type()) {
	case Boundary::povRay: {
	  __boundaryMesh[bc[i].boundary()] = 0;
	  __hasPOVReference = true;
	  break;
	}
	case Boundary::surfaceMesh: {
	  const BoundarySurfaceMesh& b
	    = dynamic_cast<const BoundarySurfaceMesh&>(*bc[i].boundary());
	  const SurfaceMesh* t = b.surfaceMesh();
	  __boundaryMesh[bc[i].boundary()] = t;
	  break;
	}
	case Boundary::references: {
	  const BoundaryReferences& b
	    = dynamic_cast<const BoundaryReferences&>(*bc[i].boundary());
	  
	  MeshExtractor<typename MeshType::BorderMeshType>
	    extractor(M.borderMesh());

	  __boundaryMesh[bc[i].boundary()]
	    = extractor(b.references());
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "unknown boundary type",
			     ErrorHandler::unexpected);
	}
	}
      }
    }
  }

  /** 
   * Stores boundaries and meshes from a given border operator
   * and a given mesh
   * 
   * @param borderOperator the border operator
   * @param M the given mesh
   */
  template <typename MeshType>
  void __storesBoundariesAndMeshes(const VariationalBorderOperator& borderOperator,
				   const MeshType& M)
  {
    if (__boundaryMesh.find(borderOperator.boundary())
	== __boundaryMesh.end()) {
      switch(borderOperator.boundary()->type()) {
      case Boundary::povRay: {
	__boundaryMesh[borderOperator.boundary()] = 0;
	__hasPOVReference = true;
	break;
      }
      case Boundary::surfaceMesh: {
	const BoundarySurfaceMesh& b
	  = dynamic_cast<const BoundarySurfaceMesh&>(*borderOperator.boundary());
	const SurfaceMesh* t = b.surfaceMesh();
	__boundaryMesh[borderOperator.boundary()] = t;
	break;
      }
      case Boundary::references: {
	const BoundaryReferences& b
	  = dynamic_cast<const BoundaryReferences&>(*borderOperator.boundary());
	  
	MeshExtractor<typename MeshType::BorderMeshType>
	  extractor(M.borderMesh());

	__boundaryMesh[borderOperator.boundary()]
	  = extractor(b.references());
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "unknown boundary type",
			   ErrorHandler::unexpected);
      }
      }
    }
  }

public:
  typedef MapBoundaryToMesh::const_iterator const_iterator;

  /** 
   * returns begin iterator on association
   * 
   * @return the begining
   */
  const_iterator begin() const
  {
    return __boundaryMesh.begin();
  } 

  /** 
   * returns the end off the association
   * 
   * @return the end
   */
  const_iterator end() const
  {
    return __boundaryMesh.end();
  } 

  /** 
   * Read only access to the pov-ray boundary presence
   * 
   * @return __hasPOVReference
   */
  const bool& hasPOVReferences() const
  {
    return __hasPOVReference;
  }

  /** 
   * Set meshes related to POV-Ray references
   * 
   * @param omega the computational domain (needed for reference translation)
   * @param mesh the complete domain mesh
   */
  void setPOVMeshes(const Domain& omega,
		    ConstReferenceCounting<SurfaceMeshOfTriangles> mesh)
  {
    for (MapBoundaryToMesh::iterator i = __boundaryMesh.begin();
	 i != __boundaryMesh.end(); ++i) {
      const Boundary& b = (*(i->first));
      if (b.type() == Boundary::povRay) {
	ASSERT(i->second == 0);
	const BoundaryPOVRay& pov = dynamic_cast<const BoundaryPOVRay&>(b);
	std::set<size_t> references;
	for (size_t n=0; n<pov.numberOfPOVReferences(); ++n) {
	  references.insert(omega.reference(pov.povReference(n)));
	}
	MeshExtractor<SurfaceMeshOfTriangles> extractor(mesh);
	__boundaryMesh[i->first] = extractor(references);
      }
    }
  }

  /** 
   * Access to the mesh related to a given boundary
   * 
   * @param b the boundary
   * 
   * @return the mesh
   */
  ConstReferenceCounting<SurfaceMesh> operator[](const Boundary& b) const
  {
    MapBoundaryToMesh::const_iterator i = __boundaryMesh.find(&b);
    if (i != __boundaryMesh.end()) {
      return i->second;
    } else {
      const std::string errorMsg
	= "the boundary "+stringify(b)+" was unexpected\n";
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg,
			 ErrorHandler::normal);
    }
  }

  /** 
   * Constructor
   * 
   * @param P the problem
   * @param M the 3d mesh
   */
  template <typename MeshType>
  BoundaryMeshAssociation(const Problem& P,
			  MeshType& M)
    : __hasPOVReference(false)
  {
    switch (P.type()) {
    case Problem::pde: {
      const PDESystem& system = static_cast<const PDESystem&>(P);
      for (size_t i=0; i<system.numberOfUnknown(); ++i) {
	this->__storesBoundariesAndMeshes(P.boundaryConditionSet(i), M);
      }
      break;
    }
    case Problem::variational: {
      const VariationalProblem& V = static_cast<const VariationalProblem&>(P);
      for (size_t i=0; i<V.numberOfUnknown(); ++i) {
	this->__storesBoundariesAndMeshes(P.boundaryConditionSet(i), M);
      }
      for (VariationalProblem::bilinearBorderOperatorConst_iterator i
	     = V.beginBilinearBorderOperator();
	   i != V.endBilinearBorderOperator(); ++i) {
	this->__storesBoundariesAndMeshes(**i, M);
      }
      for (VariationalProblem::linearBorderOperatorConst_iterator i
	     = V.beginLinearBorderOperator();
	   i != V.endLinearBorderOperator(); ++i) {
	this->__storesBoundariesAndMeshes(**i, M);
      }
      break;
    }
    }
  }

  /** 
   * The destructor
   * 
   */
  ~BoundaryMeshAssociation()
  {
    ;
  }
};

#endif // BOUNDARY_MESH_ASSOCIATION_HPP
