//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef LEGENDRE_BASIS_HPP
#define LEGENDRE_BASIS_HPP

#include <Types.hpp>
#include <Vector.hpp>

/**
 * @file   LegendreBasis.hpp
 * @author Dris Yakoubi
 * @date   Sun Apr 29 20:49:14 2007
 * 
 * @brief  Manages basis of Legendre polynomials of degree @f$ d @f$
 * 
 */
class LegendreBasis
{
private:
  const size_t __degree;	/**< degree @f$ d @f$ of the basis */

public:

  /** 
   * Read-only access to the degree of the basis
   * 
   * @return __degree
   */
  size_t degree() const
  {
    return __degree;
  }

  /** 
   * Read only access to the dimension of the spawned space
   * 
   * @return __degree+1
   */
  size_t dimension() const
  {
    return __degree +1;
  }

  /** 
   * Computes values of all polynomials of the basis at position @a x
   * 
   * @param x evaluation point @f$ x @f$
   * @param values computed values
   */  
  void getValues(const real_t& x,
		 Vector<real_t>& values) const;

  /** 
   * Computes the value @f$ L_i(x) @f$
   * 
   * @param x position
   * @param i number of the Legendre polynom
   * 
   * @return  @f$ L_i(x) @f$
   */
  real_t getValue(const real_t& x,
		  const size_t& i) const;

  /** 
   * Computes values of first derivative of all polynomials of the
   * basis at position @a x
   * 
   * @param x evaluation point @f$ x @f$
   * @param values computed values
   */  
  void getDerivativeValues(const real_t& x,
			   Vector<real_t>& values) const;

  /** 
   * Computes the value @f$ L'_i(x) @f$
   * 
   * @param x position
   * @param i number of the Legendre polynom
   * 
   * @return  @f$ L'_i(x) @f$
   */
  real_t getDerivativeValue(const real_t& x,
			    const size_t& i) const;

  
  /** 
   * Computes values of second derivative of all polynomials of the
   * basis at position @a x
   * 
   * @param x evaluation point @f$ x @f$
   * @param values computed values
   */  
  void getSecondDerivativeValues(const real_t& x,
				 Vector<real_t>& values) const;
  
  /** 
   * Computes the value @f$ L''_i(x) @f$
   * 
   * @param x position
   * @param i number of the Legendre polynom
   * 
   * @return  @f$ L''_i(x) @f$
   */
  real_t getSecondDerivativeValue(const real_t& x,
				  const size_t& i) const;
  
  /** 
   * Constructor
   * 
   * @param degree degree of the basis to built
   */
  LegendreBasis(const size_t& degree);
  
  /** 
   * Copy constructor
   * 
   * @param legendreBasis a given legendre basis
   */
  LegendreBasis(const LegendreBasis& legendreBasis);
  
  /** 
   * Destructor
   * 
   */
  ~LegendreBasis();
};

#endif // LEGENDRE_BASIS_HPP
