//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_OF_HEXAHEDRA_HPP
#define MESH_OF_HEXAHEDRA_HPP

/**
 * @file   MeshOfHexahedra.hpp
 * @author Stephane Del Pino
 * @date   Tue Nov 26 17:34:43 2002
 * 
 * @brief  Describes meshes composed of hexahedra
 * 
 * @todo doxygen documentation not up to date
 */

#include <Mesh.hpp>

#include <SurfaceMeshOfQuadrangles.hpp>
#include <FacesSet.hpp>

#include <Hexahedron.hpp>

#include <Connectivity.hpp>
#include <Octree.hpp>

#include <list>
#include <map>

class MeshOfHexahedra
  : public Mesh
{
public:
  typedef Hexahedron CellType;
  typedef Hexahedron::FaceType FaceType;

  typedef MeshOfHexahedra Transformed;

  typedef SurfaceMeshOfQuadrangles BorderMeshType;

  enum {
    family = Mesh::volume
  };
private:
  ReferenceCounting<Vector<Hexahedron> > __cells; /**< Cells list */

  ReferenceCounting<BorderMeshType>
  __borderMesh; /**< The various border lists*/

  ReferenceCounting<FacesSet<Quadrangle> >
  __facesSet;  /**< internal and external quadrangles set */

  Connectivity<MeshOfHexahedra> __connectivity; /**< Connectivity */

  ReferenceCounting<Octree<size_t, 3> > __octree; /**< Octree refering to the cells */

  TinyVector<3,real_t> __a;	/**< bounding box lower  corner */
  TinyVector<3,real_t> __b;	/**< bounding box higher corner */

  /** 
   * Forbids copy contructor
   * 
   * @param m another mesh
   * 
   */
  MeshOfHexahedra(const MeshOfHexahedra& m);

public:
  std::string typeName() const
  {
    return "unstructured mesh of hexahedra";
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const real_t& x, const real_t& y, const real_t& z) const
  {
    return not(this->find(x, y, z).end());
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const TinyVector<3>& p) const
  {
    return this->inside(p[0], p[1], p[2]);
  }

  inline bool hasBorderMesh() const
  {
    return (__borderMesh != 0);
  }

  ConstReferenceCounting<BorderMeshType> borderMesh() const
  {
    return __borderMesh;
  }

  ConstReferenceCounting<Mesh> borderBaseMesh() const
  {
    return static_cast<const BorderMeshType*>(__borderMesh);
  }

  /** 
   * Builds the internal edges of the mesh
   * 
   */
  void buildEdges();

  /** 
   * Builds the internal faces of the mesh
   * 
   */
  bool hasFaces() const
  {
    return (__facesSet != 0);
  }

  /** 
   * Builds the internal faces of the mesh
   * 
   */
  void buildFaces();

  /** 
   * Read-only access to a face
   * 
   * @param i the face number
   * 
   * @return the \a i th face
   */
  const FaceType& face(const size_t& i) const
  {
    return (*__facesSet)[i];
  }

  size_t faceNumber(const FaceType& f) const
  {
    return (*__facesSet).number(f);
  }

  //! Read-only access to the number of cells.
  inline const size_t& numberOfFaces() const
  {
    return (*__facesSet).numberOfFaces();
  }

  //! Read-only access to the number of cells.
  inline const size_t& numberOfCells() const
  {
    return (*__cells).size();
  }

  size_t cellNumber(const Hexahedron& h) const
  {
    return (*__cells).number(h);
  }

  typedef Mesh::T_iterator<MeshOfHexahedra, Hexahedron> iterator;
  typedef Mesh::T_iterator<const MeshOfHexahedra, const Hexahedron> const_iterator;

  void buildLocalizationTools();

  MeshOfHexahedra::const_iterator find(const double& x,
				       const double& y,
				       const double& z) const;

  MeshOfHexahedra::const_iterator find(const TinyVector<3>& X) const
  {
    return find(X[0], X[1], X[2]);
  }

  /** 
   * Access to the Cell \a i of the mesh.
   * 
   * @param i an hexahedron number
   * 
   * @return the corresponding hexahedron 
   */
  inline Hexahedron& cell(const size_t& i)
  {
    return (*__cells)[i];
  }

  /** 
   *  Read-only access to the Cell \a i of the mesh.
   * 
   * @param i an hexahedron number
   * 
   * @return the corresponding hexahedron
   */
  inline const Hexahedron& cell(const size_t& i) const
  {
    return (*__cells)[i];
  }

  /** 
   * Read only access to the mesh connectivity
   * 
   * @return the connectivity
   */
  const Connectivity<MeshOfHexahedra>& connectivity() const
  {
    return __connectivity;
  }

  /** 
   * Access to the mesh connectivity
   * 
   * @return the connectivity
   */
  Connectivity<MeshOfHexahedra>& connectivity()
  {
    return __connectivity;
  }

  /** 
   * Hexahedra mesh constructor
   * 
   * @param numberOfVertices the number of vertices
   * @param numberOfCells the number of cells
   * 
   */
  MeshOfHexahedra(const size_t& numberOfVertices,
		  const size_t& numberOfCells)
    : Mesh(Mesh::hexahedraMesh,
	   Mesh::volume,
 	   numberOfVertices), 
      __cells(new Vector<Hexahedron>(numberOfCells)),
      __borderMesh(0),
      __facesSet(0),
      __connectivity(*this)
  {
    this->buildLocalizationTools();
  }

  /** 
   * Hexahedra mesh constructor
   * 
   * @param vertices a set of vertices
   * @param correspondances "real" vertices numbers
   * @param hexahedra the hexahedra
   * @param borderMesh the surface mesh
   * 
   */
  MeshOfHexahedra(ReferenceCounting<VerticesSet> vertices,
		  ReferenceCounting<VerticesCorrespondance> correspondances,
		  ReferenceCounting<Vector<Hexahedron> > hexahedra,
		  ReferenceCounting<BorderMeshType> borderMesh)
    : Mesh(Mesh::hexahedraMesh,
	   Mesh::volume,
	   vertices,
	   correspondances),
      __cells(hexahedra),
      __borderMesh(borderMesh),
      __facesSet(0),
      __connectivity(*this)
  {
    this->buildLocalizationTools();
  }

  /** 
   * With this constructor, border has to be built
   * 
   * @param vertices a set of vertices
   * @param correspondances "real" vertices numbers
   * @param hexahedra the hexahedra
   * 
   */
  MeshOfHexahedra(ReferenceCounting<VerticesSet> vertices,
		  ReferenceCounting<VerticesCorrespondance> correspondances,
		  ReferenceCounting<Vector<Hexahedron> > hexahedra)
    : Mesh(Mesh::hexahedraMesh,
	   Mesh::volume,
	   vertices,
	   correspondances),
      __cells(hexahedra),
      __borderMesh(0),
      __facesSet(0),
      __connectivity(*this)
  {
    this->buildLocalizationTools();
  }

  /** 
   * Destructor
   * 
   * 
   */
  ~MeshOfHexahedra()
  {
    ;
  }
};

#endif // MESH_OF_HEXAHEDRA_HPP
