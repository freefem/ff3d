//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_READER_BUILDER_HPP
#define SCALAR_FUNCTION_READER_BUILDER_HPP

#include <ReferenceCounting.hpp>
#include <ScalarFunctionReaderBase.hpp>

#include <FileDescriptor.hpp>

#include <string>

class Mesh;

/**
 * @file   ScalarFunctionReaderBuilder.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 11:29:09 2006
 * 
 * @brief  Factory to build function readers
 * 
 */
class ScalarFunctionReaderBuilder
{
private:
  const std::string __filename;	/**< file name */
  ConstReferenceCounting<Mesh>
  __mesh;			/**< associated mesh */
  const FileDescriptor
  __fileDescriptor;		/**< required type of reader */

  std::string __functionName;	/**< function name */
  int __componentNumber;	/**< component number */

public:
  /** 
   * sets the name of the function to read
   * 
   * @param functionName the name of the function
   */
  void setFunctionName(const std::string& functionName);

  /** 
   * Sets the component number
   * 
   * @param component number of the component
   */
  void setComponent(const size_t& component);

  /** 
   * Build the appropriate file reader
   * 
   * @return file reader
   */
  ReferenceCounting<ScalarFunctionReaderBase>
  getReader() const;

  /** 
   * Constructor
   * 
   * @param filename file name
   * @param mesh given mesh
   * @param format file descriptor
   */
  ScalarFunctionReaderBuilder(const std::string& filename,
			      ConstReferenceCounting<Mesh> mesh,
			      const FileDescriptor& format);

  /** 
   * Copy constructor
   * 
   * @param builder given reader builder
   */
  ScalarFunctionReaderBuilder(const ScalarFunctionReaderBuilder& builder);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionReaderBuilder();
};

#endif // SCALAR_FUNCTION_READER_BUILDER_HPP
