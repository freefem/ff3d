//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 Stephane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

#ifndef DEGREE_OF_FREEDOM_SET_MANAGER_HPP
#define DEGREE_OF_FREEDOM_SET_MANAGER_HPP

#include <ThreadStaticBase.hpp>
#include <DiscretizationType.hpp>

#include <ReferenceCounting.hpp>

class Mesh;
class ScalarDegreeOfFreedomPositionsSet;

class DegreeOfFreedomSetManager
  : public ThreadStaticBase<DegreeOfFreedomSetManager>
{
private:
  class Internal;
  Internal* __internal;

public:
  ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet>
  getDOFPositionsSet(const Mesh& mesh,
		     const ScalarDiscretizationTypeBase& discretizationType);

  void unsubscribe(ConstReferenceCounting<ScalarDegreeOfFreedomPositionsSet> dofSet);

  explicit DegreeOfFreedomSetManager();

  ~DegreeOfFreedomSetManager();
};

#endif // DEGREE_OF_FREEDOM_SET_MANAGER_HPP
