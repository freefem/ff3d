//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// This is used to describe Surfacic Quadrangles Elements

#ifndef  QUADRANGLE_HPP
#define  QUADRANGLE_HPP

#include <cmath>
#include <SurfElem.hpp>

#include <TinyVector.hpp>
#include <Edge.hpp>

/**
 * @file   Quadrangle.hpp
 * @author St�phane Del Pino
 * @date   Sat Jul 27 17:33:58 2002
 * 
 * @brief  Describes quadrangles in 3d
 * 
 */
class Quadrangle
  : public SurfElem
{
private:

  void __computeVolume()
  {
    const TinyVector<3>& v0 = *(__vertices[0]);
    const TinyVector<3>& v1 = *(__vertices[1]);
    const TinyVector<3>& v2 = *(__vertices[2]);
    const TinyVector<3>& v3 = *(__vertices[3]);

    const TinyVector<3> A(v1-v0);
    const TinyVector<3> B(v3-v0);

    const TinyVector<3> C(v1-v2);
    const TinyVector<3> D(v3-v2);

    __volume = 0.5*(Norm(A^B)+Norm(C^D));
  }

public:
  enum {
    NumberOfVertices = 4,	/**< number of vertices */
    NumberOfFaces    = 4,	/**< number of faces    */
    NumberOfEdges    = 4	/**< number of edges    */
  };

  typedef Edge FaceType;

  static const size_t faces[NumberOfEdges][FaceType::NumberOfVertices];
  static const size_t edges[NumberOfEdges][Edge::NumberOfVertices];

  /** 
   * Returns the *mean* normal to the face.
   * 
   * @return 
   */
  const TinyVector<3, real_t> normal() const
  {
    // IN This Function We Assume That The 4 Vertices Are Aligned.
    // Could be better to use the mean value ...

    const TinyVector<3>& v0 = *(__vertices[0]);
    const TinyVector<3>& v1 = *(__vertices[1]);
    const TinyVector<3>& v2 = *(__vertices[2]);
    const TinyVector<3>& v3 = *(__vertices[3]);

    const TinyVector<3> A(v2-v0);
    const TinyVector<3> B(v3-v1);
    TinyVector<3> N(A^B);
    const real_t n = Norm(N);
    N/=n;
    return N;
  }

  /** 
   * Access to the number of vertices of this Cell.
   * 
   * 
   * @return NumberOfVertices
   */
  size_t numberOfVertices() const
  {
    return NumberOfVertices;
  }

  /** 
   * Returns the type of the cell
   * 
   * @return Cell::quadrangle3d
   */
  virtual Cell::Type type() const
  {
    return Cell::quadrangle3d;
  }

  /** 
   * Constructs the structure
   * 
   * 
   */
  Quadrangle()
    : SurfElem(NumberOfVertices)
  {
    __volume = 0.;
  }

  /** 
   * Constructor: the quadrangle is defined by 4 vertices
   * 
   * @param V0 first vertex
   * @param V1 second vertex
   * @param V2 third vertex
   * @param V3 fourth vertex
   * @param reference the reference of this quadrangle (default is 0)
   *
   */
  Quadrangle(const Vertex& V0,
	     const Vertex& V1,
	     const Vertex& V2,
	     const Vertex& V3,
	     const size_t reference=0)
    : SurfElem(NumberOfVertices, reference)
  {
    __vertices[0] = (Vertex*)&V0;
    __vertices[1] = (Vertex*)&V1;
    __vertices[2] = (Vertex*)&V2;
    __vertices[3] = (Vertex*)&V3;

    this->__computeVolume();
  }

  /** 
   * Constructor: the quadrangle is defined by 4 vertices
   * 
   * @param vertices the vertices set
   * @param reference the reference of this quadrangle (default is 0)
   *
   */
  Quadrangle(const TinyVector<NumberOfVertices, Vertex*>& vertices,
	     const size_t reference=0)
    : SurfElem(NumberOfVertices, reference)
  {
    for (size_t i=0; i<NumberOfVertices; ++i) {
      __vertices[i] = vertices[i];
    }

    this->__computeVolume();
  }


  /** 
   * Copy Constructor
   * 
   * @param q the quadrangle to copy
   * 
   */
  Quadrangle(const Quadrangle& q)
    : SurfElem(q)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Quadrangle()
  {
    ;
  }
};

#endif // QUADRANGLE_HPP

