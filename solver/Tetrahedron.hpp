//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef TETRAHEDRON_HPP
#define TETRAHEDRON_HPP

/**
 * @file   Tetrahedron.hpp
 * @author Stephane Del Pino
 * @date   Tue Feb 11 09:31:08 2003
 * 
 * @brief  Describes Tetrahedron (using 4 vertices)
 * 
 * 
 */
#include <Cell.hpp>
#include <Triangle.hpp>

#include <TinyMatrix.hpp>

class Tetrahedron
  : public Cell
{
public:
  enum {
    NumberOfVertices   = 4,	/**< number of vertices */
    NumberOfFaces      = 4,	/**< number of faces */
    NumberOfEdges      = 6	/**< number of edges */
  };

  typedef Triangle FaceType;

  static const size_t faces[NumberOfFaces][FaceType::NumberOfVertices];
  static const size_t edges[NumberOfEdges][Edge::NumberOfVertices];

  virtual Cell::Type type() const
  {
    return Cell::tetrahedron;
  }

  /** 
   * computes barycentric coordinates in the current tetrahedron
   * related to a given \a X position.
   * 
   * @param X Cartesian coordinates
   * @param lambda barycentric coordinates
   *
   * @todo This could be optimized
   */
  inline void getBarycentricCoordinates(const TinyVector<3, real_t>& X,
					TinyVector<4, real_t>& lambda) const
  {
    TinyVector<4, real_t> X2;
    for (size_t i=0; i<3; ++i) X2[i] = X[i];
    X2[3] = 1;

    TinyMatrix<4, 4, real_t> M = 0;
    for (size_t i=0; i<4; ++i) {
      const Vertex& V = (*this)(i);
      for (size_t j=0; j<3; ++j) {
	M(j, i) = V[j];
      }
    }

    for (size_t i=0; i<4; ++i) {
      M(3,i) = 1;
    }

    lambda = X2/M;
  }

  //! Number of Vertices.
  size_t numberOfVertices() const
  {
    return NumberOfVertices;
  }

  //! Number of Edges.
  size_t numberOfEdges() const
  {
    return NumberOfEdges;
  }

  //! sets equal to a given tetrahedron
  inline const Tetrahedron& operator=(const Tetrahedron& T)
  {
    Cell::operator=(T);
    return *this;
  }

  //! Default constructor does nothing but reserving memory
  Tetrahedron()
    : Cell(Tetrahedron::NumberOfVertices)
  {
    ;
  }

  Tetrahedron(Vertex& x0,
	      Vertex& x1,
	      Vertex& x2,
	      Vertex& x3,
	      const size_t& ref = 0);

  ~Tetrahedron()
  {
    ;
  }

};

#endif // TETRAHEDRON_HPP

