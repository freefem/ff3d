//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef _FATBOUNDARY_HPP_
#define _FATBOUNDARY_HPP_

#include <Method.hpp>

#include <FatBoundaryOptions.hpp>
#include <GetParameter.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <Mesh.hpp>
#include <Structured3DMesh.hpp>

#include <Problem.hpp>

/*!
  \class FatBoundary

  This class implements the fictitious domain like method developped by
  Bertrand Maury <maury@ann.jussieu.fr>.

  \author St�phane Del Pino.
*/

class FatBoundary
  : public Method
{
private:
  ReferenceCounting<BaseMatrix> __A;
  ReferenceCounting<BaseVector> __b;

  ConstReferenceCounting<Mesh> __mesh;

  ConstReferenceCounting<Problem> __problem;

public:
  GetParameter<FatBoundaryOptions> __options;

  const Problem& problem() const
  {
    return *__problem;
  }

  const Structured3DMesh& mesh() const
  {
    return dynamic_cast<const Structured3DMesh&>(*__mesh);
  }

  FatBoundary(const DiscretizationType& discretizationType,
	      ConstReferenceCounting<Mesh> mesh)
    : Method(discretizationType),
      __mesh(mesh)
  {
    ;
  }

  void Discretize (ConstReferenceCounting<Problem> problem);

  void Compute (Solution& u);

  ~FatBoundary()
  {
    ;
  }
};

#endif // _FATBOUNDARY_HPP_

