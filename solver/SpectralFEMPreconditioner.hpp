//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SPECTRAL_FEM_PRECONDITIONER
#define SPECTRAL_FEM_PRECONDITIONER

#include <Preconditioner.hpp>

/**
 * @file   SpectralFEMPreconditioner.hpp
 * @author St�phane Del Pino
 * @date   Fri Nov  2 21:15:32 2007
 * 
 * @brief  Preconditions spectral method using FEM
 * 
 */
class SpectralFEMPreconditioner
  : public Preconditioner
{
private:
  class Internal;

  ReferenceCounting<Internal>
  __internal;			/**< Internal structure  */
  
public:
  //! Initialization of the Preconditioner.
  void initializes();
  
  //! Computes \f$ z = P^{-1} r \f$.
  void computes(const Vector<real_t>& r ,
		Vector<real_t>& z) const;

  //! Computes \f$ z = P^{-T} r \f$.
  void computesTransposed(const Vector<real_t>& r ,
			  Vector<real_t>& z) const;

  /** 
   * Name of the preconditioner 
   * 
   * @return the name of the preconditioner
   */
  std::string name() const
  {
    return "Finite element preconditioner for spectral method";
  }

  /** 
   * Constructor
   * 
   * @param problem the problem to precondition
   */
  SpectralFEMPreconditioner(const Problem& problem);

  /** 
   * Destructor
   * 
   */
  ~SpectralFEMPreconditioner();
};

#endif // SPECTRAL_FEM_PRECONDITIONER
