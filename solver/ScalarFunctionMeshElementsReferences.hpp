//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SCALAR_FUNCTION_MESH_ELEMENTS_REFERENCES_HPP
#define SCALAR_FUNCTION_MESH_ELEMENTS_REFERENCES_HPP

#include <ScalarFunctionBase.hpp>
#include <ReferenceCounting.hpp>

#include <map>

class Mesh;

/**
 * @file   ScalarFunctionMeshElementsReferences.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 10:48:50 2006
 * 
 * @brief  function defined by mesh element's references
 * 
 * 
 */
class ScalarFunctionMeshElementsReferences
  : public ScalarFunctionBase
{
public:
  typedef
  std::map<size_t, ConstReferenceCounting<ScalarFunctionBase> >
  FunctionMap;			/**< @typedef std::map<size_t, ConstReferenceCounting<ScalarFunctionBase> > FunctionMap
				   association of reference and function */

private:
  FunctionMap
  __functionMap;		/**< associates refernces and
				   functions */
  ConstReferenceCounting<Mesh>
  __mesh;			/**< the mesh */

  /** 
   * Write a function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "references(elements,{mesh}";
    for (FunctionMap::const_iterator i = __functionMap.begin();
	 i != __functionMap.end(); ++i) {
      os << ',' << i->first << ':' << *i->second;
    }
    os << ')';
    return os;
  }

  /** 
   * Evaluates function at position @f$ X @f$
   * 
   * @param X position
   * 
   * @note this is a specialization according to mesh type
   * 
   * @return @f$ f_{\mbox{ref}}(X) @f$
   */
  template <typename MeshType>
  real_t __evaluate(const TinyVector<3,real_t>& X) const;

public:
  /** 
   * Evaluates function at position @f$ X @f$
   * 
   * @param X position
   * 
   * @return @f$ f_{\mbox{ref}}(X) @f$
   */
  real_t operator()(const TinyVector<3, real_t>& X) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param functionMap reference/function association
   * @param mesh given mesh
   */
  ScalarFunctionMeshElementsReferences(const FunctionMap& functionMap,
				       ConstReferenceCounting<Mesh> mesh);

  /** 
   * Constructor
   * 
   * @param f original function
   */
  ScalarFunctionMeshElementsReferences(const ScalarFunctionMeshElementsReferences& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionMeshElementsReferences();
};

#endif // SCALAR_FUNCTION_MESH_ELEMENTS_REFERENCES_HPP
