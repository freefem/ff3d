//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MESH_TETRAHEDRIZOR_HPP
#define MESH_TETRAHEDRIZOR_HPP

#include <MeshGenerator.hpp>

/**
 * @file   MeshTetrahedrizor.hpp
 * @author Stephane Del Pino
 * @date   Sat Sep  4 20:22:47 2004
 * 
 * @brief Transforms a given mesh in a tetrahedral mesh
 */
class MeshTetrahedrizor
  : public MeshGenerator
{
public:
  typedef Vector<const Cell*> CellMapping;
  
protected:
  ConstReferenceCounting<Mesh> __input; /**< given mesh */

  /** 
   * This template function builds the mesh for a given type of input
   * mesh
   * 
   * @param translateSurface tetrahedrize also the surface mesh
   */
  template <typename MeshType>
  void __build(const bool& translateSurface);

  /** 
   * Implements the motherCells() function.
   * 
   * @return the mother to cell mapping
   */
  template <typename MeshType>
  ReferenceCounting<CellMapping>
  __motherCells();

private:
  /** 
   * Copy constructor is forbiden. There is no use to copy this kind
   * of object
   * 
   * @param M a given MeshTetrahedrizor
   * 
   */
  MeshTetrahedrizor(const MeshTetrahedrizor& M);

public:

  /** 
   * Runs the tetrahedral mesh generation
   * 
   * @param translateSurface tetrahedrize also the surface mesh
   */
  virtual void run(const bool& translateSurface = true);

  /** 
   * Computes and returns a cell mapping between the original mesh and
   * the tetrahedrized one. More precisely, it associates to new cells
   * the cell it comes from.
   * 
   * @return mapping of cells
   */
  ReferenceCounting<CellMapping>
  motherCells();

  /** 
   * Constructor
   * 
   * @param inputMesh the mesh to tetrahedrize
   * 
   */
  MeshTetrahedrizor(ConstReferenceCounting<Mesh> inputMesh)
    : MeshGenerator(),
      __input(inputMesh)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~MeshTetrahedrizor()
  {
    ;
  }
};

#endif // MESH_TETRAHEDRIZOR_HPP
