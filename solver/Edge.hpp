//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef LINE_HPP
#define LINE_HPP

#include <Vertex.hpp>
#include <utility>

/**
 * @file   Edge.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 15 20:15:23 2003
 * 
 * @brief  This class provides a Edge description.
 * 
 * This class provides edge description.  An edge is defined by two
 * vertices and a reference.
 *
 * @todo This class is really hugly should rework it a lot!
 */

class Edge
{
public:
  enum {
    NumberOfVertices = 2
  };

  typedef std::pair<Vertex*, Vertex*> Pair;
private:
  Edge::Pair __verticesPair;	/**< the two vertices defining the edge */

  size_t __reference;		/**< the reference of the edge */

public:
  real_t lenght() const
  {
    return Norm(*__verticesPair.first - *__verticesPair.second);
  }

  /** 
   * Read-only access to the reference of the edge
   * 
   * 
   * @return the reference
   */
  const size_t& reference() const
  {
    return __reference;
  }

  /** 
   * Access to the reference of the edge
   * 
   * 
   * @return the reference
   */
  size_t& reference()
  {
    return __reference;
  }

  /** 
   * Access to the ith vertex of the edge
   * 
   * 
   * @return the ith vertex
   */
  inline  Vertex& operator() (const size_t& i) 
  {
    ASSERT (i<2);
    switch(i) {
    case 0:
      return *(__verticesPair.first);
    case 1:
      return *(__verticesPair.second);
    }
    return *(__verticesPair.first); // Never reached
  }

   /** 
   * Access to the ith vertex of the edge
   * 
   * 
   * @return the ith vertex
   */
  inline  const Vertex& operator() (const size_t& i) const
  {
    ASSERT (i<2);
    switch(i) {
    case 0:
      return *(__verticesPair.first);
    case 1:
      return *(__verticesPair.second);
    }
    return *(__verticesPair.first); // Never reached
  }

  /** 
   * Returns true if edges have \b only same \b vertices
   * 
   * @param e another edge
   * 
   * @return true if vertices are the same
   */
  inline bool operator==(const Edge& e) const
  {
    return (__verticesPair==e.__verticesPair);
  }

  /** 
   * Sets an order for edges
   * 
   * @param e a given edge
   * 
   * @return true is the edge is sort before the given one
   */
  inline bool operator<(const Edge& e) const
  {
    if (__verticesPair.first == e.__verticesPair.first) {
      return (__verticesPair.second < e.__verticesPair.second);
    } else {
      return (__verticesPair.first < e.__verticesPair.first);
    }
  }

  /** 
   * Sets the current edge to a given one
   * 
   * @param e a given edge
   * 
   * @return \f$ e := e' \f$
   */
  inline const Edge& operator=(const Edge& e)
  {
    __verticesPair = e.__verticesPair;
    __reference = e.__reference;

    return *this;
  }

  //! Cast to a Edge::Pair
  inline operator Pair&()
  {
    return __verticesPair;
  }

  /** 
   * Automatic cast to a Edge::Pair
   * 
   * 
   * @return the vertices pair
   */
  inline operator const Pair&() const
  {
    return __verticesPair;
  }

  /** 
   * find first common vertex
   * 
   * @param e a given edge
   * 
   * @return common vertex of two edges, 0 is none.
   */
  Vertex* firstCommonVertex(const Edge::Pair& e) const
  {
    if ((__verticesPair.first == e.first)
	or (__verticesPair.first == e.second)) {
      return __verticesPair.first;
    }

    if ((__verticesPair.second == e.first)
	or (__verticesPair.second == e.second)) {
      return __verticesPair.second;
    }

    return 0;
  }

  /** 
   * Default constructor
   *
   * @note do no initialization 
   */
  Edge()
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param E a given edge
   */
  Edge(const Edge& E)
    : __verticesPair(E.__verticesPair),
      __reference(E.__reference)
  {
    ;
  }

  /** 
   * Constructs an edge using two vertices
   * 
   * @param v1 the first vertex
   * @param v2 the second vertex
   * @param reference a given reference
   *
   * @note if reference is not provided, it is set to 0
   * @note vertices are sorted to optimization purpose
   * @warning const_cast usage is not good for design
   */
  Edge(const Vertex& v1, const Vertex& v2,
       const size_t& reference=0)
    : __reference(reference)
  {
    Vertex* V1 = const_cast<Vertex*>((&v1<&v2) ? &v1 : &v2);
    Vertex* V2 = const_cast<Vertex*>((&v1>&v2) ? &v1 : &v2);
    __verticesPair = Pair(V1, V2);
  }

  /** 
   * Constructs an edge using a pair of vertices and a given reference
   * 
   * @param vp the pair of vertices
   * @param reference the givene reference
   */
  Edge(const Edge::Pair& vp,
       const size_t& reference=0)
    : __verticesPair(vp),
      __reference(reference)
  {
    ;
  }

  /** 
   * The destructor
   * 
   */
  ~Edge()
  {
    ;
  }

  /** 
   * Prints the edge to a given stream
   * 
   * @param os the given stream
   * @param e the edge to print
   * 
   * @return the modified stream
   */
  friend std::ostream& operator << (std::ostream& os,
				    const Edge& e);
};

#endif // LINE_HPP

