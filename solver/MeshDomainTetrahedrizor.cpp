//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <MeshOfTetrahedra.hpp>
#include <MeshDomainTetrahedrizor.hpp>

#include <set>

void
MeshDomainTetrahedrizor::
run(const bool& builtLocalizationTools)
{
  // We don't need connectivity to be built
  this->MeshTetrahedrizor::run(false);

  const Domain& d = *__domain;
  const MeshOfTetrahedra& fullMesh
    = static_cast<const MeshOfTetrahedra&>(*__mesh);

  // List of vertices that are inside the domain
  Vector<bool> insideVertices(fullMesh.numberOfVertices());

  for (size_t i=0; i<fullMesh.numberOfVertices(); ++i) {
    insideVertices[i] = d.inside(fullMesh.vertex(i));
  }

  // Set of tetrahedra that will be kept 
  typedef std::set<const Tetrahedron*> KeptTetrahedra;
  KeptTetrahedra keptTetrahedra;
  for (MeshOfTetrahedra::const_iterator i(fullMesh);
       not(i.end()); ++i) {
    const Tetrahedron& t = *i;
    for (int l=0; l<MeshOfTetrahedra::CellType::NumberOfVertices; ++l) {
      if (insideVertices[fullMesh.vertexNumber(t(l))]) {
	keptTetrahedra.insert(&t);
      }
    }
  }

  ffout(3) << "- keeping " << keptTetrahedra.size()
	   << " tetrahedra over " << fullMesh.numberOfCells()
	   << '\n';

  // new create list of kept vertices
  typedef std::map<size_t, size_t> KeptVerticesNumbers;
  KeptVerticesNumbers keptVerticesNumbers;
  
  size_t vertexNumber = 0;
  for (KeptTetrahedra::const_iterator i = keptTetrahedra.begin();
       i != keptTetrahedra.end(); ++i) {
    const Tetrahedron& t = **i;
    for (int l=0; l<MeshOfTetrahedra::CellType::NumberOfVertices; ++l) {
      const size_t globalVertexNumber = fullMesh.vertexNumber(t(l));
      if (keptVerticesNumbers.find(globalVertexNumber)
	  == keptVerticesNumbers.end()) {
	keptVerticesNumbers[globalVertexNumber] = vertexNumber;
	vertexNumber++;
      }
    }    
  }

  VerticesSet* pVerticesSet = new VerticesSet(keptVerticesNumbers.size());
  VerticesSet& verticesSet = *pVerticesSet;

  VerticesCorrespondance* pCorrespondances
    = new VerticesCorrespondance(keptVerticesNumbers.size());
  VerticesCorrespondance& correspondances = *pCorrespondances;

  // renumbers vertices correspondances
  std::map<size_t, size_t> keptCorrespondances;
  for (KeptVerticesNumbers::const_iterator i = keptVerticesNumbers.begin();
       i != keptVerticesNumbers.end(); ++i) {
    keptCorrespondances[fullMesh.correspondance(i->first)] = 0;
  }

  {
    size_t n = 0;
    for (std::map<size_t, size_t>::iterator i = keptCorrespondances.begin();
	 i != keptCorrespondances.end(); ++i) {
      i->second = n;
      n++;
    }
  }

  // Copies usefull vertices
  for (KeptVerticesNumbers::const_iterator i = keptVerticesNumbers.begin();
       i != keptVerticesNumbers.end(); ++i) {
    verticesSet[i->second] = fullMesh.vertex(i->first);
    correspondances[i->second] = keptVerticesNumbers[fullMesh.correspondance(i->first)];
  }

  Vector<Tetrahedron>* pTetrahedra
    = new Vector<Tetrahedron>(keptTetrahedra.size());
  Vector<Tetrahedron>& tetrahedra = *pTetrahedra;

  size_t tetrahedronNumber = 0;
  for (KeptTetrahedra::const_iterator i = keptTetrahedra.begin();
       i != keptTetrahedra.end(); ++i) {
    const Tetrahedron& T = **i;
    
    tetrahedra[tetrahedronNumber]
      = Tetrahedron(verticesSet[keptVerticesNumbers[fullMesh.vertexNumber(T(0))]],
		    verticesSet[keptVerticesNumbers[fullMesh.vertexNumber(T(1))]],
		    verticesSet[keptVerticesNumbers[fullMesh.vertexNumber(T(2))]],
		    verticesSet[keptVerticesNumbers[fullMesh.vertexNumber(T(3))]],
		    T.reference());
    tetrahedronNumber++;
  }

  MeshOfTetrahedra* meshOfTetrahedra
    = new MeshOfTetrahedra(pVerticesSet,
			   pCorrespondances,
			   pTetrahedra,
			   0); // <= Border mesh of triangles

  __mesh = meshOfTetrahedra;
}
