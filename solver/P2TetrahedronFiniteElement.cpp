//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <P2TetrahedronFiniteElement.hpp>

TinyVector<3, real_t> P2TetrahedronFiniteElement::__massCenter(0.25, 0.25, 0.25);
const size_t

P2TetrahedronFiniteElement::
facesDOF[Tetrahedron::NumberOfFaces][P2TetrahedronFiniteElement::numberOfFaceLivingDegreesOfFreedom]
= {{1,2,3,9,8,7},
   {3,2,0,5,6,9},
   {0,1,3,8,6,4},
   {2,1,0,4,5,7}};

real_t
P2TetrahedronFiniteElement::W(const size_t& i,
			      const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    const real_t lambda = 1-x-y-z;
    return lambda*(2*lambda-1);
  }
  case 1: {
    return x*(2*x-1);
  }
  case 2: {
    return y*(2*y-1);
  }
  case 3: {
    return z*(2*z-1);
  }
    // Edges basis functions
  case 4: {
    return 4*(1-x-y-z)*x;
  }
  case 5: {
    return 4*(1-x-y-z)*y;
  }
  case 6: {
    return 4*(1-x-y-z)*z;
  }
  case 7: {
    return 4*x*y;
  }
  case 8: {
    return 4*x*z;
  }
  case 9: {
    return 4*y*z;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
P2TetrahedronFiniteElement::dxW(const size_t& i,
				const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return -3+4*x+4*y+4*z;
  }
  case 1: {
    return 4*x-1;
  }
  case 2: {
    return 0;
  }
  case 3: {
    return 0;
  }
    // Edges basis functions
  case 4: {
    return 4*(1-2*x-y-z);
  }
  case 5: {
    return -4*y;
  }
  case 6: {
    return -4*z;
  }
  case 7: {
    return 4*y;
  }
  case 8: {
    return 4*z;
  }
  case 9: {
    return 0;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
P2TetrahedronFiniteElement::dyW(const size_t& i,
				const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return -3+4*x+4*y+4*z;
  }
  case 1: {
    return 0;
  }
  case 2: {
    return 4*y-1;
  }
  case 3: {
    return 0;
  }
    // Edges basis functions
  case 4: {
    return -4*x;
  }
  case 5: {
    return 4*(1-x-2*y-z);
  }
  case 6: {
    return -4*z;
  }
  case 7: {
    return 4*x;
  }
  case 8: {
    return 0;
  }
  case 9: {
    return 4*z;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t 
P2TetrahedronFiniteElement::dzW(const size_t& i,
				const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];
  const real_t& z = X[2];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return -3+4*x+4*y+4*z;
  }
  case 1: {
    return 0;
  }
  case 2: {
    return 0;
  }
  case 3: {
    return 4*z-1;
  }
    // Edges basis functions
  case 4: {
    return -4*x;
  }
  case 5: {
    return -4*y;
  }
  case 6: {
    return 4*(1-x-y-2*z);
  }
  case 7: {
    return 0;
  }
  case 8: {
    return 4*x;
  }
  case 9: {
    return 4*y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}
