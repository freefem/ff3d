//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FFThread.hpp>

#include <StreamCenter.hpp>
#include <Timer.hpp>

#include <ReferenceCounting.hpp>

#include <FFLexer.hpp>
#include <ParametersInitialization.hpp>

#include <RealExpression.hpp>

#include <Expression.hpp>
#include <Instruction.hpp>

#include <ErrorHandler.hpp>

#include <RunningOptions.hpp>

#include <vector>
#include <fstream>

// ****************** GUI CONSOLE REDIRECTION ***************
#ifdef HAVE_GUI_LIBS

#include <QTextEdit>

void FFThread::
setConsole(QTextEdit* console)
{
  __console = console;
}

#endif // HAVE_GUI_LIBS

// **********************************************************

// The lexer pointer.
Lexer* fflexer;

// The parser function.
int ffparse();

std::vector<ReferenceCounting<Instruction> > iSet;

void FFThread::run()
{
  ThreadStaticCenter threadStaticCenter;

#ifdef HAVE_GUI_LIBS
  if (__console != 0) {
    StreamCenter::instance().setConsole(__console);
  }
#endif // HAVE_GUI_LIBS

  StreamCenter::instance().setDebugLevel(RunningOptions::instance().getVerbosity());

#ifdef HAVE_GUI_LIBS
  if (not(RunningOptions::instance().haveDisplay())) {
    fferr(0) << "Warning: Cannot open DISPLAY, falling back to text mode\n";
  }
#endif // HAVE_GUI_LIBS

#ifdef HAVE_GUI_LIBS
  if (__console == 0) {
#endif // HAVE_GUI_LIBS
    ffout(3) << "=========================\n";
    ffout(3) << "== Text mode execution ==\n";
    ffout(3) << "=========================\n";
#ifdef HAVE_GUI_LIBS
  }
#endif // HAVE_GUI_LIBS

  try {
    Timer totalTime;
    totalTime.start();
    ffout(1) << "FreeFEM3D computing thread: started\n";

    iSet.clear();

    ffout(2) << "Parsing data\n";
    ParametersInitialization();
    {
      fflexer = new FFLexer(*__in);
      ffparse();
      delete fflexer;
    }
    ffout(2) << "Parsing data: done\n";

    for (std::vector<ReferenceCounting<Instruction> >::iterator i = iSet.begin();
	 i != iSet.end(); ++i) {
      (*(*i)).execute();
    }
    totalTime.stop();
    ffout(1) << "FreeFEM3D computing thread: done [costs: " << totalTime << "]\n";
  }
  catch(ErrorHandler e) {
    e.writeErrorMessage();    
  }
  catch (std::exception e) {
    fferr(0) << "error: " << e.what() << "!\n";

    fferr(0) << "\nSTDC++ ERROR: this should not occure, please report it\n";
    fferr(0) << "\nBUG REPORT: Please send bug reports to:\n"
	     << "  ff3d-dev@nongnu.org or freefem@ann.jussieu.fr\n"
	     << "or better, use the Bug Tracking System:\n"
	     << "  http://savannah.nongnu.org/bugs/?group=ff3d\n";
  }
  catch(...) {
    fferr(0) << "error: Unknown exception caught!\n";
    fferr(0) << "\nUNEXPECTED ERROR: this should not occure, please report it\n";
    fferr(0) << "\nBUG REPORT: Please send bug reports to:\n"
	     << "  ff3d-dev@nongnu.org or freefem@ann.jussieu.fr\n"
	     << "or better, use the Bug Tracking System:\n"
	     << "  http://savannah.nongnu.org/bugs/?group=ff3d\n";
  }

  iSet.clear(); // cleans memory to avoid bad exit
}

void FFThread::setInput(std::istream& fin)
{
  __in = &fin;
}

FFThread::FFThread()
  : 
#ifdef HAVE_GUI_LIBS
  __console(0),
#endif // HAVE_GUI_LIBS
  __in(0)
{
  ;
}

FFThread::~FFThread()
{
#ifdef HAVE_GUI_LIBS
  this->wait();
#else  // HAVE_GUI_LIBS
  this->join();
#endif // HAVE_GUI_LIBS
}
