//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <BoundaryExpressionPOVRay.hpp>
#include <BoundaryPOVRay.hpp>

std::ostream&
BoundaryExpressionPOVRay::
put(std::ostream& os) const
{
  os << (*__povrayReference);
  return os;
}

void
BoundaryExpressionPOVRay::
__execute()
{
  __povrayReference->execute();

  BoundaryPOVRay* b = new BoundaryPOVRay();

  TinyVector<3> ref;
  for (size_t i=0; i<3; ++i)
    ref[i] = (*__povrayReference).value(i);

  (*b).addPOVReference(ref);
  __boundary = b;
}

BoundaryExpressionPOVRay::
BoundaryExpressionPOVRay(ReferenceCounting<Vector3Expression> r)
  : BoundaryExpression(BoundaryExpression::povray),
    __povrayReference(r)
{
  ;
}

BoundaryExpressionPOVRay::
BoundaryExpressionPOVRay(const BoundaryExpressionPOVRay& r)
  : BoundaryExpression(r),
    __povrayReference(r.__povrayReference)
{
  ;
}

BoundaryExpressionPOVRay::
~BoundaryExpressionPOVRay()
{
  ;
}
