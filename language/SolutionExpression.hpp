//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _SOLUTION_EXPRESSION_HPP_
#define _SOLUTION_EXPRESSION_HPP_

#include <Expression.hpp>
#include <Variable.hpp>

/*!
  \class SolutionExpression

  This class defines the base class of Solution expressions.

  \author Stephane Del Pino
 */
class SolutionExpression
  : public Expression
{
protected:
  ReferenceCounting<FunctionExpression> __solution;

public:
  enum SolutionType {
    undefined,
    value,
    variable
  };

  ReferenceCounting<FunctionExpression>
  operator=(ReferenceCounting<FunctionExpression> u);

private:
  SolutionExpression::SolutionType __solutionType;

public:
  ReferenceCounting<FunctionExpression> solution();

  const SolutionExpression::SolutionType& solutionType() const
  {
    return __solutionType;
  }

  SolutionExpression(const SolutionExpression& u);

  SolutionExpression(const SolutionExpression::SolutionType& t);

  virtual ~SolutionExpression();
};

class SolutionExpressionVariable
  : public SolutionExpression
{
private:
  ReferenceCounting<SolutionVariable> __solutionVariable;

  std::ostream& put(std::ostream& os) const
  {
    os << (*__solutionVariable).name() << ": " << (*(*__solutionVariable).expression());
    return os;
  }

public:
  void __execute()
  {
    ;
  }

  ReferenceCounting<SolutionVariable> variable() const
  {
    return __solutionVariable;
  }

  SolutionExpressionVariable(ReferenceCounting<SolutionVariable> r);

  SolutionExpressionVariable(const SolutionExpressionVariable& e);

  ~SolutionExpressionVariable();
};

class SolutionExpressionUndefined
  : public SolutionExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "undefined solution";
    return os;
  }

public:
  void __execute();

  SolutionExpressionUndefined(); 

  SolutionExpressionUndefined(const SolutionExpressionUndefined& u);

  ~SolutionExpressionUndefined();
};

#endif // _SOLUTION_EXPRESSION_HPP_

