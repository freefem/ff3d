//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionComposed.hpp>
#include <ScalarFunctionComposed.hpp>

void 
FunctionExpressionComposed::
__execute()
{
  __function->execute();
  __functionExpressionX->execute();
  __functionExpressionY->execute();
  __functionExpressionZ->execute();

  __scalarFunction
    = new ScalarFunctionComposed(__function->function(),
				 __functionExpressionX->function(),
				 __functionExpressionY->function(),
				 __functionExpressionZ->function());
}

FunctionExpressionComposed::
FunctionExpressionComposed(ReferenceCounting<FunctionExpression> f,
			   ReferenceCounting<FunctionExpression> X,
			   ReferenceCounting<FunctionExpression> Y,
			     ReferenceCounting<FunctionExpression> Z)
  : FunctionExpression(FunctionExpression::composed),
    __function(f),
    __functionExpressionX(X),
    __functionExpressionY(Y),
    __functionExpressionZ(Z)
{
  ;
}

FunctionExpressionComposed::
FunctionExpressionComposed(const FunctionExpressionComposed& f)
  : FunctionExpression(f),
    __function(f.__function),
    __functionExpressionX(f.__functionExpressionX),
    __functionExpressionY(f.__functionExpressionY),
    __functionExpressionZ(f.__functionExpressionZ)
{
  ;
}

FunctionExpressionComposed::
~FunctionExpressionComposed()
{
  ;
}
