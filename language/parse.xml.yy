%{ /* -*- c++ -*- */

   /*
    *  This file is part of ff3d - http://www.freefem.org/ff3d
    *  Copyright (C) 2001, 2002, 2003 St�phane Del Pino
    * 
    *  This program is free software; you can redistribute it and/or modify
    *  it under the terms of the GNU General Public License as published by
    *  the Free Software Foundation; either version 2, or (at your option)
    *  any later version.
    * 
    *  This program is distributed in the hope that it will be useful,
    *  but WITHOUT ANY WARRANTY; without even the implied warranty of
    *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *  GNU General Public License for more details.
    * 
    *  You should have received a copy of the GNU General Public License
    *  along with this program; if not, write to the Free Software Foundation,
    *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
    * 
    *  $Id$
    * 
    */

#define YYDEBUG 1

#include <Types.hpp>
#include <ErrorHandler.hpp>
#include <XMLLexer.hpp>

#include <XMLFileReader.hpp>

#include <XMLTag.hpp>
#include <XMLTree.hpp>

#include <XMLContentPosition.hpp>

#include <list>
  typedef std::list<XMLAttribute*> XMLAttributeList;

#include <string>

  extern XMLLexer* xmllexer;
  void yyerror (char* s);

#define xmllex xmllexer->xmllex

%}

%union {
  std::string* str;
  XMLAttribute* xmlattribute;
  XMLAttributeList* xmlattributelist;
  XMLTag* xmltag;
  std::istream::pos_type* position;
};

%token <str> NAME
%token <str> STRING
%token <position> POSITION
%token SPACE

%type <xmltag> opentag
%type <xmlattribute> attribute
%type <xmlattributelist> attributelist

%token LT
%token GT

/* Don't care about following shift/reduce problems: */
%expect 0

%% /* Grammar rules and actions follow */
  
input:    /* empty */
head data
{
  ;
}
;

head:
 LT '?' NAME spacelist NAME '=' STRING spacelistornot '?' GT
{
  XMLFileReader::instance().xmlTree()->setHead(*$3,*$5,*$7);
}
;

data:
  taglist
{
}
;

taglist:
  tag
{
}
| taglist tag
{
}
;

tag:
  spacelistornot opentag
{
}
| spacelistornot closetag
{
}
| spacelistornot POSITION
{
  if (not XMLFileReader::instance().xmlTree()->hasOpenTag()) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "no open tag",
		       ErrorHandler::compilation);
  }
  XMLFileReader::instance().xmlTree()->getCurrentTag()->add(new XMLContentPosition(*$2));
}
;

opentag:
  LT NAME attributelist spacelistornot  GT
{
  $$ = new XMLTag(*$2);
  for (XMLAttributeList::const_iterator i = $3->begin();
       i != $3->end(); ++i) {
    $$->add(*i);
  }
  XMLFileReader::instance().xmlTree()->addTag($$);
}
| LT NAME spacelistornot GT
{
  $$ = new XMLTag(*$2);
  XMLFileReader::instance().xmlTree()->addTag($$);
}
;

closetag:
 LT '/' NAME spacelistornot GT
{
  XMLFileReader::instance().xmlTree()->closeTag(*$3);
}
;

attributelist:
  spacelist attribute
{
  $$ = new std::list<XMLAttribute*>;
  $$->push_back($2);
}
| attributelist spacelist attribute
{
  $$ = $1;
  $$->push_back($3);
}
;

attribute:
 NAME spacelistornot '=' spacelistornot STRING
{
  $$ = new XMLAttribute(*$1,*$5);
}
;

spacelistornot:
| spacelist
{
  ;
}
;

spacelist:
SPACE
{
  ;
}
| spacelist SPACE
{
  ;
}
;

%%
     
void yyerror(char * s) {
   throw ErrorHandler("PARSED FILE",xmllexer->lineno(),
		      stringify(s)+" after '"+xmllexer->YYText()+"'",
		      ErrorHandler::compilation);
}

