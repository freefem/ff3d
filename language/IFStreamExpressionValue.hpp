//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef IFSTREAM_EXPRESSION_VALUE_HPP
#define IFSTREAM_EXPRESSION_VALUE_HPP

#include <Expression.hpp>
#include <StringExpression.hpp>

#include <fstream>
/**
 * @file   IFStreamExpressionValue.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 30 00:27:13 2006
 * 
 * @brief  ifstream expression instanciation
 */
class IFStreamExpressionValue
  : public IFStreamExpression
{
private:
  ReferenceCounting<StringExpression>
  __filename;			/**< expression for the filename */

public:
  /** 
   * Creates the ifstream
   * 
   */
  void __execute()
  {
    __filename->execute();
    __fin = new std::ifstream(__filename->value());
    if (not *__fin) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Cannot open file '"+stringify(__filename->value())+"'",
			 ErrorHandler::normal);
    }
  }

  /** 
   * Constructor
   * 
   * @param filename given filename expression
   */
  IFStreamExpressionValue(ReferenceCounting<StringExpression> filename)
    : IFStreamExpression(IFStreamExpression::value),
      __filename(filename)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param ifs 
   */
  IFStreamExpressionValue(const IFStreamExpressionValue& ifs)
    : IFStreamExpression(ifs),
      __filename(ifs.__filename)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~IFStreamExpressionValue()
  {
    ;
  }
};

#endif // IFSTREAM_EXPRESSION_VALUE_HPP
