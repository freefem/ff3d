//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2008 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionDG.hpp>

#include <MeshExpression.hpp>
#include <Information.hpp>

#include <DGFunctionBuilder.hpp>
#include <DGFunctionBase.hpp>

#include <ScalarFunctionBase.hpp>

#include <Mesh.hpp>

#include <ScalarDiscretizationTypeDG.hpp>

void
FunctionExpressionDG::
__execute()
{
  __mesh->execute();

  Information::instance().setMesh(__mesh->mesh());

  DGFunctionBuilder builder;

  if (__functionExpression != 0) {
    __functionExpression->execute();

    builder.build(ScalarDiscretizationTypeDG(__discretizationType),
		  __mesh->mesh(),
		  *(__functionExpression->function()));
  } else {
    builder.build(ScalarDiscretizationTypeDG(__discretizationType),
		  __mesh->mesh());
  }

  __scalarFunction = builder.getBuiltScalarFunction();

  // function has now been evaluated.
  __functionExpression = 0;
  Information::instance().unsetMesh();
}

ScalarDiscretizationTypeBase::Type
FunctionExpressionDG::
__DGTypeFromFEMType(const ScalarDiscretizationTypeBase::Type& d)
{
  switch(d) {
  case ScalarDiscretizationTypeBase::lagrangianFEM0: {
    return ScalarDiscretizationTypeBase::DGFEM0;
  }
  case ScalarDiscretizationTypeBase::lagrangianFEM1: {
    return ScalarDiscretizationTypeBase::DGFEM1;
  }
  case ScalarDiscretizationTypeBase::lagrangianFEM2: {
    return ScalarDiscretizationTypeBase::DGFEM2;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot convert '"+ScalarDiscretizationTypeBase::name(d)
		       +"' to a Discontinuous Galerkin type",
		       ErrorHandler::unexpected);
  }
  }
}

FunctionExpressionDG::
FunctionExpressionDG(ReferenceCounting<MeshExpression> mesh,
		      ReferenceCounting<FunctionExpression> function,
		      const ScalarDiscretizationTypeBase::Type& discretizationType)
  : FunctionExpression(FunctionExpression::dg),
    __discretizationType(__DGTypeFromFEMType(discretizationType)),
    __mesh(mesh),
    __functionExpression(function)
{
  ;
}

FunctionExpressionDG::
FunctionExpressionDG(const FunctionExpressionDG& dgFunction)
  : FunctionExpression(dgFunction),
    __discretizationType(dgFunction.__discretizationType),
    __mesh(dgFunction.__mesh),
    __functionExpression(dgFunction.__functionExpression)
{
  ;
}

FunctionExpressionDG::~FunctionExpressionDG()
{
  ;
}
