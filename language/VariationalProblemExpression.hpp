//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_PROBLEM_EXPRESSION_HPP
#define VARIATIONAL_PROBLEM_EXPRESSION_HPP

#include <Expression.hpp>

#include <ProblemExpression.hpp>

#include <VariationalFormulaExpression.hpp>
#include <VariationalDirichletListExpression.hpp>

#include <UnknownListExpression.hpp>
#include <TestFunctionExpressionList.hpp>

#include <BoundaryConditionSet.hpp>
class PDECondition;
class Boundary;

/**
 * @file   VariationalProblemExpression.hpp
 * @author Stephane Del Pino
 * @date   Wed May 29 17:32:59 2002
 * 
 * @brief  This class describes Variational Problems
 * 
 * This class describes Variational Problems
 */
class VariationalProblem;
class VariationalProblemExpression
  : public ProblemExpression
{
private:
  ReferenceCounting<VariationalFormulaExpression>
  __variationalFormula;		/**< the variational formula */

  ReferenceCounting<VariationalDirichletListExpression>
  __dirichletList;		/**< the NON-natural boundary conditions */

  ReferenceCounting<UnknownListExpression>
  __unknownList;		/**< list of unknowns */

  ReferenceCounting<TestFunctionExpressionList>
  __testFunctionList;		/**< list of testFunctions */

  ReferenceCounting<VariationalProblem>
  __variationalProblem;		/**< Variational Problem */

  /** 
   * Expression::put() overloading
   * 
   * @param os input stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__variationalFormula << '\n';
    os << *__dirichletList << '\n';
    return os;
  }

  class AlgebraicPlus;
  class AlgebraicMinus;

  /** 
   * Converts bilinear expression to variationnal form
   * 
   * @param bilinearList the bilinear expression
   * 
   */
  template <typename AlgebraicOperator>
  void __internalSetBilinear(VariationalFormulaExpression::BilinearOperatorList& bilinearList);

  /** 
   * Converts linear expression to variationnal form
   * 
   * @param linearList the linear expression
   * 
   */
  template <typename AlgebraicOperator>
  void __internalSetLinear(VariationalFormulaExpression::LinearOperatorList& linearList);

  /** 
   * Converts bilinear boundary expression to variationnal form
   * 
   * @param bilinearList the bilinear expression
   * 
   */
  template <typename AlgebraicOperator>
  void __internalSetBilinearBC(VariationalFormulaExpression::BilinearOperatorList& bilinearList);

  /** 
   * Converts linear boundary expression to variationnal form
   * 
   * @param linearList the bilinear expression
   * 
   */
  template <typename AlgebraicOperator>
  void __internalSetLinearBC(VariationalFormulaExpression::LinearOperatorList& linearList);

  /** 
   * Splites linear bounday variational expressions list
   * 
   * @param testNumber number of the test function
   * @param f the function in the linear expression
   * @param b the boundary
   */
  void __splitLinearBoundaryList(const size_t& testNumber,
				 const ScalarFunctionBase* f,
				 const Boundary* b);

  /** 
   * Splits bilinear boundary variational expression list
   * 
   * @param unknownNumber 
   * @param testNumber 
   * @param alpha 
   * @param b 
   */
  void __splitBilinearBoundaryList(const size_t& unknownNumber,
				   const size_t& testNumber,
				   const ScalarFunctionBase* alpha,
				   const Boundary* b);

  /** 
   * Splits list of boundary conditions (Dirichlet case)
   * 
   * @param bcSet the set of boundary condition currently created
   * @param pde the condition
   * @param b the boundary
   */
  void __splitBoundaryList(BoundaryConditionSet& bcSet,
			   const PDECondition* pde,
			   const Boundary* b);

public:

  /** 
   * Checks if the problem expression contains POVRay references.
   * This verification is required by standard FEM.
   * 
   * @return true if one of the boundaries is defined on a POV surface
   */
  bool hasPOVBoundary() const;

  /** 
   * Checks if the problem expression contains predefined surface
   * meshes. If it does the execution is stopped. This verification is
   * required by standard FEM.
   * 
   * @return true if one of the boundaries is defined using an
   * external mesh
   */
  bool hasPredefinedBoundary() const;

  /** 
   * 
   * Access to the variational problem
   * 
   * @return __variationalProblem
   */
  ReferenceCounting<VariationalProblem> variationalProblem();

  /** 
   * Expression::execute() overloading
   * 
   */
  void __execute();

  /** 
   * Constructs the variational problem
   * 
   * @param v a variational formula
   * @param d dirichlet boundary condtions
   * @param t the test functions set
   */
  VariationalProblemExpression(ReferenceCounting<VariationalFormulaExpression> v,
			       ReferenceCounting<VariationalDirichletListExpression> d,
			       ReferenceCounting<TestFunctionExpressionList> t);

  /** 
   * Copy constructor
   * 
   * @param vp a variational problem
   */
  VariationalProblemExpression(const VariationalProblemExpression& vp);

  /** 
   * The destructor
   *
   */
  ~VariationalProblemExpression();
};

#endif // VARIATIONAL_PROBLEM_EXPRESSION_HPP

