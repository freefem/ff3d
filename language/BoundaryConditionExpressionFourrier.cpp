//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <BoundaryConditionExpressionFourrier.hpp>

#include <FunctionExpression.hpp>
#include <BoundaryCondition.hpp>

#include <UnknownListExpression.hpp>

#include <Information.hpp>

#include <ScalarFunctionBase.hpp>
#include <Fourrier.hpp>

std::ostream&
BoundaryConditionExpressionFourrier::
put(std::ostream& os) const
{
  os << "\t\t" << (*__Alpha) << '*' << __unknownName
     <<" + dnu(" << __unknownName << ") = " << (*__g)
     << " on " << (*__boundary);
  return os;
}

void
BoundaryConditionExpressionFourrier::
__execute()
{
  __boundary->execute();
  __Alpha->execute();
  __g->execute();
  ConstReferenceCounting<ScalarFunctionBase> alpha = __Alpha->function();
  ConstReferenceCounting<ScalarFunctionBase> g = __g->function();

  ReferenceCounting<UnknownListExpression> L
    = Information::instance().getUnknownList();

  size_t n = L->number(__unknownName);

  ReferenceCounting<PDECondition> F = new Fourrier(alpha,g,n);

  __boundaryCondition
      = new BoundaryCondition(F, __boundary->boundary());
}

BoundaryConditionExpressionFourrier::
BoundaryConditionExpressionFourrier(const std::string& unknownName,
				    ReferenceCounting<FunctionExpression> alpha,
				    ReferenceCounting<FunctionExpression> g,
				    ReferenceCounting<BoundaryExpression> boundary)
  : BoundaryConditionExpression(boundary,
				unknownName,
				BoundaryConditionExpression::neumann),
    __Alpha(alpha),
    __g(g)
{
  ;
}

BoundaryConditionExpressionFourrier::
BoundaryConditionExpressionFourrier(const BoundaryConditionExpressionFourrier& f)
  : BoundaryConditionExpression(f),
    __Alpha(f.__Alpha),
    __g(f.__g)
{
  ;
}

BoundaryConditionExpressionFourrier::
~BoundaryConditionExpressionFourrier()
{
  ;
}
