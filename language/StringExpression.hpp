//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _STRING_EXPRESSION_HPP_
#define _STRING_EXPRESSION_HPP_

#include <Expression.hpp>
#include <cmath>

#include <Variable.hpp>

class StringExpression
  : public Expression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->value();
    return os;
  }

public:
  /*!
    Returns the value of the expression.
    \todo Return type should use traits.
   */
  virtual const char* value() const = 0;

  StringExpression(const StringExpression& e)
    : Expression(e)
  {
    ;
  }

  StringExpression()
    : Expression(Expression::string)
  {
    ;
  }

  virtual ~StringExpression()
  {
    ;
  }
};

class StringExpressionValue
  : public StringExpression
{
private:
  std::string __stringExpressionValue;

public:
  const char* value() const
  {
    return __stringExpressionValue.c_str();
  }

  void __execute()
  {
    ;
  }

  StringExpressionValue(const std::string& d)
    : __stringExpressionValue(d)
  {
    ;
  }

  StringExpressionValue(const StringExpressionValue& re)
    : __stringExpressionValue(re.__stringExpressionValue)
  {
    ;
  }

  ~StringExpressionValue()
  {
    ;
  }
};

class StringExpressionConcat
  : public StringExpression
{
private:
  std::string __stringExpressionValue;
  ReferenceCounting<StringExpression> __string1;
  ReferenceCounting<StringExpression> __string2;

public:
  const char* value() const
  {
    return __stringExpressionValue.c_str();
  }

  void __execute()
  {
    (*__string1).execute();
    (*__string2).execute();
    __stringExpressionValue  = (*__string1).value();
    __stringExpressionValue += (*__string2).value();
  }

  StringExpressionConcat(ReferenceCounting<StringExpression> s1,
			 ReferenceCounting<StringExpression> s2)
    : __string1(s1),
      __string2(s2)
  {
    ;
  }

  StringExpressionConcat(const StringExpressionConcat& re)
    : __stringExpressionValue(re.__stringExpressionValue),
      __string1(re.__string1),
      __string2(re.__string2)
  {
    ;
  }

  ~StringExpressionConcat()
  {
    ;
  }
};

class RealExpression;
class StringExpressionReal
  : public StringExpression
{
private:
  std::string __stringExpressionValue;
  ReferenceCounting<RealExpression> __realExpression;

public:
  const char* value() const
  {
    return __stringExpressionValue.c_str();
  }

  void __execute();

  StringExpressionReal(ReferenceCounting<RealExpression> r);

  StringExpressionReal(const StringExpressionReal& re);

  ~StringExpressionReal();
};


class StringExpressionVariable
  : public StringExpression
{
private:
  ReferenceCounting<StringVariable> __stringVariable;
  real_t __value;

public:
  const char* value() const;

  void __execute();

  StringExpressionVariable(ReferenceCounting<StringVariable> r);

  StringExpressionVariable(const StringExpressionVariable& e);

  ~StringExpressionVariable();
};

#endif // _REAL_EXPRESSION_HPP_

