//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Lexer.hpp>

#ifndef _POVLEXER_HPP_
#define _POVLEXER_HPP_

/*!
  \class POVLexer

  This class is a the specialized Lexer for the POV-Ray language.

  \author St�phane Del Pino
*/
class POVLexer : public Lexer
{
private:
  /*! This function returns the token associated to the name "word" if it
    corresponds to a Keyword, else, it returns "-1".
   */
  int isKeyword (const std::string& word);

public:

  //! The lexer function.
  int yylex();

  //! Constructs a POVLexer for given std::istream and std::ostream.
  POVLexer(std::istream& In,
	   std::ostream& Out = std::cout);

  //! Destructor.
  ~POVLexer()
  {
    ;
  }
};

#endif // _POVLEXER_HPP_

