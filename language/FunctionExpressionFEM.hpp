//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_FEM_HPP
#define FUNCTION_EXPRESSION_FEM_HPP

#include <FunctionExpression.hpp>

#include <ScalarDiscretizationTypeBase.hpp>

class MeshExpression;

/**
 * @file   FunctionExpressionFEM.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul  5 14:31:21 2006
 * 
 * @brief Manipulates finite element function expressions
 * 
 */
class FunctionExpressionFEM
  : public FunctionExpression
{
private:
  const ScalarDiscretizationTypeBase::Type
  __discretizationType;		/**< type of discretization */

  ReferenceCounting<MeshExpression>
  __mesh;			/**< mesh of discretization */

  ReferenceCounting<FunctionExpression>
  __functionExpression;		/**< expression defining the function */

public:
  /** 
   * Access to the mesh
   * 
   * @bug This function should not exist
   * @return __mesh
   */
  ReferenceCounting<MeshExpression> mesh() const
  {
#warning SHOULD NOT USE THIS FUNCTION
    return __mesh;
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param mesh mesh of discretization
   * @param e expression of the function
   * @param femType discretization type
   * 
   */
  FunctionExpressionFEM(ReferenceCounting<MeshExpression> mesh,
			ReferenceCounting<FunctionExpression> e,
			const ScalarDiscretizationTypeBase::Type& femType);

  /** 
   * Copy constructor
   * 
   * @param f given finite element function expression
   * 
   */
  FunctionExpressionFEM(const FunctionExpressionFEM& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionFEM();
};

#endif // FUNCTION_EXPRESSION_FEM_HPP
