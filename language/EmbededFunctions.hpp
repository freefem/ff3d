//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef EMBEDED_FUNCTIONS_HPP
#define EMBEDED_FUNCTIONS_HPP

#include <Types.hpp>

#include <StreamCenter.hpp>

#include <cmath>
// unary operators
real_t unaryMinus(real_t x);

bool not_(bool a);
bool and_(bool a, bool b);
bool or_ (bool a, bool b);
bool xor_(bool a, bool b);


// binary operators
real_t sum(real_t x, real_t y);
real_t difference(real_t x, real_t y);
real_t division(real_t x, real_t y);
real_t product(real_t x, real_t y);

real_t modulo(real_t x, real_t y);

// binary bool operators.
bool gt(real_t x, real_t y);
bool lt(real_t x, real_t y);
bool ge(real_t x, real_t y);
bool le(real_t x, real_t y);
bool eq(real_t x, real_t y);
bool ne(real_t x, real_t y);

// Base functions
inline real_t function_x(real_t x, real_t y, real_t z)
{
  return x;
}

inline real_t function_y(real_t x, real_t y, real_t z)
{
  return y;
}

inline real_t function_z(real_t x, real_t y, real_t z)
{
  return z;
}

/*!
  \class ExpressionBinaryOperator

  This is the base class for a statical polymorphism set of binary
  operators.

  \author Stephane Del Pino.
*/
template <typename B,
	  typename TypeExpression>
class ExpressionBinaryOperator
{
public:
  friend std::ostream& operator << (std::ostream& os,
				    const ExpressionBinaryOperator<B, TypeExpression>& bo)
  {
    bo.put(os);
    return os;
  }

  B& theBinaryOperator()
  {
    return static_cast<B&>(*this);
  }

  const B& theBinaryOperator() const
  {
    return static_cast<const B&>(*this);
  }

  std::ostream& put(std::ostream& os) const
  {
    theBinaryOperator().put(os);
    return os;
  }

  const TypeExpression& operator()(const TypeExpression& a, const TypeExpression& b) const
  {
    return theBinaryOperator().apply(a,b);
  }

  real_t operator()(real_t a, real_t b) const
  {
    return theBinaryOperator().apply(a,b);
  }
};

/*!
  \class ExpressionPlus

  computes the addition of two expressions.

  \author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionPlus
  : public ExpressionBinaryOperator<ExpressionPlus<TypeExpression>,
				    TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '+';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a, const TypeExpression& b) const
  {
    return  a+b;
  }

  real_t apply(real_t a, real_t b) const
  {
    return  a+b;
  }
};

/*!
  \class ExpressionMinus

  computes the difference of two expressions.

  \author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionMinus
  : public ExpressionBinaryOperator<ExpressionMinus<TypeExpression>,
				    TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '-';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a, const TypeExpression& b) const
  {
    return  a-b;
  }

  real_t apply(real_t a, real_t b) const
  {
    return  a-b;
  }
};

/*!
  \class ExpressionMultiplies

  computes the product of two expressions.

  \author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionMultiplies
  : public ExpressionBinaryOperator<ExpressionMultiplies<TypeExpression>,
				    TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '*';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a, const TypeExpression& b) const
  {
    return  a*b;
  }

  real_t apply(real_t a, real_t b) const
  {
    return  a*b;
  }
};

/*!
  \class ExpressionDivides

  computes the ratio of two expressions.

  \author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionDivides
  : public ExpressionBinaryOperator<ExpressionDivides<TypeExpression>,
				    TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '/';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a, const TypeExpression& b) const
  {
    return  a/b;
  }

  real_t apply(real_t a, real_t b) const
  {
    return  a/b;
  }
};

/*!
  @class ExpressionPower

  computes the ratio of two expressions.

  @author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionPower
  : public ExpressionBinaryOperator<ExpressionPower<TypeExpression>,
				    TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '^';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a, const TypeExpression& b) const
  {
    return std::pow(a,b);
  }

  real_t apply(real_t a, real_t b) const
  {
    return std::pow(a,b);
  }
};



/*!
  \class ExpressionUnaryOperator

  This is the base class for a statical polymorphism set of unary
  operators.

  \author Stephane Del Pino.
*/

template <typename U, typename TypeExpression>
class ExpressionUnaryOperator
{
public:
  friend std::ostream& operator << (std::ostream& os,
				    const ExpressionUnaryOperator<U, TypeExpression>& uo)
  {
    uo.put(os);
    return os;
  }

  U& theUnaryOperator()
  {
    return static_cast<U&>(*this);
  }

  const U& theUnaryOperator() const
  {
    return static_cast<const U&>(*this);
  }

  std::ostream& put(std::ostream& os) const
  {
    theUnaryOperator().put(os);
    return os;
  }

  const TypeExpression& operator()(const TypeExpression& a) const
  {
    return theUnaryOperator().apply(a);
  }

  real_t operator()(real_t a) const
  {
    return theUnaryOperator().apply(a);
  }
};


/*!
  \class ExpressionUnaryMinus

  computes the product of two expressions.

  \author Stephane Del Pino.
*/
template <typename TypeExpression>
class ExpressionUnaryMinus
  : public ExpressionUnaryOperator<ExpressionUnaryMinus<TypeExpression>,
				   TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    os << '-';
    return os;
  }

  const TypeExpression& apply(const TypeExpression& a) const
  {
    return -a;
  }

  real_t apply(real_t a) const
  {
    return -a;
  }
};

/*!
  Associates name to cmath functions
 */
template <real_t (*F)(real_t x)>
std::ostream& functionName(std::ostream& os)
{
  os << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
  return os;
}

template<>
std::ostream& functionName<std::log>(std::ostream& os);

template<>
std::ostream& functionName<std::sin>(std::ostream& os);

template<>
std::ostream& functionName<std::cos>(std::ostream& os);

template<>
std::ostream& functionName<std::tan>(std::ostream& os);

template<>
std::ostream& functionName<std::asin>(std::ostream& os);

template<>
std::ostream& functionName<std::acos>(std::ostream& os);

template<>
std::ostream& functionName<std::atan>(std::ostream& os);

template<>
std::ostream& functionName<std::exp>(std::ostream& os);

template<>
std::ostream& functionName<std::abs>(std::ostream& os);

template<>
std::ostream& functionName<std::sqrt>(std::ostream& os);

/*!
  Associates name to boolean functions
 */
template <bool (*F)(real_t x, real_t y)>
std::ostream& operatorName(std::ostream& os)
{
  os << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
  return os;
}

template <>
std::ostream& operatorName<gt>(std::ostream& os);

template <>
std::ostream& operatorName<lt>(std::ostream& os);

template <>
std::ostream& operatorName<ge>(std::ostream& os);

template <>
std::ostream& operatorName<le>(std::ostream& os);

template <>
std::ostream& operatorName<eq>(std::ostream& os);

template <>
std::ostream& operatorName<ne>(std::ostream& os);

/*!
  Associates name to boolean operators
 */
template <bool (*F)(bool x, bool y)>
std::ostream& binaryBooleanName(std::ostream& os)
{
  os << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
  return os;
}

template <>
std::ostream& binaryBooleanName<and_>(std::ostream& os);

template <>
std::ostream& binaryBooleanName<or_>(std::ostream& os);

template <>
std::ostream& binaryBooleanName<xor_>(std::ostream& os);

/*!
  Associates name to boolean operators
 */
template <bool (*F)(bool x)>
std::ostream& unaryBooleanName(std::ostream& os)
{
  os << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
  return os;
}

template <>
std::ostream& unaryBooleanName<not_>(std::ostream& os);

/*!
  \class ExpressionStdFunction
*/
template <typename TypeExpression,
	  real_t (*F)(real_t x)>
class ExpressionStdFunction
  : public ExpressionUnaryOperator<ExpressionStdFunction<TypeExpression, F>,
				   TypeExpression>
{
public:
  std::ostream& put(std::ostream& os) const
  {
    return functionName<F>(os);
  }

  const TypeExpression& apply(const TypeExpression& a) const
  {
    return F(a);
  }

  real_t apply(real_t a) const
  {
    return F(a);
  }
};

#endif // EMBEDED_FUNCTIONS_HPP
