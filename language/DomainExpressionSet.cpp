//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <DomainExpressionSet.hpp>

#include <Scene.hpp>

#include <SceneExpression.hpp>
#include <InsideListExpression.hpp>

#include <Domain.hpp>

std::ostream& DomainExpressionSet::put(std::ostream& os) const
{
  os << __FILE__ << ':' << __LINE__ << ": NOT IMPLEMENTED\n";
  return os;
}

ReferenceCounting<SceneExpression>
DomainExpressionSet::scene()
{
  return __scene;
}

void DomainExpressionSet::__execute()
{
  ffout(2) << "Building Domain ...\n";

  __scene->execute();
  __domain = new Domain(__scene->scene());

  if(__definition != 0) {
    __definition->execute();
    ffout(2) << "\tDomain is: " << *__definition << '\n';
    const Scene& S = *__scene->scene();
    __domain->setObjects(__definition->objects(S));
  }
  ffout(2) << "Done\n";
}

DomainExpressionSet::
DomainExpressionSet(ReferenceCounting<SceneExpression> S,
		    ReferenceCounting<InsideListExpression> def)
  : DomainExpression(DomainExpression::set),
    __scene(S),
    __definition(def)
{
  ;
}


DomainExpressionSet::
DomainExpressionSet(const DomainExpressionSet& m)
  : DomainExpression(m),
    __scene(m.__scene)
{
  ;
}

DomainExpressionSet::
~DomainExpressionSet()
{
  ;
}

