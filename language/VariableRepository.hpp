//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIABLE_PARSER_REPOSITORY_HPP
#define VARIABLE_PARSER_REPOSITORY_HPP

#include <map>
#include <vector>

#include <ReferenceCounting.hpp>
#include <Variable.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/**
 * @file   VariableRepository.hpp
 * @author St�phane Del Pino
 * @date   Sat Feb 10 14:57:45 2007
 * 
 * @brief  Variable repository during execution
 */
class VariableRepository
  : public ThreadStaticBase<VariableRepository>
{
private:
  //! The container of variables.
  typedef std::map<std::string,ReferenceCounting<Variable> > Container;
  std::vector<Container>
  __variableList;		/**< list of variable per level */

  size_t __blockLevel;		/**< current level */

  VariableRepository(const VariableRepository& vpr);

public:
  /** 
   * Search for a variable according to its name
   * 
   * @param name name of the variable
   * 
   * @return pointer to the variable
   */
  ReferenceCounting<Variable> find(const std::string& name) const;

  /** 
   * Search for a variable and returns a pointer to it
   * 
   * @param name name of the variable
   * 
   * @return pointer of the correct type to the variable
   */
  template <typename VariableType>
  ReferenceCounting<VariableType> findVariable(const std::string& name) const
  {
    ReferenceCounting<Variable> reference = this->find(name);
    Variable* variable = reference;
    ASSERT(variable != 0);

    VariableType* castedVariable = dynamic_cast<VariableType*>(variable);
    if(not(castedVariable)) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot cast "+name
			 +" of type "+stringify(variable->type()),
			 ErrorHandler::unexpected);
    }

    return castedVariable;
  }

  /** 
   * Adds the variable @p v to the list
   * 
   * @param v variable to add
   */
  void add(ReferenceCounting<Variable> v);

  /** 
   * Remove a variable named @p name
   * 
   * @param name name of the variable to remove
   */
  void remove(const std::string& name);

  /** 
   * Begins a new block
   * 
   */
  void beginBlock();

  /** 
   * Ends a new block
   * 
   */
  void endBlock();

  /** 
   * Constructor
   * 
   */
  VariableRepository();

  /** 
   * Destructor
   * 
   */
  ~VariableRepository();
};

#endif // VARIABLE_PARSER_REPOSITORY_HPP
