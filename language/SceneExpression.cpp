//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <SceneExpression.hpp>
#include <FieldExpression.hpp>
#include <Variable.hpp>

#include <Scene.hpp>
#include <Information.hpp>

#include <fstream>
#include <string>

#include <POVLexer.hpp>

#include <VariableRepository.hpp>

Scene* pScene;
POVLexer* povlexer;

// The parse function
int povparse();

ReferenceCounting<Scene> SceneExpression::scene() const
{
  ASSERT(__sceneType != SceneExpression::undefined);
  return __scene;
}

SceneExpression::SceneExpression(const SceneExpression& e)
  : Expression(e),
    __scene(e.__scene),
    __sceneType(e.__sceneType)
{
  ;
}

SceneExpression::SceneExpression(ReferenceCounting<Scene> S,
				 const SceneExpression::SceneType& t)
  : Expression(Expression::scene),
    __scene(S),
    __sceneType(t)
{
  ;
}

SceneExpression::~SceneExpression()
{
  ;
}


std::ostream& SceneExpressionPOVRay::put(std::ostream& os) const
{
  //  Scene& S = static_cast<Scene&>(*__scene);
  os << '\n' << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
  return os;
}

void SceneExpressionPOVRay::__execute()
{
  (*__filename).execute();
  const std::string& filename = (*__filename).value();

  ffout(2) << "Building Scene ... ";
  __scene = new Scene();
  ffout(2) << "done\n";

  std::ifstream POVin(filename.c_str());
  if (not(POVin)) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: cannot open file '"+stringify(filename)+"'",
		       ErrorHandler::normal);
  }

  /**********************************************************************
   * VERY IMPORTANT: PARSER/LEXER MUST KNOW WHICH SCENE IT'S WORKING ON *
   **********************************************************************/
  pScene = __scene;
  ffout(2) << "Parsing POVRay file: \"" << filename << "\"\n";

  // Initialize Lexer to read geometry.
  povlexer = new POVLexer(POVin);

  // Parse the file to read geometry.
  povparse ();

  ffout(3) << "- the scene is composed of " << (*pScene).nbObjects()
	    << " object(s)\n";
  for (size_t i=0; i<(*__scene).nbObjects(); i++)
    ffout(4) << '\t' << (*__scene).object(i) << '\n';

  ffout(2) << "Parsing POVRay file: done\n";

  Information::instance().setScene(__scene);
}

SceneExpressionPOVRay::
SceneExpressionPOVRay(ReferenceCounting<StringExpression> s)
  : SceneExpression(0, SceneExpression::povray),
    __filename(s)
{
  ;
}


SceneExpressionPOVRay::
SceneExpressionPOVRay(const SceneExpressionPOVRay& m)
  : SceneExpression(m),
    __filename(m.__filename)
{
  ;
}

SceneExpressionPOVRay::
~SceneExpressionPOVRay()
{
  ;
}

void SceneExpressionVariable::__execute()
{
  __sceneVariable = VariableRepository::instance().findVariable<SceneVariable>(__sceneName);
  __scene = __sceneVariable->expression()->scene();
}


SceneExpressionVariable::
SceneExpressionVariable(const std::string& sceneName)
  : SceneExpression(0,SceneExpression::variable),
    __sceneName(sceneName),
    __sceneVariable(0)
{
  ;
}

SceneExpressionVariable::SceneExpressionVariable(const SceneExpressionVariable& e)
  : SceneExpression(e),
    __sceneName(e.__sceneName),
    __sceneVariable(e.__sceneVariable)
{
  ;
}

SceneExpressionVariable::~SceneExpressionVariable()
{
  ;
}


std::ostream& SceneExpressionTransform::put(std::ostream& os) const
{
  os << "transform(" << (*__sceneExpression) << ',' << (*__fieldExpression) << ')';
  return os;
}

void SceneExpressionTransform::__execute()
{
  throw ErrorHandler(__FILE__, __LINE__,
		     "not yet implemented",
		     ErrorHandler::unexpected);
}

SceneExpressionTransform::
SceneExpressionTransform(const SceneExpressionTransform& s)
  : SceneExpression(s),
    __sceneExpression(s.__sceneExpression),
    __fieldExpression(s.__fieldExpression)
{
}

SceneExpressionTransform::
SceneExpressionTransform(ReferenceCounting<SceneExpression> sceneExpression,
			 ReferenceCounting<FieldExpression> fieldExpression)
  : SceneExpression(0, SceneExpression::transform),
    __sceneExpression(sceneExpression),
    __fieldExpression(fieldExpression)
{
  ;
}

SceneExpressionTransform::
~SceneExpressionTransform()
{
  ;
}



SceneExpressionUndefined
::SceneExpressionUndefined()
  : SceneExpression(0,
		    SceneExpression::undefined)
{
  ;
}

SceneExpressionUndefined
::SceneExpressionUndefined(const SceneExpressionUndefined& m)
  : SceneExpression(m)
{
  ;
}

SceneExpressionUndefined
::~SceneExpressionUndefined()
{
  ;
}

