//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FIELD_EXPRESSION_HPP
#define FIELD_EXPRESSION_HPP

#include <Expression.hpp>

#include <ReferenceCounting.hpp>
#include <FunctionExpression.hpp>

#include <FieldOfScalarFunction.hpp>
#include <vector>

/**
 * @file   FieldExpression.hpp
 * @author Stephane Del Pino
 * @date   Sun Feb  9 16:41:04 2003
 * 
 * @brief  Describes functions fields
 */
class FieldExpression
  : public Expression
{
private:
  std::vector <ReferenceCounting<FunctionExpression> > __fieldExpression;

  ReferenceCounting<FieldOfScalarFunction> __field;

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

  /** 
   * Copy constructor is forbiden
   * 
   */
  FieldExpression(const FieldExpression&);

public:
  /** 
   * Access to the field of functions
   * 
   * @return the field
   */
  ReferenceCounting<FieldOfScalarFunction> field();

  /** 
   * Read-only access to the field of functions
   * 
   * @return the field
   */
  ConstReferenceCounting<FieldOfScalarFunction> field() const;

  /** 
   * Executes the field expression
   * 
   */
  void __execute();

  /** 
   * Checks if one of the field's functions has a boundary
   * 
   * @return true if at least one function has a boundary
   */
  bool hasBoundaryExpression() const;

  /** 
   * Access to the number of components
   * 
   * @return the number of components of the field
   */
  size_t numberOfComponents() const;

  /** 
   * Adds a component to the field
   * 
   * @param functionExpression next component function
   */
  void add(ReferenceCounting<FunctionExpression> functionExpression);

  /** 
   * Constructor
   */
  FieldExpression();

  /** 
   * Destructor
   * 
   */
  ~FieldExpression();
};

#endif // FIELD_EXPRESSION_HPP
