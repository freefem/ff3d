//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FUNCTION_EXPRESSION_UNARY_MINUS_HPP
#define FUNCTION_EXPRESSION_UNARY_MINUS_HPP

#include <FunctionExpression.hpp>
/**
 * @file   FunctionExpressionUnaryMinus.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 11:21:12 2006
 * 
 * @brief This class manages unary minus operator on function
 * expressions
 * 
 */
class FunctionExpressionUnaryMinus
  : public FunctionExpression
{
private:
  ReferenceCounting<FunctionExpression>
  __givenFunction;		/**< Function foe which the opposit
				   will be taken */

public:
  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Checks if this function expression requires te use of a boundary
   * for evaluation
   * 
   * @return @b true if __givenFunction requires a boundary
   */
  bool hasBoundaryExpression() const;

  /** 
   * Constructor
   * 
   * @param f the function whose opposite will be computed
   */
  FunctionExpressionUnaryMinus(ReferenceCounting<FunctionExpression> f);

  /** 
   * Copy constructor
   * 
   * @param f a given FunctionExpressionUnaryMinus
   */
  FunctionExpressionUnaryMinus(const FunctionExpressionUnaryMinus& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionUnaryMinus();
};

#endif // FUNCTION_EXPRESSION_UNARY_MINUS_HPP
