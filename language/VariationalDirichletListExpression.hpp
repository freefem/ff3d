//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef VARIATIONAL_DIRICHLET_LIST_EXPRESSION_HPP
#define VARIATIONAL_DIRICHLET_LIST_EXPRESSION_HPP

#include <BoundaryConditionExpression.hpp>
class BoundaryConditionExpressionDirichlet;

#include <list>

/**
 * @file   VariationalDirichletListExpression.hpp
 * @author Stephane Del Pino
 * @date   Mon Jun  3 08:47:43 2002
 * 
 * @brief  A list of Dirichlet Boundary Conditions.
 * 
 */
class VariationalDirichletListExpression
  : public Expression
{
private:
  typedef
  std::list<ReferenceCounting<BoundaryConditionExpressionDirichlet> >
  ListType;

  ListType __list;		/**< The list of Dirichlet bc */

  /** 
   * Overload of Expression::put
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Checks if the boundary contains a POV-Ray boundary
   * 
   * @return true if boundary has POV-Ray boundary
   */
  bool hasPOVBoundary() const;

  /** 
   * Checks if the boundary contains a boundary on a predefined mesh
   * 
   * @return true if boundary has predefined mesh
   */
  bool hasPredefinedBoundary() const;

  typedef ListType::iterator iterator;
  typedef ListType::const_iterator const_iterator;

  /** 
   * 
   * Returns an iterator on the begining of the list
   * 
   * @return begining
   */
  iterator begin();

  /** 
   * 
   * Returns an iterator on the begining of the list
   * 
   * @return begining
   */
  const_iterator begin() const;
  /** 
   * 
   * Returns an iterator at the end of the list
   * 
   * @return end
   */
  const_iterator end() const;

  /** 
   * Overload of Expression::execute()
   * 
   */
  void __execute();

  /** 
   * Adds a Dirichlet BC in the list
   * 
   * @param d a Dirichlet BC
   */
  void add(ReferenceCounting<BoundaryConditionExpressionDirichlet> d);

  /** 
   * Default constructor
   * 
   */
  VariationalDirichletListExpression();

  /** 
   * Copy constructor
   * 
   * @param V another VariationalDirichletListExpression
   * 
   */
  VariationalDirichletListExpression(const VariationalDirichletListExpression& V);

  /** 
   * Destructor
   * 
   */
  ~VariationalDirichletListExpression();
};

#endif // VARIATIONAL_DIRICHLET_LIST_EXPRESSION_HPP

