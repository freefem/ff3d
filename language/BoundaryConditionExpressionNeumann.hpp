//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_CONDITION_EXPRESSION_NEUMANN_HPP
#define BOUNDARY_CONDITION_EXPRESSION_NEUMANN_HPP

#include <BoundaryConditionExpression.hpp>

/**
 * @file   BoundaryConditionExpressionNeumann.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 12:19:59 2006
 * 
 * @brief This class defines the class of neumann BoundaryCondition
 * expressions
 */
class BoundaryConditionExpressionNeumann
  : public BoundaryConditionExpression
{
private:
  ReferenceCounting<FunctionExpression>
  __g;				/**< @f$ g @f$ */

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param unknownName the unknown variable
   * @param g @f$ g @f$
   * @param boundary boundary expression
   */
  BoundaryConditionExpressionNeumann(const std::string& unknownName,
				     ReferenceCounting<FunctionExpression> g,
				     ReferenceCounting<BoundaryExpression> boundary);

  /** 
   * Copy constructor
   * 
   * @param n given Neumann expression
   */
  BoundaryConditionExpressionNeumann(const BoundaryConditionExpressionNeumann& n);

  /** 
   * Destructor
   * 
   */
  ~BoundaryConditionExpressionNeumann();
};

#endif // BOUNDARY_CONDITION_EXPRESSION_NEUMANN_HPP
