//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionRead.hpp>

#include <MeshExpression.hpp>
#include <StringExpression.hpp>

#include <ScalarFunctionBase.hpp>

#include <ScalarFunctionReaderBuilder.hpp>
#include <FileDescriptor.hpp>

#include <FEMFunctionBuilder.hpp>
#include <ScalarDiscretizationTypeBase.hpp>
#include <Mesh.hpp>

#include <fstream>

void FunctionExpressionRead::
__execute()
{
  __mesh->execute();
  __fileName->execute();

  const std::string& filename = __fileName->value();
  const FileDescriptor& fileDescriptor = *__fileDescriptor;

  ScalarFunctionReaderBuilder readerBuilder(filename,
					    __mesh->mesh(),
					    fileDescriptor);

  if (__functionName != 0) {
    __functionName->execute();
    readerBuilder.setFunctionName(__functionName->value());
  }

  if (__componentNumber != 0) {
    __componentNumber->execute();
    readerBuilder.setComponent(size_t(__componentNumber->realValue()));
  }

  ReferenceCounting<ScalarFunctionReaderBase> reader
    = readerBuilder.getReader();

  __scalarFunction = reader->getFunction();
}

FunctionExpressionRead::
FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		       ReferenceCounting<StringExpression> fileName,
		       ReferenceCounting<MeshExpression> mesh)
  : FunctionExpression(FunctionExpression::read),
    __fileDescriptor(descriptor),
    __fileName(fileName),
    __functionName(0),
    __componentNumber(0),
    __mesh(mesh)
{
  ;
}

FunctionExpressionRead::
FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		       ReferenceCounting<StringExpression> fileName,
		       ReferenceCounting<StringExpression> functionName,
		       ReferenceCounting<RealExpression> componentNumber,
		       ReferenceCounting<MeshExpression> mesh)
  : FunctionExpression(FunctionExpression::read),
    __fileDescriptor(descriptor),
    __fileName(fileName),
    __functionName(functionName),
    __componentNumber(componentNumber),
    __mesh(mesh)
{
  ;
}

FunctionExpressionRead::
FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		       ReferenceCounting<StringExpression> fileName,
		       ReferenceCounting<StringExpression> functionName,
		       ReferenceCounting<MeshExpression> mesh)
  : FunctionExpression(FunctionExpression::read),
    __fileDescriptor(descriptor),
    __fileName(fileName),
    __functionName(functionName),
    __componentNumber(0),
    __mesh(mesh)
{
  ;
}

FunctionExpressionRead::
FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		       ReferenceCounting<StringExpression> fileName,
		       ReferenceCounting<RealExpression> componentNumber,
		       ReferenceCounting<MeshExpression> mesh)
  : FunctionExpression(FunctionExpression::read),
    __fileDescriptor(descriptor),
    __fileName(fileName),
    __functionName(0),
    __componentNumber(componentNumber),
    __mesh(mesh)
{
  ;
}


FunctionExpressionRead::
FunctionExpressionRead(const FunctionExpressionRead& f)
  : FunctionExpression(f),
    __fileDescriptor(f.__fileDescriptor),
    __fileName(f.__fileName),
    __functionName(f.__functionName),
    __componentNumber(f.__componentNumber),
    __mesh(f.__mesh)
{
  ;
}

FunctionExpressionRead::
~FunctionExpressionRead()
{
  ;
}
