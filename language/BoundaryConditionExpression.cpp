//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <BoundaryConditionExpression.hpp>
#include <FunctionExpression.hpp>
#include <BoundaryCondition.hpp>

#include <Fourrier.hpp>
#include <Neumann.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

ConstReferenceCounting<BoundaryCondition>
BoundaryConditionExpression::boundaryCondition() const
{
  return __boundaryCondition;
}


BoundaryConditionExpression::
BoundaryConditionExpression(const BoundaryConditionExpression& e)
  : Expression(e),
    __boundaryCondition(e.__boundaryCondition),
    __boundary(e.__boundary),
    __unknownName(e.__unknownName),
    __boundaryConditionType(e.__boundaryConditionType)
{
  ;
}

BoundaryConditionExpression::
BoundaryConditionExpression(ReferenceCounting<BoundaryExpression> boundary,
			    const std::string& unknownName,
			    const BoundaryConditionExpression::BoundaryConditionType& t)
  : Expression(Expression::boundaryCondition),
    __boundary(boundary),
    __unknownName(unknownName),
    __boundaryConditionType(t)
{
  ;
}

BoundaryConditionExpression::~BoundaryConditionExpression()
{
  ;
}

