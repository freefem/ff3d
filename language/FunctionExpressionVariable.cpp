//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <FunctionExpressionVariable.hpp>
#include <ScalarFunctionBase.hpp>

#include <VariableRepository.hpp>

std::ostream&
FunctionExpressionVariable::
put(std::ostream& os) const
{
  os << __functionName;
  if (StreamCenter::instance().getDebugLevel() > 3) {
    os << '{' << (*__scalarFunction) << '}';
  }
  return os;
}

ConstReferenceCounting<FunctionVariable>
FunctionExpressionVariable::
variable() const
{
  return __functionVariable;
}

ReferenceCounting<FunctionVariable>
FunctionExpressionVariable::
variable()
{
  return __functionVariable;
}

void FunctionExpressionVariable::
__execute()
{
  __functionVariable = VariableRepository::instance().findVariable<FunctionVariable>(__functionName);
  __functionVariable->expression()->execute();

  __scalarFunction = __functionVariable->expression()->function();

  ScalarFunctionBase& function
    = const_cast<ScalarFunctionBase&>(*__scalarFunction);
  function.setName(__functionVariable->name());
}

FunctionExpressionVariable::
FunctionExpressionVariable(const std::string& functionName,
			   const ScalarDiscretizationTypeBase::Type& discretizationType)
  : FunctionExpression(FunctionExpression::variable),
    __functionName(functionName),
    __functionVariable(0),
    __discretizationType(discretizationType)
{
  ;
}

FunctionExpressionVariable::
FunctionExpressionVariable(const FunctionExpressionVariable& f)
  : FunctionExpression(f),
    __functionName(f.__functionName),
    __functionVariable(f.__functionVariable),
    __discretizationType(f.__discretizationType)
{
  ;
}

FunctionExpressionVariable::
~FunctionExpressionVariable()
{
  ;
}
