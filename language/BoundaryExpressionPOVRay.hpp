//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef BOUNDARY_EXPRESSION_POVRAY_HPP
#define BOUNDARY_EXPRESSION_POVRAY_HPP

#include <BoundaryExpression.hpp>

/**
 * @file   BoundaryExpressionPOVRay.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 15:32:25 2006
 * 
 * @brief This class defines the class of POVRay Boundary expressions
 */
class BoundaryExpressionPOVRay
  : public BoundaryExpression
{
private:
  ReferenceCounting<Vector3Expression>
  __povrayReference;		/**< Reference of the POV-Ray
				   objects */

  /** 
   * Writes the boundary expression to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Checks that the boundary is a POVRay boundary
   * 
   * @return true
   */
  bool hasPOVBoundary() const
  {
    return true;
  }

  /** 
   * Checks that the boundary is a predefined mesh boundary
   * 
   * @return false
   */
  bool hasPredefinedBoundary() const
  {
    return false;
  }

  /** 
   * Executes the expression
   * 
   */
  void __execute();

  /** 
   * Constructor
   * 
   * @param r POV-Ray reference
   */
  BoundaryExpressionPOVRay(ReferenceCounting<Vector3Expression> r);

  /** 
   * Copy constructor
   * 
   * @param r original boundary expression
   */
  BoundaryExpressionPOVRay(const BoundaryExpressionPOVRay& r);

  /** 
   * Destructor
   * 
   */
  ~BoundaryExpressionPOVRay();
};

#endif // BOUNDARY_EXPRESSION_POVRAY_HPP
