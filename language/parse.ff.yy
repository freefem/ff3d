%{ /* -*- c++ -*- */

   /*
    *  This file is part of ff3d - http://www.freefem.org/ff3d
    *  Copyright (C) 2001, 2002, 2003 St�phane Del Pino
    * 
    *  This program is free software; you can redistribute it and/or modify
    *  it under the terms of the GNU General Public License as published by
    *  the Free Software Foundation; either version 2, or (at your option)
    *  any later version.
    * 
    *  This program is distributed in the hope that it will be useful,
    *  but WITHOUT ANY WARRANTY; without even the implied warranty of
    *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *  GNU General Public License for more details.
    * 
    *  You should have received a copy of the GNU General Public License
    *  along with this program; if not, write to the Free Software Foundation,
    *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
    * 
    *  $Id$
    * 
    */
   
#include <Types.hpp>

#include <stdlib.h>
#include <cmath>
#include <iostream>
#include <vector>

  int yyerror (const char*);
  int yyerror2(const char*);

#include<FFLexer.hpp>
  extern FFLexer* fflexer;

#define fflex fflexer->fflex
#define YYERROR_VERBOSE

#include <MeshExpression.hpp>
#include <SceneExpression.hpp>

#include <InsideExpression.hpp>
#include <InsideListExpression.hpp>

#include <BooleanExpression.hpp>

#include <BoundaryConditionExpression.hpp>

#include <BoundaryConditionExpressionDirichlet.hpp>
#include <BoundaryConditionExpressionFourrier.hpp>
#include <BoundaryConditionExpressionNeumann.hpp>

#include <BoundaryConditionListExpression.hpp>

#include <BoundaryExpression.hpp>
#include <BoundaryExpressionList.hpp>
#include <BoundaryExpressionPOVRay.hpp>
#include <BoundaryExpressionReferences.hpp>
#include <BoundaryExpressionSurfaceMesh.hpp>

#include <DomainExpression.hpp>
#include <DomainExpressionAnalytic.hpp>
#include <DomainExpressionSet.hpp>
#include <DomainExpressionUndefined.hpp>
#include <DomainExpressionVariable.hpp>

#include <EmbededFunctions.hpp>
#include <ErrorHandler.hpp>
#include <Expression.hpp>

#include <FieldExpression.hpp>
#include <FieldExpressionList.hpp>

#include <FunctionExpression.hpp>
#include <FunctionExpressionBinaryOperation.hpp>
#include <FunctionExpressionCFunction.hpp>
#include <FunctionExpressionComposed.hpp>
#include <FunctionExpressionConstant.hpp>
#include <FunctionExpressionConvection.hpp>
#include <FunctionExpressionDerivative.hpp>
#include <FunctionExpressionDG.hpp>
#include <FunctionExpressionDomainCharacteristic.hpp>
#include <FunctionExpressionFEM.hpp>
#include <FunctionExpressionIntegrate.hpp>
#include <FunctionExpressionLagrange.hpp>
#include <FunctionExpressionLegendre.hpp>
#include <FunctionExpressionLinearBasis.hpp>
#include <FunctionExpressionMeshReferences.hpp>
#include <FunctionExpressionMeshCharacteristic.hpp>
#include <FunctionExpressionNormalComponent.hpp>
#include <FunctionExpressionNot.hpp>
#include <FunctionExpressionObjectCharacteristic.hpp>
#include <FunctionExpressionRead.hpp>
#include <FunctionExpressionUnaryMinus.hpp>
#include <FunctionExpressionVariable.hpp>

#include <Instruction.hpp>

#include <IntegratedExpression.hpp>
#include <IntegratedOperatorExpression.hpp>

#include <LinearExpression.hpp>

#include <MultiLinearExpression.hpp>
#include <MultiLinearFormExpression.hpp>

#include <IFStreamExpression.hpp>
#include <IFStreamExpressionUndefined.hpp>
#include <IFStreamExpressionValue.hpp>
#include <IFStreamExpressionVariable.hpp>

#include <IStreamExpression.hpp>
#include <IStreamExpressionList.hpp>

#include <OFStreamExpression.hpp>
#include <OFStreamExpressionUndefined.hpp>
#include <OFStreamExpressionValue.hpp>
#include <OFStreamExpressionVariable.hpp>

#include <OStreamExpression.hpp>
#include <OStreamExpressionList.hpp>

#include <PDEEquationExpression.hpp>

#include <PDEOperatorExpression.hpp>
#include <PDEOperatorSumExpression.hpp>

#include <PDEProblemExpression.hpp>
#include <PDESystemExpression.hpp>

#include <ProblemExpression.hpp>

#include <RealExpression.hpp>

#include <StringExpression.hpp>
#include <Stringify.hpp>

#include <SolverOptionsExpression.hpp>
#include <SolverExpression.hpp>

#include <SubOptionExpression.hpp>
#include <SubOptionListExpression.hpp>

#include <TestFunctionExpressionList.hpp>

#include <UnknownExpression.hpp>
#include <UnknownExpressionDeclaration.hpp>
#include <UnknownExpressionFunction.hpp>
#include <UnknownListExpression.hpp>

#include <VariableLexerRepository.hpp>

#include <VariationalDirichletListExpression.hpp>

#include <VariationalFormulaExpression.hpp>
#include <VariationalProblemExpression.hpp>

#include <Vector3Expression.hpp>

  //! The set of instructions.
  extern std::vector<ReferenceCounting<Instruction> > iSet;
%}

%union {
  real_t value;
  int entier;

  Expression* expression;
  RealExpression* realExp;
  BooleanExpression* boolean;
  Vector3Expression* vector3;
  StringExpression* string;

  FunctionExpression* function;
  FunctionExpressionMeshReferences::ReferencesSet* reference;
  FieldExpressionList* fieldlist;
  FieldExpression* field;

  MeshExpression* mesh;
  MeshExpressionSpectral* spectralmesh;
  MeshExpressionSurface* surfacemesh;
  MeshExpressionStructured* structured3dmesh;
  MeshExpressionOctree* octreemesh;
  MeshExpressionPeriodic::MappedReferencesList* mapreference;
  SceneExpression* scene;
  DomainExpression* domain;
  OFStreamExpression* ofstream;
  IFStreamExpression* ifstream;

  UnknownExpression* unknownExpression;
  UnknownListExpression* unknownList;

  TestFunctionVariable* testFunction;
  TestFunctionExpressionList* testFunctionList;

  BoundaryConditionListExpression* boundaryConditionList;
  BoundaryConditionExpression* boundaryCondition;
  BoundaryExpression* boundary;

  PDEOperatorExpression* pdeOperatorExpression;
  PDEScalarOperatorExpressionOrderZero* zerothOrderOp;
  PDEVectorialOperatorExpressionOrderOne* vectorialFirstOrderOp;
  PDEScalarOperatorExpressionOrderOne* firstOrderOp;
  PDEVectorialOperatorExpressionOrderTwo* matricialSecondOrderOp;

  PDEOperatorSumExpression* pdeOperatorSumExpression;

  PDEEquationExpression* pdeEquation;

  PDEProblemExpression* pdeProblem;
  PDESystemExpression* pdeSystem;
  SolverOptionsExpression* solverOptions;

  OptionExpression* option;

  OStreamExpression * ostreamExpression;
  OStreamExpressionList * ostreamExpressionList;

  IStreamExpression * istreamExpression;
  IStreamExpressionList * istreamExpressionList;

  SolverOptionsExpression* solver;
  SubOptionExpression* subOption;
  SubOptionListExpression* subOptionList;

  Variable* variable;
  RealVariable* realVariable;
  Vector3Variable* vector3Variable;
  FunctionVariable* functionVariable;
  OFStreamVariable* ofstreamVariable;
  IFStreamVariable* ifstreamVariable;

  FunctionVariable* unknownVariable;

  StringVariable* stringVariable;

  MeshVariable* meshVariable;
  SceneVariable* sceneVariable;
  DomainVariable* domainVariable;
  InsideExpression* insideExpression;
  InsideListExpression* insideListExpression;

  Instruction* instruction;

  IntegratedExpression* integratedFunction;
  IntegratedOperatorExpression* integratedOperatorExp;

  LinearExpression* linearexp;

  MultiLinearExpression* multilinearexp;
  MultiLinearExpressionSum* multilinearexpsum;
  MultiLinearFormExpression* multilinearform;
  MultiLinearFormSumExpression* multilinearformsum;

  VariationalDirichletListExpression* variationalDirichletList;
  BoundaryConditionExpressionDirichlet* boundaryConditionDirichlet;

  VariationalFormulaExpression* variationalFormula;
  VariationalProblemExpression* variationalProblem;

  ProblemExpression* problem;

  char* str;
  const std::string* aString;

  FileDescriptor::FileType fileType;
  FileDescriptor::FormatType formatType;
  ScalarDiscretizationTypeBase::Type discretizationType;
  ScalarFunctionNormal::ComponentType normalComponent;
}

%token <value> NUM "real_t value" /* real_t precision number */

/* Variables */
%token <str> VARREALID "real_t variable"
%token <str> VARVECTID "vector variable"
%token <str> VARFNCTID "function variable"
%token <str> MESHID

%token <str> VARSCENEID "scene variable"
%token <str> VARDOMAINID "domain variable"
%token <str> VARUNKNOWNID "unkown variable"
%token <str> VARTESTFUNCTIONID "test function variable"

%token <str> VAROFSTREAMID
%token <str> VARIFSTREAMID

%type <integratedFunction> operatorFunctionExp
%type <integratedFunction> operatorOrFunctionExp

%type <integratedOperatorExp> elementarylinearexp

%type <linearexp> linearexp
%type <multilinearexp> multilinearexp
%type <multilinearexpsum> multilinearsumexp
%type <multilinearform> multiLinearFormExp
%type <multilinearformsum> multiLinearSumExp
%type <variationalFormula> variationalFormula

%type <realExp> realexp  "double"
%type <vector3> vector3exp "vector"
%type <boolean> boolexp "bool"
%type <mesh> meshexp "mesh"
%type <octreemesh> octreemeshexp "octree mesh"
%type <surfacemesh> surfacemeshexp "surface mesh"
%type <structured3dmesh> structured3dmeshexp "structured 3d mesh"
%type <spectralmesh> spectralmeshexp "spectral mesh"
%type <scene> sceneexp "scene"
%type <domain> domainexp "domain"
%type <insideExpression> inoutexp 
%type <insideListExpression> domaindefexp

%type <unknownExpression> unknownexp "unknown"
%type <unknownList> unknownlistexp declareSolve "unknown list"

%type <ofstream> ofstreamexp "ofstream"
%type <ifstream> ifstreamexp "ifstream"

%type <testFunction> testfunctionexp "test function"
%type <testFunctionList> testFunctionsExp testfunctionlistexp "test function list"

%type <string> stringexp "string"
%type <string> strexp

%type <function> functionexp boolfunctionexp "function"
%type <reference> refexp "reference expression"
%type <mapreference> refmapexp "reference map expression"

%type <function> fdexp "function or real"

%type <field> fieldexp "field"
%type <field> fieldorfunctionexp componentsexp "field or function"

%type <fieldlist> fieldlistexp fieldorfieldlistexp " field or function list"

%type <boundary> singleBoundaryexp boundaryexp "boundary"

%type <pdeEquation> pdeexp "PDE"

%type <pdeOperatorExpression> pdeop "PDE operator"
%type <zerothOrderOp> zerothOrderOp "order zero PDE operator"
%type <firstOrderOp> firstOrderOp firstOrderOpSimple "order one PDE operator"
%type <vectorialFirstOrderOp> vectorialFirstOrderOp "order one vectorial PDE operator"
%type <matricialSecondOrderOp> matricialSecondOrderOp "order two PDE operator"

%type <pdeOperatorSumExpression> vectpdeop "vector PDE operator"

%type <function> rightHandSide "right hand side"

%type <problem> problemexp "problem"

%type <pdeSystem> pdesystemexp "pde system"
%type <variationalProblem> variationalexp "variational problem"

%type <variationalDirichletList> variationalDirichletList "Dirichlet boundary conditions"
%type <boundaryConditionDirichlet> variationalDirichlet "Dirichlet boundary condition"

%type <pdeProblem> pdeproblemexp "pde problem"

%type <instruction> instlist inst simpleinst otherinst declaration affectation
%type <instruction> block statement selectionStatement

%type <boundaryConditionList> boundaryConditionList "boundary condition list"
%type <boundaryCondition> boundaryCondition "boundary condition"

%type <ostreamExpression> ostreamexp "ostream"
%type <expression> ofluxexp
%type <ostreamExpressionList> ofluxexplist

%type <istreamExpression> istreamexp "istream"
%type <expression> ifluxexp
%type <istreamExpressionList> ifluxexplist

%token TRANSFORM SIMPLIFY EXTRACT PERIODIC

%token <str> DXYZ
%token <str> DXYZOP
%token <str> XYZ

%token VECTOR
%token FUNCTION "function"
%token DOUBLE "real"
%token MESH "mesh"
%token DOMAIN_ "domain" /* Cannot use DOMAIN (used by math.h) */
%token OFSTREAM "ofstream"
%token IFSTREAM "ifstream"

%token<str> STRING

/* Declaration keywords */
%token LEGENDRE LAGRANGE DGFUNCTION FEMFUNCTION OCTREEMESH STRUCTMESH SPECTRALMESH SURFMESH  SCENE

/* This is use to specify global use
   (e.g.: if one has 2 scenes and wants one to be used has default).
 */
%token USING "using"
%token FINEMESH "finemesh"
%token COARSEMESH "coarsemesh"
// statements
%token IF ELSE DO WHILE FOR EXIT

%token LT "<"
%token GT ">"
%token LEQ "<="
%token GEQ ">="
%token EQ "=="
%token NE "!="
%token OR "or"
%token XOR "xor"
%token AND "and"
%token TRUEVALUE "true"
%token FALSEVALUE "false"
%token NOT "not"
%token LRAFTER "<<"
%token RRAFTER ">>"

%token DIV "div"
%token GRAD "grad"

%token SOLVE "solve"

%token OPENBLOCK "{"
%token CLOSEBLOCK "}"

%token DNU CONVECT

%token INCR DECR

%token ONE "one"
%token REFERENCE "reference"

%token <fileType> FILETYPE "file type"
%token <formatType> FILEFORMAT "file format"
%token <discretizationType> DISCRETIZATIONTYPE "discretization type";

%token SAVE "save"
%token PLOT "plot"
%token CAT "cat"
%token EXEC "exec"
%token READ "read"
%token TETRAHEDRIZE "tetrahedrize"

%token <str> FACE
%token <str> OPTION
%type  <subOption> suboption
%type  <subOptionList> suboptionList

%type  <solverOptions> optionlist solveopts
%type  <option> option

%token INT
%token EPSILON MAXITER
%token KEEPMATRIX
%token METHOD PENALTY FATBOUNDARY
%token PDEQ /* PDE is the name of the class */
%token TESTS
%token IN ON BY INSIDE OUTSIDE POV
%token COUT
%token CIN
%token CERR

%token <str> ITEMS
%token <str> CFUNCTION
%token <str> MINMAX
// %token ABS SIN COS TAN ASIN ACOS ATAN SQRT EXP LOG

%token <normalComponent> NORMALCOMPONENT

/* Variable, Function and User Function */
%token <aString> NAME

%right '='
%left '-' '+' OR XOR
%left '*' '/' AND LEQ GEQ EQ GT LT
%left '%' NEG NOT  /* Negation--unary minus          */
%right '^' '(' ')'  /* Exponentiation and parenthesis */

%expect 9

%%

input:
  instlist
{
  iSet.push_back($1);
}
;

instlist:
/* empty */
{
  $$ = new InstructionList();
}
| instlist inst
{
  static_cast<InstructionList*>($1)->add($2);
  $$ = $1;
}
;

inst:
  simpleinst ';'
{
  $$ = $1;
}
| block
{
  $$ = $1;
}
| statement
{
  $$ = $1;
}
| otherinst
{
  $$ = $1;
}
;

simpleinst:

{
  $$ = new InstructionNone();
}
| declaration
{
  $$ = $1;
}
| affectation
{
  $$ = $1;
}
;

block:
  openblockexp instlist closeblockexp
{
  InstructionList* i= new InstructionList();
  i->add(new InstructionBlockBegin());
  i->add($2);
  i->add(new InstructionBlockEnd());
  $$ = i;
}
;

openblockexp:
  OPENBLOCK
{
  VariableLexerRepository::instance().beginBlock();
}
;

closeblockexp:
 CLOSEBLOCK
{
  VariableLexerRepository::instance().endBlock();
}
;


statement:
  selectionStatement
{
  $$ = $1;
}
| DO inst WHILE '(' boolexp ')'
{
  $$ = new InstructionDoWhileStatement($2,$5);
}
| WHILE '(' boolexp ')' inst
{
  $$ = new InstructionWhileStatement($3,$5);
}
| FOR '(' simpleinst ';' boolexp ';' simpleinst ')' inst
{
  $$ = new InstructionForStatement($3,$5,$7,$9);
}
;

selectionStatement:
  IF '(' boolexp ')' inst
{
  ReferenceCounting<Instruction> I
    = new InstructionNone();
  $$ = new InstructionIfStatement($3,$5,I);
}
| IF '(' boolexp ')' inst ELSE inst
{
  $$ = new InstructionIfStatement($3,$5,$7);
}
;

declaration:
  DOUBLE NAME
{
  //! Default value is set to 0.
  VariableLexerRepository::instance().add(*$2,VARREALID);
  ReferenceCounting<RealExpression> zero
    = new RealExpressionValue(0);
  $$ = new InstructionDeclaration<RealExpression, RealVariable>(*$2, zero);
}
| DOUBLE NAME '=' realexp
{
  VariableLexerRepository::instance().add(*$2,VARREALID);
  $$ = new InstructionDeclaration<RealExpression, RealVariable>(*$2,$4);
}
| VECTOR NAME
{
  VariableLexerRepository::instance().add(*$2,VARVECTID);
  ReferenceCounting<RealExpression> zero
    = new RealExpressionValue(0);
  ReferenceCounting<Vector3Expression> zeroV
    = new Vector3ExpressionValue(zero,zero,zero);
  $$ = new InstructionDeclaration<Vector3Expression, Vector3Variable>(*$2,zeroV);
}
| VECTOR NAME '=' vector3exp
{
  VariableLexerRepository::instance().add(*$2,VARVECTID);
  $$ = new InstructionDeclaration<Vector3Expression, Vector3Variable>(*$2,$4);
}
| FUNCTION NAME
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,zero);
}
| LAGRANGE NAME '(' meshexp ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionLagrange($4, zero);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| LEGENDRE NAME '(' meshexp ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionLegendre($4, zero);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| FEMFUNCTION NAME '(' meshexp ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionFEM($4, zero,
				ScalarDiscretizationTypeBase::lagrangianFEM1);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| FEMFUNCTION NAME '(' meshexp ':' DISCRETIZATIONTYPE ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);

  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionFEM($4, zero, $6);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| DGFUNCTION NAME '(' meshexp ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionDG($4, zero,
			       ScalarDiscretizationTypeBase::lagrangianFEM1);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| DGFUNCTION NAME '(' meshexp ':' DISCRETIZATIONTYPE ')'
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);

  ReferenceCounting<RealExpression> __zero
    = new RealExpressionValue(0.);

  ReferenceCounting<FunctionExpression> zero
    = new FunctionExpressionConstant(__zero);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionDG($4, zero, $6);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| FUNCTION NAME '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,$4);
}
| LAGRANGE NAME '(' meshexp ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionLagrange($4, $7);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| LEGENDRE NAME '(' meshexp ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);

  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionLegendre($4, $7);

  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| FEMFUNCTION NAME '(' meshexp ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionFEM($4, $7,				
				ScalarDiscretizationTypeBase::lagrangianFEM1);
  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| FEMFUNCTION NAME '(' meshexp ':' DISCRETIZATIONTYPE ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionFEM($4, $9, $6);
  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| DGFUNCTION NAME '(' meshexp ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionDG($4, $7,				
			       ScalarDiscretizationTypeBase::lagrangianFEM1);
  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| DGFUNCTION NAME '(' meshexp ':' DISCRETIZATIONTYPE ')' '=' fdexp
{
  VariableLexerRepository::instance().add(*$2,VARFNCTID);
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionDG($4, $9, $6);
  $$ = new InstructionDeclaration<FunctionExpression, FunctionVariable>(*$2,f);
}
| MESH NAME
{
  VariableLexerRepository::instance().add(*$2,MESHID);
  ReferenceCounting<MeshExpression> M = new MeshExpressionUndefined();
  $$ = new InstructionDeclaration<MeshExpression, MeshVariable>(*$2,M);
}
| MESH NAME '=' meshexp
{
  VariableLexerRepository::instance().add(*$2,MESHID);
  $$ = new InstructionDeclaration<MeshExpression, MeshVariable>(*$2,$4);
}
| SCENE NAME
{
  VariableLexerRepository::instance().add(*$2,VARSCENEID);
  ReferenceCounting<SceneExpression> M = new SceneExpressionUndefined();
  $$ = new InstructionDeclaration<SceneExpression, SceneVariable>(*$2,M);
}
| SCENE NAME '=' sceneexp
{
  VariableLexerRepository::instance().add(*$2,VARSCENEID);
  $$ = new InstructionDeclaration<SceneExpression, SceneVariable>(*$2,$4);
}
| DOMAIN_ NAME
{
  VariableLexerRepository::instance().add(*$2,VARDOMAINID);
  ReferenceCounting<DomainExpression> D
    = new DomainExpressionUndefined();
  $$ = new InstructionDeclaration<DomainExpression,
    DomainVariable>(*$2,D);
}
| DOMAIN_ NAME '=' domainexp
{
  VariableLexerRepository::instance().add(*$2,VARDOMAINID);

  $$ = new InstructionDeclaration<DomainExpression,
    DomainVariable>(*$2,$4);
}
| OFSTREAM NAME
{
  VariableLexerRepository::instance().add(*$2,VAROFSTREAMID);

  ReferenceCounting<OFStreamExpression> ofstreamExpression
    = new OFStreamExpressionUndefined();
  $$ = new InstructionDeclaration<OFStreamExpression,
                                  OFStreamVariable>(*$2,ofstreamExpression);
}
| OFSTREAM NAME '=' ofstreamexp
{
  VariableLexerRepository::instance().add(*$2,VAROFSTREAMID);

  $$ = new InstructionDeclaration<OFStreamExpression,
                                  OFStreamVariable>(*$2,$4);
}
| IFSTREAM NAME
{
  VariableLexerRepository::instance().add(*$2,VARIFSTREAMID);

  ReferenceCounting<IFStreamExpression> ifstreamExpression
    = new IFStreamExpressionUndefined();
  $$ = new InstructionDeclaration<IFStreamExpression,
                                  IFStreamVariable>(*$2,ifstreamExpression);
}
| IFSTREAM NAME '=' ifstreamexp
{
  VariableLexerRepository::instance().add(*$2,VARIFSTREAMID);

  $$ = new InstructionDeclaration<IFStreamExpression,
                                  IFStreamVariable>(*$2,$4);
}
;

affectation:
  VARREALID '=' realexp
{
  $$ = new InstructionAffectation<RealExpression, RealVariable>($1,$3);
}
| VARREALID INCR
{
  $$ = new InstructionRealExpressionIncrement($1);
}
| INCR VARREALID
{
  $$ = new InstructionRealExpressionIncrement($2);
}
| VARREALID DECR
{
  $$ = new InstructionRealExpressionDecrement($1);
}
| DECR VARREALID
{
  $$ = new InstructionRealExpressionDecrement($2);
}
| VARVECTID '=' vector3exp
{
  $$ = new InstructionAffectation<Vector3Expression, Vector3Variable>($1,$3);
}
| VARFNCTID '=' fdexp
{
  $$ = new InstructionAffectation<FunctionExpression, FunctionVariable>($1,$3);
}
| MESHID '=' meshexp
{
  $$ = new InstructionAffectation<MeshExpression, MeshVariable>($1,$3);
}
| VARDOMAINID '=' domainexp
{
  $$ = new InstructionAffectation<DomainExpression, DomainVariable>($1,$3);
}
| VARSCENEID '=' sceneexp
{
  $$ = new InstructionAffectation<SceneExpression, SceneVariable>($1,$3);
}
| VAROFSTREAMID '=' ofstreamexp
{
  $$ = new InstructionAffectation<OFStreamExpression,
                                  OFStreamVariable>($1,$3);
}
| VARIFSTREAMID '=' ifstreamexp
{
  $$ = new InstructionAffectation<IFStreamExpression,
                                  IFStreamVariable>($1,$3);
}
;

declareSolve:
  SOLVE '(' unknownlistexp ')'
{
  $$ = $3;
}
;

otherinst:
  ostreamexp ofluxexplist ';'
{
  $$ = new InstructionOutput($1,$2);
}
| istreamexp ifluxexplist ';'
{
  $$ = new InstructionInput($1,$2);
}
| CAT '(' stringexp ')' ';'
{
  $$ = new InstructionCat($3);
}
| EXEC '(' stringexp ')' ';'
{
  $$ = new InstructionExec($3);
}
| PLOT '(' meshexp ')' ';'
{
  $$ = new InstructionPlot($3);
}
| PLOT '(' functionexp ',' meshexp ')' ';'
{
  $$ = new InstructionPlot($5, $3);
}
| SAVE '(' FILEFORMAT ',' stringexp ',' meshexp ')' ';'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new InstructionSaveMesh(f, $5, $7);
}
| SAVE '(' FILEFORMAT ',' stringexp ',' meshexp ',' FILETYPE ')' ';'
{
  FileDescriptor* f = new FileDescriptor($3, $9);
  $$ = new InstructionSaveMesh(f, $5, $7);
}
| SAVE '(' FILEFORMAT ',' stringexp ',' fieldorfieldlistexp ',' meshexp ')' ';'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new InstructionSaveFieldList(f, $5, $7, $9);
}
| SAVE '(' FILEFORMAT ',' stringexp ',' fieldorfieldlistexp ',' meshexp ',' FILETYPE ')' ';'
{
  FileDescriptor* f = new FileDescriptor($3, $11);
  $$ = new InstructionSaveFieldList(f, $5, $7, $9);
}
| declareSolve IN domainexp BY meshexp solveopts openblockexp problemexp closeblockexp
{
  $$ = new InstructionList();

  ReferenceCounting<SolverExpression> solver
    = new SolverExpression($1, $5, $6, $8, $3);

  $$ = new InstructionEvaluation<SolverExpression>(solver);
}
| declareSolve IN meshexp solveopts openblockexp problemexp closeblockexp
{
  $$ = new InstructionList();

  ReferenceCounting<SolverExpression> solver
    = new SolverExpression($1, $3, $4, $6);

  $$ = new InstructionEvaluation<SolverExpression>(solver);
}
| USING sceneexp ';'
{
  $$ = new InstructionUsingScene($2);
}
| COARSEMESH ';'
{
  $$ = new InstructionCoarseMesh(true);
}
| FINEMESH ';'
{
  $$ = new InstructionCoarseMesh(false);
}
| EXIT ';'
{
  $$ = new InstructionExit();
}
| EXIT '(' realexp ')' ';'
{
  $$ = new InstructionExit($3);  
}
;

fieldorfieldlistexp:
  fieldorfunctionexp
{
  $$ = new FieldExpressionList;
  $$->add($1);
}
| OPENBLOCK fieldlistexp CLOSEBLOCK
{
  $$ = $2;
}
;

fieldlistexp:
  fieldorfunctionexp
{
  $$ = new FieldExpressionList;
  $$->add($1);
}
| fieldlistexp ',' fieldorfunctionexp
{
  $$ = $1;
  $1->add($3);
}
;

fieldorfunctionexp:
  fdexp
{
  $$ = new FieldExpression;
  $$->add($1);
}
| fieldexp
{
  $$ = $1;
}
;

fieldexp:
  '[' componentsexp ']'
{
  $$ = $2;
}
;

componentsexp:
  fdexp
{
  $$ = new FieldExpression;
  $$->add($1);
}
| componentsexp ',' fdexp
{
  $$ = $1;
  $$->add($3);
}
;

ostreamexp:
  COUT
{
  $$ = new OStreamExpression(&ffout(0));
}
| CERR
{
  $$ = new OStreamExpression(&fferr(0));
}
| VAROFSTREAMID
{
  $$ = new OStreamExpression($1);
}
;

istreamexp:
  CIN
{
  $$ = new IStreamExpression(&std::cin);
}
| VARIFSTREAMID
{
  $$ = new IStreamExpression($1);
}
;

ofluxexplist:
{
  $$ = new OStreamExpressionList();
}
| ofluxexplist LRAFTER ofluxexp
{
  (*$$) << $3;
}
;

ifluxexplist:
{
  $$ = new IStreamExpressionList();
}
| ifluxexplist RRAFTER ifluxexp
{
  (*$$) >> $3;
}
;

ofluxexp:
  stringexp
{
  $$ = $1;
}
| functionexp
{
  $$ = $1;
}
| realexp
{
  $$ = $1;
}
| vector3exp
{
  $$ = $1;
}
;

ifluxexp:
  VARREALID
{
  $$ = new RealExpressionVariable($1);
}
;

meshexp:
  structured3dmeshexp
{
  $$ = $1;
}
| spectralmeshexp
{
  $$ = $1;
}
| surfacemeshexp
{
  $$ = $1;
}
| octreemeshexp
{
  $$ = $1;
}
| MESHID
{
  $$ = new MeshExpressionVariable($1);
}
| READ '(' FILEFORMAT ',' stringexp ')'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new MeshExpressionRead(f, $5);
}
| TETRAHEDRIZE '(' meshexp ')'
{
  $$ = new MeshExpressionTetrahedrize($3);
}
| TETRAHEDRIZE '(' domainexp ',' meshexp ')'
{
  $$ = new MeshExpressionTetrahedrizeDomain($5, $3);
}
| EXTRACT '(' meshexp ',' realexp ')'
{
  $$ = new MeshExpressionExtract($3, $5);
}
| TRANSFORM '(' meshexp ',' fieldexp ')'
{
  $$ = new MeshExpressionTransform($3, $5);
}
| SIMPLIFY '(' meshexp ')'
{
  $$ = new MeshExpressionSimplify($3);
}
| PERIODIC '(' meshexp ',' refmapexp ')'
{
  $$ = new MeshExpressionPeriodic($3,$5);
}
;

refmapexp:
  realexp ':' realexp
{
  $$ = new MeshExpressionPeriodic::MappedReferencesList();
  (*$$).push_back(std::make_pair($1,$3));
}
| refmapexp ',' realexp ':' realexp
{
  $$ = $1;
  (*$$).push_back(std::make_pair($3,$5));
}
;

octreemeshexp:
  OCTREEMESH '(' domainexp ',' meshexp ',' realexp')'
{
  $$ = new MeshExpressionOctree($3,$5,$7);
}
;

structured3dmeshexp:
  STRUCTMESH '(' vector3exp ',' vector3exp ',' vector3exp ')'
{
  $$ = new MeshExpressionStructured($3,$5,$7);
}
;

spectralmeshexp:
  SPECTRALMESH '(' vector3exp ',' vector3exp ',' vector3exp ')'
{
  $$ = new MeshExpressionSpectral($3,$5,$7);
}
;

surfacemeshexp:
  SURFMESH '(' domainexp ',' meshexp ')'
{
  $$ = new MeshExpressionSurface($3,$5);
}
| SURFMESH '(' meshexp ')'
{
  $$ = new MeshExpressionSurface($3);
}
;

sceneexp:
  POV '(' stringexp ')'
{
  $$ = new SceneExpressionPOVRay($3);
}
| VARSCENEID
{
  $$ = new SceneExpressionVariable($1);
}
| TRANSFORM '(' sceneexp ',' fieldexp ')'
{
  $$ = new SceneExpressionTransform($3,$5);
}

;

domainexp:
  DOMAIN_ '(' sceneexp ',' domaindefexp ')'
{
  $$ = new DomainExpressionSet($3, $5);
}
| DOMAIN_ '(' boolfunctionexp ')'
{
  $$ = new DomainExpressionAnalytic($3);
}
| VARDOMAINID
{
  $$ = new DomainExpressionVariable($1);
}
;

ofstreamexp:
  OFSTREAM '(' stringexp ')'
{
  $$ = new OFStreamExpressionValue($3);
}
| VAROFSTREAMID
{
  $$ = new OFStreamExpressionVariable($1);
}
;

ifstreamexp:
  IFSTREAM '(' stringexp ')'
{
  $$ = new IFStreamExpressionValue($3);
}
| VARIFSTREAMID
{
  $$ = new IFStreamExpressionVariable($1);
}
;

domaindefexp:
  inoutexp
{
  $$ = new InsideListExpressionLeaf($1);
}
| '(' domaindefexp ')'
{
  $$ = $2;
}
| NOT domaindefexp %prec NEG
{
  $$ = new InsideListExpressionNot($2);
}
| domaindefexp AND domaindefexp
{
  $$ = new InsideListExpressionAnd($1,$3);
}
| domaindefexp OR domaindefexp
{
  $$ = new InsideListExpressionOr($1,$3);
}
;

inoutexp:
  INSIDE '(' vector3exp')'
{
  $$ = new InsideExpression(true,$3);
}
| OUTSIDE '(' vector3exp')'
{
  $$ = new InsideExpression(false,$3);
}
;

solveopts:
{
  $$ = new SolverOptionsExpression();
}
| optionlist
{
  $$ = $1;
}
;

optionlist:
  option
{
  $$ = new SolverOptionsExpression();
  (*$$).add($1);
}
| optionlist ',' option
{
  $$ = $1;
  (*$$).add($3);
}
;

option:
  OPTION '(' suboptionList  ')'
{
  ReferenceCounting<StringExpression> S
    = new StringExpressionValue($1);
  $$ = new OptionExpression(S,$3);
  delete [] $1;
}
;

suboptionList:
  suboption
{
  $$ = new SubOptionListExpression;
  (*$$).add($1);
}
| suboption ',' suboptionList
{
  $$ = $3;
  (*$$).add($1);
}
;

suboption:
  OPTION '=' OPTION
{
  ReferenceCounting<StringExpression> S1
    = new StringExpressionValue($1);
  ReferenceCounting<StringExpression> S2
    = new StringExpressionValue($3);

  $$ = new SubOptionExpressionString(S1,S2);

  delete [] $1;
  delete [] $3;
}
| OPTION '=' realexp
{
  ReferenceCounting<StringExpression> S
    = new StringExpressionValue($1);
  $$ = new SubOptionExpressionReal(S,$3);

  delete [] $1;
}
;

problemexp:
  pdesystemexp
{
  $$ = $1;
}
| variationalexp
{
  $$ = $1;
}
;

variationalexp:
  testFunctionsExp
{
  (*fflexer).switchToContext(FFLexer::variationalContext);
}
  variationalFormula ';' variationalDirichletList
{
  $$ = new VariationalProblemExpression($3,$5,$1);

  (*fflexer).switchToContext(FFLexer::defaultContext);
}
;

testFunctionsExp:
  TESTS '(' testfunctionlistexp ')'
{
  $$ = $3;
}
;

testfunctionlistexp:
  testfunctionexp
{
  $$ = new TestFunctionExpressionList();
  (*$$).add($1);
}
| testfunctionlistexp ',' testfunctionexp
{
  $$ = $1;
  (*$$).add($3);
}
;

testfunctionexp:
  NAME
{
  std::string name(*$1);
  VariableLexerRepository::instance().add(name,VARTESTFUNCTIONID);
  $$ = new TestFunctionVariable(name);
}
;

variationalFormula:
  multiLinearSumExp '=' NUM
{
  if ($3 != 0) {
    yyerror("Should be equal to 0");
  }
  $$ = new VariationalFormulaExpression($1);
}
| multiLinearSumExp '=' multiLinearSumExp
{
  (*$1) -= (*$3);
  (*$3).clear();
  $$ = new VariationalFormulaExpression($1);
}
;

multiLinearSumExp:
  multiLinearFormExp
{
  $$ = new MultiLinearFormSumExpression();
  (*$$).plus($1);
}
| multiLinearSumExp '+' multiLinearFormExp
{
  $$ = $1;
  (*$$).plus($3);
}
| multiLinearSumExp '-' multiLinearFormExp
{
  $$ = $1;
  (*$$).minus($3);
}
| '-' multiLinearFormExp %prec NEG
{
  $$ = new MultiLinearFormSumExpression();
  (*$$).minus($2);
}
;

multiLinearFormExp:
  INT '[' boundaryexp ']'  '(' multilinearsumexp ')'
{
  $$ = new MultiLinearFormExpression($6, MultiLinearFormExpression::twoD, $3);
}
| INT '(' multilinearsumexp ')'
{
  $$ = new MultiLinearFormExpression($3, MultiLinearFormExpression::threeD);
}
;

multilinearsumexp:
  multilinearexp
{
  $$ = new MultiLinearExpressionSum();
  (*$$).plus($1);
}
| multilinearsumexp '+'  multilinearexp
{
  $$ = $1;
  (*$$).plus($3);

}
| multilinearsumexp '-'  multilinearexp
{
  (*$$).minus($3);
}
| '-'  multilinearexp %prec NEG
{
  $$ = new MultiLinearExpressionSum();
  (*$$).minus($2);
}
;

multilinearexp:
  linearexp
{
  $$ = new MultiLinearExpression($1);
}
| multilinearexp '*' linearexp
{
  $$ = $1;
  (*$$).times($3);
}
| multilinearexp '*' functionexp
{
  $$ = $1;
  (*$$).times($3);
}
| multilinearexp '*' realexp
{
  $$ = $1;
  (*$$).times($3);
}
| '(' multilinearexp ')'
{
  $$ = $2;
}
;


linearexp:
  elementarylinearexp
{
  $$ = new LinearExpressionElementary($1);
}
| functionexp '*' elementarylinearexp
{
  $$ = new LinearExpressionElementaryTimesFunction($3,$1);
}
| realexp '*' elementarylinearexp
{
  $$ = new LinearExpressionElementaryTimesReal($3,$1);
}
;

elementarylinearexp:
  operatorFunctionExp
{
  $$ = new IntegratedOperatorExpressionOrderZero($1);
}
| GRAD '(' operatorOrFunctionExp ')'
{
  $$ = new IntegratedOperatorExpressionGrad($3);
}
| DXYZOP '(' operatorOrFunctionExp ')'
{
  $$ = new IntegratedOperatorExpressionDx($1,$3);
}
| '[' elementarylinearexp ']'
{
  $2->setJump();
  $$ = $2;
}
| OPENBLOCK elementarylinearexp CLOSEBLOCK 
{
  $2->setMean();
  $$ = $2;
}
;

operatorOrFunctionExp:
  operatorFunctionExp
{
  $$ = $1;
}
| functionexp
{
  $$ = new IntegratedExpressionFunctionExpression($1);
}
;

operatorFunctionExp:
  VARUNKNOWNID
{  
  $$ = new IntegratedExpressionUnknown($1);
}
| VARTESTFUNCTIONID
{
  $$ = new IntegratedExpressionTest($1);
}
;

variationalDirichletList:
{
  $$ = new VariationalDirichletListExpression;
}

| variationalDirichletList variationalDirichlet ';'
{
  $$ = $1;
  (*$$).add($2);
}
;

variationalDirichlet:
  VARUNKNOWNID '=' fdexp ON boundaryexp
{
  $$ = new BoundaryConditionExpressionDirichlet($1,$3,$5);
}
;

pdesystemexp:
  pdeproblemexp
{
  $$ = new PDESystemExpression();
  (*$$).add($1);
}
| pdesystemexp pdeproblemexp
{
  $$ = $1;
  (*$$).add($2);
}
;

pdeproblemexp:
  PDEQ '(' VARUNKNOWNID ')' pdeexp ';' boundaryConditionList
{
  $$ = new PDEProblemExpressionDescription($3, $5, $7);
}
;

pdeexp:
  vectpdeop '='rightHandSide
{
  $$ = new PDEEquationExpression($1,$3);
}
;

rightHandSide:
  fdexp
{
  $$ = $1;
}
;

boundaryConditionList:
  /* No Boundary Conditions */
{
  $$ = new BoundaryConditionListExpressionSet();
}
| boundaryCondition ';' boundaryConditionList
{
  static_cast<BoundaryConditionListExpressionSet*>($3)->add($1);
  $$ = $3;
}
;

vectpdeop:
  pdeop
{
  $$ = new PDEOperatorSumExpression();
  (*$$).add($1);
}
| '(' vectpdeop ')'
{
  $$ = $2;
}
|  '-' vectpdeop %prec NEG
{
  (*$2).unaryMinus();
  $$ = $2;
}
|  vectpdeop '+' vectpdeop
{
  $$ = $3;
  (*$$).add($1);
}
| vectpdeop '-' vectpdeop
{
  $$ = $1;
  (*$$).minus($3);
}
;

pdeop:
  DIV '(' GRAD '(' VARUNKNOWNID ')' ')'
{
  RealExpression* One = new RealExpressionValue(1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new PDEScalarOperatorExpressionDivMuGrad($5,OneFunction);
}
| DIV '(' realexp '*' GRAD '(' VARUNKNOWNID ')' ')'
{
  ReferenceCounting<FunctionExpression> F
    = new FunctionExpressionConstant($3);
  $$ = new PDEScalarOperatorExpressionDivMuGrad($7,F);
}
| DIV '(' functionexp '*' GRAD '(' VARUNKNOWNID ')' ')'
{
  $$ = new PDEScalarOperatorExpressionDivMuGrad($7,$3);
}
| matricialSecondOrderOp
{
  $$ = $1;
}
| firstOrderOp
{
  $$ = $1;
}
| zerothOrderOp
{
  $$ = $1;
}
;

zerothOrderOp:
  VARUNKNOWNID
{
  RealExpression* One = new RealExpressionValue(1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new PDEScalarOperatorExpressionOrderZero($1,OneFunction);
}
| functionexp '*' VARUNKNOWNID
{
  $$ = new PDEScalarOperatorExpressionOrderZero($3,$1);
}
| realexp '*' VARUNKNOWNID
{
  ReferenceCounting<FunctionExpression> F
    = new FunctionExpressionConstant($1);
  $$ = new PDEScalarOperatorExpressionOrderZero($3,F);
}
;

firstOrderOp:
  firstOrderOpSimple
{
  $$ = $1;
}
| functionexp '*' firstOrderOp
{
  $$ = $3;
  (*$3) *= $1;
}
| realexp '*' firstOrderOp
{
  
  ReferenceCounting<FunctionExpression> F = new FunctionExpressionConstant($1);
  $$ = $3;
  (*$3) *= F;
}
;

firstOrderOpSimple:
  DXYZ '(' VARUNKNOWNID ')'
{
  RealExpression* One = new RealExpressionValue(1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new PDEScalarOperatorExpressionOrderOne($1,
					       $3,
					       OneFunction);
}
;

matricialSecondOrderOp:
  DXYZ '(' vectorialFirstOrderOp ')'
{
  $$ = new PDEVectorialOperatorExpressionOrderTwo();
  (*$$).add($1,$3);
}
;

vectorialFirstOrderOp:
  firstOrderOpSimple
{
  $$ = new PDEVectorialOperatorExpressionOrderOne();
  (*$$) += $1;
}
| '-' vectorialFirstOrderOp %prec NEG
{
  $$ = new PDEVectorialOperatorExpressionOrderOne();
  (*$$) -= $2;
}
| vectorialFirstOrderOp '+' vectorialFirstOrderOp
{
  $$ = $1;
  (*$$) += $3;
}
| vectorialFirstOrderOp '-' vectorialFirstOrderOp
{
  $$ = $1;
  (*$$) -= $3;
}
| functionexp '*' vectorialFirstOrderOp
{
  $$ = $3;
  (*$3) *= $1;
}
| realexp '*' vectorialFirstOrderOp
{
  $$ = $3;
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionConstant($1);
  (*$3) *= f;
}
| '(' vectorialFirstOrderOp ')'
{
  $$ = $2;
}
;

unknownlistexp:
  unknownexp
{
  $$ = new UnknownListExpressionSet();
  static_cast<UnknownListExpressionSet*>($$)->add($1);
}
| unknownlistexp ',' unknownexp
{
  $$ = $1;
  static_cast<UnknownListExpressionSet*>($$)->add($3);
}
;

boundaryCondition: /* Dirichlet like boundary conditions */
  VARUNKNOWNID '=' fdexp ON boundaryexp
{
  $$ = new BoundaryConditionExpressionDirichlet($1,$3,$5);
}
/* Neumann like boundary conditions. The conormal is given! */
| DNU '(' VARUNKNOWNID ')' '=' fdexp ON boundaryexp
{
  $$ = new BoundaryConditionExpressionNeumann($3,$6,$8);
}
/* Fourrier like Boundary conditions */
| functionexp '*' VARUNKNOWNID '+' DNU '(' VARUNKNOWNID ')' '=' fdexp ON boundaryexp
{
  $$ = new BoundaryConditionExpressionFourrier($3,$1,$10,$12);
}
| VARUNKNOWNID '+' DNU '(' VARUNKNOWNID ')' '=' fdexp ON boundaryexp
{
  RealExpression* One = new RealExpressionValue(1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new BoundaryConditionExpressionFourrier($5,OneFunction,$8,$10);
}
| DNU '(' VARUNKNOWNID ')' '+' VARUNKNOWNID '=' fdexp ON boundaryexp
{
  RealExpression* One = new RealExpressionValue(1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new BoundaryConditionExpressionFourrier($3,OneFunction,$8,$10);
}
| DNU '(' VARUNKNOWNID ')' '+' fdexp '*' VARUNKNOWNID '=' fdexp ON boundaryexp
{
  $$ = new BoundaryConditionExpressionFourrier($3,$6,$10,$12);
}
| DNU '(' VARUNKNOWNID ')' '-' VARUNKNOWNID '=' fdexp ON boundaryexp
{
  RealExpression* One = new RealExpressionValue(-1);
  FunctionExpression* OneFunction = new FunctionExpressionConstant(One);
  $$ = new BoundaryConditionExpressionFourrier($3,OneFunction,$8,$10);
}
| DNU '(' VARUNKNOWNID ')' '-' fdexp '*' VARUNKNOWNID '=' fdexp ON boundaryexp
{
  FunctionExpression* MinusFDexp = new FunctionExpressionUnaryMinus($6);
  $$ = new BoundaryConditionExpressionFourrier($3,MinusFDexp,$10,$12);
}
;

boundaryexp:
  singleBoundaryexp
{
  $$ = $1;
}
| boundaryexp ',' singleBoundaryexp
{
  if ((*$1).boundaryType() != BoundaryExpression::list) {
    BoundaryExpressionList* bl = new BoundaryExpressionList();
    bl = new BoundaryExpressionList();
    bl->add($1); bl->add($3);
    $$ = bl;
  } else {
    dynamic_cast<BoundaryExpressionList&>(*$1).add($3);
    $$ = $1;
  }
}
;

singleBoundaryexp:
  vector3exp 
{
  $$ = new BoundaryExpressionPOVRay($1);
}
| realexp 
{
  BoundaryExpressionReferences* b = new BoundaryExpressionReferences();
  b->add($1);
  $$ = b;
}
| MESHID
{
  ReferenceCounting<MeshExpression> m
    = new MeshExpressionVariable($1);
  $$ = new BoundaryExpressionSurfaceMesh(m);
}
| MESHID FACE
{
  $$ = new BoundaryExpressionReferences($2);
}
;

unknownexp:
  NAME
{
  VariableLexerRepository::instance().add(*$1,VARUNKNOWNID);
  $$ = new UnknownExpressionDeclaration(*$1);
}
| NAME ':' DISCRETIZATIONTYPE
{
  VariableLexerRepository::instance().add(*$1,VARUNKNOWNID);
  $$ = new UnknownExpressionDeclaration(*$1,$3);
}
| VARUNKNOWNID
{
  VariableLexerRepository::instance().markAsUnknown($1);
  $$ = new UnknownExpressionFunction($1);
}
| VARUNKNOWNID ':' DISCRETIZATIONTYPE
{
  VariableLexerRepository::instance().markAsUnknown($1);
  $$ = new UnknownExpressionFunction($1,$3);
}
| VARFNCTID
{
  VariableLexerRepository::instance().markAsUnknown($1);
  $$ = new UnknownExpressionFunction($1);
}
| VARFNCTID ':' DISCRETIZATIONTYPE
{
  VariableLexerRepository::instance().markAsUnknown($1);
  $$ = new UnknownExpressionFunction($1,$3);
}
;

fdexp:
  realexp
{
  $$ = new FunctionExpressionConstant($1);
}
| functionexp
{
  $$ = $1;
}
;

functionexp: 
  XYZ
{
  $$ = new FunctionExpressionLinearBasis($1);
}
| NORMALCOMPONENT
{
  $$ = new FunctionExpressionNormalComponent($1);
}
| functionexp '(' functionexp ',' functionexp ',' functionexp ')'
{
  $$ = new FunctionExpressionComposed($1,$3,$5,$7);
}
| functionexp '(' functionexp ',' functionexp ',' realexp ')'
{
  ReferenceCounting<FunctionExpression> F3 = new FunctionExpressionConstant($7);
  $$ = new FunctionExpressionComposed($1,$3,$5,F3);
}
| functionexp '(' functionexp ',' realexp ',' functionexp ')'
{
  ReferenceCounting<FunctionExpression> F2 = new FunctionExpressionConstant($5);
  $$ = new FunctionExpressionComposed($1,$3,F2,$7);
}
| functionexp '(' functionexp ',' realexp ',' realexp ')'
{
  ReferenceCounting<FunctionExpression> F2 = new FunctionExpressionConstant($5);
  ReferenceCounting<FunctionExpression> F3 = new FunctionExpressionConstant($7);
  $$ = new FunctionExpressionComposed($1,$3,F2,F3);
}
| functionexp '(' realexp ',' functionexp ',' functionexp ')'
{
  ReferenceCounting<FunctionExpression> F1 = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionComposed($1,F1,$5,$7);
}
| functionexp '(' realexp ',' functionexp ',' realexp ')'
{
  ReferenceCounting<FunctionExpression> F1 = new FunctionExpressionConstant($3);
  ReferenceCounting<FunctionExpression> F3 = new FunctionExpressionConstant($7);
  $$ = new FunctionExpressionComposed($1,F1,$5,F3);
}
| functionexp '(' realexp ',' realexp ',' functionexp ')'
{
  ReferenceCounting<FunctionExpression> F1 = new FunctionExpressionConstant($3);
  ReferenceCounting<FunctionExpression> F2 = new FunctionExpressionConstant($5);
  $$ = new FunctionExpressionComposed($1,F1,F2,$7);
}
| VARFNCTID
{
  $$ = new FunctionExpressionVariable($1);
}
| ONE '(' boolfunctionexp ')'
{
  $$ = $3;
}
| REFERENCE '(' ITEMS ',' meshexp ',' refexp ')'
{
  $$ = new FunctionExpressionMeshReferences($3, $5, $7);
}
| '(' functionexp ')'
{
  $$ = $2;
}
| READ '(' FILEFORMAT ',' stringexp ',' meshexp ')'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new FunctionExpressionRead(f, $5, $7);
}
| READ '(' FILEFORMAT ',' stringexp ':' stringexp ',' meshexp ')'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new FunctionExpressionRead(f, $5, $7, $9);
}
| READ '(' FILEFORMAT ',' stringexp ':' stringexp ':' realexp ',' meshexp ')'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new FunctionExpressionRead(f, $5, $7, $9, $11);
}
| READ '(' FILEFORMAT ',' stringexp ':' realexp ',' meshexp ')'
{
  FileDescriptor* f = new FileDescriptor($3, FileDescriptor::formatDefault);
  $$ = new FunctionExpressionRead(f, $5, $7, $9);
}
| ONE '(' vector3exp ')'
{
  $$ = new FunctionExpressionObjectCharacteristic($3);
}
| ONE '(' domainexp ')'
{
  $$ = new FunctionExpressionDomainCharacteristic($3);
}
| ONE '(' meshexp ')'
{
  $$ = new FunctionExpressionMeshCharacteristic($3);
}
| MINMAX '(' functionexp ',' functionexp ')'
{
  $$ = new FunctionExpressionBinaryOperation($1,$3,$5);
}
| functionexp '%' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::modulo,$1,$3);
}
| functionexp '+' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::sum,$1,$3);
}
| functionexp '-' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::difference,$1,$3);
}
| functionexp '*' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::product,$1,$3);
}
| functionexp '/' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::division,$1,$3);
}
| functionexp '^' realexp
{
  ReferenceCounting<FunctionExpression> g
    = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::power,$1,g);
}
| functionexp '^' functionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::power,$1,$3);
}
| realexp '^' functionexp
{
  ReferenceCounting<FunctionExpression> f
    = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::power,f,$3);
}
| '-' functionexp %prec NEG
{
  $$ = new FunctionExpressionUnaryMinus($2);
}
| DXYZ '(' fdexp ')'
{
  $$ = new FunctionExpressionDerivative($1,$3);
}
| CONVECT '(' fieldexp ',' realexp ',' fdexp ')'
{
  $$ = new FunctionExpressionConvection($3,$5,$7);
}
| CFUNCTION '(' functionexp ')'
{
  $$ = new FunctionExpressionCFunction($1, $3);
}
/*
  Those declarations are to avoid conflicts using constants
  There might be a cleaner way to do it ?
*/
| MINMAX '(' realexp ',' functionexp ')'
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation($1,C,$5);
}
| MINMAX '(' functionexp ',' realexp ')'
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($5);
  $$ = new FunctionExpressionBinaryOperation($1,$3,C);
}
| realexp '%' functionexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::modulo,C,$3);
}
| functionexp '%' realexp 
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::modulo,$1,C);
}
| realexp  '+' functionexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::sum, C,$3);
}
| functionexp '+' realexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::sum, $1, C);
}
| realexp  '*' functionexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::product, C, $3);
}
| functionexp '*' realexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::product, $1, C);
}
| realexp  '-' functionexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::difference, C, $3);
}
| functionexp '-' realexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::difference, $1, C);
}
| realexp  '/' functionexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($1);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::division, C, $3);
}
| functionexp '/' realexp
{
  ReferenceCounting<FunctionExpression> C = new FunctionExpressionConstant($3);
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::division, $1, C);
}
| INT '[' fdexp ',' fdexp  ']' '(' fdexp ')' DXYZ
{
  $$ = new FunctionExpressionIntegrate($3, $5, $8, $10);
}
;

refexp:
  realexp ':' fdexp
{
  $$ = new FunctionExpressionMeshReferences::ReferencesSet($1, $3);
}
| refexp ',' realexp ':' fdexp
{
  $$ = $1;
  $$->add($3, $5);
}
;

vector3exp:
  LT realexp ',' realexp ',' realexp GT
{
  $$ = new Vector3ExpressionValue($2, $4, $6);
}
| VARVECTID
{
  $$ = new Vector3ExpressionVariable($1);
}
| '(' realexp ',' realexp ',' realexp ')'
{
  $$ = new Vector3ExpressionValue($2, $4, $6);
}
| vector3exp '+' vector3exp
{
  $$ = new Vector3ExpressionBinaryOperator<sum>($1,$3);
}
| vector3exp '-' vector3exp
{
  $$ = new Vector3ExpressionBinaryOperator<difference>($1, $3);
}
| realexp '*' vector3exp
{
  $$ = new Vector3ExpressionTimesScalar($3, $1);
}
| vector3exp '*' realexp
{
  $$ = new Vector3ExpressionTimesScalar($1, $3);
}
;

stringexp:
  strexp
{
  $$ = $1;
}
| stringexp '.' strexp
{
  $$ = new StringExpressionConcat($1,$3);
}
| stringexp '.' realexp
{
  $$ = new StringExpressionConcat($1,
				  new StringExpressionReal($3));
}
;

strexp:
  STRING
{
  std::string s($1);
  delete [] $1;
  $$ = new StringExpressionValue(s);
}
;

realexp:
  NUM
{
  $$ = new RealExpressionValue($1);
}
| VARREALID
{
  $$ = new RealExpressionVariable($1);
}
| functionexp '(' vector3exp ')'
{
  $$ = new RealExpressionFunctionEvaluate($1, $3);
}
| functionexp '(' realexp ',' realexp ',' realexp ')'
{
  $$ = new RealExpressionFunctionEvaluate($1, $3, $5, $7);
}
| INT '[' meshexp ']' '(' fdexp ')'
{
  $$ = new RealExpressionIntegrate($6,$3);
}
| INT '[' meshexp ':' DISCRETIZATIONTYPE ']' '(' fdexp ')'
{
  $$ = new RealExpressionIntegrate($8,$3,$5);
}
| INT '[' meshexp ':' DISCRETIZATIONTYPE ':' vector3exp ']' '(' fdexp ')'
{
  $$ = new RealExpressionIntegrate($10,$3,$5,$7);
}
| MINMAX '[' meshexp ']' '(' fdexp ')'
{
  $$ = new RealExpressionMinMax($1,$6,$3);
}
| '(' realexp ')'
{
  $$ = $2;
}
/* Birary Operators */
| realexp '%' realexp
{ 
  $$ = new RealExpressionBinaryOperator<modulo>($1,$3);
}
| realexp '+' realexp
{ 
  $$ = new RealExpressionBinaryOperator<sum>($1,$3);
}
| realexp '-' realexp
{
  $$ = new RealExpressionBinaryOperator<difference>($1,$3);
}
| realexp '*' realexp
{
  $$ = new RealExpressionBinaryOperator<product>($1,$3);
}
| realexp '/' realexp
{
  $$ = new RealExpressionBinaryOperator<division>($1,$3);
}
| realexp '^' realexp
{
  $$ = new RealExpressionBinaryOperator<std::pow>($1,$3);
}
/* Unary Operators */
| '-' realexp  %prec NEG
{
  $$ = new RealExpressionUnaryOperator<unaryMinus>($2);
}
| INCR VARREALID
{
  $$ = new RealExpressionPreIncrement(new RealExpressionVariable($2));
}
| VARREALID INCR
{
  $$ = new RealExpressionPostIncrement(new RealExpressionVariable($1));
}
| DECR VARREALID
{
  $$ = new RealExpressionPreDecrement(new RealExpressionVariable($2));
}
| VARREALID DECR
{
  $$ = new RealExpressionPostDecrement(new RealExpressionVariable($1));
}
| CFUNCTION  '(' realexp ')'
{
  $$ = new RealExpressionCFunction($1,$3);
}
/* Casts */
| '(' DOUBLE ')' boolexp
{
  $$ = new RealExpressionBoolean($4);
}
;

boolexp:
  TRUEVALUE
{
  $$ = new BooleanExpressionValue(true);
}
| FALSEVALUE
{
  $$ = new BooleanExpressionValue(false);
}
| realexp GT realexp
{
  $$ = new BooleanExpressionCompareOperator<gt>($1,$3);
}
| realexp LT realexp
{
  $$ = new BooleanExpressionCompareOperator<lt>($1,$3);
}
| realexp LEQ realexp
{
  $$ = new BooleanExpressionCompareOperator<le>($1,$3);
}
| realexp GEQ realexp
{
  $$ = new BooleanExpressionCompareOperator<ge>($1,$3);
}
| realexp EQ  realexp
{
  $$ = new BooleanExpressionCompareOperator<eq>($1,$3);
}
| realexp NE  realexp
{
  $$ = new BooleanExpressionCompareOperator<ne>($1,$3);
}
| boolexp OR   boolexp
{
  $$ = new BooleanExpressionBinaryOperator<or_>($1,$3);
}
| boolexp XOR  boolexp
{
  $$ = new BooleanExpressionBinaryOperator<xor_>($1,$3);
}
| boolexp AND  boolexp
{
  $$ = new BooleanExpressionBinaryOperator<and_>($1,$3);
}
| '(' boolexp ')'
{
  $$ = $2;
}
| NOT boolexp
{
  $$ = new BooleanExpressionUnaryOperator<not_>($2);
}
;

boolfunctionexp:
  TRUEVALUE
{
  ReferenceCounting<RealExpression> one = new RealExpressionValue(1);
  $$ = new FunctionExpressionConstant(one);
}
| FALSEVALUE
{
  ReferenceCounting<RealExpression> zero = new RealExpressionValue(0);
  $$ = new FunctionExpressionConstant(zero);
}
| fdexp GT fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::gt,$1,$3);
}
| fdexp LT fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::lt,$1,$3);
}
| fdexp GEQ fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::ge,$1,$3);
}
| fdexp LEQ fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::le,$1,$3);
}
| fdexp EQ fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::eq,$1,$3);
}
| fdexp NE fdexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::ne,$1,$3);
}
| boolfunctionexp OR boolfunctionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::or_,$1,$3);
}
| boolfunctionexp XOR boolfunctionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::xor_,$1,$3);
}
| boolfunctionexp AND boolfunctionexp
{
  $$ = new FunctionExpressionBinaryOperation(BinaryOperation::and_,$1,$3);
}
| '(' boolfunctionexp ')'
{
  $$ = $2;
}
| NOT boolfunctionexp
{
  $$ = new FunctionExpressionNot($2);
}
;

/* End of grammar */
%%

int yyerror (const char * s)  /* Called by yyparse on error */ {
  throw ErrorHandler("PARSED FILE",fflexer->lineno(),
		     stringify(s)+": [\""+fflexer->YYText()+"\" unexpected]",
		     ErrorHandler::compilation);
  return 0;
}

int yyerror2 (const char * s)  /* Called by yyparse on error */ {
  throw ErrorHandler("PARSED FILE",fflexer->lineno(),
		     stringify(s),
		     ErrorHandler::compilation);
  return 0;
}

