//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Lexer.hpp>

#include <list>
#include <ReferenceCounting.hpp>

#include <IdentifierSet.hpp>

#include <iostream>

#ifndef FFLEXER_HPP
#define FFLEXER_HPP
/*!
  \class FFLexer

  This class is a the specialized Lexer for the FreeFEM language.

  \author St�phane Del Pino
*/
class Variable;
class FFLexer
  : public Lexer
{
private:
  /*! This function returns the token associated to the name "word" if it
    corresponds to a Keyword, else, it returns "-1".
   */
  int isKeyword (const std::string& word);

  /*! This function returns the token associated to the name "word" if it
    corresponds to a variable, else, it returns "-1".
   */
  int isVariable(const std::string& word);

  /*! This function returns the token associated to the name "word" if it
    corresponds to an Option, else, it returns "-1".
   */
  int isOption (const std::string& word);

  //! This contains the list of kewords used for options management
  IdentifierSet __optionsKeyWords;
public:

  

  /** 
   * Fills @a kw the keyword/token associatino list
   * 
   * @param kw keywords list
   */
  static void define(KeyWordList& kw);

  //! The lexer function.
  int yylex();

  enum Context {
    defaultContext,
    variationalContext
  };

  void switchToContext(const FFLexer::Context context);

  //! Constructs a FFLexer for given std::istream and std::ostream.
  FFLexer(std::istream& In,
	  std::ostream& Out = std::cout);

  //! Destructor.
  ~FFLexer()
  {
    ;
  }
};

#endif // FFLEXER_HPP
