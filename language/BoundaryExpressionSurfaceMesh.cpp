//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <BoundaryExpressionSurfaceMesh.hpp>
#include <MeshExpression.hpp>

#include <BoundarySurfaceMesh.hpp>

#include <Mesh.hpp>
#include <Structured3DMesh.hpp>
#include <MeshOfHexahedra.hpp>
#include <MeshOfTetrahedra.hpp>

std::ostream&
BoundaryExpressionSurfaceMesh::
put(std::ostream& os) const
{
  os << "Surface Mesh";
  return os;
}

void
BoundaryExpressionSurfaceMesh::
__execute()
{
  __surfaceMeshExpression->execute();

  const Mesh* m = __surfaceMeshExpression->mesh();

  switch (m->family()) {
  case Mesh::volume: {
    // If mesh is a volume it is not a predefined surface mesh
    __isPredefinedSurfaceMesh = false;

    switch (m->type()) {
    case Mesh::cartesianHexahedraMesh: {
      const Structured3DMesh& mesh = dynamic_cast<const Structured3DMesh&>(*m);
      const SurfaceMesh* surface = mesh.borderMesh();
      __boundary = new BoundarySurfaceMesh(surface);
      break;
    }
    case Mesh::tetrahedraMesh: {
      const MeshOfTetrahedra& mesh = dynamic_cast<const MeshOfTetrahedra&>(*m);
      const SurfaceMesh* surface = mesh.borderMesh();
      __boundary = new BoundarySurfaceMesh(surface);
      break;
    }
    case Mesh::hexahedraMesh: {
      const MeshOfHexahedra& mesh = dynamic_cast<const MeshOfHexahedra&>(*m);
      const SurfaceMesh* surface = mesh.borderMesh();
      __boundary = new BoundarySurfaceMesh(surface);
      break;
    }
    default: {
      throw ErrorHandler(__BASE_FILE__,__LINE__,
			 "Unexpected volume mesh type",
			 ErrorHandler::unexpected);
    }
    }
    break;
  }
  case Mesh::surface: {
    ASSERT(m->type() == Mesh::surfaceMeshTriangles);
    __isPredefinedSurfaceMesh = true;
    __boundary
      = new BoundarySurfaceMesh(dynamic_cast<const SurfaceMeshOfTriangles*>(m));
    break;
  }
  default: {
    throw ErrorHandler(__BASE_FILE__,__LINE__,
		       "Unexpected mesh family",
		       ErrorHandler::unexpected);
  }
  }
}

BoundaryExpressionSurfaceMesh::
BoundaryExpressionSurfaceMesh(ReferenceCounting<MeshExpression> m)
  : BoundaryExpression(BoundaryExpression::surfaceMesh),
    __surfaceMeshExpression(m),
    __isPredefinedSurfaceMesh(true)
{
  ;
}

BoundaryExpressionSurfaceMesh::
BoundaryExpressionSurfaceMesh(const BoundaryExpressionSurfaceMesh& m)
  : BoundaryExpression(m),
    __surfaceMeshExpression(m.__surfaceMeshExpression),
    __isPredefinedSurfaceMesh(false)
{
  ;
}

BoundaryExpressionSurfaceMesh::
~BoundaryExpressionSurfaceMesh()
{
  ;
}

