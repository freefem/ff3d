//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef IFSTREAM_EXPRESSION_HPP
#define IFSTREAM_EXPRESSION_HPP

#include <Expression.hpp>
#include <StringExpression.hpp>
#include <ErrorHandler.hpp>

#include <fstream>

/**
 * @file   IFStreamExpression.hpp
 * @author Stephane Del Pino
 * @date   Sun Sep 28 23:59:36 2008
 * 
 * @brief  Base class for ifstream expression
 */
class IFStreamExpression
  : public Expression
{
public:
  enum Type {
    undefined,
    value,
    variable
  };

protected:
  const Type __type;		/**< type of the expression */
  ReferenceCounting<std::ifstream> __fin;

  virtual std::ostream&
  put(std::ostream & os) const
  {
    return os;
  }

public:

  /** 
   * Access to the file stream pointer
   * 
   * @return __fin
   */
  std::ifstream* ifstream()
  {
    if (__type == undefined) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "using undefined stream",
			 ErrorHandler::normal);
    }
    return __fin;
  }

  IFStreamExpression(const IFStreamExpression::Type& type)
    : Expression(Expression::ifstreamexpression),
      __type(type),
      __fin(0)
  {
    ;
  }

  IFStreamExpression(const IFStreamExpression& ifs)
    : Expression(ifs),
      __type(ifs.__type),
      __fin(ifs.__fin)
  {
    ;
  }

  virtual ~IFStreamExpression()
  {
    ;
  }
};

#endif // IFSTREAM_EXPRESSION_HPP
