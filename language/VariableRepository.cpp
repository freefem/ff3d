//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <VariableRepository.hpp>

#include <ReferenceCounting.hpp>
#include <Variable.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

ReferenceCounting<Variable> VariableRepository::
find(const std::string& name) const
{
  Container::const_iterator i;
  size_t level=0;
  for (; level<=__blockLevel;++level) {
    i = __variableList[level].find(name);
    if (i != __variableList[level].end()) break;
  }
  if (level>__blockLevel) return 0;
  if (i == __variableList[level].end()) return 0;
  else {
    return i->second;
  } 
}

void VariableRepository::
add(ReferenceCounting<Variable> v)
{
  if (find(v->name())) {
    throw ErrorHandler (__FILE__,__LINE__,
			"variable "+v->name()+" already declared",
			ErrorHandler::unexpected);
  }
  __variableList[__blockLevel][v->name()]=v;
}

void VariableRepository::
remove(const std::string& name)
{
  Container::const_iterator i;
  size_t level=0;
  for (; level<=__blockLevel;++level) {
    i = __variableList[level].find(name);
    if (i != __variableList[level].end()) break;
  }
  if ((i != __variableList[level].end()) and (level>__blockLevel)) {
    __variableList[level].erase(name);
  } else {
    const std::string errorMsg
      = "cannot erase variable "+name
      +", since it is not declared";

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg,
		       ErrorHandler::unexpected);
  }
}

void VariableRepository::
beginBlock()
{
  __blockLevel++;
  __variableList.push_back(Container());
}

void VariableRepository::
endBlock()
{
  ASSERT(__blockLevel>0);
  __variableList[__blockLevel].clear();
  __blockLevel--;
  __variableList.pop_back();
}

VariableRepository::
VariableRepository()
  : __blockLevel(0)
{
  __variableList.push_back(Container());
}

VariableRepository::
~VariableRepository()
{
  ;
}
