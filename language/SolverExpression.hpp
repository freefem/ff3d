//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef SOLVER_EXPRESSION_HPP
#define SOLVER_EXPRESSION_HPP

#include <Expression.hpp>

#include <UnknownListExpression.hpp>

#include <DomainExpression.hpp>
#include <MeshExpression.hpp>

#include <SolverOptionsExpression.hpp>
#include <ProblemExpression.hpp>

#include <Variable.hpp>

class DiscretizationType;
class ScalarDiscretizationTypeBase;

/*!
  \class SolverExpression

  This class defines the base class of Solver expressions.

  \author Stephane Del Pino
 */
class SolverExpression
  : public Expression
{
protected:
  ReferenceCounting<UnknownListExpression> __unknownList;
  ReferenceCounting<MeshExpression> __mesh;

  ReferenceCounting<SolverOptionsExpression> __solverOptions;
  ReferenceCounting<ProblemExpression> __problemExpression;

  ReferenceCounting<DomainExpression> __domain;

private:
  void __setScene(ReferenceCounting<DomainExpression> d) const;

  std::ostream& put(std::ostream& os) const;

  SolverExpression(const SolverExpression&);

  void __solveFEM(const DiscretizationType& discretization);
  void __solveLagrange(const DiscretizationType& discretization);
  void __solveLegendre(const DiscretizationType& discretization);

public:
  void __execute();

  SolverExpression(ReferenceCounting<UnknownListExpression> unknownList,
		   ReferenceCounting<MeshExpression> mesh,
		   ReferenceCounting<SolverOptionsExpression> solverOptions,
		   ReferenceCounting<ProblemExpression> problemExpression,
		   ReferenceCounting<DomainExpression> domainExpression = 0);

  ~SolverExpression();
};

#endif // SOLVER_EXPRESSION_HPP
