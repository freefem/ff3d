//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <DomainExpressionAnalytic.hpp>
#include <FunctionExpression.hpp>
#include <AnalyticShape.hpp>
#include <Scene.hpp>
#include <Domain.hpp>

std::ostream& DomainExpressionAnalytic::
put(std::ostream& os) const
{
  return os;
}

void DomainExpressionAnalytic::
__execute()
{
  __function->execute();

  ReferenceCounting<Object> object
    = new Object(new AnalyticShape(__function->function()));
  object->setReference(TinyVector<3,real_t>(0,0,0));

  ReferenceCounting<Scene> scene = new Scene();
  scene->add(object);

  __domain = new Domain(scene);
  __domain->setObjects(object);
}

DomainExpressionAnalytic::
DomainExpressionAnalytic(ReferenceCounting<FunctionExpression> f)
  : DomainExpression(DomainExpression::analytic),
    __function(f)
{
  ;
}

DomainExpressionAnalytic::
DomainExpressionAnalytic(const DomainExpressionAnalytic& d)
  : DomainExpression(d),
    __function(d.__function)
{
  ;
}

DomainExpressionAnalytic::
~DomainExpressionAnalytic()
{
  ;
}
