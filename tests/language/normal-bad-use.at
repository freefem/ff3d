# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_SETUP([Normal bad use])

##########################
# NORMAL IN AFFECTATIONS #
##########################

AT_DATA([normal-affectation.ff],
[[
double n = nx(0,0,0);
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-affectation.ff],0,
[],
[[error: Cannot evaluate "nx": missing boundary
]])

#########################
# NORMAL IN INTEGRATION #
#########################

AT_DATA([normal-integration.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
double value = int[m](nx);
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-integration.ff],0,
[],
[[error: Cannot evaluate "nx": missing boundary
]])

#############################
# NORMAL IN VARIATIONAL RHS #
#############################

AT_DATA([normal-variational-rhs.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m
{
  test(v) int(u*v) = int(v*nz);
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-variational-rhs.ff],0,
[],
[[error: cannot evaluate the expression "nz*v" in the volume
]])

#############################
# NORMAL IN VARIATIONAL LHS #
#############################

AT_DATA([normal-variational-lhs.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m
{
  test(v) int(ny*u*v) = 0;
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-variational-lhs.ff],0,
[],
[[error: cannot evaluate the expression "ny*u*v" in the volume
]])

##########################
# TESTING NORMAL PDE RHS #
##########################

AT_DATA([normal-pde-rhs.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m {
 pde(u) u = ny;
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-pde-rhs.ff],0,
[],
[[error: cannot evaluate the expression "ny" in the volume
]])

#############################
# NORMAL IN PDE LHS ORDER 0 #
#############################

AT_DATA([normal-pde-lhs-0.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m {
 pde(u) nx*u = 0;
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-pde-lhs-0.ff],0,
[],
[[error: cannot evaluate the expression "nx*u" in the volume
]])

#############################
# NORMAL IN PDE LHS ORDER 1 #
#############################

AT_DATA([normal-pde-lhs-1.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m {
 pde(u) nz*dx(u) = 0;
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-pde-lhs-1.ff],0,
[],
[[error: cannot evaluate the expression "(nz*1)*dx(u)" in the volume
]])

#############################
# NORMAL IN PDE LHS ORDER 2 #
#############################

AT_DATA([normal-pde-lhs-2.ff],
[[vertex a = (0,0,0);
vertex b = (1,1,1);
vector n = (2,2,2);
mesh m = structured(n,a,b);
solve(u) in m {
 pde(u) dy(ny*dz(u)) = 0;
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 normal-pde-lhs-2.ff],0,
[],
[[error: cannot evaluate the expression "dy((ny*1)*dz(u))" in the volume
]])

AT_CLEANUP
