# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_BANNER([Numerics])
m4_include([numeric/quadrature.at])

AT_BANNER([  -> Scalar PDEs])
m4_include([numeric/pde/l2-projection-cartesian-q1.at])
m4_include([numeric/pde/l2-projection-cartesian-q2.at])
m4_include([numeric/pde/l2-projection-p1.at])
m4_include([numeric/pde/l2-projection-p2.at])
m4_include([numeric/pde/l2-derivative-projection-cartesian-q1.at])
m4_include([numeric/pde/l2-derivative-projection-cartesian-q2.at])
m4_include([numeric/pde/l2-derivative-projection-p1.at])
m4_include([numeric/pde/l2-derivative-projection-p2.at])
m4_include([numeric/pde/laplace-dirichlet-cartesian-q1.at])
m4_include([numeric/pde/laplace-dirichlet-cartesian-q2.at])
m4_include([numeric/pde/laplace-dirichlet-p1.at])
m4_include([numeric/pde/laplace-dirichlet-p2.at])

AT_BANNER([  -> Scalar variational formulae])
m4_include([numeric/variational/l2-projection-cartesian-q1.at])
m4_include([numeric/variational/l2-projection-cartesian-q2.at])
m4_include([numeric/variational/l2-projection-lagrange.at])
m4_include([numeric/variational/l2-projection-legendre.at])
m4_include([numeric/variational/l2-projection-p1.at])
m4_include([numeric/variational/l2-projection-p2.at])

m4_include([numeric/variational/l2-derivative-projection-cartesian-q1.at])
m4_include([numeric/variational/l2-derivative-projection-cartesian-q2.at])
m4_include([numeric/variational/l2-derivative-projection-lagrange.at])
m4_include([numeric/variational/l2-derivative-projection-legendre.at])
m4_include([numeric/variational/l2-derivative-projection-p1.at])

m4_include([numeric/variational/l2-rhs-f-grad-g-cartesian-q1.at])
m4_include([numeric/variational/l2-rhs-f-grad-g-cartesian-q2.at])
m4_include([numeric/variational/l2-rhs-f-grad-g-lagrange.at])
m4_include([numeric/variational/l2-rhs-f-grad-g-legendre.at])
m4_include([numeric/variational/l2-rhs-f-grad-g-p1.at])
#m4_include([numeric/variational/l2-rhs-f-grad-g-p2.at])

m4_include([numeric/variational/laplace-dirichlet-cartesian-q1.at])
m4_include([numeric/variational/laplace-dirichlet-cartesian-q2.at])
m4_include([numeric/variational/laplace-dirichlet-lagrange.at])
m4_include([numeric/variational/laplace-dirichlet-legendre.at])
m4_include([numeric/variational/laplace-dirichlet-p1.at])
m4_include([numeric/variational/laplace-dirichlet-p2.at])

AT_BANNER([  -> Systems of PDEs])
m4_include([numeric/pde-system/laplacian-order1-cartesian-q1.at])
m4_include([numeric/pde-system/laplacian-order1-cartesian-q2.at])
m4_include([numeric/pde-system/laplacian-order1-p1.at])
m4_include([numeric/pde-system/laplacian-order1-p2.at])

AT_BANNER([  -> Systems variational formulae])
m4_include([numeric/variational-system/laplacian-order1-1-cartesian-q1.at])
m4_include([numeric/variational-system/laplacian-order1-1-cartesian-q2.at])
m4_include([numeric/variational-system/laplacian-order1-1-lagrange.at])
m4_include([numeric/variational-system/laplacian-order1-1-legendre.at])
m4_include([numeric/variational-system/laplacian-order1-1-p1.at])
m4_include([numeric/variational-system/laplacian-order1-1-p2.at])

m4_include([numeric/variational-system/laplacian-order1-2-cartesian-q1.at])
m4_include([numeric/variational-system/laplacian-order1-2-cartesian-q2.at])
m4_include([numeric/variational-system/laplacian-order1-2-p1.at])
m4_include([numeric/variational-system/laplacian-order1-2-p2.at])

m4_include([numeric/variational-system/laplacian-order1-3-cartesian-q1.at])
m4_include([numeric/variational-system/laplacian-order1-3-cartesian-q2.at])
m4_include([numeric/variational-system/laplacian-order1-3-p1.at])
m4_include([numeric/variational-system/laplacian-order1-3-p2.at])

AT_BANNER([  -> Numeric utilities])
m4_include([numeric/utils/femfunction-simplification.at])
m4_include([numeric/utils/function-affectation-cartesian-q1.at])
m4_include([numeric/utils/function-affectation-cartesian-q2.at])
m4_include([numeric/utils/function-affectation-lagrange.at])
m4_include([numeric/utils/function-affectation-legendre.at])
m4_include([numeric/utils/function-affectation-p1.at])
m4_include([numeric/utils/function-affectation-p2.at])
m4_include([numeric/utils/function-derivative-lagrange.at])
m4_include([numeric/utils/function-derivative-legendre.at])
m4_include([numeric/utils/function-integrale-cartesian-q1.at])
m4_include([numeric/utils/function-reference-elements-p1.at])
m4_include([numeric/utils/function-reference-elements-cartesian-q1.at])
m4_include([numeric/utils/function-reference-vertices-p1.at])
m4_include([numeric/utils/function-reference-vertices-cartesian-q1.at])
