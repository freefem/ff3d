# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id$

AT_SETUP([Laplacian/order 1 [ver.3] (cartesian Q1-FEM)])

AT_DATA([test.ff],
[[
vertex a = (0,0,0);
vertex b = (1,2,3);
vertex n = (3,7,6);

mesh M = structured(n,a,b);

double a1 = 01;
double b1 = 02;
double c1 = 03;
double d1 = 04;
double e1 = 05;
double f1 = 06;
double g1 = 07;
double h1 = 08;

double a2 = 09;
double b2 = 10;
double c2 = 11;
double d2 = 12;
double e2 = 13;
double f2 = 14;
double g2 = 15;
double h2 = 16;

double a3 = 09;
double b3 = 10;
double c3 = 11;
double d3 = 12;
double e3 = 13;
double f3 = 14;
double g3 = 15;
double h3 = 16;

double ap = 17;
double bp = 18;
double cp = 19;
double dp = 20;
double ep = 21;
double fp = 22;
double gp = 23;
double hp = 24;

function u1exact = a1*x + b1*y + c1*z + d1*y*z + e1*x*z + f1*x*y + g1*x*y*z + h1;
function u2exact = a2*x + b2*y + c2*z + d2*y*z + e2*x*z + f2*x*y + g2*x*y*z + h2;
function u3exact = a3*x + b3*y + c3*z + d3*y*z + e3*x*z + f3*x*y + g3*x*y*z + h3;

function pexact = ap*x + bp*y + cp*z + dp*y*z + ep*x*z + fp*x*y + gp*x*y*z + hp;

solve(u1,u2,u3,p) in M
  krylov(type=bicg),
  bicg(epsilon=1E-20)
{
  test(v1,v2,v3,q)
    int(grad(u1)*grad(v1) -p*dx(v1))
   +int(grad(u2)*grad(v2) -p*dy(v2))
   +int(grad(u3)*grad(v3) -p*dz(v3))
   +int(grad(p)*grad(q) -u1*dx(q)-u2*dy(q)-u3*dz(q))
   =
    int((ap+ep*z+fp*y+gp*y*z)*v1)
   +int((bp+dp*z+fp*x+gp*x*z)*v2)
   +int((cp+dp*y+ep*x+gp*x*y)*v3)
   +int(q*(a1+e1*z+f1*y+g1*y*z + b2+d2*z+f2*x+g2*x*z  + c3+d3*y+e3*x+g3*x*y));

   u1 = u1exact on M;
   u2 = u2exact on M;
   u3 = u3exact on M;
   p  = pexact  on M;
}


double l2u1error = sqrt(int[M]((u1-u1exact)^2));
if (l2u1error < 1E-8)
  cout << "u1: ok\n";
else
  cout << "oops: dx(u) u1-u1exact " << l2u1error << "\n";

double l2u2error = sqrt(int[M]((u2-u2exact)^2));
if (l2u2error < 1E-8)
  cout << "u2: ok\n";
else
  cout << "oops: dx(u) u2-u2exact " << l2u2error << "\n";

double l2u3error = sqrt(int[M]((u3-u3exact)^2));
if (l2u3error < 1E-8)
  cout << "u3: ok\n";
else
  cout << "oops: dx(u) u3-u3exact " << l2u3error << "\n";

double l2perror = sqrt(int[M]((p-pexact)^2));
if (l2perror < 1E-8)
  cout << "p: ok\n";
else
  cout << "oops: dx(u) p-pexact " << l2perror << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -np -V 0 test.ff],0,
[[u1: ok
u2: ok
u3: ok
p: ok
]])

AT_CLEANUP
