dnl -*- Mode: Autoconf -*- 
dnl	$Id$	

m4_include([m4/utils.m4])
m4_include([m4/cvssources.m4])

m4_include([m4/compile.m4])
m4_include([m4/vtk.m4])
m4_include([m4/qt.m4])
m4_include([m4/gui.m4])
m4_include([m4/pthread.m4])
m4_include([m4/petsc.m4])
m4_include([m4/mpi.m4])
m4_include([m4/documentation.m4])

m4_include([m4/libtool.m4])
