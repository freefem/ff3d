//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef FF_SYNTAX_HIGHLIGHTER_HPP
#define FF_SYNTAX_HIGHLIGHTER_HPP

#include <FFLexer.hpp>

#include <QtGui/QTextEdit>
#include <QtCore/QString>

#include <SyntaxHighLighter.hpp>

/**
 * @file   FFSyntaxHighlighter.hpp
 * @author Stephane Del Pino
 * @date   Sat Nov 27 01:32:21 2004
 * 
 * @brief This class builds a simple syntax highlighter for ff3d
 * language files
 * 
 */
class FFSyntaxHighlighter
  : public SyntaxHighLighter
{
private:
  QFont __defaultFont;
  QFont __keywordFont;
  QFont __commentFont;
  QFont __stringFont;

  QColor __defaultColor;
  QColor __keywordColor;
  QColor __commentColor;
  QColor __stringColor;

public:

  /** 
   * Constructs a syntax highlighter for ff3d
   * 
   * @param textEdit the text editor to highlight
   * 
   */
  FFSyntaxHighlighter(QTextEdit* textEdit)
    : __defaultFont(textEdit->currentFont()),
      __defaultColor(0,0,0),
      __keywordColor(0,0,255),
      __commentColor(150,150,150),
      __stringColor(50,155,155)
  {
    QTextCharFormat defaultFormat;
    defaultFormat.setFont(__defaultFont);

    QTextCharFormat keywordFormat = defaultFormat;
    keywordFormat.setFontWeight(QFont::Bold);
    keywordFormat.setForeground(__keywordColor);
    Lexer::KeyWordList keywordList;
    FFLexer::define(keywordList);
    QString keywordsRegExp("\\b(");
    const size_t numberOfKeyWords = keywordList.size();
    size_t n = 0;
    for (Lexer::KeyWordList::const_iterator i=keywordList.begin();
	 i != keywordList.end(); ++i,++n) {
      keywordsRegExp.append(i->first);
      if (n<numberOfKeyWords-1) keywordsRegExp.append("|");
    }
    keywordsRegExp.append(")\\b"); 
    this->addMapping(keywordsRegExp, keywordFormat);

    QTextCharFormat singleLineCommentFormat = defaultFormat;
    singleLineCommentFormat.setForeground(__commentColor);
    singleLineCommentFormat.setFontItalic(true);
    this->addMapping("//[^\n]*", singleLineCommentFormat);

    QTextCharFormat quotationFormat = defaultFormat;
    quotationFormat.setForeground(__stringColor);
    this->addMapping("\"[^\n]*\"", quotationFormat);

    textEdit->setFont(defaultFormat.font());
    textEdit->setCurrentFont(defaultFormat.font());
    textEdit->document()->setDefaultFont(defaultFormat.font());
    this->addToDocument(textEdit->document());
  }

  /** 
   * Destructor
   * 
   * 
   * @return 
   */
  virtual ~FFSyntaxHighlighter()
  {
    ;
  }
};


#endif // FF_SYNTAX_HIGHLIGHTER_HPP
