//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <sstream>

#include <QtGui/QMainWindow>

class FFThread;
class EditorWindow;
class QTextEdit;

class MainWindow
  : public QMainWindow
{
  Q_OBJECT
public:
  MainWindow();
  ~MainWindow();

protected:
  void closeEvent(QCloseEvent* event);

private slots:
  void newFile();
  void choose();
  void load(const QString &fileName);
  void save();
  void saveAs();
  void print();

  void run();
  void pause();
  void resume();
  void stop();

  void aboutff3d();
  void aboutQt();
  void aboutVtk();
//   void whatsThis();

private:
  void __buildFileTools();
  void __buildEditorTools();
  void __buildActionTools();
  void __buildHelpTools();

  QPrinter *printer;
  EditorWindow* __editor;
  QTextEdit* __console;
  QString filename;
  FFThread* __ffThread;
};

#endif // MAIN_WINDOW_HPP
