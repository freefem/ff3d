#!/usr/bin/perl -w
use strict;
use diagnostics;

sub open_itemize
{
    my $init=shift(@_);
    if ($init==0) {
	print "\\begin{itemize}\n";
	$init=1;
    }
    return $init;
}

sub close_itemize
{
    my $init=shift(@_);
    if ($init) {
	print "\\end{itemize}\n";
	$init=0;
    }
    return $init;
}

sub get_descrition
{
    my $desc=shift(@_);
    if ($desc eq "") {
	$desc=": this option is not documented";
    }
    return $desc;
}

my $init=0;

my $l='';
map {chomp;$l.="$_ "} <>;
#Ouch �a, c'est de la regexp
while ($l=~/static\sconst\schar\s*\*\sidentifier\s*\(\s*\)\s*\{.*?(autodoc:\s*\"(.*?)\"\s*?)??return.*?\"(.*?)\".*?\}/g) {
    my $desc=$2;
    $desc="\\Red{not documented}" unless defined($desc);
    my $option=$3;
    $option = "cannot find option name!" unless defined($option);
    print "\\item[\\Blue{\\texttt{$option}}]  \\emph{$desc}";
}

#
# Checks for integer parameters.
#

my $first=1;
while ($l=~/(\"([^\"]*?)\")?\s+?add\(new\s+IntegerParameter\s*\((.+?)\s*,.*?\"(.+?)\".*?\)/g) {
    if ($first) {
	$init=open_itemize($init);
	print "\\item integer parameters\n";
	print "\\begin{itemize}\n";
	$first=0;
    }
    my $desc=$2;
    $desc="\\Red{not documented}" unless defined($desc);
    my $option=$4;
    $option = "cannot find option name!" unless defined($option);
    my $value=$3;
    $value = "\\Red{unknown}" unless defined($value);

    print "\\item[\$\\diamond\$]\\Blue{\\texttt{$option}} \\emph{$desc.} Default value is \\Blue{$value}\n";
}
if (!$first) {
    print "\\end{itemize}\n"
}

#
# Checks for double parameters.
#

$first=1;
while ($l=~/(\"([^\"]*?)\")?\s+?add\(new\s+DoubleParameter\s*\((.+?)\s*,.*?\"(.+?)\".*?\)/g) {
    if ($first) {
	$init=open_itemize($init);
	print "\\item double parameters\n";
	print "\\begin{itemize}\n";
	$first=0;
    }
    my $desc=$2;
    $desc="\\Red{not documented}" unless defined($desc);
    my $option=$4;
    $option = "cannot find option name!" unless defined($option);
    my $value=$3;
    $value = "\\Red{unknown}" unless defined($value);

    print "\\item[\$\\diamond\$]\\Blue{\\texttt{$option}} \\emph{$desc.} Default value is \\Blue{$value}\n";
}

if (!$first) {
    print "\\end{itemize}\n"
}

#
# Checks for selectable parameters.
#

$first=1;
my $ll=$l;
while ($ll=~/(autodoc:\s*\"([^\"]*?)\")?\s+?EnumParameter\s*<.*?>\s*\*\s*(.+?)\s*=.*?\(.*?::\s*([^,]*?)\s*,\s*\"(.+?)\".*?\)/g) {
    my $m=$3;
    my $defaultType=$4;
    my $type=$5;

    my $desc=$2;

    if ($first) {
	$init=open_itemize($init);
	print "\\item selectable parameters\n";
	print "\\begin{itemize}\n";
	$first=0;
    }
    my $default;
    while ($l=~/\(\*$m\)\.addSwitch\s*\(\s*\"([^\"]+)\"\s*,.*?::\s*?$defaultType\s*?\)/g) {
	$default=$1;
    }

    $default="\\Red{no default}" unless defined($default);
    $desc="\\Red{not documented}" unless defined($desc);

    print "\\item[\$\\diamond\$]  \\Blue{\\texttt{$type}} \\emph{$desc}. Default value is \\Blue{\\texttt{$default}}. Available values are";
    print "\\begin{itemize}\n";

    while ($l=~/(autodoc:\s*\"([^\"]*?)\")?\s+?\(\*$m\)\.addSwitch\s*\(\"(.+?)\".*?\s*,.+?\)/g) {
	my $desc=$2;
	$desc="\\Red{not documented}" unless defined($desc);
	my $option=$3;
	$option = "cannot find option name!" unless defined($option);

	print "\\item[\$\\circ\$]\\Blue{\\texttt{$option}}: \\emph{$desc}\n";
    }
    print "\\end{itemize}\n";
}

if (!$first) {
    print "\\end{itemize}\n"
}

$init=close_itemize($init);

