//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef XML_TREE_HPP
#define XML_TREE_HPP

#include <ReferenceCounting.hpp>
#include <XMLTag.hpp>

#include <Assert.hpp>

#include <vector>
#include <string>
#include <map>

/**
 * @file   XMLTree.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 10 17:15:01 2007
 * 
 * @brief  XML tree stored into memory
 */
class XMLTree
{
public:
  typedef std::multimap<std::string,
			ReferenceCounting<XMLTag> > TagList;

  typedef std::pair<TagList::iterator,TagList::iterator> range;
  typedef std::pair<TagList::const_iterator,TagList::const_iterator> const_range;

private:
  TagList __tagList;		/**< tag list */

  std::vector<ReferenceCounting<XMLTag> >
  __openTags;			/**< open tags */

  /** 
   * forbidden copy constructor
   * 
   */
  XMLTree(const XMLTree&);

  /** 
   * Gets the tag "full name" (ie: including the path to the tag)
   * 
   * @return full tag name '>' is the separator
   */
  std::string
  __getFullTagName() const
  {
    std::string path;
    for (std::vector<ReferenceCounting<XMLTag> >::const_iterator i=__openTags.begin();
	 i != __openTags.end(); ++i) {
      path += '>'+(*i)->name();
    }
    return path;
  }

  /** 
   * Gets standard xml path name from local description
   * 
   * @param pathName given local description
   * 
   * @return xml path
   */
  std::string
  __tagFromPath(const std::string& pathName) const;

public:
  /** 
   * Sets file description
   * 
   * @param xml xml tag
   * @param version version attribute
   * @param versionNumber version number
   */
  void setHead(const std::string& xml,
	       const std::string& version,
	       const std::string& versionNumber)
  {
    if ((xml != "xml")or(version != "version")or(versionNumber != "1.0"))
      {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot read file format: <?"+xml+" "+version+"=\""+versionNumber+"\"?>",
			 ErrorHandler::normal);
    }
  }

  /** 
   * Checks that xml tree is correct
   * 
   */
  void check() const;

  /** 
   * Checks that a given path exists or throw an exception
   * 
   * @param pathName pathName for error message
   * @param r range to check
   */
  void checkRange(const std::string& pathName,
		  const_range r) const;

  /** 
   * Adds a tag to the list
   * 
   * @param tag tag to add
   */
  void addTag(ReferenceCounting<XMLTag> tag);

  /** 
   * Closes current tag
   * 
   * @param tagName name of the tag
   */
  void closeTag(const std::string& tagName);

  /** 
   * gets tag list end()
   * 
   * @return __tagList.end();
   */
  TagList::const_iterator end() const
  {
    return __tagList.end();
  }

  /** 
   * Checks if some tags are still opened
   * 
   * @return true if there are some
   */
  bool hasOpenTag() const
  {
    return (__openTags.rbegin() != __openTags.rend());
  }

  /** 
   * Access to the current tag
   * 
   * @return the last tag opened
   */
  ReferenceCounting<XMLTag>
  getCurrentTag()
  {
    return *__openTags.rbegin();
  }

  /** 
   * Read-only access to the current tag
   * 
   * @return the last tag opened
   */
  ConstReferenceCounting<XMLTag>
  getCurrentTag() const
  {
    return *__openTags.rbegin();
  }

  /** 
   * Checks if a pathName to a tag exists
   * 
   * @param pathName tag name
   * 
   * @return true if it exists
   */
  bool hasTag(const std::string& pathName) const
  {
    ASSERT(pathName[0] == '>');
    return (__tagList.find(pathName) != __tagList.end());
  }

  /** 
   * Searches for a tag according to its path name
   * 
   * @param pathName tag name
   * 
   * @return the tag if found
   */
  ReferenceCounting<XMLTag>
  findTag(const std::string& pathName)
  {
    ASSERT(pathName[0] == '>');
    range r = __tagList.equal_range(pathName);
    this->checkRange(pathName,r);
    if (distance(r.first, r.second) != 1) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "tag "+this->__tagFromPath(pathName)+" is not unique!",
			 ErrorHandler::normal);
    }
    
    return r.first->second;
  }

  /** 
   * Searches for a tag according to its path name
   * 
   * @param pathName tag name
   * 
   * @return the tag if found
   */
  ReferenceCounting<XMLTag>
  findTag(const std::string& pathName) const
  {
    ASSERT(pathName[0] == '>');
    const_range r = __tagList.equal_range(pathName);
    this->checkRange(pathName,r);
    if (distance(r.first, r.second) != 1) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "tag "+this->__tagFromPath(pathName)+" is not unique!",
			 ErrorHandler::normal);
    }
    return r.first->second;
  }

  /** 
   * Searches for a tag range according to its path name
   * 
   * @param pathName tag name
   * 
   * @return the range for this pathName
   */
  range
  findTagRange(const std::string& pathName)
  {
    ASSERT(pathName[0] == '>');
    range r = __tagList.equal_range(pathName);
    this->checkRange(pathName,r);
    return r;
  }

  /** 
   * Searches for a tag range according to its path name
   * 
   * @param pathName tag name
   * 
   * @return the range for this pathName
   */
  const_range
  findTagRange(const std::string& pathName) const
  {
    ASSERT(pathName[0] == '>');
    const_range r = __tagList.equal_range(pathName);
    this->checkRange(pathName,r);
    return r;
  }

  /** 
   * Constructor
   * 
   */
  XMLTree()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~XMLTree()
  {
    ;
  }
};

#endif // XML_TREE_HPP
