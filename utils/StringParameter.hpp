//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef STRING_PARAMETER_HPP
#define STRING_PARAMETER_HPP

#include <Parameter.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/*!
  \class StringParameter
  This is the base describes String Parameters.

  \author St�phane Del Pino.
*/
class StringParameter
  : public Parameter
{
private:
  void reset()
  {
    __stringValue = __defaultStringValue;
  }

  const std::string __defaultStringValue;
  std::string __stringValue;

  std::ostream& put (std::ostream& os) const
  {
    os << __stringValue;
    return os;
  }

public:
  //! Does not add other identifiers.
  void get(IdentifierSet& I)
  {
    ;
  }

  void set(const real_t d)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the real value '"+stringify(d)
		       +"' to a string parameter",
		       ErrorHandler::normal);
  }

  void set(const int i)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the integer value '"+stringify(i)
		       +"' to a string parameter",
		       ErrorHandler::normal);
  }

  void set(const char* c)
  {
    __stringValue = c;
  }

  operator std::string&()
  {
    return __stringValue;
  }

  operator const std::string&() const
  {
    return __stringValue;
  }

  const std::string typeName() const
  {
    return "string";
  }

  StringParameter(const StringParameter& sp)
    : Parameter(sp),
      __defaultStringValue(sp.__defaultStringValue),
      __stringValue(sp.__stringValue)
  {
    ;
  }

  StringParameter(const std::string& s, const char* label)
    : Parameter(Parameter::String, label),
      __defaultStringValue(s),
      __stringValue(__defaultStringValue)
  {
    ;
  }

  StringParameter(const char* c, const char* label)
    : Parameter(Parameter::String, label),
      __defaultStringValue(c),
      __stringValue(__defaultStringValue)
  {
    ;
  }

  ~StringParameter()
  {
    ;
  }
};

#endif // STRING_PARAMETER_HPP
