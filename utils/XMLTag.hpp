//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef XML_TAG_HPP
#define XML_TAG_HPP

#include <ReferenceCounting.hpp>

#include <XMLContentBase.hpp>
#include <XMLAttribute.hpp>

#include <ErrorHandler.hpp>

#include <Assert.hpp>

#include <iostream>
#include <string>

/**
 * @file   XMLTag.hpp
 * @author Stephane Del Pino
 * @date   Tue Aug  8 19:59:57 2006
 * 
 * @brief Simple embedding of basic XML tags
 */
class XMLTag
{
private:
  typedef std::map<std::string,
		   ReferenceCounting<XMLAttribute> > AttributeList;

  const std::string
  __name;			/**< name of the tag */

  AttributeList __attributes;	/**< list of attributes */

  ReferenceCounting<XMLContentBase>
  __content;			/**< content of the tag if any */

public:
  /** 
   * Looks for an attribute according to its name
   * 
   * @param name name of the attribute
   * @param defaultValue value to use if attribute is not found
   * 
   * @return a pointer to the attribute
   */
  ReferenceCounting<XMLAttribute>
  findAttribute(const std::string& name,
		const std::string& defaultValue = "") const
  {
    AttributeList::const_iterator attribute = __attributes.find(name);
    if (attribute == __attributes.end()) {
      if (defaultValue.size() == 0) {
	throw ErrorHandler(__FILE__,__LINE__,
			   "cannot find mandatory attribute '"+name+"' into <"+stringify(*this)+">",
			   ErrorHandler::normal);
      } else {
	return new XMLAttribute(name,defaultValue);
      }
    }

    return attribute->second;
  }

  /** 
   * Writes a tag to a stream
   * 
   * @param os given output stream
   * @param xmlTag the tag to write
   * 
   * @return os
   */
  friend std::ostream& operator<< (std::ostream& os,
				   const XMLTag& xmlTag)
  {
    os << xmlTag.__name;
    for (AttributeList::const_iterator i = xmlTag.__attributes.begin();
	 i != xmlTag.__attributes.end(); ++i) {
      os << ' ' << i->second->name() << "=\""
	 << i->second->value() << '"';
    }
    return os;
  }

  /** 
   * Closes a tag checking that the closure fits the opening
   * 
   * @param name closed tag name
   */
  void close(const std::string& name)
  {
    if (__name != name) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "tag <"+__name+"> closed by </"+name+">",
			 ErrorHandler::normal);
    }
  }

  /** 
   * Adds an attribute to a tag
   * 
   * @param xmlAttribute given attribute
   */
  void add(ReferenceCounting<XMLAttribute> xmlAttribute)
  {
    if (__attributes.find(xmlAttribute->name()) != __attributes.end()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "duplicated attribute '"+
			 xmlAttribute->name()+
			 "' in tag '"+__name+'\'',
			 ErrorHandler::normal);
    }
    __attributes[xmlAttribute->name()] = xmlAttribute;
  }

  /** 
   * Adds a content
   * 
   * @param xmlContent given content
   */
  void add(ReferenceCounting<XMLContentBase> xmlContent)
  {
    ASSERT(__content == 0);
    __content = xmlContent;
  }

  /** 
   * Access to the content of the tag
   * 
   * @return __content
   */
  ReferenceCounting<XMLContentBase>
  content()
  {
    return __content;
  }

  /** 
   * Read only access to the content of the tag
   * 
   * @return __content
   */
  ConstReferenceCounting<XMLContentBase>
  content() const
  {
    return __content;
  }

  /** 
   * Gets the tag name
   * 
   * @return __name
   */
  const std::string&
  name() const
  {
    return __name;
  }

  /** 
   * Constructor 
   * 
   * @param name name of the tag to build
   */
  XMLTag(const std::string& name)
    : __name(name),
      __content(0)
  {
    ;
  }
};

#endif // XML_TAG_HPP
