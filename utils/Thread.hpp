//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003 Pascal Hav�
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id$

/**
 * @file   Thread.hpp
 * @author Pascal Have
 * @date   Tue Oct  7 21:35:03 2003
 * 
 * @brief Thread, Semaphore and Mutex classes
 * 
 * @note If the system does not support pthread, a none threaded
 * version is used (see second part of this file)
 * 
 */

#ifndef THREAD_HPP
#define THREAD_HPP

#include <cstddef>
#include <config.h>
#include <Assert.hpp>

//! Virtual Runnable Function
/*! Used as template independent virtual class */
class Runnable {
public:
  //! Destructor 
  virtual ~Runnable() { }
  
  //! Run function
  virtual void run() = 0;

  //! Exit function
  virtual void clean() { }
};


/** 
 * We first define the pthread versions of those classes
 */
#ifdef HAVE_PTHREAD
#include <pthread.h>
#include <semaphore.h>

extern "C" {  
  //! C function called when cleaning a thread
  inline void clean_function(void * t) {
    reinterpret_cast<Runnable *>(t)->clean();
  }

  //! C function called for starting a thread class
  inline void * thread_function(void * t) {
    Runnable *f = reinterpret_cast<Runnable *>(t);
    ASSERT(f != NULL);
    pthread_cleanup_push(clean_function,f);
    f->run(); // Appel par virtual
    pthread_cleanup_pop(0);
    pthread_exit(NULL);
    return NULL;
  }
}

//! Main Thread class
class Thread
  : public Runnable
{
protected:
  pthread_t thread;		/**< The thread struture */

public:
  /** 
   * Starts the Tread
   * 
   * @return true if anything was ok
   */
  bool start()
  {
    if (thread) return false;
#ifndef NDEBUG
    const int ret = 
#endif /* NDEBUG */
      pthread_create(&thread,NULL,
		     thread_function,
		     this);
    ASSERT(ret == 0);
    return true;
  }

  /** 
   * Cancels the current thread
   * 
   */
  void cancel() const
  {
    pthread_cancel(thread);
  }

  /** 
   * Changes cancellable state of a thread
   * 
   * @param b true or false
   */
  static inline void cancellable(const bool& b)
  {
    pthread_setcancelstate((b)?PTHREAD_CANCEL_ENABLE:PTHREAD_CANCEL_DISABLE
			   ,NULL);
  }

  /** 
   * Forces cancelation of cancel request.
   * 
   */
  static inline void cancellable()
  {
    pthread_testcancel();
  }

  /** 
   * 
   * Waits for thread to finish
   * 
   * @return true if thread was already joined
   */
  inline bool join()
  {
    if (thread) {
      void * ret;
      pthread_join(thread,&ret);
      return false;
    }
    return true;
  }

  /** 
   * Gets the Id of the Thread
   * 
   */
  inline const pthread_t& getId() const
  {
    return thread;
  }

  /** 
   * Constructor
   * 
   */
  Thread()
    : thread(0)
  {
    ;
  }

  /** 
   * Thread destructir
   * 
   */
  virtual ~Thread()
  {
    this->join();
  }
};

//! Mutex class
class Mutex
{
protected:
  pthread_mutex_t mutex;	/**< the mutex */

public:
  /** 
   * Locks the mutex resource
   * 
   */
  inline void lock()
  {
    pthread_mutex_lock(&mutex); 
  }

  /** 
   * Unlocks mutex resource
   * 
   */
  inline void unlock()
  {
    pthread_mutex_unlock(&mutex);
  }

  /** 
   * Tries to lock mutex resource
   * 
   * 
   * @return 0 if locking was possible
   */
  inline int trylock()
  {
    return pthread_mutex_trylock(&mutex);
  }

  /** 
   * Constructor
   * 
   */
  Mutex()
  { 
    pthread_mutex_init(&mutex,NULL);
  }

  /** 
   * Destructor
   * 
   */
  ~Mutex()
  {
    pthread_mutex_destroy(&mutex);
  }
};

//! Semaphore class
class Semaphore
{
protected:
  sem_t semaphore;		/**< semaphore structure */

public:

  /** 
   * Waits for resource
   * 
   */
  inline void wait()
  {
    sem_wait(&semaphore);
  }

  /** 
   * Increments semaphore counter (decrease priority)
   * 
   */
  inline void post()
  {
    sem_post(&semaphore);
  }

  /** 
   * Tries to get resource
   * 
   * 
   * @return 0 if not reserved
   */
  inline int trywait()
  {
    return sem_trywait(&semaphore);
  }

  /** 
   * Constructor
   * 
   */
  Semaphore()
  { 
    sem_init(&semaphore,
	     0, // Interprocessus?
	     1  // Number of threads which can access simultaneously to the protected zone
	     );
  }

  /** 
   * Destructor
   * 
   */
  ~Semaphore()
  {
    sem_destroy(&semaphore);
  }
};

#else  // HAVE_PTHREAD

/** 
 * We now define the unthreaded versions of those classes.
 *
 * We only provide here the same interface as above without anyother
 * effort.
 */

//! Main Thread class
class Thread
  : public Runnable
{
public:
  typedef size_t Id;		/**< Defines Thread Id */

public:
  /** 
   * Starts the Tread
   * 
   * @return true if anything was ok
   */
  bool start()
  {
    this->run();
    return true;
  }

  /** 
   * Cancels the current thread
   * 
   */
  void cancel() const
  {
    ;
  }

  /** 
   * Changes cancellable state of a thread
   * 
   * @param b true or false
   */
  static inline void cancellable(const bool& b)
  {
    ;
  }

  /** 
   * Forces cancelation of cancel request.
   * 
   */
  static inline void cancellable()
  {
    ;
  }

  /** 
   * 
   * Waits for thread to finish
   * 
   * @return true if thread was already joined
   */
  inline bool join()
  {
    return true;
  }

  /** 
   * Gets the Id of the Thread
   * 
   */
  inline Id getId() const
  {
    return 0;
  }

  /** 
   * Constructor
   * 
   */
  Thread()
  {
    ;
  }

  /** 
   * Thread destructir
   * 
   */
  virtual ~Thread()
  {
    ;
  }
};

//! Mutex class
class Mutex
{
public:
  /** 
   * Locks the mutex resource
   * 
   */
  inline void lock()
  {
    ;
  }

  /** 
   * Unlocks mutex resource
   * 
   */
  inline void unlock()
  {
    ;
  }

  /** 
   * Tries to lock mutex resource
   * 
   * 
   * @return 0 if locking was possible
   */
  inline int trylock()
  {
    return 0;
  }

  /** 
   * Constructor
   * 
   */
  Mutex()
  { 
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Mutex()
  {
    ;
  }
};

//! Semaphore class
class Semaphore
{
public:
  /** 
   * Waits for resource
   * 
   */
  inline void wait()
  {
    ;
  }

  /** 
   * Increments semaphore counter (decrease priority)
   * 
   */
  inline void post()
  {
    ;
  }

  /** 
   * Tries to get resource
   * 
   * 
   * @return 0 if not reserved
   */
  inline int trywait()
  {
    return 0;
  }

  /** 
   * Constructor
   * 
   */
  Semaphore()
  { 
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Semaphore()
  {
    ;
  }
};


#endif // HAVE_PTHREAD

#endif /* THREAD_HPP */
