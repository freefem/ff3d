//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef  STREAMCENTER_HPP
#define  STREAMCENTER_HPP

#include <sstream>
#include <iostream>

#include <OStream.hpp>
#include <StaticBase.hpp>

#include <config.h>

#ifdef HAVE_GUI_LIBS
class QTextEdit;
#endif // HAVE_GUI_LIBS

class StreamCenter
  : public ThreadStaticBase<StreamCenter>
{
private:
  friend class StaticCenter;

  int __debugLevel;

  //! used as a black hole to write unwanted output.
  OStream __nullStream;

  //! Mainly used to write output in a file.
  OStream __log;

  //! standard output
  OStream __out;

  //! error output
  OStream __err;

  //! forbides the copy constructor.
  StreamCenter(const StreamCenter& SC);

public:

#ifdef HAVE_GUI_LIBS
  /** 
   * Sets the graphic console 
   *
   * @param console the graphic console
   */
  void setConsole(QTextEdit* console);
#endif // HAVE_GUI_LIBS

  inline OStream& log(int i)
  {
    if (i<=__debugLevel)
      return __log;
    else
      return __nullStream;
  }

  inline OStream& out(int i)
  {
    if (i<=__debugLevel)
      return __out;
    else
      return __nullStream;
  }

  inline OStream& err(int i)
  {
    if (i<=__debugLevel)
      return __err;
    else
      return __nullStream;
  }

  int getDebugLevel() const
  {
    return __debugLevel;
  }

  void setDebugLevel(int i)
  {
    __debugLevel = i;
  }

  StreamCenter();

  ~StreamCenter()
  {
    ;
  }
};

//! these functions are used as short cuts (ie: aliases).
inline OStream& fflog(int i)
{
  return StreamCenter::instance().log(i);
}

inline OStream& ffout(int i)
{
  return StreamCenter::instance().out(i);
}

inline OStream& fferr(int i)
{
  return StreamCenter::instance().err(i);
}

#endif //STREAMCENTER_HPP
