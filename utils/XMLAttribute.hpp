//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef XML_ATTRIBUTE_HPP
#define XML_ATTRIBUTE_HPP

/**
 * @file   XMLAttribute.hpp
 * @author Stephane Del Pino
 * @date   Tue Aug  8 20:01:55 2006
 * 
 * @brief  XML attributes
 */
class XMLAttribute
{
private:
  const std::string __name;
  const std::string __value;

  XMLAttribute(const XMLAttribute& xmlAttribute);

public:
  const std::string&
  name() const
  {
    return __name;
  }

  const std::string&
  value() const
  {
    return __value;
  }

  XMLAttribute(const std::string& name,
	       const std::string& value)
    : __name(name),
      __value(value)
  {
    ;
  }

  ~XMLAttribute()
  {
    ;
  }
};

#endif // XML_ATTRIBUTE_HPP
