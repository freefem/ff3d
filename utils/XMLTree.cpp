//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <XMLTree.hpp>

void XMLTree::
addTag(ReferenceCounting<XMLTag> tag)
{
  __openTags.push_back(tag);
  __tagList.insert(std::make_pair(this->__getFullTagName(),tag));
}

void XMLTree::
closeTag(const std::string& tagName)
{
  this->getCurrentTag()->close(tagName);
  __openTags.pop_back();
}


std::string XMLTree::
__tagFromPath(const std::string& pathName) const
{
  std::string tagName = "<";
  for (size_t i=1; i<pathName.size(); ++i) {
    if (pathName[i] == '>') {
      tagName += "><";
    } else {
      tagName += pathName[i];
    }
  }
  tagName += '>';
  return tagName;
}

void XMLTree::
checkRange(const std::string& pathName,
	   const_range r) const
{
  if (distance(r.first, r.second) == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot find tag "+this->__tagFromPath(pathName),
		       ErrorHandler::normal);
  }
}

void XMLTree::
check() const
{
  if (__openTags.size() != 0) {
    std::string path;
    for (std::vector<ReferenceCounting<XMLTag> >::const_iterator i=__openTags.begin();
	 i != __openTags.end(); ++i) {
      path += '<'+(*i)->name()+'>';
    }

    throw ErrorHandler(__FILE__,__LINE__,
		       "tag "+path+" not closed",
		       ErrorHandler::normal);
  }
}
