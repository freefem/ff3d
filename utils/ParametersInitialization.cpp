//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <ParametersInitialization.hpp>

#include <SolverDriverOptions.hpp>

#include <FatBoundaryOptions.hpp>
#include <MemoryManagerOptions.hpp>
#include <PenalizedFictitousDomainOptions.hpp>
#include <EliminatedFictitiousDomainOptions.hpp>

#include <KrylovSolverOptions.hpp>

#include <ConjugateGradientOptions.hpp>
#include <BiConjugateGradientOptions.hpp>
#include <BiConjugateGradientStabilizedOptions.hpp>

#include <FGMRESOptions.hpp>
#include <GMRESOptions.hpp>

#include <MultiGridOptions.hpp>

#include <PenalizedFictitousDomainOptions.hpp>

#include <ParameterCenter.hpp>

ParametersInitialization::ParametersInitialization()
{
  ParameterCenter::instance().subscribe(new SolverDriverOptions);

  ParameterCenter::instance().subscribe(new FatBoundaryOptions);
  ParameterCenter::instance().subscribe(new MemoryManagerOptions);
  ParameterCenter::instance().subscribe(new PenalizedFictitousDomainOptions);
  ParameterCenter::instance().subscribe(new EliminatedFictitiousDomainOptions);

  ParameterCenter::instance().subscribe(new KrylovSolverOptions);

  ParameterCenter::instance().subscribe(new ConjugateGradientOptions);
  ParameterCenter::instance().subscribe(new BiConjugateGradientOptions);
  ParameterCenter::instance().subscribe(new BiConjugateGradientStabilizedOptions);
  ParameterCenter::instance().subscribe(new FGMRESOptions);
  ParameterCenter::instance().subscribe(new GMRESOptions);

  ParameterCenter::instance().subscribe(new MultiGridOptions);

  ParameterCenter::instance().subscribe(new PenalizedFictitousDomainOptions);
}

