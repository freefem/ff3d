//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef _PARAMETER_HPP_
#define _PARAMETER_HPP_

#include <string>

#include <Types.hpp>

#include <ReferenceCounting.hpp>
#include <IdentifierSet.hpp>

/*!
  \class Parameter

  Defines the association of a keyword and its value.

  \author Stephane Del Pino
 */

class Parameter
{
public:
  enum Type {
    Double,
    Integer,
    String,
    Enum
  };

protected:
  Type __type;

private:
  const char* __label;

  virtual std::ostream& put(std::ostream&) const = 0;

public:
  virtual void get(IdentifierSet& I) = 0;

  virtual void reset() = 0;

  virtual void set(const real_t d) = 0;
  virtual void set(const int i) = 0;
  virtual void set(const char*) = 0;

  void set(const std::string& s) {
    set(s.c_str());
  }


  virtual const std::string typeName() const = 0;

  const Type& type() const
  {
    return __type;
  }

  friend std::ostream& operator << (std::ostream& os,
				    const Parameter& P)
  {
    return P.put(os);
  }

  const char* label() const;

  Parameter(const Parameter& pv)
    : __type(pv.__type),
      __label(pv.__label)
  {
    ;
  }

  Parameter(const Parameter::Type t, const char* label)
    : __type(t),
      __label(label)
  {
    ;
  }

  virtual ~Parameter()
  {
    ;
  }
};

#endif // _PARAMETER_HPP_

