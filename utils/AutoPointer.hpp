//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef AUTOPOINTER_HPP
#define AUTOPOINTER_HPP

template <typename PointedType>
class AutoPointer
{
private:
  PointedType * __pointer;

public:
  operator PointedType*() const
  {
    return __pointer;
  }

  AutoPointer<PointedType>& operator = (PointedType* p)
  {
    if(__pointer != 0)
      delete __pointer;
    __pointer = p;
    return *this;
  }

  AutoPointer<PointedType>& operator = (const AutoPointer<PointedType>& ap)
  {
    __pointer = ap.__pointer;
    return *this;
  }

  AutoPointer()
    : __pointer(0)
  {
    ;
  }

  AutoPointer(const AutoPointer<PointedType>& ap)
    : __pointer(ap.__pointer)
  {
    ;
  }

  AutoPointer(PointedType* p)
    : __pointer(p)
  {
    ;
  }

  ~AutoPointer()
  {
    if (__pointer != 0)
      delete __pointer;
  }
};

#endif // AUTOPOINTER_HPP

