//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef XML_CONTENT_POSITION_HPP
#define XML_CONTENT_POSITION_HPP

#include <XMLContentBase.hpp>
#include <istream>

/**
 * @file   XMLContentPosition.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 10 17:06:40 2007
 * 
 * @brief describe content of tags that are position in the file
 * 
 */
class XMLContentPosition
  : public XMLContentBase
{
private:
  const std::istream::pos_type
  __position;			/**< stream position */

  /** 
   * Forbidden copy constructor
   */
  XMLContentPosition(const XMLContentPosition&);
public:

  /** 
   * access to the position in the file
   * 
   * @return __position
   */
  const std::istream::pos_type& position() const
  {
    return __position;
  }

  /** 
   * Constructor
   * 
   * @param position position in the file
   */
  XMLContentPosition(const std::istream::pos_type& position)
    : XMLContentBase(XMLContentBase::filePosition),
      __position(position)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~XMLContentPosition()
  {
    ;
  }
};

#endif // XML_CONTENT_POSITION_HPP
