//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef ZONECOPY_HPP
#define ZONECOPY_HPP

#include <Types.hpp>

/**
 * @file   ZoneCopy.hpp
 * @author Stephane Del Pino
 * @date   Thu Oct 24 15:26:37 2002
 * 
 * @brief  This is a wrapper which role is to copy zones of memory.
 * 
 * 
 */

template <typename T1, typename T2>
inline void ZoneCopy(T1* const destination,
		     T2* const source,
		     size_t size)
{
  for(size_t i=0; i<size; ++i) {
    destination[i] = source[i];
  }
}

#define CONST_ZONE_COPY_DEF(T)			\
 inline void ZoneCopy(T* const destination,	\
		      const T* const source,	\
		      size_t size);

#define ZONE_COPY_DEF(T)			\
 inline void ZoneCopy(T* const destination,	\
		      const T* const source,	\
		      size_t size);

CONST_ZONE_COPY_DEF(real_t); ZONE_COPY_DEF(real_t);
CONST_ZONE_COPY_DEF(int);    ZONE_COPY_DEF(int);


#endif // ZONECOPY_HPP

