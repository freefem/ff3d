//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef STATIC_POW_HPP
#define STATIC_POW_HPP

/**
 * @file   StaticPow.hpp
 * @author Stephane Del Pino
 * @date   Sun Apr 10 17:33:51 2005
 * 
 * @brief This class is used to compute \f$ n^m \f$ at compilation
 * time
 * 
 * 
 */
template <size_t n,
	  size_t m>
struct StaticPow
{
  enum {
    value = n*StaticPow<n,m-1>::value
  };
};

template <size_t n>
struct StaticPow<n,1>
{
  enum {
    value = n
  };
};

#endif // STATIC_POW_HPP
