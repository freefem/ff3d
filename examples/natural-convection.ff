//  -*- c++ -*-

//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003  Laboratoire J.-L. Lions UPMC Paris

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

// natural convection of an incompressible fluid flow

vector n = (11,11,11);
vertex A = (0,0,0);
vertex B = (1,1,1);

scene S = pov("natural-convection.pov");
mesh M = structured(n,A,B);
domain Omega = domain(S,outside(<1,0,0>));

mesh T = surface(Omega,M);

function u1 = 0;
function u2 = 0;
function u3 = 0;
function p = 0;

function theta0 = 0;

double dt = 1;
double K = 0.1;
double eps = 1E-3;

double i;
for (i = 1;i <= 10;i = i+1) {

   cout << "========== Natural convection step " << i << "\n";

   solve(theta) in Omega by M method(type=penalty),
     krylov(precond=diagonal), memory(matrix=sparse), cg(epsilon=1E-10)
   {
      pde(theta)
      theta-div(dt*grad(theta)) = convect([u1,u2,u3],dt,theta0);
      theta = 100 on T;
      theta = 0 on M;
   };

   theta0 = theta;
   u1 = 0;
   u2 = 0;
   u3 = 0;
   p = 0;

   //   Using the following none symetric formulation requires the
   // use of bi-conjugate gradient algorithm.
   //   One could also built a symetric matrix using the symetric
   // variational formula
   solve(u1,u2,u3,p) in Omega by M
     method(type=eliminate), krylov(precond=diagonal,type=bicg),
     cg(epsilon=1E-5), memory(matrix=sparse)
   {
      pde(u1)
	-div(grad(u1))+dx(p)=0;

      u1 = 0 on M;
      u1 = 0 on T; 

      pde(u2)
	u2-div(grad(u2))+dy(p)=K*theta0;

      u2 = 0 on M;
      u2 = 0 on T; 

      pde(u3)
	u3-div(grad(u3))+dz(p)=0;

      u3 = 0 on M;
      u3 = 0 on T;

      pde(p)
	-div(eps*grad(p))+eps*p+dx(u1)+dy(u2)+dz(u3)=0;
   };
}

save(medit,"natural-convection-u",M);
save(medit,"natural-convection-u", u1, M);
save(medit,"natural-convection-v",M);
save(medit,"natural-convection-v", u2, M);
save(medit,"natural-convection-w",M);
save(medit,"natural-convection-w", u3, M);
save(medit,"natural-convection-p",M);
save(medit,"natural-convection-p", p, M);
save(medit,"natural-convection-theta",M);
save(medit,"natural-convection-theta", theta0, M);
