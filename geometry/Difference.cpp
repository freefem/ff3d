//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Difference.hpp>

std::ostream& Difference::
__put(std::ostream& os) const
{
  os << "difference {\n";
  for (Difference::const_iterator i = __objects.begin();
       i != __objects.end(); i++) {
    os << *(*i);
  }

  for (size_t i=0; i<numberOfTransformations(); i++) {
    os << __trans[i]->povWrite() << '\n';
  }
  os << "}\n";

  return os;
}

void Difference::
push_back(const ReferenceCounting<Object>& o)
{
  __objects.push_back(o);
}

bool Difference::
__inShape (const TinyVector<3, real_t>& p) const
{
  ASSERT(__objects.begin() != __objects.end());
  const_iterator i = __objects.begin();

  if (not((*i)->inside(p))) {
    return false;
  }
  ++i;

  for (; i != __objects.end(); i++) {
    if((*i)->inside(p)) return false;
  }

  return true;
}

ReferenceCounting<Shape> Difference::
__getCopy() const
{
  return new Difference(*this);
}

Difference::
Difference(const Difference& D)
  : Shape(D)
{
  for (ObjectList::const_iterator i = D.__objects.begin();
       i != D.__objects.end(); ++i) {
    __objects.push_back((*i)->getCopy());
  }
}
