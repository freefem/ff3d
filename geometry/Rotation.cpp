//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Rotation.hpp>
#include <cmath>

#include <sstream>

TinyVector<3,real_t> Rotation::
operator()(const TinyVector<3,real_t>& v) const
{
  return __matrix*v;
}

TinyVector<3,real_t> Rotation::
inverse(const TinyVector<3,real_t>& v) const
{
  // As 'matrix' is composed of rotations, and
  // as rotations are othogonal matrices
  // the inverse of 'matrix' is its transposed
  TinyVector<3,real_t> Temporary = 0;
  for (size_t i=0; i<3; i++)
    for (size_t j=0; j<3; j++) {
      Temporary[i]+=__matrix(j,i)*v[j];
    }
  return Temporary;
}

Rotation::
Rotation(const TinyVector<3,real_t>& r)
  : Transform(rotation),
    __angles(r)
{
  const real_t deg2rad=4*std::atan(1.)/180.;

  const real_t cosx = std::cos(deg2rad*r[0]);
  const real_t cosy = std::cos(deg2rad*r[1]);
  const real_t cosz = std::cos(deg2rad*r[2]);
  const real_t sinx = std::sin(deg2rad*r[0]);
  const real_t siny = std::sin(deg2rad*r[1]);
  const real_t sinz = std::sin(deg2rad*r[2]);

  __matrix(0,0) =  cosy*cosz;
  __matrix(0,1) = -sinz*cosx + sinx*siny*cosz;
  __matrix(0,2) =  sinx*sinz + cosx*siny*cosz;
  __matrix(1,0) =  cosy*sinz;
  __matrix(1,1) =  cosx*cosz + sinx*siny*sinz;
  __matrix(1,2) = -sinx*cosy + cosx*siny*sinz;
  __matrix(2,0) = -siny;
  __matrix(2,1) =  sinx*cosy;
  __matrix(2,2) =  cosx*cosy;
}

Rotation::
Rotation(const Rotation& r)
  : Transform(r),
    __angles(r.__angles),
    __matrix(r.__matrix)
{
  ;
}

//! prints rotation information to a string.
std::string Rotation::
povWrite() const
{
  std::stringstream povs;
  povs << "rotation <"
       << __angles[0]
       << ", "
       << __angles[1]
       << ", "
       << __angles[2]
       << ">";
  povs << std::ends;
  return povs.str();
}


ReferenceCounting<Transform> Rotation::
getCopy() const
{
  return new Rotation(*this);
}
