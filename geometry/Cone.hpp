//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef CONE_HPP
#define CONE_HPP

#include <Shape.hpp>
#include <Index.hpp>

/**
 * @file   Cone.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 19:12:09 2006
 * 
 * @brief This is the class which defines a Virtual Reality Cone
 */
class Cone
  : public Shape
{
private:
  friend class InfiniteCone;
  friend class Plane;

  const Vertex __center1;	/**< first center */
  const Vertex __center2;	/**< second center */

  TinyVector<3, real_t>
  __axisVector;			/**< axis vector */
  const real_t __height;	/**< height of the cone */

  const real_t __radius1;	/**< radius at first point */
  const real_t __radius2;	/**< radius ate second point */

protected:
  /** 
   * checks if a point is inside the Cone
   * 
   * @param x the point
   * 
   * @return returns true if @f$ x\in S@f$
   */
  bool __inShape(const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the Cone to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * gets a copy of the cone
   * 
   * @return a deep copy of the Cone
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Constructor
   * 
   * @param a first vertex
   * @param b second vertex
   * @param r1 radius at first vertex
   * @param r2 radius at second vertex
   */
  Cone(const Vertex& a,
       const Vertex& b,
       const real_t& r1,
       const real_t& r2);

  /** 
   * Copy constructor
   * 
   * @param C given Cone
   */
  Cone(const Cone& C);

  /** 
   * Destructor
   * 
   */
  ~Cone()
  {
    ;
  }
};

#endif // CONE_HPP
