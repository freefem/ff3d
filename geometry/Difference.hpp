//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef DIFFERENCE_HPP
#define DIFFERENCE_HPP

#include <Object.hpp>
#include <list>

/**
 * @file   Difference.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 19:35:12 2006
 * 
 * @brief This is the class which defines a POVRay set operation: the
 * \p difference
 */
class Difference
  : public Shape
{
private:
  typedef std::list<ReferenceCounting<Object> > ObjectList;

public:
  typedef std::list<ReferenceCounting<Object> >::iterator iterator;
  typedef std::list<ReferenceCounting<Object> >::const_iterator const_iterator;

private:
  ObjectList __objects;		/**< list of shapes contained in the
				   difference */

protected:
  /** 
   * Checks if a point is inside a differnce
   * 
   * @param x given point 
   * 
   * @return true if @f$ x\in S@f$
   */
  bool __inShape (const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the Difference into a stream
   * 
   * @param os the stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * Gets a copy of the Difference
   * 
   * @return deep copy of the difference
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  const const_iterator begin() const
  {
    return __objects.begin();
  }

  iterator begin()
  {
    return __objects.begin();
  }


  const const_iterator end() const
  {
    return __objects.end();
  }

  iterator end()
  {
    return __objects.end();
  }

  /** 
   * Adds an object to the difference
   * 
   * @param o given object
   */
  void push_back(const ReferenceCounting<Object>& o);

  /** 
   * Constructor
   * 
   */
  Difference()
    : Shape(Shape::difference)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param D given Difference
   */
  Difference(const Difference& D);

  /** 
   * Destuctor
   * 
   */
  ~Difference()
  {
    ;
  }
};

#endif // DIFFERENCE_HPP
