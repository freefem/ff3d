//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef INTERSECTION_HPP
#define INTERSECTION_HPP

#include <Object.hpp>
#include <ReferenceCounting.hpp>

#include <list>

/*!  \class Intersection

  

  \author St�phane Del Pino.
*/
/**
 * @file   Intersection.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 00:22:03 2006
 * 
 * @brief defines the POVRay set operation: @a intersection.
 */
class Intersection
  : public Shape
{
private:
  typedef std::list<ReferenceCounting<Object> > ObjectList;

public:
  typedef std::list<ReferenceCounting<Object> >::iterator iterator;
  typedef std::list<ReferenceCounting<Object> >::const_iterator const_iterator;

private:
  ObjectList __objects;		/**< The list of Shapes defining in
				   the intersection. */

protected:
  /** 
   * Checks if a point is inside the Intersection
   * 
   * @param x given point
   * 
   * @return true if @f$ x\in S @f$
   */
  inline bool __inShape (const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the Intersection to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * gets a copy of the Intersection
   * 
   * @return deep copy of the Intersection
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  const const_iterator begin() const
  {
    return __objects.begin();
  }

  iterator begin()
  {
    return __objects.begin();
  }

  const const_iterator end() const
  {
    return __objects.end();
  }

  iterator end()
  {
    return __objects.end();
  }

  /** 
   * Adds an Object to the Union definition
   * 
   * @param object given ojbect
   */
  inline void push_back(ReferenceCounting<Object> object)
  {
    __objects.push_back(object);
  }

  /** 
   * Default constructor
   * 
   */
  Intersection();

  /** 
   * Copy constructor
   * 
   * @param I given Intersection
   */
  Intersection(const Intersection& I);

  /** 
   * Destructor
   * 
   */
  ~Intersection()
  {
    ;
  }
};

#endif // INTERSECTION_HPP
