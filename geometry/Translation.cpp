//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Translation.hpp>
#include <TinyVector.hpp>

#include <sstream>

TinyVector<3,real_t> Translation::
operator()(const TinyVector<3,real_t>& v) const
{
  return v+__vect;
}

TinyVector<3,real_t> Translation::
inverse(const TinyVector<3,real_t>& v) const
{
  return v-__vect;
}

Translation::
Translation(const TinyVector<3,real_t>& v)
  : Transform(translation),
    __vect(v)
{
  ;
}

//! Copy constructor.
Translation::Translation(const Translation& t)
  : Transform(t),
    __vect(t.__vect)
{
  ;
}

//! Prints Translation informations to a std::string.
std::string Translation::povWrite() const
{
  std::stringstream povs;
  povs << "translation <"
       << __vect[0]
       << ", "
       << __vect[1]
       << ", "
       << __vect[2]
       << ">";
  povs << std::ends;
  return povs.str();
}

ReferenceCounting<Transform> Translation::
getCopy() const
{
  return new Translation(*this);
}
