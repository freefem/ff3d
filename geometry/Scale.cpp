//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <Scale.hpp>
#include <sstream>

TinyVector<3,real_t> Scale::
operator()(const TinyVector<3,real_t>& v) const
{
  TinyVector<3,real_t> temp = v;

  temp[0] *= __scale[0];
  temp[1] *= __scale[1];
  temp[2] *= __scale[2];

  return temp;
}

TinyVector<3> Scale::
inverse(const TinyVector<3,real_t>& v) const
{
  TinyVector<3,real_t> temp = v;

  temp[0] /= __scale[0];
  temp[1] /= __scale[1];
  temp[2] /= __scale[2];

  return temp;
}

std::string Scale::
povWrite() const
{
  std::stringstream povs;
  povs << "scale <"
       << __scale[0]
       << ", "
       << __scale[1]
       << ", "
       << __scale[2]
       << ">";
  povs << std::ends;

  return povs.str();
}

Scale::
Scale(const TinyVector<3,real_t>& v)
  : Transform(scale),
    __scale(v)
{
  ;
}

Scale::
Scale(const Scale& scale)
  : Transform(scale),
    __scale(scale.__scale)
{
  ;
}

ReferenceCounting<Transform> Scale::
getCopy() const
{
  return new Scale(*this);
}
