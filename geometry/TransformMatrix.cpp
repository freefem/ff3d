//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <TransformMatrix.hpp>
#include <cmath>

#include <sstream>

TinyVector<3,real_t> TransformMatrix::
operator()(const TinyVector<3,real_t>& v) const
{
  TinyVector<3,real_t> temp = __matrix*v;
  temp += __translate;
  return temp;
}

TinyVector<3,real_t> TransformMatrix::
inverse(const TinyVector<3,real_t>& v) const
{
  // As 'matrix' is composed of transformMatrixs, and
  // as transformMatrixs are othogonal matrices
  // the inverse of 'matrix' is its transposed
  TinyVector<3,real_t> Temporary = v - __translate;

  return __invMatrix*Temporary;
}

TransformMatrix::
TransformMatrix(const real_t m[12])
  : Transform(matrix)
{
  for (size_t i=0;i<3;++i) {
    __translate[i] = m[9+i];
    for (size_t j=0; j<3; ++j) {
      __matrix(i,j) = m[3*i+j];
    }
  }

  __invMatrix = __matrix.invert();
}

TransformMatrix::
TransformMatrix(const TransformMatrix& r)
  : Transform(r)
{
  __translate = r.__translate;
  __matrix    = r.__matrix;
  __invMatrix = r.__invMatrix;
}

std::string TransformMatrix::
povWrite() const
{
  std::stringstream povs;
  povs << "transformMatrix <";
  for (size_t i=0; i<3; ++i) {
    for(size_t j=0; j<3; ++j) {
      povs << __matrix(i,j) << ',';
    }
  }
  for (size_t i=0; i<2; ++i)
    povs << __translate << ',';
  povs << __translate[2];
  povs << ">";
  povs << std::ends;
  return povs.str();
}

ReferenceCounting<Transform> TransformMatrix::
getCopy() const
{
  return new TransformMatrix(*this);
}
