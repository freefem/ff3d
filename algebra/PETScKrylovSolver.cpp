//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#include <PETScKrylovSolver.hpp>
#include <ErrorHandler.hpp>

int monitor(KSP ksp, int it, double norm, void*)
{
  std::cout << "iteration: " << it << " residual norm = " << norm << '\n';
  return 0;
}

PETScKrylovSolver::
PETScKrylovSolver(const Vector<real_t>& b,
		  const PETScMatrix& A,
		  Vector<real_t>& x)
{
  Vec petscB;
  VecCreateSeqWithArray(PETSC_COMM_WORLD, b.size(), &b[0], &petscB);

  Vec petscX;
  VecCreateSeqWithArray(PETSC_COMM_WORLD, x.size(), &x[0], &petscX);

  KSP ksp;
  KSPCreate(PETSC_COMM_WORLD, &ksp);
  KSPSetTolerances(ksp, 1E-20, 1E-100,1E5, 500);
  KSPSetType(ksp, KSPGMRES);
//   KSPSetType(ksp, KSPBICG);
  PC pc;
  KSPGetPC(ksp, &pc);
  PCSetType(pc, PCNONE);
  KSPSetOperators(ksp, A.mat(), A.mat(), DIFFERENT_NONZERO_PATTERN);

  KSPSetRhs(ksp, petscB);
  KSPSetSolution(ksp, petscX);
  KSPSetUp(ksp);

  KSPSetMonitor(ksp, monitor, 0, 0);

  KSPSolve(ksp);

  KSPDestroy(ksp);
  VecDestroy(petscB);
  VecDestroy(petscX);

}
