//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


// This class defines a template SparseMatrix class.

#ifndef SPARSE_MATRIX_HPP
#define SPARSE_MATRIX_HPP

#include <StreamCenter.hpp>
#include <Vector.hpp>

#include <BaseMatrix.hpp>
#include <DoubleHashedMatrix.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/*!
  \class SparseElem
  This class defines SparseLine<T> elements.

  \author St�phane Del Pino.
*/

class SparseElem
{
private:
  //! Element column index
  int __column;
  //! The stored value.
  real_t __value;
public:

  //! Access to the \a value.
  inline real_t& value()
  {
    return __value;
  }

  //! Read-only access to the \a value.
  inline const real_t& value() const
  {
    return __value;
  }

  //! Access to the column number of the element.
  inline int& column()
  {
    return __column;
  }

  //! Read-only access to the column number of the element.
  inline const int& column() const
  {
    return __column;
  }

  //! Copies the SparseElem \a SE.
  inline const SparseElem& operator=(const SparseElem& SE)
  {
    __column = SE.__column;
    __value = SE.__value;
    return (*this);
  }

  //! Default constructor
  SparseElem()
    : __column(-1),		//!< Means that the element is not used. 
      __value(0)	        //!< Sould be initialized to 0 to prepare matrix assembling
  {
    ;
  } 

  //! Copy constuctor.
  SparseElem(const SparseElem& SE)
    : __column(SE.__column),
      __value(SE.__value)
  {
    ;
  }

  //! Destructor.
  ~SparseElem()
  {
    ;
  }
};

/*!
  \class SparseLine
  This class defines SparseMatrix<T> lines.

  \author St�phane Del Pino.
*/
class SparseLine
{
private:
  //! The number of non-zero elements by line (can change).
  size_t s;

  //! Elements list
  SparseElem* element;
public:
  /*! Access to the Element \a i of the SparseLine.
    \warning \a i is \b not the column number in the Matrix.
  */
  inline SparseElem& operator[](const size_t& i)
  {
    ASSERT (i<s);
    return element[i];
  }

  /*! Read-only access to the Element \a i of the SparseLine.
    \warning \a i is \b not the column number in the Matrix.
  */
  inline const SparseElem& operator[](const size_t& i) const
  {
    ASSERT (i<s);
    return element[i];
  }

  //! Read-only access to the size of the line.
  inline const size_t& size() const
  {
    return s;
  }

  //! Resize the line.
  inline void resize(const size_t& newSize)
  {
    if (s!=0)
      delete [] element;
    s = newSize;
    element = new SparseElem[s];
  }

  //! Constructs a SparseLine containing \a n elements.
  SparseLine(const size_t& n )
    : s(n)
  {
    element = new SparseElem[s];
  }

  //! Default conctructor builds a 0 element SparseLine.
  SparseLine()
    : s(0)
  {
    ;
  }

  //! Destructor.
  ~SparseLine()
  {
    if (s>0)
      delete[] element;
  }
};

/*!
  \class SparseMatrix
  This class defines a sparse matrix.

  \todo rework the impementation and add other types of matrices, and
  specializations such as symetric, ...

  \author St�phane Del Pino.
*/

class SparseMatrix
  : public BaseMatrix
{
private:
  //! SparseLines list
  SparseLine* __line;

  const real_t __zero;

  /*! Contains the result of Matrix Vector Product, this trick avoids the
    cost of memory management during conjugate gradient and same methods.
  */
  mutable Vector<real_t> __Au;

public:
  void reset()
  {
    for (size_t i=0; i<__size; ++i) {
      SparseLine& SL = __line[i];
      for (size_t j=0; j<SL.size(); ++j) {
	SL[j].value() = 0;
      }
    }
  }

  void getDiagonal(BaseVector& X) const
  {
    Vector<real_t>& x = dynamic_cast<Vector<real_t>&>(X);
    for (size_t i=0; i<this->size();++i)
      x[i] = (*this)(i,i);
  }

  void timesX(const BaseVector& X,
	      BaseVector& Z) const
  {
    const Vector<real_t>& x = dynamic_cast<const Vector<real_t>&>(X);
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);
    z = (*this)*x;
  }

  //! Read-only access to the SparseElem (\a i, \a j) of the matrix.
  const real_t& operator()(const size_t& i,
			   const size_t& j) const
  {
    SparseLine& SL = __line[i];
    size_t icol;

    for (icol=0; icol<__line[i].size(); ++icol) {
      const int& column = SL[icol].column();
      if (column == (int)j)
	break;
      else
	if (column == -1) {
	  break;
	}
    }
    if (icol == __line[i].size()) {		// means the element was not found
      return __zero;
    }
    return (SL[icol].value());
  }

  //! Read-only access to the SparseLine \a i.
  const SparseLine& line(const size_t i) const
  {
    ASSERT(i<__size);
    return __line[i];
  }

  //! Access to the SparseElem (\a i, \a j) of the matrix.
  real_t& operator()(const size_t& i,
		     const size_t& j)
  {
    SparseLine& SL = __line[i];
    size_t icol;

    for (icol=0; icol<__line[i].size(); ++icol) {
      const int& column = SL[icol].column();
      if (column == (int)j)
	break;
      else
	if (column == -1) {
	  SL[icol].column() = j;
	  break;
	}
    }
    if (icol == __line[i].size()) {		// means the element was not found
      std::string errorMsg
	= "cannot access element ("+stringify(i)+","+stringify(j)+")\n";

      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg,
			 ErrorHandler::unexpected);
    }
    return (SL[icol].value());
  }

  //! matrix vector product.
  const Vector<real_t>& operator*(const Vector<real_t>& V) const
  {
    __Au = 0;

    for (size_t i=0; i<__size; ++i) {
      const SparseLine& SL = __line[i];
      for (size_t j=0; j<SL.size(); ++j) {
	const int& k = SL[j].column();
	if (k!=-1)
	  __Au[i] += SL[j].value()*V[k];
	else
	  break;
      }
    }
    return __Au;
  }

  //! matrix vector product.
  void transposedTimesX(const BaseVector& X, BaseVector& Z) const
  {
    const Vector<real_t>& x = dynamic_cast<const Vector<real_t>&>(X);
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);
    ASSERT(x.size()==z.size());
    ASSERT(this->size()==x.size());
    z = 0;

    for (size_t i=0; i<__size; ++i) {
      const SparseLine& SL = __line[i];
      for (size_t j=0; j<SL.size(); ++j) {
	const int& k = SL[j].column();
	if (k!=-1)
	  z[k] += SL[j].value()*x[i];
	else
	  break;
      }
    }
  }

  void copyProfile(const SparseMatrix& A)
  {
    __size = A.size();
    __Au.resize(__size);

    __line = new SparseLine[__size];
    for (size_t i=0; i<__size; ++i) {
      const SparseLine& Aline = A.line(i);
      const size_t s = Aline.size();
      __line[i].resize(s);
    }
  }

  SparseMatrix(const DoubleHashedMatrix& A)
    : BaseMatrix(BaseMatrix::sparseMatrix, A.numberOfLines()),
      __zero(0),
      __Au(A.numberOfLines())
  {
    __line = new SparseLine[__size];
    for (size_t i=0; i<__size; ++i) {
      __line[i].resize(A.numberOfLineNonNull(i));

      for (DoubleHashedMatrix::const_iterator browsLine = A.beginOfLine(i);
	   browsLine != A.endOfLine(i); ++browsLine) {
	(*this)(i,browsLine.first()) = browsLine.second();
      }
    }    
  };

  SparseMatrix()
    : BaseMatrix(BaseMatrix::sparseMatrix),
      __zero(0)
  {
    ;
  };

  //! Destructor.
 ~SparseMatrix()
  {
     delete[] __line;
  }
};

#endif // SPARSE_MATRIX_HPP
