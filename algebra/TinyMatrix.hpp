//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef TINY_MATRIX_HPP
#define TINY_MATRIX_HPP

#include <TinyVector.hpp>

/**
 * @file   TinyMatrix.hpp
 * @author St�phane Del Pino
 * @date   Thu Aug  7 17:00:51 2003
 * 
 * @brief Template martix class. Dimensions and types are given at
 * compilation time
 * 
 * 
 */
template <size_t M, size_t N,
	  typename T=real_t>
class TinyMatrix
{
public:
  typedef TinyVector<N>
  AssociatedVector;		/**< Type of compatibke vector */

  typedef TinyVector<M>
  AssociatedTransposedVector;	/**< Type of compatible transposed
				   vector */

  typedef T ValueType;		/**< The type of the elements of the
				   TinyMatrix */

  enum {
    NumberOfLines = M,		/**< number of lines */
    NumberOfRow   = N		/**< number of rows */
  };

private:
  T __x[M][N];			/**< values of the matrix */

public:
  
  /** 
   * Access to the \f$(i,j)\f$ coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  inline T& operator()(const size_t& i, const size_t& j)
  {
    ASSERT ((i<M) && (j<N));
    return __x[i][j];
  }

  /** 
   * Read-only access to the (i,j) coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  inline const T& operator()(const size_t& i, const size_t& j) const
  {
    ASSERT ((i<M) && (j<N));
    return __x[i][j];
  }

  /** 
   * Makes the current matrix be equal to a given one
   * 
   * @param B the given matrix
   * 
   * @return the matrix \f$ A := B \f$
   */
  inline const TinyMatrix<M,N,T>& operator=(const TinyMatrix<M,N,T>& B)
  {
    T* y = __x[0];
    const T* B_y = B.__x[0];
    for (size_t i=0; i<M*N; i++)
      y[i] = B_y[i];
    return (*this);
  }

  /** 
   * Adds a matrix to a given one
   * 
   * @param B the matrix to add
   * 
   * @return \f$ A := A +B \f$
   */
  inline const TinyMatrix<M,N,T>& operator += (const TinyMatrix<M,N,T>& B)
  {
    T* y = __x[0];
    const T* B_y = B.__x[0];
    for (size_t i=0; i<M*N; i++)
      y[i] += B_y[i];
    return (*this);
  }

  /** 
   * Substracts a given matrix
   * 
   * @param B the matrix to substract
   * 
   * @return \f$ A := A-B \f$
   */
  inline const TinyMatrix<M,N,T>& operator -= (const TinyMatrix<M,N,T>& B)
  {
    T* y = __x[0];
    const T* B_y = B.__x[0];
    for (size_t i=0; i<M*N; i++)
      y[i] -= B_y[i];
    return (*this);
  }

  /** 
   * Computes the product on the right to a given \b compatible matrix
   * 
   * @param B the given matrix
   * 
   * @return \f$ AB \f$
   */
  template <size_t P>
  inline TinyMatrix<M,P> operator * (const TinyMatrix<N,P>& B) const
  {
    TinyMatrix<M,P> C(0);
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<P; j++)
	for (size_t k=0; k<N; k++)
	  C(i,j) += __x[i][k] * B(k,j);
    return C;
  }

  /** 
   * Computes the product with a given compatible vector
   * 
   * @param v the given vector
   * 
   * @return \f$ Av \f$
   */
  inline TinyVector<M,T> operator * (const TinyVector<N,T>& v) const
  {
    TinyVector<M,T> u = 0;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	  u[i] += __x[i][j] * v[j];
    return u;
  }

  /** 
   * Computes the product with a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ t A \f$
   */
  inline TinyMatrix<M,N,T> operator * (const T& t) const
  {
    TinyMatrix<M,N,T> B;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	  B.__x[i][j] = __x[i][j] * t;
    return B;
  }

  /** 
   * Computes the product with the invert of a given vector on the right
   * 
   * @param t the given vector
   * 
   * @return \f$ t^{-1} A \f$
   */
  inline TinyMatrix<M,N,T> operator / (const T& t) const
  {
    ASSERT(t != 0);
    TinyMatrix<M,N,T> B;
    const T t_1 = 1./t;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	  B.__x[i][j] = __x[i][j] * t_1;
    return B;
  }

  /** 
   * Multiplies a matrix by a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ A := t A \f$
   */
  inline const TinyMatrix<M,N,T>& operator *= (const T& t)
  {
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] *= t;
    return (*this);
  }

  /** 
   * Multiplies a matrix by the invert of a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ A := t^{-1} A \f$
   */
  inline const TinyMatrix<M,N,T>& operator /= (const T& t)
  {
    const T t_1 = 1./t;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] *= t_1;
    return (*this);
  }

  /** 
   * Computes the product by a T on the left
   * 
   * @param t the multiplicative T
   * @param A the matrix
   * 
   * @return \f$ t A \f$
   */
  inline friend TinyMatrix<M,N,T> operator * (const T& t,
					      const TinyMatrix<M,N,T>& A)
  {
    return (A*t);
  }

  /** 
   * Computes the sum of two matrices
   * 
   * @param B the second matrix
   * 
   * @return \f$ A+B \f$
   */
  inline TinyMatrix<M,N,T> operator + (const TinyMatrix<M,N,T>& B) const
  {
    TinyMatrix<M,N,T> C;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	C.__x[i][j] = __x[i][j] + B.__x[i][j];
    return C;
  }

  /** 
   * Computes the difference of two matrices
   * 
   * @param B the second matrix
   * 
   * @return \f$ A - B \f$
   */
  inline TinyMatrix<M,N,T> operator - (const TinyMatrix<M,N,T>& B) const
  {
    TinyMatrix<M,N,T> C;
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	  C.__x[i][j] = __x[i][j] - B.__x[i][j];
    return C;
  }

  /** 
   * Prints a matrix to a stream
   * 
   * @param os the given stream
   * @param A the given matrix
   * 
   * @return the modified stream
   */
  friend std::ostream& operator << (std::ostream& os,
				    const TinyMatrix<M,N,T>& A)
  {
    for (size_t i=0; i<M; i++) {
      for (size_t j=0; j<N; j++)
	os << A(i,j) << " ";
      os << '\n';
    }
    return os;
  }

  /** 
   * Default constructor
   * 
   * @note matrix is initialized to 0
   */
  TinyMatrix()
  {
    T* y = __x[0];
    for (size_t i=0; i<M*N; i++) {
      y[i] = 0;
    }
  }

  /** 
   * Copy constructor
   * 
   * @param B the matrix to copy
   */
  TinyMatrix(const TinyMatrix<M,N,T>& B)
  {
    T* y = __x[0];
    const T* B_y = B.__x[0];
    for (size_t i=0; i<M*N; i++) {
      y[i] = B_y[i];
    }
  }

  /** 
   * Constructs a matrix using a given T
   * 
   * @param t the given T
   *
   * @note the built matrix is diagonal. Its coefficients are t for
   * the first diagonal terms, 0 for others
   * @note does have no sens (diagonal is not well defined) 
   */
  TinyMatrix(const T& t)
  {
    for (size_t i=0; i<M; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = (i==j)?t:0;
  }

  /** 
   * The destructor
   * 
   */
  ~TinyMatrix()
  {
    ;
  }
};

/**
 * @class  TinyMatrix<N,N,T>
 * @author St�phane Del Pino
 * @date   Thu Aug  7 18:31:38 2003
 * 
 * @brief this class is used to manage small squared matrices. There
 * size is to be known at compilation time
 * 
 */
template <size_t N,
	  typename T>
class TinyMatrix<N,N,T>
{
public:
  typedef TinyVector<N>
  AssociatedVector;		/**< Type of compatible vector */

  typedef TinyVector<N> 
  AssociatedTransposedVector;	/**< Type of compatible transposed
				   vector */

  typedef T ValueType;		/**< The type of the elements of the
				   TinyMatrix */

  enum {
    NumberOfLines = N,		/**< number of lines */
    NumberOfRow   = N		/**< number of rows */
  };

private:
  T __x[N][N];			/**< values of the matrix */

public:

  /** 
   * Returns the sizes of the matrix
   * 
   * @return N
   */
  size_t size() const
  {
    return N;
  }

  /** 
   * Access to the (i,j) coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  inline T& operator()(const size_t& i, const size_t& j)
  {
    ASSERT ((i<N) && (j<N));
    return __x[i][j];
  }

  /** 
   * Read-only access to the (i,j) coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  inline const T& operator()(const size_t& i, const size_t& j) const
  {
    ASSERT ((i<N) && (j<N));
    return __x[i][j];
  }

  /** 
   * Computes the invert of the matrix
   * 
   * @return  \f$ A^{-1} \f$
   *
   * @note uses a PA=LU decomposition
   */
  inline TinyMatrix<N,N,T> invert() const
  {
    TinyMatrix<N,N,T> M;
    TinyMatrix<N,N,T> lu;
    TinyMatrix<N,N,T> P;
    LU(*this, lu, P);

    TinyVector<N,T> v;
    TinyVector<N,T> u;

    for (size_t i=0; i<N; ++i) {
      for (size_t j=0; j<N; ++j)
	v[j] = (i==j)?1:0;

      u = lu.applyLU(P*v);
      for (size_t j=0; j<N; ++j)
	M(j,i) = u[j];
    }

    return M;
  }

  /** 
   * Makes the current matrix be equal to a given one
   * 
   * @param B the given matrix
   * 
   * @return the matrix \f$ A := B \f$
   */
  inline TinyMatrix<N,N,T>& operator = (const TinyMatrix<N,N,T>& B)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = B.__x[i][j];
    return (*this);
  }

  /** 
   * Adds a matrix to a given one
   * 
   * @param B the matrix to add
   * 
   * @return \f$ A := A +B \f$
   */
  inline const TinyMatrix<N,N,T>& operator += (const TinyMatrix<N,N,T>& B)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] += B.__x[i][j];
    return (*this);
  }

  /** 
   * Substracts a given matrix
   * 
   * @param B the matrix to substract
   * 
   * @return \f$ A := A-B \f$
   */
  inline const TinyMatrix<N,N,T>& operator -= (const TinyMatrix<N,N,T>& B)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] -= B.__x[i][j];
    return (*this);
  }

  /** 
   * Makes matrix diagonal of value of a given T
   * 
   * @param t the given value
   * 
   * @return \f$ A := \textrm{diag}(t) \f$
   */
  inline const TinyMatrix<N,N,T>& operator = (const T& t)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = (i==j)?t:0;
    return (*this);
  }

  /** 
   * Computes the product on the right to a given \b compatible matrix
   * 
   * @param B the given matrix
   * 
   * @return \f$ AB \f$
   */
  template <size_t P>
  inline TinyMatrix<N,P> operator * (const TinyMatrix<N,P>& B) const
  {
    TinyMatrix<N,P> C(0);
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<P; j++)
	for (size_t k=0; k<N; k++)
	  C(i,j) += __x[i][k] * B(k,j);
    return C;
  }

  /** 
   * Computes the product with a given compatible vector
   * 
   * @param v the given vector
   * 
   * @return \f$ Av \f$
   */
  inline TinyVector<N,T> operator * (const TinyVector<N,T>& v) const
  {
    TinyVector<N,T> u = 0;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	  u[i] += __x[i][j] * v[j];
    return u;
  }

  /** 
   * Computes the product with a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ t A \f$
   */
  inline TinyMatrix<N,N,T> operator * (const T& t) const
  {
    TinyMatrix<N,N,T> B;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	  B.__x[i][j] = __x[i][j] * t;
    return B;
  }

  /** 
   * Computes the product with the invert of a given vector on the right
   * 
   * @param t the given vector
   * 
   * @return \f$ t^{-1} A \f$
   */
  inline TinyMatrix<N,N,T> operator / (const T& t) const
  {
    ASSERT (t!=0);
    const T t_1 = 1./t;
    TinyMatrix<N,N,T> B;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	  B.__x[i][j] = __x[i][j] * t_1;
    return B;
  }

  /** 
   * Multiplies a matrix by a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ A := t A \f$
   */
  inline const TinyMatrix<N,N,T>& operator *= (const T& t)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] *= t;
    return (*this);
  }

  /** 
   * Multiplies a matrix by the invert of a given T on the right
   * 
   * @param t the given T
   * 
   * @return \f$ A := t^{-1} A \f$
   */
  inline TinyMatrix<N,N,T>& operator /= (const T& t) {
    const T t_1 = 1./t;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] *= t_1;
    return (*this);
  }

  /** 
   * Computes the sum of two matrices
   * 
   * @param B the second matrix
   * 
   * @return the matrix \f$ C=A+B\f$
   */
  inline TinyMatrix<N,N,T> operator + (const TinyMatrix<N,N,T>& B) const
  {
    TinyMatrix<N,N,T> C;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	  C.__x[i][j] = __x[i][j] + B.__x[i][j];
    return C;
  }

  /** 
   * Computes the difference of two matrices
   * 
   * @param B the second matrix
   * 
   * @return \f$ A - B \f$
   */
  inline TinyMatrix<N,N,T> operator - (const TinyMatrix<N,N,T>& B) const
  {
    TinyMatrix<N,N,T> C;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	  C.__x[i][j] = __x[i][j] - B.__x[i][j];
    return C;
  }

  /** 
   * Computes the product one the left of a T and a matrix
   * 
   * @param t a given T
   * @param A a given matrix
   *
   * @return \f$ t A \f$
   */
  inline friend TinyMatrix<N,N,T> operator * (const T& t,
					      const TinyMatrix<N,N,T>& A)
  {
    return (A*t);
  }

  /** 
   * Returns the opposed matrix
   * 
   * 
   * @return \f$ -A \f$
   */
  inline TinyMatrix<N,N,T> operator - () const
  {
    TinyMatrix<N,N,T> B;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	B.__x[i][j] = - __x[i][j];
    return B;
  }

  /** 
   * Prints a matrix to a stream
   * 
   * @param os the given stream
   * @param A the given matrix
   * 
   * @return the modified stream
   */
  friend std::ostream& operator << (std::ostream& os,
				    const TinyMatrix<N,N,T>& A)
  {
    for (size_t i=0; i<N; i++) {
      for (size_t j=0; j<N; j++)
	os << A(i,j) << " ";
      os << '\n';
    }
    return os;
  }

  /** 
   * Computes the determinent of a square matrix
   * 
   * @param A the matrix
   * 
   * @return \f$ |A| \f$
   */
  friend T det(const TinyMatrix<N,N,T>& A)
  {
    T d = 1.;
    TinyMatrix<N,N,T> B = A;

    for (size_t k=0; k<N-1; k++) {
      size_t maxLine = k;
      for (size_t i=k+1; i<N; i++) {
	maxLine = (std::abs(B(i,k))>std::abs(B(maxLine,k)))?i:maxLine;
      }
      if (k != maxLine) {
	B.swapLines(k,maxLine);
	d *= -1;
      }

      for(size_t i=k+1; i<N; ++i) {
	for (size_t j=k+1; j<N; j++) {
	  B(i,j) -= B(i,k)*B(k,j)/B(k,k);
	}
	B(i,k) = 0;
      }
    }
    for (size_t i=0; i<N; ++i)
      d *= B(i,i);
    return d;
  }

  /** 
   * Solves a linear system using the Gauss method
   * 
   * @param A the given matrix
   * @param rightHandSide the right hand and side
   * @param X the solution vector
   * 
   * @return the determinent of A (none 0 if invertible)
   */
  template <typename T2>
  friend T gaussPivot(const TinyMatrix<N,N,T>& A,
		      const TinyVector<N,T2>& rightHandSide,
		      TinyVector<N,T2>& X)
  {
    T t = 1.;
    TinyMatrix<N,N,T> M = A;
    TinyVector<N,T2> b = rightHandSide;

    for (size_t k=0; k<N-1; k++) {
      size_t maxLine = k;
      for (size_t i=k+1; i<N; i++) {
	maxLine = (std::abs(M(i,k))>std::abs(M(maxLine,k)))?i:maxLine;
      }
      if (k != maxLine) {
	M.swapLines(k,maxLine);
	std::swap(b[k], b[maxLine]);
	t *= -1;
      }

      for(size_t i=k+1; i<N; ++i) {
	const real_t coef = M(i,k)/M(k,k);
	for (size_t j=k; j<N; j++) {
	  M(i,j) -= coef * M(k,j);
	}
	b[i] -= coef*b[k];
      }
    }
    for (size_t i=0; i<N; ++i)
      t *= M(i,i);

    for (size_t i = N-1; i < N; --i) {
      X[i] = 0;
      for (size_t j = i+1; j<N; j++) {
	X[i] -= M(i,j)*X[j];
      }
      X[i] += b[i];
      X[i] /= M(i,i);
    }
    return t;
  }

  /** 
   * Swaps two lines of the current matrix
   * 
   * @param n first line
   * @param m second line
   */
  void swapLines(const size_t& n, const size_t& m)
  {
    if (n==m)
      return;

    T temp;
    for (size_t i=0; i<N; ++i) {
      temp = __x[n][i];
      __x[n][i] = __x[m][i];
      __x[m][i] = temp;
    }
  }

  /** 
   * Computes the PA=LU factorization
   * 
   * @param A the given \f$ A \f$ matrix
   * @param lu the computed \f$ LU \f$
   * @param P the computation \f$ P \f$ (permutation) matrix
   */
  friend void LU(const TinyMatrix<N,N,T>& A,
		 TinyMatrix<N,N,T>& lu,
		 TinyMatrix<N,N,T>& P)
  {
    // Reinitialize P to Identity
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++) {
	P(i,j) = (i==j);
      }

    lu = A;
    
    for (size_t k=0; k<N-1; k++) {
      // Checkes for Pivot.
      size_t maxLine = k;
      for (size_t i=k+1; i<N; i++) {
	maxLine = (std::abs(lu(i,k))<std::abs(lu(maxLine,k)))?maxLine:i;
      }
      lu.swapLines(k,maxLine);
      P.swapLines(k,maxLine);

      for (size_t i=k+1; i<N; i++) {
	lu(i,k) /= lu(k,k);
	for (size_t j=k+1; j<N; j++)
	  lu(i,j) -=lu(i,k)*lu(k,j);
      }
    }
  }

  /** 
   * Applies LU matrices to a given vector
   * 
   * @param b the given vector
   * 
   * @return \f$ (LU)^{-1}b \f$
   */
  inline TinyVector<N,T> applyLU(const TinyVector<N,T>& b) const
  {
    const TinyMatrix<N,N,T>& lu = (*this);

    TinyVector<N,T> y = b;
    // Go down
    for (size_t i=0; i<N; i++) {
      for (size_t j=0; j<i; j++)
	y[i] -= y[j]*lu(i,j);
    }

    // go up
    for (size_t i=N-1; i<N; i--) {
      for (size_t j=i+1; j<N; j++)
	y[i] -= y[j]*lu(i,j);

      y[i] /= lu(i,i);
    }
        
    return y;
  }

  /** 
   * Solves a linear system
   * 
   * @param b the given second member
   * @param A the given matrix
   * 
   * @return \f$ A^{-1} b \f$
   */
  friend TinyVector<N,T> operator/(const TinyVector<N,T>& b,
				   const TinyMatrix<N,N,T>& A)
  {
    TinyMatrix<N,N,T> lu;
    TinyMatrix<N,N,T> P;
    LU(A,lu,P);

    return lu.applyLU(P*b);
  }

  /** 
   * Default constructor
   * 
   * @note sets matrix to 0
   */
  TinyMatrix()
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = 0;
  }

  /** 
   * Copy constructor
   * 
   * @param B the matrix to copy
   */
  TinyMatrix(const TinyMatrix<N,N,T>& B)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = B.__x[i][j];
  }

  /** 
   * Constructs a matrix using a given T
   * 
   * @param t the given T
   *
   * @note the built matrix is diagonal. Its coefficients are \a t for
   * diagonal terms 0 for others
   */
  TinyMatrix(const T& t)
  {
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<N; j++)
	__x[i][j] = (i==j)?t:0;
  }

  /** 
   * Destructor
   * 
   */
  ~TinyMatrix()
  {
    ;
  }
};


/**
 * @class   TinyMatrix<1,1,T>
 * @author St�phane Del Pino
 * @date   Thu Aug  7 19:06:05 2003
 * 
 * @brief  specialization for matrices 1x1
 * 
 */
template<typename T>
class TinyMatrix<1,1,T>
{
public:
  typedef T
  AssociatedVector;		/**< Type of compatibke vector */

  typedef T
  AssociatedTransposedVector;	/**< Type of compatible transposed
				   vector */

  typedef T ValueType;		/**< The type of the elements of the
				   TinyMatrix */

  enum {
    NumberOfLines = 1,		/**< number of lines */
    NumberOfRow   = 1		/**< number of rows */
  };

private:
  T __x;			/**< The value */

public:
  /** 
   * Cast operator
   * 
   * @return the value
   */
  inline operator T&()
  {
    return __x;
  }

  /** 
   * Cast operator
   * 
   * @return the value
   */
  inline operator const T&() const
  {
    return __x;
  }

  /** 
   * Access to the (i,j) coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  T& operator()(const size_t& i, const size_t& j)
  {
    ASSERT ((i==0)&&(j==0));
    return __x;
  }

  /** 
   * read-only access to the (i,j) coefficient of the matrix
   * 
   * @param i the line number
   * @param j the row number
   *
   * @return \f$ A_{ij} \f$
   */
  const T& operator()(const size_t& i, const size_t& j) const
  {
    ASSERT ((i==0)&&(j==0));
    return __x;
  }

  /// Computes the determinent of a scalar: itself
  friend const T& det(const TinyMatrix<1,1,T>& A)
  {
    return A.__x;
  }

  /** 
   * Default constructor
   * 
   */
  TinyMatrix()
    : __x(0)
  {
    ;
  }

  /** 
   * Constructs the matrix using a T
   * 
   * @param t the given value 
   */
  TinyMatrix(const T& t)
    : __x(t)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param B the given matrix
   */
  TinyMatrix(const TinyMatrix<1,1,T>& B)
    : __x(B.__x)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~TinyMatrix()
  {
    ;
  }  
};

#endif // TINY_MATRIX_HPP
