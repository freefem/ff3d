//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$

#ifndef GMRES_OPTIONS_HPP
#define GMRES_OPTIONS_HPP

#include <ParametrizableObject.hpp>

class GMRESOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  static const char* identifier()
  {
    // autodoc: "specifies gmres options"
    return "gmres";
  }

  real_t epsilon()
  {
    real_t eps = 0;
    get("epsilon",eps);
    return eps;
  }

  int maxiter()
  {
    int maxiter = 0;
    get("maxiter",maxiter);
    return maxiter;
  }

  int basis()
  {
    int basis = 20;
    get("basis",basis);
    return basis;
  }

  explicit GMRESOptions()
  {
    // autodoc: "sets maximum number of iteration"
    add(new IntegerParameter(500, "maxiter"));
    // autodoc: "sets the number of vectors to build in the Krylov basis"
    add(new IntegerParameter(100, "basis"));
    // autodoc: "sets the required reduction factor of the residu"
    add(new DoubleParameter (1E-5,"epsilon"));
  }

  ~GMRESOptions()
  {
    ;
  }
};

#endif // GMRES_OPTIONS_HPP
