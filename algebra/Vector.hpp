//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id$


#ifndef VECTOR_HPP
#define VECTOR_HPP

// Include STD definitions to get same environment ...
#include <stddef.h>
#include <cstdlib>
#include <cmath>

#include <Assert.hpp>

#include <ZoneCopy.hpp>

#include <iostream>

#include <BaseVector.hpp>
#include <TermToTerm.hpp>

/*!
  class Vector
  This class defines a template Vector class to store huge quantity of datas.
  The stored elements type is the template argument.

  \todo Use expression template to provide a more efficient code.
  \author St�phane Del Pino.
*/

template <typename T>
class Vector
  : public BaseVector
{
public:
  //! T ValueType Used to be able to access to T using \a typename.
  typedef T ValueType;
protected:
  //! The number of elements of the vector.
  size_t __size;

  //! The elements of the vector.
  T* __restrict __values;

public:
  //! Read-only access to the size of the vector.
  inline const size_t& size() const
  {
    return __size;
  }

  //! Finds the number of an element of the vector.
  inline size_t number(T const& e) const
  {
    ASSERT ((&e >= __values)&&(static_cast<size_t>(&e-__values) < __size));
    return (&e-__values);
  }

  //! writes a vector in a stream.
  friend std::ostream& operator << (std::ostream& os, const Vector<T>& V)
  {
    for (size_t i=0; i<V.size(); ++i) {
      os << V[i] << '\n';
    }
    return os;
  }

  //! Read-only access to the ith __values of the vector.
  inline const T& operator[](const size_t& i) const
  {
    ASSERT (i<__size);
    return __values[i];
  }

  //! Access to the ith element of the vector.
  inline T& operator[](const size_t& i)
  {
    ASSERT (i<__size);
    return __values[i];
  }

  /*!
    Read-only access to the ith element of the vector.
    Used to provide a Blitz-like interface.
  */
  inline const T& operator()(const size_t& i) const
  {
    ASSERT (i<__size);
    return __values[i];
  }

  /*!
    Access to the ith element of the vector.
    Used to provide a Blitz-like interface.
  */
  inline T& operator()(const size_t& i)
  {
    ASSERT (i<__size);
    return __values[i];
  }

  /*!
    This function is to resize the vector.
    Used to provide a Blitz-like interface, the elements are lost if the size
    is changed.
   */
  void resize(const size_t& newSize)
  {
    ASSERT (newSize>=0);

    if (newSize != __size) {
      if (__size!=0)	{		// Destroy previous vector
	delete [] __values;
      }
      __size = newSize;
      __values = new ValueType[__size];
    } else {
      ;				// else do nothing
    }
  }

  //! \c operator= overloading. Initializes every element to the value \a t.
  inline const Vector<T>& operator=(const T& t)
  {
    for (size_t i=0; i<__size; ++i)
      __values[i] = t;
    return (*this);
  }

  //! \c operator= overloading. Copies the vector \a V.
  inline const Vector<T>& operator=(const Vector<T>& V)
  {
    ASSERT (__size == V.__size);
    ZoneCopy(__values, V.__values, __size);

    return (*this);
  }


  /*!
    \c operator* overloading. Returns \a t * \a this.
    \warning a temporary is created!
   */
  inline Vector<T> operator*(const T& t) const
  {
    Vector<T> prod(*this);
    TermToTermProduct<T> tttp;
    for (size_t i=0; i<__size; ++i)
      prod.__values[i] = tttp(__values[i],t);
    return prod;
  }


  /*!
    \c operator* overloading (friend version). Returns \a t * \a this.
    \warning a temporary is created!
   */
  friend Vector<T> operator*(const T& t, const Vector<T>& V)
  {
    Vector<T> sum(V.__size);
    TermToTermProduct<T> tttp;
    for (size_t i=0; i<V.__size; ++i)
      sum.__values[i] = tttp(V.__values[i],t);

    return sum;
  }


  //! \c operator*= overloading. The vector is multiplied by \a t.
  template <class T2>
  inline const Vector<T>& operator*=(const T2& t)
  {
    TermToTermProduct<T> tttp;
    for (size_t i=0; i<__size; ++i)
      __values[i] = tttp(__values[i],t);
    return *this;
  }

  /*!
    \c operator+ overloading. Computes sum with \a V.
    \warning a temporary is created!
  */
  inline Vector<T> operator+(const Vector<T>& V) const
  {
    ASSERT(__size==V.__size);
    Vector<T> W(__size);
    for (size_t i=0; i<__size; ++i)
      W.__values[i] = __values[i] + V.__values[i];
    return W;
  }

  /*!
    \c operator- overloading. Computes difference with \a V.
    \warning a temporary is created!
  */
  inline Vector<T> operator-(const Vector<T>& V) const
  {
    ASSERT(__size==V.__size);

    Vector<T> W(__size);
    for (size_t i=0; i<__size; ++i) {
      W.__values[i] = __values[i] - V.__values[i];
    }

    return W;
  }


  //! \c operator+= overloading. Addes \a V to the vector.
  inline const Vector<T>& operator+=(const Vector<T>& V)
  {
    ASSERT(__size==V.__size);

    for (size_t i=0; i<__size; ++i)
      __values[i] += V.__values[i];

    return *this;
  }

  //! \c operator+= overloading. Addes \a V to the vector.
  inline const Vector<T>& operator+=(const T& t)
  {
    for (size_t i=0; i<__size; ++i)
      __values[i] += t;

    return *this;
  }

  //! \c operator-= overloading. Substractes \a V to the vector.
  inline const Vector<T>& operator-=(const Vector<T>& V)
  {
    ASSERT(__size==V.__size);

    for (size_t i=0; i<__size; ++i)
      __values[i] -= V.__values[i];

    return (*this);
  }
 
  //! \c operator* overloading. Scalar product.
  inline real_t operator*(const Vector<T>& V) const
  {
    ASSERT(__size==V.__size);

    real_t t = 0;
    for (size_t i=0; i<__size; ++i)
      t += __values[i] * V.__values[i];

    return t;
  }

  /*!
    Computes the � norm � of a given vector \a v. The result is a T!
    If T is of scalar type it returns the usual norm.
    \warning only works if the template type \a T allows scalar product.
    \todo Name might be change?
  */
  inline friend real_t Norm(const Vector<T>& v)
  {
    return sqrt(v*v);
  }


  //! Default constructor initializes a dimension 0 vector.
  Vector()
    : __size(0),
      __values(0)
  {
    ;
  }

  /*!
    Constructs a vector which size is \a givenSize.
    __values values are garbage.
  */
  Vector(size_t size)
    : __size(size),
      __values(0)
  {
    if (__size>0)
      __values = new T[__size];
  }

  //! Copy constructor.
  Vector(const Vector<T>& V)
    : __size(V.__size),
      __values(0)
  {
    if (__size>0)
      __values = new T[__size];
    ZoneCopy(__values,V.__values,__size);
  }

  //! Destructor.
  ~Vector()
  {
    if (__size > 0)
      delete[] __values;
  }
};

#endif // VECTOR_HPP
